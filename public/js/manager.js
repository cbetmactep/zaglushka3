$(document).ready(function () {
    $(".footer").css({"marginBottom":"80px"});
    window.Compare = {
        add: function (id, color) {
            var defer = $.Deferred();

            $.ajax({
                url: '/compare/add/' + id + '/' + color
            }).then(function (res) {
                $('.js-manager-panel-compare-count').text(res.length);
                defer.resolve(res);
            }, function (res) {
                defer.reject(res);
            });

            return defer;
        },
        remove: function (id, color) {
            var defer = $.Deferred();

            $.ajax({
                url: '/compare/remove/' + id + '/' + color
            }).then(function (res) {
                $('.js-manager-panel-compare-count').text(res.length);
                defer.resolve(res);
            }, function (res) {
                defer.reject(res);
            });

            return defer;
        },
        get: function () {
            var defer = $.Deferred();

            $.ajax({
                url: '/compare/get'
            }).then(function (res) {
                $('.js-manager-panel-compare-count').text(res.Items.length);
                defer.resolve(res);
            }, function (res) {
                defer.reject(res);
            });

            return defer;
        },
        flush: function () {
            var defer = $.Deferred();

            $.ajax({
                url: '/compare/flush/'
            }).then(function (res) {
                $('.js-manager-panel-compare-count').text(0);
                defer.resolve(res);
            }, function (res) {
                defer.reject(res);
            });

            return defer;
        }
    };

    (function () {
        $('.compare-flush').on('click', function () {
            Compare.flush();

            $('.product-table-manager-compare_active').each(function () {
                $(this).removeClass('product-table-manager-compare_active');
            });

            $('.product-item-compare_active').each(function () {
                $(this).removeClass('product-item-compare_active');
            });
        });
    })();

    (function () {
        var $btn = $('.compare-button'),
            $availabilities = $('.product-item-availability__key').closest('tr'),
            rows = '.product-table__tr';

        init();

        $(App).on('itemListUpdate', function () {
            init();
        });

        function init() {
            Compare.get().then(function (res) {
                var compareItems = res.Items,
                    $rows = $(rows);

                $rows.each(function () {
                    var $row = $(this),
                        $colorColumn = $row.find('.product-table__td_colors'),
                        id = +$row.data('id'),
                        color = +$row.data('color');

                    if (id && color) {
                        $colorColumn.append('<div class="product-table-manager-compare ' + (compareItems.find(function (item) { return item.item_id === id && item.color === color; }) ? 'product-table-manager-compare_active' : '') + '">' +
                            '<i class="fa fa-star-o"></i>' +
                            '<i class="fa fa-star"></i>' +
                            '</div>');

                        $colorColumn.find('.product-table-manager-compare').on('click', function () {
                            var $compare = $(this);
                            if ($compare.hasClass('product-table-manager-compare_active')) {
                                Compare.remove(id, color).then(function () {
                                    $compare.removeClass('product-table-manager-compare_active');
                                });

                                if ($btn.length) {
                                    $compare.closest('tr').remove();
                                }
                            } else {
                                Compare.add(id, color).then(function () {
                                    $compare.addClass('product-table-manager-compare_active');
                                });
                            }
                        });
                    }
                });

                $availabilities.each(function () {
                    var $row = $(this),
                        id = +$row.closest('table').data('id'),
                        color = +$row.data('color');

                    $row.prepend('<div class="product-item-compare ' + (compareItems.find(function (item) { return item.item_id === id && item.color === color; }) ? 'product-item-compare_active' : '') + '">' +
                        '<i class="fa fa-star-o"></i>' +
                        '<i class="fa fa-star"></i>' +
                        '</div>');

                    $row.find('.product-item-compare').on('click', function () {
                        var $compare = $(this);
                        if ($compare.hasClass('product-item-compare_active')) {
                            Compare.remove(id, color).then(function () {
                                $compare.removeClass('product-item-compare_active');
                            });
                        } else {
                            Compare.add(id, color).then(function () {
                                $compare.addClass('product-item-compare_active');
                            });
                        }
                    });
                });
            });
        }
    })();



    (function () {
        var $dom = {
                button: $('.header-contacts__city, .js-footer-city, .js-delivery-city'),
                modal: $('#changeCityModal'),
                input: $('.change-city__input-field'),
                regionLink: $('.change-city__link'),
                popupYourCity: $('.header-popup-your-city')
            };

        $dom.input.each(function () {
            var $input = $(this);

            $input.autocomplete({
                paramName: 'query',
                serviceUrl: '/cities/autocomplete',
                dataType: 'json',
                noCache: true,
                params: {
                    country: $input.siblings('[name="county"]').val()
                },
                triggerSelectOnValidInput: false,
                transformResult: function(response) {
                    return {
                        suggestions: $.map(response, function(item) {
                            return { value: item.title, data: { id: item.id, region: item.region, free_delivery_price: +item.free_delivery_price } };
                        })
                    };
                },
                onSelect: function (suggestion) {
                    changeCity(suggestion.data.id);
                },
                formatResult: function (suggestion, currentValue) {
                    return suggestion.value.replace(new RegExp(currentValue, 'ig'), '<strong>$&</strong>') + ', ' + suggestion.data.region + ((suggestion.data.free_delivery_price > 0) ? ', ' + Formatter.number(suggestion.data.free_delivery_price) + 'р.' : '');
                }
            });
        });

        function changeCity(id) {
            location.href = location.origin + '/change-city/' + id;
        }

    })();

});
