<?php
use App\Models\Category;
use App\Models\Item;
use App\Models\Tag;

// Заглушка.Ру
Breadcrumbs::register('home', function($breadcrumbs)
{
    $breadcrumbs->push(_t('Заглушка.Ру', 'common'), route('home'));
});

// Заглушка.Ру > Поиск
Breadcrumbs::register('search', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push(_t('Поиск', 'common'), route('search'));
});

// Заглушка.Ру > О компании
Breadcrumbs::register('about', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push(_t('О компании', 'common'), route('about'));
});

// Заглушка.Ру > Доставка продукции
Breadcrumbs::register('delivery', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push(_t('Доставка продукции', 'common'), route('delivery'));
});
Breadcrumbs::register('pay',function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(_t('Способы оплаты','common'), route('pay'));
});
// Заглушка.Ру > Доставка продукции
Breadcrumbs::register('contacts', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push(_t('Контакты', 'common'), route('contacts'));
});

// Заглушка.Ру > Продукция
Breadcrumbs::register('catalog', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push(_t('Продукция', 'common'), route('catalog'));
});

// Заглушка.Ру > Продукция > Специальные предложения
Breadcrumbs::register('specials', function($breadcrumbs)
{
    $breadcrumbs->parent('catalog');
    $breadcrumbs->push(_t('Специальные предложения', 'common'), route('specials'));
});

// Заглушка.Ру > Продукция > [Category] (> [Subcategory])
Breadcrumbs::register('category', function($breadcrumbs, Category $Category)
{
    $breadcrumbs->parent('catalog');

    if ($Category->parent_id) {
        $ParentCategory = Category::cachedOne($Category->parent_id);

        $breadcrumbs->push($ParentCategory->getTitle(), route('slug', ['slug' => $ParentCategory->slug()]), ['catalog' => 'category', 'categoryId' => $ParentCategory->id]);
    }

    $breadcrumbs->push($Category->getTitle(),   route('slug', ['slug' => $Category->slug()]), ['catalog' => 'category', 'categoryId' => $Category->id]);
});

// Заглушка.Ру > Продукция > [Category] (> [Subcategory]) > [Item]
Breadcrumbs::register('item', function($breadcrumbs, Category $Category, Item $Item)
{
    $breadcrumbs->parent('category', $Category);
    $breadcrumbs->push($Item->title, route('slug', ['slug' => $Item->slug()]));
});

// Заглушка.Ру > Продукция > [Category] (> [Subcategory]) > [Tag]
Breadcrumbs::register('tags', function($breadcrumbs, Category $Category, Tag $Tag)
{
    $breadcrumbs->parent('category', $Category);
    $breadcrumbs->push($Tag->title, route('tags', ['categorySlug' => $Category->slug(), 'tagSlug' => $Tag->slug()]), ['catalog' => 'tag']);
});

// Заглушка.Ру > Корзина
Breadcrumbs::register('cart', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push(_t('Корзина', 'common'), route('cart'));
});

// Заглушка.Ру > Продукция > Сравнение
Breadcrumbs::register('compare', function($breadcrumbs)
{
    $breadcrumbs->parent('catalog');
    $breadcrumbs->push(_t('Сравнение', 'common'), route('compare'));
});