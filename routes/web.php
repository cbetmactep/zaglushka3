<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Enum\Subjects;
use App\Models\Category;
use App\Models\Item;
use App\Models\Tag;

App::setLocale('ru');

$subdomain = mb_strtolower(preg_replace(['#' . preg_quote(getenv('APP_HOST')) . '$#', '#^www.#', '#\.$#'], '', Request::getHost()));

if ($subdomain && $subdomain != 'www') { // todo выпилить после того как уберем www
    Config::set('app.subdomain', $subdomain);
}

if (!Request::ajax() && !Config::get('app.subdomain')) {
    if (in_array(Request::segment(1), Config::get('app.locales'))) {
        App::setLocale(Request::segment(1));
        Config::set('app.locale_prefix', App::isLocale('ru') ? '' : App::getLocale());
    }
}

Route::post('/system/update', 'SystemController@tameraUpdate');
Route::get('/system/get-images', 'SystemController@getArticleImages');
Route::get('/system/get-articles', 'SystemController@getAllArticleCodes');

Route::group(['middleware' => 'admin'], function () {
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});

Route::middleware('ajax')->group(function () {
    Route::get('/cart/set/{itemId}/{color}/{quantity}', 'CartController@set')->name('cart-set');
    Route::get('/cart/get/{itemId}/{color}', 'CartController@get');
    Route::get('/cart/promo/{itemId}/{color}', 'CartController@promo');
    Route::get('/cart/flush', 'CartController@flush');
    
    Route::get('/item/{itemId}', 'ItemController@get');
    
    Route::get('/search/article', 'SearchController@article');
    Route::get('/search/size', 'SearchController@size');
    Route::get('/search/size-tooltip', 'SearchController@sizeTooltip');
    Route::get('/search/size-autocomplete', 'SearchController@sizeAutocomplete');
    Route::get('/search/article-autocomplete', 'SearchController@articleAutocomplete');

    Route::get('/cities/autocomplete', 'CityController@autocomplete');

    Route::get('/delivery/calculate', 'OrderController@calcDelivery');
});

Route::post('/order/send', 'OrderController@send');

Route::get('/change-language/{lang}', 'MainController@changeLanguage')->name('change-language');

Route::get('sitemap.xml', function () {
    // create new sitemap object
    $sitemap = App::make("sitemap");

    // set cache key (string), duration in minutes (Carbon|Datetime|int), turn on/off (boolean)
    // by default cache is disabled
    $sitemap->setCache('https_nowww__laravel.sitemap.' . Config::get('app.subdomain'), 60 * 24 * 3);

    // check if there is cached sitemap and build new only if is not
    if (!$sitemap->isCached()) {
        // add item to the sitemap (url, date, priority, freq)
        $sitemap->add(route('home'), date('Y-m-d'), '0.8', 'daily');
        $sitemap->add(route('about'), date('Y-m-d'), '0.8', 'daily');
        //$sitemap->add(route('about_new'), date('Y-m-d'), '0.8', 'daily');
        $sitemap->add(route('pay'), date('Y-m-d'), '0.8', 'daily');
        $sitemap->add(route('contacts'), date('Y-m-d'), '0.8', 'daily');
        $sitemap->add(route('cart'), date('Y-m-d'), '0.8', 'daily');
        $sitemap->add(route('delivery'), date('Y-m-d'), '0.8', 'daily');
        $sitemap->add(route('specials'), date('Y-m-d'), '0.8', 'daily');
        $sitemap->add(route('search'), date('Y-m-d'), '0.8', 'daily');
        $sitemap->add(route('catalog'), date('Y-m-d'), '0.8', 'daily');

        $Categories = Category::where(['enabled' => true])->get();  // sitemap сам кеширутся, забить
        foreach ($Categories as $Category) {
            $sitemap->add(route('slug', ['slug' => $Category->slug()]), date('Y-m-d'), '0.8', 'daily');

            $Tags = $Category->getTags()->get(); // sitemap сам кеширутся, забить

            foreach ($Tags as $Tag) {
                $sitemap->add(route('tags', ['categorySlug' => $Category->slug(), 'tagSlug' => $Tag->slug()]), date('Y-m-d'), '0.8', 'daily');
            }
        }

        $Items = Item::where(['enabled' => true])->get();

        foreach ($Items as $Item) {
            $sitemap->add(route('slug', ['slug' => $Item->slug()]), date('Y-m-d'), '0.8', 'daily');
        }
    }

    // show your sitemap (options: 'xml' (default), 'html', 'txt', 'ror-rss', 'ror-rdf')
    return $sitemap->render('xml');
});

Route::get('/change-city/{cityId}', 'MainController@changeCity')->name('change-city');

Route::get('robots.txt', function () {
    //if ((App::environment() === 'production')) {
        if (Config::get('app.subdomain')) {
            $robots = 'robots/subdomain.txt';
        } else {
            $robots = 'robots/prod.txt';
        }
    //} else {
    //    $robots = 'robots/dev.txt';
   //}

    $robots = Storage::exists($robots) ? str_replace('{subdomain}', Config::get('app.subdomain'), Storage::get($robots)) : '';

    return response($robots, 200)->header('Content-Type', 'text/plain');
});

Route::group(['middleware' => ['manager']], function () {
    Route::get('/compare', 'User\Manager\CompareController@index')->name('compare');
    Route::get('/compare/flush', 'User\Manager\CompareController@flush')->name('compare-flush');

    Route::middleware('ajax')->group(function () {
        Route::get('/compare/add/{itemId}/{color}', 'User\Manager\CompareController@add');
        Route::get('/compare/remove/{itemId}/{color}', 'User\Manager\CompareController@remove');
        Route::get('/compare/get', 'User\Manager\CompareController@get');
        Route::get('/compare/flush', 'User\Manager\CompareController@flush')->name('compare-flush');
    });
});

Route::group(['prefix' => Config::get('app.locale_prefix')], function () {
    Auth::routes();

    // главная
    Route::get('/', 'MainController@index')->name('home');

    // pages
    Route::get('/about_new', 'PageController@about')->name('about');
    Route::get('/about', 'PageController@aboutnew')->name('aboutnew');
    Route::get('/pay', 'PageController@pay')->name('pay');
    Route::get('/delivery', 'PageController@delivery')->name('delivery');
    Route::get('/contacts', 'PageController@contacts')->name('contacts');

    // поиск
    Route::get('/search', 'SearchController@index')->name('search');

    // каталог
    Route::get('/catalog', 'CategoryController@index')->name('catalog');

    // заказ
    Route::get('/cart', 'CartController@index')->name('cart');

    Route::get('/order', 'OrderController@index')->name('order');

    Route::get('/result', 'OrderController@result')->name('result');
    Route::get('/result_payment/{orderId}', 'OrderController@resultPayment')->name('result_payment');

    
    // specials
    Route::get('/specials', 'SpecialsController@index')->name('specials');

    // test route
    Route::get('/test_route', 'CategoryController@test')->name('test_roite');
    
    Route::get('/{categorySlug}/{tagSlug}', function ($categorySlug, $slug) {
        $SlugCategory = \App\Models\Slug::getSlug($categorySlug, Subjects::SUBJECT_CATEGORY);
        $SlugTag = \App\Models\Slug::getSlug($slug, Subjects::SUBJECT_TAG);

        if (!($SlugCategory && $SlugTag)) {
            abort(404);
        }

        if (!($SlugTag->current && $SlugCategory->current)) {

            return Redirect::to(route('tags', ['categorySlug' => $SlugCategory->slug, 'tagSlug' => $SlugTag->slug]), 301);
        }

        $controller = App::make(\App\Http\Controllers\CategoryController::class);

        return $controller->callAction('tag', [$SlugCategory->object_id, $SlugTag->object_id]);
    })->name('tags');
    Route::redirect('/catalog', '/', 301);
    Route::redirect('/akrobaticheskie-elementy', '/gimnasticheskie-koltsa', 301);
    Route::get('/{slug}', function ($slug) {
        $SlugModel = \App\Models\Slug::getSlug($slug);

        if (!$SlugModel) {
            abort(404);
        }

        if ($SlugModel->subject_id == Subjects::SUBJECT_CATEGORY) {
            if ($SlugModel->current) {
                $controller = App::make(\App\Http\Controllers\CategoryController::class);

                return $controller->callAction('category', [$SlugModel->object_id]);
            }

            $Category = Category::cachedOne($SlugModel->object_id);

            if ($Category) {

                return Redirect::to(route('slug', ['slug' => $Category->slug()]), 301);
            }
        } elseif ($SlugModel->subject_id == Subjects::SUBJECT_ITEM) {
            if ($SlugModel->current) {
                $controller = App::make(\App\Http\Controllers\ItemController::class);

                return $controller->callAction('index', [$SlugModel->object_id]);
            }

            $Item = Item::find($SlugModel->object_id); // todo cache

            if ($Item) {

                return Redirect::to(route('slug', ['slug' => $Item->slug()]), 301);
            }
        }

        abort(404);
    })->name('slug');

});

// todo legacy

Route::group(['prefix' => Config::get('app.locale_prefix')], function () {
    // главная
    Route::get('/', 'MainController@index')->name('home');

    // pages
    Route::get('/about.html', function () {

        return Redirect::to('/about', 301);
    });
    
    Route::get('/about_new.html', function () {

        return Redirect::to('/about_new', 301);
    });

    Route::get('/delivery.html', function () {

        return Redirect::to('/delivery', 301);
    });

    Route::get('/contacts.html', function () {

        return Redirect::to('/contacts', 301);
    });

    Route::get('/search.html', function () {

        return Redirect::to('/search', 301);
    });

    Route::get('/catalog.html', function () {

        return Redirect::to('/catalog', 301);
    });

    Route::get('/specials.html', function () {

        return Redirect::to('/specials', 301);
    });

    Route::get('/catalog/{categoryId}.html', function ($categoryId) {
        $Category = Category::cachedOne($categoryId);

        if (!$Category) {
            abort(404);
        }

        return Redirect::to(route('slug', ['slug' => $Category->slug()]), 301);
    });

    Route::get('/catalog/{categoryId}/cloud/{slug}.html', function ($categoryId, $slug) {
        /** @var Tag $Tag */
        $Tag = Tag::where(['translit' => $slug])->first();
        $Category = Category::cachedOne($categoryId);

        if (!$Tag || !$Category) {
            abort(404);
        }

        return Redirect::to(route('tags', ['categorySlug' => $Category->slug(), 'tagSlug' => $Tag->slug()]), 301);
    });

    Route::get('/catalog/{categoryId}/{id}.html', function ($categoryId, $id) {
        $Item = Item::find($id);

        if (!$Item) {
            abort(404);
        }

        return Redirect::to(route('slug', ['slug' => $Item->slug()]), 301);
    });
});

