<?php
/**
 * @author narmiel
 */

namespace Modules\Files\Traits;

use Illuminate\Support\Facades\Storage;
use Modules\Files\Exceptions\FileException;
use Modules\Files\Interfaces\Attachable as AttachableInterface;
use Modules\Files\Models\File;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

trait Attachable
{
    /**
     * @inheritdoc
     *
     * @see Attachable addFile
     */
    public function addFile(UploadedFile $file, $type = 0, $public = false, $details = null, $crop = null)
    {
        if (in_array(mb_strtolower($file->getClientOriginalExtension()), File::getUnsafeExtensions())) {
            // todo log

            return null;
        }

        if (!$this instanceof AttachableInterface) {
            throw new FileException('Model must implement attachable interface');
        }

        /** @var $this Model */
        if (!$this->exists) {
            throw new FileException('Model does not exists');
        }

        $path = $public ? Storage::disk('public_files')->putFile($this->storeFolder, $file) : $file->store($this->storeFolder);

        if ($path) {
            $File = new File([
                'subject_id' => $this::getType(),
                'object_id'  => $this->id,
                'filename'   => $file->getClientOriginalName(),
                'filesize'   => $file->getSize(),
                'mime'       => $file->getMimeType(),
                'src'        => $path,
                'type'       => $type,
            ]);

            if ($File->save()) {

                return $File;
            }

        }

        return null;
    }

    /**
     * @inheritdoc
     *
     * @see Attachable addFile
     */
    public function replaceFile(UploadedFile $UploadedFile, $type = 0, $public = false)
    {
        $File = $this->file();

        if ($File && $File->type == $type) {
            if ($File->type == $type) {
                $File->delete();
            }
        }

        return $this->addFile($UploadedFile, $type, $public);
    }

    /**
     * @inheritdoc
     *
     * @see Attachable files
     *
     * @return File|null
     */
    public function files($limit = null, $order = 'DESC', $type = 0)
    {
        if (!$this instanceof AttachableInterface) {
            throw new FileException('Model must implement attachable interface');
        }

        /** @var $this Model */
        if (!$this->exists) {
            throw new FileException('Model does not exists');
        }

        /** @var $this AttachableInterface */
        $Files = File::where([
            'subject_id' => $this->getType(),
            'object_id'  => $this->id,
            'type'       => $type,
        ])
            ->orderBy('id', $order)
            ->limit($limit)
            ->get();

        return $Files;
    }

    /**
     * @inheritdoc
     *
     * @see Attachable files
     *
     * @return File[]|null
     */
    public function file($order = 'DESC', $type = 0)
    {
        if (!$this instanceof AttachableInterface) {
            throw new FileException('Model must implement attachable interface');
        }

        /** @var $this Model */
        if (!$this->exists) {
            throw new FileException('Model does not exists');
        }

        /** @var $this AttachableInterface */
        $File = File::where([
            'subject_id' => $this->getType(),
            'object_id'  => $this->id,
            'type'       => $type,
        ])->orderBy('id', $order)
          ->first();

        return $File;
    }

    /**
     * @inheritdoc
     *
     * @see Attachable deleteFile
     */
    public function deleteFile($id)
    {
        if (!$this instanceof AttachableInterface) {
            throw new FileException('Model must implement attachable interface');
        }

        /** @var $this Model */
        if (!$this->exists) {
            throw new FileException('Model does not exists');
        }

        /** @var $this AttachableInterface */
        $File = File::find(['subject_id' => $this->getType(), 'object_id' => $this->id, 'id' => $id]);

        return $File && $File->delete();
    }
}
