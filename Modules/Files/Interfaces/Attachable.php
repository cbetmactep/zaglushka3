<?php

namespace Modules\Files\Interfaces;

use Modules\Files\Exceptions\FileException;
use Modules\Files\Models\File;
use Illuminate\Http\UploadedFile;

/**
 * Поддержка прикрепления файлов, при использовании подлючите трейт Attachable
 */
interface Attachable {

    /**
     * @return int тип текущей модели
     */
    public static function getType();

    /**
     * Добавление файла
     *
     * @param UploadedFile $file
     *
     * @param int          $type
     * @param null         $details
     *
     * @param array        $crop [w, h, x, y]
     *
     * @return File|null
     */
    public function addFile(UploadedFile $file, $type = 0, $public = false, $details = null, $crop = null);

    /**
     * Возвращает файлы объекта
     *
     * @param int|null $limit лимит
     *
     * @param string   $order
     * @param int      $type
     *
     * @return File[]|null
     */
    public function files($limit = null, $order = 'DESC', $type = 0);

    /**
     * Удаляет файл объекта
     *
     * @param int $id id файла
     *
     * @return bool
     *
     * @throws FileException
     */
    public function deleteFile($id);
}
