<?php

namespace Modules\Rin\Models;

use Cache;
use Spatie\Activitylog\Traits\LogsActivity;

class Color extends \App\Models\Color
{
    use LogsActivity;
    protected static $logFillable = true;
    protected static $logOnlyDirty = true;

    /**
     * @var array
     */
    protected $fillable = ['name', 'hex'];

    /**
     * @param array $options
     *
     * @return bool
     */
    public function save(array $options = [])
    {
        if ($this->exists) {
            Cache::delete('color_' . $this->id);
        }

        return parent::save($options);
    }
}
