<?php

namespace Modules\Rin\Models;

use Exception;
use Modules\Rin\Enum\Subjects;
use Spatie\Activitylog\Traits\LogsActivity;

class Meta extends \App\Models\Meta
{
    use LogsActivity;
    protected static $logFillable = true;
    protected static $logOnlyDirty = true;

    /**
     * @var array
     */
    protected $fillable = ['title', 'description', 'keywords'];

    /**
     * @param int $subjectId
     *
     * @throws \Exception
     *
     * @return string[]
     */
    public static function getReplacements(int $subjectId): array
    {
        switch ($subjectId) {
            case Subjects::SUBJECT_CATEGORY:
                $replacements = [
                    '[НАЗВАНИЕ_КАТЕГОРИИ]',
                    '[ОТ_МИНИМАЛЬНАЯ_ЦЕНА]',
                    '[ГОРОДЕ]',
                ];

                break;
            case Subjects::SUBJECT_ITEM:
                $replacements = [
                    '[НАЗВАНИЕ_КАТЕГОРИИ]',
                    '[ОТ_МИНИМАЛЬНАЯ_ЦЕНА]',
                    '[НАЗВАНИЕ_ТОВАРА]',
                    '[НАЗВАНИЕ_ТОВАРА_ДЛЯ_ЯНДЕКСА]',
                    '[ГОРОДЕ]',
                ];

                break;
            case Subjects::SUBJECT_TAG:
                $replacements = [
                    '[НАЗВАНИЕ_ТЕГА]',
                    '[НАЗВАНИЕ_КАТЕГОРИИ]',
                    '[ОТ_МИНИМАЛЬНАЯ_ЦЕНА]',
                    '[ГОРОДЕ]',
                ];

                break;
            default:
                throw new Exception('Unsupported subject');
        }

        return $replacements;
    }
}
