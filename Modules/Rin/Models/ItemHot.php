<?php

namespace Modules\Rin\Models;

use Cache;
use Modules\Rin\Traits\RinModel;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 *
 */
class ItemHot extends \App\Models\ItemHot
{
    use RinModel;
    use LogsActivity;
    protected static $logFillable = true;
    protected static $logOnlyDirty = true;

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'category_id',
        'text',
        'add_text_1',
        'add_text_2',
        'subject_id',
        'object_id',
        'size',
        'icon_id_1',
        'icon_id_2',
    ];

    private function rules()
    {
        return [
            'title' => 'required|string|max:255',
            'category_id' => 'required|exists:cats,id',
            'text' => 'string|max:255',
            'add_text_1' => 'nullable|string|max:30',
            'add_text_2' => 'nullable|string|max:30',
            'subject_id' => 'required|integer',
            'object_id' => 'required|integer',
            'size' => 'boolean',
            'icon_id_1' => 'integer',
            'icon_id_2' => 'nullable|integer',
        ];
    }

    /**
     * @inheritdoc
     */
    public function save(array $options = [])
    {
        Cache::delete('banner_' . $this->category_id);

        return parent::save($options);
    }
}
