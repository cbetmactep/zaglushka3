<?php

namespace Modules\Rin\Models;

use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\Property
 *
 * @property integer $id
 * @property integer $type
 * @property string  $title
 */
class Property extends \App\Models\Property
{
    use LogsActivity;
    protected static $logFillable = true;
    protected static $logOnlyDirty = true;

    /**
     * @var array
     */
    protected $fillable = ['type', 'title'];
}
