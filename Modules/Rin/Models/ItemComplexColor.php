<?php

namespace Modules\Rin\Models;

use Spatie\Activitylog\Traits\LogsActivity;

class ItemComplexColor extends \App\Models\ItemComplexColor
{
    use LogsActivity;
    protected static $logFillable = true;
    protected static $logOnlyDirty = true;
}
