// TODO: Добавить комментарии
const formattingToSend = obj => {
  for (let property in obj) {
    if (obj.hasOwnProperty(property)) {
      if (typeof obj[property] === 'boolean') {
        obj[property] = +(obj[property])
      } else if (typeof obj[property] === 'undefined' || obj[property] === null) {
        obj[property] = ''
      } else if (typeof obj[property] === 'object') {
        obj[property] = formattingToSend(obj[property])
      }
    }
  }

  return obj
}

export default formattingToSend
