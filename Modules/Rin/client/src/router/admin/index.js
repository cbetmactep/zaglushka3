import Space from '@/components/Space'

import users from './users'
import activity from './activity'

export default {
  path: '/admin',
  component: Space,
  children: [
    users,
    activity
  ]
}
