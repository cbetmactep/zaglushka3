import Space from '@/components/Space'

import ManagementOrders from '@/views/management/orders/Index'
import ManagementOrdersList from '@/views/management/orders/List'

export default {
  path: 'orders',
  component: Space,
  children: [
    {
      path: '/',
      name: 'management-orders-list',
      component: ManagementOrdersList,
      meta: {
        title: 'Список "Заказов"',
        permission: 'read orders'
      }
    },
    {
      path: ':id(\\d+)',
      name: 'management-orders',
      component: ManagementOrders,
      meta: {
        title: 'Заказ',
        permission: 'read orders'
      }
    }
  ]
}
