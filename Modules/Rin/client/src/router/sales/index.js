import Space from '@/components/Space'

import orders from './orders'

export default {
  path: '/sales',
  component: Space,
  children: [
    orders
  ]
}
