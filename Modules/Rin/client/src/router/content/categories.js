import Space from '@/components/Space'

import ContentCategories from '@/views/content/categories/Index'
import ContentCategoriesList from '@/views/content/categories/List'
import ContentCategoriesCreate from '@/views/content/categories/Create'

export default {
  path: 'categories',
  component: Space,
  children: [
    {
      path: '/',
      name: 'content-categories-list',
      component: ContentCategoriesList,
      meta: {
        title: 'Список "Категорий"',
        permission: 'read categories'
      }
    },
    {
      path: ':id(\\d+)',
      name: 'content-categories',
      component: ContentCategories,
      meta: {
        title: 'Редактирование категории',
        permission: 'read categories'
      }
    },
    {
      path: 'create',
      name: 'content-categories-create',
      component: ContentCategoriesCreate,
      meta: {
        title: 'Добавление категории',
        permission: 'add categories'
      }
    }
  ]
}
