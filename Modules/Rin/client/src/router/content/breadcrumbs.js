import Space from '@/components/Space'

import ContentBreadcrumbsCategories from '@/views/content/breadcrumbs/Categories'
import ContentBreadcrumbs from '@/views/content/breadcrumbs/Index'

export default {
  path: 'breadcrumbs',
  component: Space,
  children: [
    {
      path: '/',
      name: 'content-breadcrumbs-categories',
      component: ContentBreadcrumbsCategories,
      meta: {
        title: 'Список "Товаров"',
        permission: 'read breadcrumbs'
      }
    },
    {
      path: ':id(\\d+)',
      name: 'content-breadcrumbs',
      component: ContentBreadcrumbs,
      meta: {
        title: 'Список "Товаров"',
        permission: 'read breadcrumbs'
      }
    }
  ]
}
