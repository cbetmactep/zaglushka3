import Space from '@/components/Space'

import items from './items'
import categories from './categories'
import complexItems from './complex-items'
import list from './list'
import hot from './hot'
import breadcrumbs from './breadcrumbs'
import tags from './tags'
import ImportPrice from '@/views/content/ImportPrice'
import MassImages from '@/views/content/MassImages'

export default {
  path: '/content',
  component: Space,
  children: [
    items,
    complexItems,
    list,
    hot,
    breadcrumbs,
    categories,
    tags,
    {
      path: '/import-price',
      name: 'content-import-price',
      component: ImportPrice,
      meta: {
        title: 'Импорт цен',
        permission: 'edit items'
      }
    },
    {
      path: '/mass-images',
      name: 'content-mass-images',
      component: MassImages,
      meta: {
        title: 'Массовая загрузка изображений',
        permission: 'add mass-images'
      }
    }
  ]
}
