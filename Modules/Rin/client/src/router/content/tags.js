import Space from '@/components/Space'

import ContentTags from '@/views/content/tags/Index'
import ContentTagsTop from '@/views/content/tags/Top'
import ContentTagsUnder from '@/views/content/tags/Under'
import ContentTagsSeo from '@/views/content/tags/Seo'
import ContentTagsCreate from '@/views/content/tags/Create'

export default {
  path: 'tags',
  component: Space,
  children: [
    {
      path: ':id(\\d+)',
      name: 'content-tags',
      component: ContentTags,
      meta: {
        title: 'Редактирование тегов',
        permission: 'edit tags'
      }
    },
    {
      path: 'top',
      component: Space,
      children: [
        {
          path: '/',
          name: 'content-tags-top-list',
          component: ContentTagsTop,
          meta: {
            title: 'Список верхних "Тегов"',
            permission: 'edit tags'
          }
        },
        {
          path: ':id(\\d+)',
          name: 'content-tags-top',
          component: ContentTags,
          meta: {
            title: 'Редактирование тегов',
            permission: 'edit tags'
          }
        }
      ]
    },
    {
      path: 'under',
      component: Space,
      children: [
        {
          path: '/',
          name: 'content-tags-under-list',
          component: ContentTagsUnder,
          meta: {
            title: 'Список нижних "Тегов"',
            permission: 'edit tags'
          }
        },
        {
          path: ':id(\\d+)',
          name: 'content-tags-under',
          component: ContentTags,
          meta: {
            title: 'Редактирование тегов',
            permission: 'edit tags'
          }
        }
      ]
    },
    {
      path: 'seotags',
      component: Space,
      children: [
        {
          path: '/',
          name: 'content-tags-seo-list',
          component: ContentTagsSeo,
          meta: {
            title: 'Список Seo "Тегов"',
            permission: 'edit tags'
          }
        },
        {
          path: ':id(\\d+)',
          name: 'content-tags-seo',
          component: ContentTags,
          meta: {
            title: 'Редактирование тегов',
            permission: 'edit tags'
          }
        }
      ]
    },
    {
      path: 'create',
      name: 'content-tags-create',
      component: ContentTagsCreate,
      meta: {
        title: 'Добавление тегов',
        permission: 'add tags'
      }
    }
  ]
}
