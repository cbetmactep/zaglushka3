import Space from '@/components/Space'

import ContentHotList from '@/views/content/hot/List'
import ContentHot from '@/views/content/hot/Index'
import ContentCreate from '@/views/content/hot/Create'

export default {
  path: 'hot',
  component: Space,
  children: [
    {
      path: '/',
      component: Space,
      meta: {
        title: 'Список "Баннеров"'
      },
      children: [
        {
          path: '',
          name: 'content-hot-list',
          component: ContentHotList,
          meta: {
            title: 'Список "Баннеров"',
            permission: 'read items-hot'
          }
        },
        {
          path: 'create',
          name: 'content-hot-create',
          component: ContentCreate,
          meta: {
            title: 'Добавление баннера',
            permission: 'add items-hot'
          }
        },
        {
          path: ':id(\\d+)',
          name: 'content-hot',
          component: ContentHot,
          meta: {
            title: 'Редактирование баннера',
            permission: 'read items-hot'
          }
        }
      ]
    }
  ]
}
