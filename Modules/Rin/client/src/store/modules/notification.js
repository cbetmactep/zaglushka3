const state = {
  items: []
}

const mutations = {
  remove (state, item) {
    state.items.splice(state.items.findIndex(findItem => findItem === item), 1)
  },
  hide (state, item) {
    state.items.find(findItem => findItem === item).show = false
  },
  started (state, item) {
    state.items.find(findItem => findItem === item).isStarted = true
  },
  add (state, item) {
    state.items.push({
      _key: Date.now(),
      duration: 0,
      isStarted: false,
      show: true,
      ...item
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations
}
