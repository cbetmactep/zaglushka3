import moment from 'moment'

const state = {
  state: 'complete'
}

const mutations = {
  complete (state) {
    state.state = 'complete'
  },
  incomplete (state) {
    state.state = 'incomplete'
  },
  error (state) {
    state.state = 'error'
  }
}

const actions = {
  complete ({ commit }) {
    if (+moment().format('X') > 1516232397) { // TODO: Выпилить это
      commit('complete')
    } else {
      setTimeout(() => {
        commit('complete')
      }, 2000)
    }
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
