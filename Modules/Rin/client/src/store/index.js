import Vue from 'vue'
import Vuex from 'vuex'

import notification from '@/store/modules/notification'
import pageload from '@/store/modules/pageload'
import user from '@/store/modules/user'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    pageload,
    user,
    notification
  },
  strict: process.env.NODE_ENV !== 'production'
})
