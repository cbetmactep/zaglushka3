<?php

namespace Modules\Rin\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;

class RinController extends BaseController
{
    /**
     * @param \Illuminate\Http\Request $Request
     *
     * @return \App\Models\User|\Illuminate\Http\JsonResponse|null
     */
    public function login(Request $Request)
    {
        if (Auth::attempt(['email' => $Request->get('email'), 'password' => $Request->get('password')])) {

            return Auth::user();
        }

        return response()->json([
            'message' => 'Not allowed',
        ], 401);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return void
     */
    public function logout()
    {
        Auth::logout();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('rin::index');
    }
}
