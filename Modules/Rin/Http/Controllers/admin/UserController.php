<?php

namespace Modules\Rin\Http\Controllers\Admin;

use App\Models\User;
use Auth;
use Illuminate\Routing\Controller as BaseController;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class UserController extends BaseController
{
    /**
     * @return array
     */
    public function getList()
    {
        $Users = User::all();

        foreach ($Users as $User) {
            $User->getAllPermissions();
        }

        return [
            'Users' => $Users,
            'Roles' => Role::all(),
        ];
    }

    /**
     * @param int $id
     *
     * @return array
     */
    public function get($id)
    {
        /** @var User $User */
        $User = User::findOrFail($id);

        $User->getAllPermissions();

        return [
            'User'  => $User,
            'Roles' => Role::all(),
        ];
    }

    /**
     * @param int     $id
     * @param Request $Request
     *
     * @return array
     */
    public function update($id, Request $Request)
    {
        /** @var User $User */
        $User = User::findOrFail($id);

        if (Auth::user()->id == $User->id) {
            throw new AccessDeniedHttpException();
        }

        $User->syncRoles([$Request->get('role')]);

        return [
            'User'  => $User,
            'Roles' => Role::all(),
        ];
    }
}
