<?php

namespace Modules\Rin\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Modules\Rin\Enum\Subjects;
use Modules\Rin\Models\Category;
use Modules\Rin\Models\City;
use Modules\Rin\Models\Color;
use Modules\Rin\Models\Item;
use Modules\Rin\Models\ItemHot;
use Modules\Rin\Models\Manager;
use Modules\Rin\Models\Property;
use Modules\Rin\Models\Slug;
use Modules\Rin\Models\Tag;
use Spatie\Activitylog\Models\Activity;

class ActivityController extends BaseController
{

    /**
     * Show the profile for the given user.
     *
     * @param string $subject_type
     * @param int $id
     *
     * @return Activity[]
     */
    public function getBySubject($subject_type, $id)
    {
        switch ($subject_type) {
            case Subjects::SUBJECT_ITEM: $subject_type = Item::class;
                break;
            case Subjects::SUBJECT_COLOR: $subject_type = Color::class;
                break;
            case Subjects::SUBJECT_CITY: $subject_type = City::class;
                break;
            case Subjects::SUBJECT_CATEGORY: $subject_type = Category::class;
                break;
            case Subjects::SUBJECT_TAG: $subject_type = Tag::class;
                break;
            case Subjects::SUBJECT_ITEM_HOT: $subject_type = ItemHot::class;
                break;
            case Subjects::SUBJECT_MANAGER: $subject_type = Manager::class;
                break;
            case Subjects::SUBJECT_PROPERTY: $subject_type = Property::class;
                break;
            case Subjects::SUBJECT_SLUG: $subject_type = Slug::class;
                break;
        }

        /** @var Activity $Activity */
        $Activities = Activity::where([
            'subject_id' => $id,
            'subject_type' => $subject_type
        ])->orderBy('created_at', 'DESC')->paginate(100);

        foreach ($Activities as $Activity) {
            $Activity->causer = $Activity->causer_type::find($Activity->causer_id);
            $Activity->subject = $Activity->subject_type::find($Activity->subject_id);
            if (method_exists($Activity->subject_type, 'getType')) {
                $Activity->subject_type = $Activity->subject_type::getType();
                $Activity->subject_name = Subjects::getSubjectName($Activity->subject_type);
            }
        }

        return $Activities;
    }

    /**
     * Show the profile for the given user.
     *
     * @param int $id
     *
     * @return Activity[]
     */
    public function getByUser($id)
    {
        /** @var Activity $Activity */
        $Activities = Activity::where([
            'causer_id' => $id,
        ])->orderBy('created_at', 'DESC')->paginate(100);

        foreach ($Activities as $Activity) {
            $Activity->causer = $Activity->causer_type::find($Activity->causer_id);
            $Activity->subject = $Activity->subject_type::find($Activity->subject_id);
            if (method_exists($Activity->subject_type, 'getType')) {
                $Activity->subject_type = $Activity->subject_type::getType();
                $Activity->subject_name = Subjects::getSubjectName($Activity->subject_type);
            }
        }

        return $Activities;
    }

    /**
     * Show the profile for the given user.
     *
     * @return Activity[]
     */
    public function getList()
    {
        /** @var Activity[] $Activity */
        $Activities = Activity::query()->orderBy('created_at', 'DESC')->paginate(100);

        foreach ($Activities as $Activity) {
            if(isset($Activity->causer)) {
                $Activity->causer = $Activity->causer_type::find($Activity->causer_id);
                $Activity->subject = $Activity->subject_type::find($Activity->subject_id);
                if (method_exists($Activity->subject_type, 'getType')) {
                    $Activity->subject_type = $Activity->subject_type::getType();
                    $Activity->subject_name = Subjects::getSubjectName($Activity->subject_type);
                }
            }
        }

        return $Activities;
    }
}