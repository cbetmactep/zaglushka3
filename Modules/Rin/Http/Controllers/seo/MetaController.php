<?php

namespace Modules\Rin\Http\Controllers\Seo;

use Cache;
use Config;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Modules\Rin\Models\Meta;

class MetaController extends BaseController
{
    /**
     * @param Request $Request
     *
     * @return array []
     * @throws \Exception
     */
    public function get(Request $Request)
    {
        /** @var Meta[] $Mets */
        $Mets = Meta::where([
            'subject_id' => $Request->get('subject_id'),
            'object_id'  => $Request->get('object_id'),
        ])->get();

        return [
            'Mets' => $Mets,

            'subdomains' => Config::get('app.subdomain_names'),

            'languages' => array_filter(Config::get('app.locale_names'), function ($key) {
                return in_array($key, Config::get('app.locales'));
            }, ARRAY_FILTER_USE_KEY),

            'replacements' => Meta::getReplacements((int) $Request->get('subject_id')),
        ];
    }

    /**
     * @param Request $Request
     *
     * @return Meta
     */
    public function update(Request $Request)
    {
        /** @var Meta $Meta */
        $Meta = Meta::where([
            'subject_id' => $Request->get('subject_id'),
            'object_id'  => $Request->get('object_id'),
            'subdomain'  => $Request->get('subdomain'),
            'language'   => $Request->get('language'),
        ])->first();

        if (!$Meta) {
            $Meta = new Meta();

            $Meta->subject_id = $Request->get('subject_id');
            $Meta->object_id  = $Request->get('object_id');
            $Meta->subdomain  = $Request->get('subdomain');
            $Meta->language   = $Request->get('language');
        }

        $Meta->fill($Request->all());

        $Meta->title       = $Meta->title ?: '';
        $Meta->description = $Meta->description ?: '';
        $Meta->keywords    = $Meta->keywords ?: '';

        $Meta->saveOrFail();

        Cache::forget('meta_' . $Meta->subject_id . '_' . $Meta->object_id . '_' . $Meta->subdomain . '_' . $Meta->language);

        return $this->get($Request);
    }
}
