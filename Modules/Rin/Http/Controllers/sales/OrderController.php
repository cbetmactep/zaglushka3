<?php

namespace Modules\Rin\Http\Controllers\Sales;

use App\Helpers\Formatter;
use App\Models\Order;
use File;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Storage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class OrderController extends BaseController
{
    /**
     * @return Order[]
     */
    public function getList(Request $Request)
    {
        $Orders = Order::query();

        if ($Request->get('date_from')) {
            $Orders = $Orders->where('date', '>=', $Request->get('date_from'));
        }

        if ($Request->get('date_to')) {
            $Orders = $Orders->where('date', '<=', $Request->get('date_to'));
        }

        $Orders = $Orders->orderBy('date', 'DESC');

        $Orders = $Orders->paginate(30);
/*
        foreach ($Orders as $Order) {
            $Order->total_price = Formatter::price($Order->total_price, true);
        }*/

        return $Orders;
    }

    /**
     * @return Order
     */
    public function get($id)
    {
        $Order = Order::findOrFail($id);

        $Order->items;

        foreach ($Order->items as $item) {
            $item->total_price = Formatter::price($item->price * $item->quant, true);
            $item->price       = Formatter::price($item->price, true);
        }
/*
        $Order->total_price = Formatter::price($Order->total_price, true);*/

        $Order->files = $Order->files();

        return $Order;
    }

    /**
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function downloadFile($id)
    {
        $Order = Order::findOrFail($id);

        if (!isset($Order->files()[0]) || !File::exists(storage_path('app/' . $Order->files()[0]->src))) {
            throw new NotFoundHttpException();
        }

        return response()->download(storage_path('app/' . $Order->files()[0]->src));

    }
}
