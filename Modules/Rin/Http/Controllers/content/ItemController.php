<?php

namespace Modules\Rin\Http\Controllers\Content;

use Cache;
use DB;
use Modules\Rin\Enum\Subjects;
use Modules\Rin\Models\Color;
use Modules\Rin\Models\ItemComplex;
use Modules\Rin\Models\Category;
use Modules\Rin\Models\ItemHasAddTag;
use Modules\Rin\Models\ItemHasTag;
use Modules\Rin\Models\ItemSame;
use Modules\Rin\Models\Property;
use File;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Modules\Rin\Http\Controllers\ItemController as BaseItemController;
use Modules\Rin\Models\Item;
use Modules\Rin\Models\ItemColor;
use Modules\Rin\Models\ItemSize;
use Modules\Rin\Models\Tag;
use Modules\Rin\Models\Slug as ModelSlug;
use Slug;
use App\Models\ProductionItems;
use App\Models\Production;

class ItemController extends BaseItemController
{

    /**
     * Show the profile for the given user.
     *
     * @param                          $id
     *
     * @param \Illuminate\Http\Request $Request
     *
     * @return bool
     */
    public function validate($id, Request $Request)
    {
        /** @var Item $Item */
        $Item = Item::findOrNew($id);
        $Item->fill($Request->get('item'));

        $Item->validate();

        return '';
    }

    /**
     * Show the profile for the given user.
     *
     * @param $id
     *
     * @return array
     */
    public function get($id = null)
    {
        /** @var Item $Item */
        $Item       = ($id === null) ? null : Item::findOrFail($id);
        $Categories = Category::getTopCategories();

        foreach ($Categories as &$Category) {
            $Category->subcategories;
        }

        if ($Item) {
            $Item->ItemComplex;
            $Item->itemSizes;
            $Item->itemColors;
            $Item->tags;
            $Item->addTags;
            $Item->item_same = ItemSame::getItems($Item->id);
            $Item->images    = $Item->getImagesWithUrl();
        }

        return [
            'Item' => $Item,

            'Categories'       => $Categories,
            'PropsMaterial'    => Property::where(['type' => Property::TYPE_MATERIAL])->get(),
            'PropsPackage'     => Property::where(['type' => Property::TYPE_PACKAGE])->get(),
            'PropsFit'         => Property::where(['type' => Property::TYPE_FIT])->get(),
            'Tags'             => Tag::all(),
            'Colors'           => Color::all(),
            'AdditionalPrices' => ItemColor::$additionalPrices,
            'SizeTypes'        => ItemSize::$types,
            'link'             => $Item ? $Item->getRoute() : '',
        ];
    }

    /**
     * @param int     $cloneId
     * @param Request $Request
     *
     * @return \Modules\Rin\Models\Item
     */
    public function copy($cloneId, Request $Request)
    {
        DB::transaction(function () use ($cloneId, $Request) {
            /** @var Item $OldItem */
            $OldItem = Item::findOrFail($cloneId);

            $NewItem = $OldItem->replicate();

            $NewItem->saveOrFail();

            $NewItem->generateSlug();

			// copy images after validation and saving
			$OldImages = array_keys($OldItem->getImages());
            foreach ($OldImages as $OldImage) {
                $newPath = public_path($NewItem->getImagePath($OldImage));
                File::makeDirectory(File::dirname($newPath), 0777, true, true);
                File::copy(public_path($OldItem->getImagePath($OldImage)), $newPath);
            }

            return $this->update($NewItem->id, $Request);
        });
    }

    /**
     * @param Request $Request
     *
     * @return Item
     */
    public function create(Request $Request)
    {
        DB::transaction(function () use ($Request) {
            $Item = new Item();

            // TODO: Фильтр применяемых значений
            $Item->fill($Request->get('item'));

            $Item->meta_title       = '';
            $Item->meta_description = '';
            $Item->meta_keywords    = '';

            // todo выпилить
            $Item->video_id    = 0;
            $Item->prop_expire = '';

            $Item->saveOrFail();

            $Item->generateSlug();

            return $this->update($Item->id, $Request);
        });
    }

    /**
     * @param int     $id
     * @param Request $Request
     *
     * @return \Modules\Rin\Models\Item
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update($id, Request $Request)
    {
        DB::transaction(function () use ($id, $Request) {
            /** @var Item $Item */
            $Item = Item::findOrFail($id);

            if (isset($Request->get('item_sizes')['deleted'])) {
                foreach ($Request->get('item_sizes')['deleted'] as $deleted) {
                    $Item->itemSizes()->where(['id' => $deleted])->delete();
                }
            }
            $request=$Request->all();
            //$log = date('Y-m-d H:i:s') . ' ' . print_r($Request->all(), true);
            //file_put_contents('/var/www/zaglushka/log.txt', $log . PHP_EOL, FILE_APPEND);
            
            // потом переместить в модель
            if(isset($request['item']['production'])) {
                $production=Production::where('production',$request['item']['production'])->first();
                if(!$production) {
                    $production = new Production;
                    $production->production=$request['item']['production'];
                    $production->save();
                }
                $productionItem=ProductionItems::where('iditem',$id)->first();
                if(!$productionItem) {
                    $productionItem=new ProductionItems();
                }
                $productionItem->iditem=$id;
                $productionItem->idproduction=$production->id;
                $productionItem->save();
                
            }
            /*$production=Production::where('production',$Request->input('production'))->first();
            
            if($production) {
                // id
            } else {
                $production = new Production;
                $production->production=$Request->input('production');
                $production->save();
            }*/
            
            if (isset($Request->get('item_sizes')['replaced'])) {
                foreach ($Request->get('item_sizes')['replaced'] as $replaced) {
                    /** @var ItemSize $ItemSize */
                    $ItemSize = $Item->itemSizes()->where(['id' => $replaced['id']])->first();

                    for ($i = 1; $i <= 6; $i++) {
                        if (isset($replaced['size_' . $i]) && $replaced['size_' . $i]) {
                            $replaced['size_' . $i] = str_replace(',', '.', $replaced['size_' . $i]);
                        }
                    }

                    $ItemSize->type = $replaced['type'];

                    $ItemSize->fill($replaced);
                    $ItemSize->validate();
                    $ItemSize->saveOrFail();
                }
            }

            if (isset($Request->get('item_sizes')['added'])) {
                foreach ($Request->get('item_sizes')['added'] as $added) {
                    $ItemSize = new ItemSize();

                    for ($i = 1; $i <= 6; $i++) {
                        if (isset($added['size_' . $i]) && $added['size_' . $i]) {
                            $added['size_' . $i] = str_replace(',', '.', $added['size_' . $i]);
                        }
                    }

                    $ItemSize->type    = $added['type'];
                    $ItemSize->item_id = $Item->id;

                    $ItemSize->fill($added);
                    $ItemSize->validate();
                    $ItemSize->saveOrFail();
                }
            }

            if (isset($Request->get('item_colors')['deleted'])) {
                foreach ($Request->get('item_colors')['deleted'] as $deleted) {
                    $Item->itemColors()->where(['id' => $deleted])->delete();
                }
            }

            if (isset($Request->get('item_colors')['replaced'])) {
                foreach ($Request->get('item_colors')['replaced'] as $replaced) {
                    $ItemColor = $Item->itemColors()->where(['id' => $replaced['id']])->first();

                    if (!$ItemColor) {
                        $ItemColor = new ItemColor();

                        $ItemColor->item_id = $Item->id;
                    }

                    $ItemColor->fill($replaced);
                    $ItemColor->validate();
                    $ItemColor->saveOrFail();
                }
            }

            Cache::delete('item_colors_exists_' . $Item->id);
            Cache::delete('item_main_color_' . $Item->id);
            Cache::delete('item_colors_available_' . $Item->id);
            Cache::delete('item_colors_' . $Item->id);

            if (isset($Request->get('item_tags')['deleted'])) {
                foreach ($Request->get('item_tags')['deleted'] as $deleted) {
                    ItemHasTag::where(['tag_id' => $deleted, 'item_id' => $Item->id])->delete();
                }
            }

            if (isset($Request->get('item_tags')['added'])) {
                foreach ($Request->get('item_tags')['added'] as $added) {
                    $Tag = Tag::find($added);

                    if ($Tag && $Item->tags()->where(['tag_id' => $added])->count() === 0) {
                        $ItemTag          = new ItemHasTag();
                        $ItemTag->item_id = $Item->id;
                        $ItemTag->tag_id  = $Tag->id;

                        $ItemTag->validate();
                        $ItemTag->saveOrFail();
                    }
                }
            }

            if (isset($Request->get('item_tags')['deleted']) ||
                isset($Request->get('item_tags')['added']) ||
                $Item->isDirty('cat_id') ||
                $Item->isDirty('add_cat_id')) {

                $redis = Cache::getRedis();
                $keys1 = $redis->keys(Cache::getPrefix() . 'top_tags_*');
                $keys2 = $redis->keys(Cache::getPrefix() . 'low_tags_*');

                foreach (array_merge($keys1, $keys2) as $key) {
                    $redis->del($key);
                }
            }

            if (isset($Request->get('item_add_tags')['deleted'])) {
                foreach ($Request->get('item_add_tags')['deleted'] as $deleted) {
                    ItemHasAddTag::where(['tag_id' => $deleted, 'item_id' => $Item->id])->delete();
                }
            }

            if (isset($Request->get('item_add_tags')['added'])) {
                foreach ($Request->get('item_add_tags')['added'] as $added) {
                    $Tag = Tag::find($added);

                    if ($Tag && $Item->addTags()->where(['tag_id' => $added])->count() === 0) {
                        $ItemAddTag          = new ItemHasAddTag();
                        $ItemAddTag->item_id = $Item->id;
                        $ItemAddTag->tag_id  = $Tag->id;

                        $ItemAddTag->validate();
                        $ItemAddTag->saveOrFail();
                    }
                }
            }

            if (isset($Request->get('item_same')['deleted'])) {
                foreach ($Request->get('item_same')['deleted'] as $deleted) {
                    ItemSame::where(function ($query) use ($deleted, $Item) {
                        $query->where(['same_item_id' => $deleted, 'item_id' => $Item->id]);
                    })->orWhere(function ($query) use ($deleted, $Item) {
                        $query->where(['same_item_id' => $Item->id, 'item_id' => $deleted]);
                    })->delete();
                }
            }

            if (isset($Request->get('item_same')['added'])) {
                foreach ($Request->get('item_same')['added'] as $added) {
                    $ItemLink = Item::find($added['id']);

                    if ($ItemLink &&
                        ($ItemLink->id != $Item->id) &&
                        ItemSame::where(function ($query) use ($Item, $ItemLink) {
                            $query->where(['same_item_id' => $ItemLink->id, 'item_id' => $Item->id]);
                        })->orWhere(function ($query) use ($Item, $ItemLink) {
                            $query->where(['same_item_id' => $Item->id, 'item_id' => $ItemLink->id]);
                        })->count() === 0) {

                        $ItemSame               = new ItemSame();
                        $ItemSame->item_id      = $Item->id;
                        $ItemSame->same_item_id = $ItemLink->id;

                        $ItemSame->validate();
                        $ItemSame->saveOrFail();
                    }
                }
            }

            // TODO: Фильтр применяемых значений
            $Item->fill($Request->get('item'));

            $Item->validate();
            $Item->saveOrFail();

            if (isset($Request->get('item_complex')['deleted'])) {
                foreach ($Request->get('item_complex')['deleted'] as $deleted) {
                    ItemComplex::where(['complect_item_id' => $deleted, 'item_id' => $Item->id])->delete();
                }
            }

            if (isset($Request->get('item_complex')['added'])) {
                foreach ($Request->get('item_complex')['added'] as $added) {
                    $ItemLink = Item::find($added['id']);

                    if ($ItemLink &&
                        ($ItemLink->id != $Item->id) &&
                        ItemComplex::where([
                            'complect_item_id' => $ItemLink->id,
                            'item_id'          => $Item->id,
                        ])->count() === 0) {
                        $ItemComplex                   = new ItemComplex();
                        $ItemComplex->item_id          = $Item->id;
                        $ItemComplex->complect_item_id = $ItemLink->id;

                        $ItemComplex->saveOrFail();
                    }
                }
            }

            $ItemComplex = $Item->ItemComplex;

            if (count($ItemComplex) > 0) {
                ItemComplex::updateComplexItem($Item, $ItemComplex);
            }

            // если входит в комплекты
            $ItemsIds = ItemComplex::where(['complect_item_id' => $Item->id])
                ->select(['item_id'])
                ->pluck('item_id');

            $ParentItemsForUpdate = Item::whereIn('id', $ItemsIds)->get();

            foreach ($ParentItemsForUpdate as $ParentItem) {
                ItemComplex::updateComplexItem($ParentItem, $ParentItem->ItemComplex);
            }

            //

            if (isset($Request->get('item_images')['deleted'])) {
                foreach ($Request->get('item_images')['deleted'] as $deleted) {
                    File::delete(public_path($Item->getImagePath($deleted)));
                }
            }

            if (isset($Request->allFiles()['images']['replaced'])) {
                foreach ($Request->allFiles()['images']['replaced'] as $key => $replaced) {
                    /** @var \Illuminate\Http\UploadedFile $replaced */
                    $replaced->move(public_path($Item->getImageFolder()), $key);

                    if ($key == Item::IMAGE_TYPE_4) {
                        // resizing an uploaded file
                        Image::make(public_path($Item->getImagePath(Item::IMAGE_TYPE_4)))
                            ->resize(95, 95)
                            ->save(public_path($Item->getImagePath(Item::IMAGE_TYPE_8)), 100);
                    }
                    if ($key == Item::IMAGE_TYPE_29) {
                        // resizing an uploaded file
                        Image::make(public_path($Item->getImagePath(Item::IMAGE_TYPE_29)))
                            ->resize(95, 95)
                            ->save(public_path($Item->getImagePath(Item::IMAGE_TYPE_30)), 100);
                    }
                }
            }

            //

            $Item = $Item->fresh();

            $Item->ItemComplex;
            $Item->itemSizes;
            $Item->itemColors;
            $Item->tags;
            $Item->addTags;
            $Item->item_same = ItemSame::getItems($Item->id);
            $Item->images    = $Item->getImagesWithUrl();

            return $Item;
        });
    }

    /**
     * @param int                      $id
     * @param \Illuminate\Http\Request $Request
     *
     * @return \Modules\Rin\Models\Item
     */
    public function imageSwap($id, Request $Request)
    {
        /** @var Item $Item */
        $Item = Item::findOrFail($id);

        File::move(public_path($Item->getImageFolder() . $Request->get('image_1')),
            public_path($Item->getImageFolder() . $Request->get('image_1')) . '_tmp');
        File::move(public_path($Item->getImageFolder() . $Request->get('image_2')),
            public_path($Item->getImageFolder() . $Request->get('image_1')));
        File::move(public_path($Item->getImageFolder() . $Request->get('image_1')) . '_tmp',
            public_path($Item->getImageFolder() . $Request->get('image_2')));

        $Item->ItemComplex;
        $Item->itemSizes;
        $Item->itemColors;
        $Item->tags;
        $Item->addTags;
        $Item->item_same = ItemSame::getItems($Item->id);
        $Item->images    = $Item->getImagesWithUrl();

        return $Item;
    }

    public function imageMultiChange(Request $Request)
    {
        if ($Request->get('items') && $Request->get('delete')) {
            /** @var Item[] $Items */
            $Items = Item::whereIn('id', $Request->get('items'))->get();

            foreach ($Items as $Item) {
                foreach ($Request->get('delete') as $delete) {
                    File::delete(public_path($Item->getImageFolder() . $delete));
                }
            }
        }

        if (isset($Request->allFiles()['images']) && $Request->get('items')) {
            /** @var Item[] $Items */
            $Items = Item::whereIn('id', $Request->get('items'))->get();

            foreach ($Items as $Item) {
                foreach ($Request->allFiles()['images'] as $key => $replaced) {
                    if (!File::exists(public_path($Item->getImageFolder()))) {
                        File::makeDirectory(public_path($Item->getImageFolder()), $mode = 0777, true, true);
                    }

                    File::copy($replaced, public_path($Item->getImageFolder() . $key));

                    /** @var \Illuminate\Http\UploadedFile $replaced */
                    if ($key == Item::IMAGE_TYPE_4) {
                        // resizing an uploaded file
                        Image::make(public_path($Item->getImagePath(Item::IMAGE_TYPE_4)))
                            ->resize(95, 95)
                            ->save(public_path($Item->getImagePath(Item::IMAGE_TYPE_8)), 100);
                    }
                    if ($key == Item::IMAGE_TYPE_29) {
                        // resizing an uploaded file
                        Image::make(public_path($Item->getImagePath(Item::IMAGE_TYPE_29)))
                            ->resize(95, 95)
                            ->save(public_path($Item->getImagePath(Item::IMAGE_TYPE_30)), 100);
                    }
                }
            }

            return [];
        }
    }

    public function deleteSlug($id)
    {
        $ModelSlugs = ModelSlug::where([
            'object_id'  => $id,
            'subject_id' => Subjects::SUBJECT_ITEM,
        ])->get();

        foreach ($ModelSlugs as $ModelSlug) {
            $ModelSlug->delete();
        }
    }

    public function createSlug($id)
    {
        /** @var Item $Item */
        $Item = Item::findOrFail($id);

        $Slug = ModelSlug::where([
            'subject_id' => Subjects::SUBJECT_ITEM,
            'object_id'  => $Item->id,
            'current'    => true,
        ])->first();

        if (!$Slug) {
            $Slug = new ModelSlug();

            $Slug->subject_id = Subjects::SUBJECT_ITEM;
            $Slug->object_id  = $Item->id;

            $Slug->slug = str_slug(Slug::make(html_entity_decode(($Item->yandex_name ? $Item->yandex_name . '-' : '') . $Item->title)));
            $Slug->current = true;

            $Slug->saveOrFail();
        }
    }
}
