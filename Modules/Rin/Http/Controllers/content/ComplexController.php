<?php

namespace Modules\Rin\Http\Controllers\Content;

use Modules\Rin\Models\Color;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Validation\ValidationException;
use Modules\Rin\Models\Item;
use Modules\Rin\Models\ItemColor;
use Modules\Rin\Models\ItemComplexColor;
use Illuminate\Http\Request;

class ComplexController extends BaseController
{
    /**
     * @return array
     */
    public function getList()
    {
        $ItemsIds = ItemComplexColor::select(['item_id'])->pluck('item_id');
        $Items    = Item::whereIn('id', $ItemsIds)->get();

        return $Items;
    }

    /**
     * @param $id
     *
     * @return array
     */
    public function get($id)
    {
        /** @var Item $Item */
        $Item = Item::findOrFail($id);

        $Item->item_complex_colors = $Item->itemComplexColors()
            ->select('items.id', 'title')
            ->distinct()
            ->get();

        foreach ($Item->itemColors as $Color) {
            $Color->complex_color = $Color->complexColors();
        }

        return [
            'Item'             => $Item,
            'Colors'           => Color::all(),
            'AdditionalPrices' => ItemColor::$additionalPrices,
            'link'             => $Item ? $Item->getRoute() : '',
        ];
    }

    /**
     * @param                   int    $id
     * @param \Illuminate\Http\Request $Request
     *
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update($id, Request $Request)
    {
        /** @var Item $Item */
        $Item = Item::findOrFail($id);

        if (isset($Request->get('item_colors')['deleted'])) {
            foreach ($Request->get('item_colors')['deleted'] as $deleted) {
                $Item->itemColors()->where(['id' => $deleted])->delete();
            }
        }

        if (isset($Request->get('item_complex_colors')['deleted'])) {
            foreach ($Request->get('item_complex_colors')['deleted'] as $deleted) {
                ItemComplexColor::where([
                    'complex_item_id'   => $Item->id,
                    'component_item_id' => $deleted,
                ])->delete();

            }
        }

        $componentItems = [];

        if (isset($Request->get('item_colors')['replaced'])) {
            foreach ($Request->get('item_colors')['replaced'] as $replaced) {
                $ItemColor = $Item->itemColors()->where(['id' => $replaced['id']])->first();

                if (!$ItemColor) {
                    $ItemColor = new ItemColor();

                    $ItemColor->item_id = $Item->id;
                }

                $ItemColor->fill($replaced);

                if (!$ItemColor->validate()) {
                    throw new ValidationException($ItemColor->getValidator());
                }

                $ItemColor->saveOrFail();

                ItemComplexColor::where([
                    'complex_item_id'    => $Item->id,
                    'complex_item_color' => $ItemColor->color,
                ])->delete();

                if (isset($replaced['complex_color'])) {
                    foreach ($replaced['complex_color'] as $key => $value) {
                        /** @var Item $ItemLink */
                        $ItemLink = Item::find($key);
                        /** @var ItemColor $ItemLinkColor */
                        $ItemLinkColor = ItemColor::where(['color' => $value, 'item_id' => $ItemLink->id])->first();

                        if ($ItemLink && $ItemLinkColor && ($ItemLink->id != $Item->id)) {
                            $ItemComplexColor                    = new ItemComplexColor();
                            $ItemComplexColor->complex_item_id   = $Item->id;
                            $ItemComplexColor->component_item_id = $ItemLink->id;

                            $ItemComplexColor->complex_item_color   = $ItemColor->color;
                            $ItemComplexColor->component_item_color = $ItemLinkColor->color;

                            if ($ItemComplexColor->save()) {
                                $componentItems[] = $ItemLink->id;
                            }
                        };
                    }
                }
            }

            ItemComplexColor::where([
                'complex_item_id' => $Item->id,
            ])->whereNotIn('component_item_id', $componentItems)
                ->delete();
        }

        ItemComplexColor::updateComplexItem($Item);

        $Item = $Item->refresh();

        $Item->item_complex_colors = $Item->itemComplexColors()
            ->select('items.id', 'title')
            ->distinct()
            ->get();

        foreach ($Item->itemColors as $Color) {
            $Color->complex_color = $Color->complexColors();
        }

        return [
            'Item'             => $Item,
            'Colors'           => Color::all(),
            'AdditionalPrices' => ItemColor::$additionalPrices,
            'link'             => $Item ? $Item->getRoute() : '',
        ];
    }
}
