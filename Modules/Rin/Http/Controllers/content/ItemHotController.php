<?php

namespace Modules\Rin\Http\Controllers\Content;

use Modules\Rin\Enum\Subjects;
use Modules\Rin\Models\Category;
use Modules\Rin\Models\Item;
use Modules\Rin\Models\ItemHot;
use Illuminate\Http\Request;
use Modules\Rin\Http\Controllers\TagController;
use Modules\Rin\Models\Tag;

class ItemHotController extends TagController
{

    /**
     * Show the profile for the given user.
     *
     * @param                          $id
     *
     * @param \Illuminate\Http\Request $Request
     *
     * @return bool
     */
    public function validate($id, Request $Request)
    {
        /** @var Item $Item */
        $ItemHot = ItemHot::findOrNew($id);
        $ItemHot->fill($Request->all());

        $ItemHot->validate();

        return '';
    }

    /**
     * @return ItemHot[]
     */
    public function getList()
    {
        /** @var ItemHot[] $HotItems */
        $HotItems = ItemHot::withTrashed()->get();

        foreach ($HotItems as $HotItem) {
            $HotItem->enabled = ($HotItem->deleted_at === null);
        }

        return $HotItems;
    }

    /**
     * @param $id
     *
     * @return ItemHot
     */
    public function get($id)
    {
        /** @var ItemHot $ItemHot */
        $ItemHot = ItemHot::withTrashed()->findOrFail($id);

        // todo вынести всю логику в модель

        $ItemHot->enabled = ($ItemHot->deleted_at === null);

        $Object = null;
        switch ($ItemHot->subject_id) {
            case Subjects::SUBJECT_CATEGORY:
                $Object = Category::find($ItemHot->object_id);
                break;
            case Subjects::SUBJECT_ITEM:
                $Object = Item::find($ItemHot->object_id);
                break;
            case Subjects::SUBJECT_TAG:
                $Object = Tag::find($ItemHot->object_id);
                break;
        }

        if ($Object) {
            $ItemHot->object_name = $Object->title;
        }

        if ($ItemHot->tag_category_id) {
            $Category = Category::find($ItemHot->tag_category_id);

            if ($Category) {
                $ItemHot->tag_category_name = $Category->title;
            }
        }

        $Category = Category::find($ItemHot->category_id);

        if ($Category) {
            $ItemHot->category_name = $Category->title;
        }

        $Photo = $ItemHot->file();
        if ($Photo) {
            $ItemHot->photo = '/files/' . $Photo->src;
        }

        return $ItemHot;
    }

    /**
     * @param int                      $id
     * @param \Illuminate\Http\Request $Request
     *
     * @return ItemHot
     */
    public function update($id, Request $Request)
    {
        /** @var ItemHot $ItemHot */
        $ItemHot = ItemHot::withTrashed()->findOrFail($id);

        $ItemHot->fill($Request->all());

        if ($Request->get('tag_category_id')) {
            $ItemHot->additional_info = json_encode(['tag_category_id' => (int) $Request->get('tag_category_id')]);
        }

        $ItemHot->validate();
        $ItemHot->saveOrFail();

        $Photo = $Request->file('photo');
        if ($Photo) {
            $ItemHot->replaceFile($Photo, 0, true);
        }

        if ($Request->get('enabled') === '0') {
            $ItemHot->delete();
        } elseif ($Request->get('enabled') && $ItemHot->deleted_at) {
            $ItemHot->restore();
        }

        return $ItemHot;
    }

    /**
     * @param \Illuminate\Http\Request $Request
     *
     * @return ItemHot
     */
    public function add(Request $Request)
    {
        $ItemHot = new ItemHot($Request->all());

        $ItemHot->validate();
        $ItemHot->saveOrFail();

        $Photo = $Request->file('photo');
        if ($Photo) {
            $ItemHot->addFile($Photo, 0, true);
        }

        if ($Request->get('enabled') === '0') {
            $ItemHot->delete();
        }

        return $ItemHot;
    }

    /**
     * @param int $id
     */
    public function delete($id) : void
    {
        $ItemHot = ItemHot::onlyTrashed()->findOrFail($id);

        $ItemHot->forceDelete();
    }
}
