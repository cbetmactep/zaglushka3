<?php

namespace Modules\Rin\Http\Controllers\Content;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Modules\Rin\Models\Property;
use Validator;

class PropertyController extends BaseController
{

    /**
     * @param $type
     *
     * @return Property[]
     */
    public function getList($type)
    {

        return Property::where(['type' => $type])->get();
    }

    /**
     * @param int                      $id
     * @param \Illuminate\Http\Request $Request
     *
     * @return Property
     */
    public function update($id, Request $Request)
    {
        Validator::make($Request->all(), [
            'title' => 'required',
        ])->validate();

        /** @var Property $Property */
        $Property = Property::findOrFail($id);

        $Property->fill($Request->all());

        $Property->saveOrFail();

        return $Property;
    }

    public function delete($id)
    {
        $Property = Property::findOrFail($id);

        $Property->delete();
    }

    /**
     * @param \Illuminate\Http\Request $Request
     *
     * @return Property
     */
    public function add(Request $Request)
    {
        Validator::make($Request->all(), [
            'title' => 'required',
            'type'  => 'required',
        ])->validate();

        $Property = new Property($Request->all());

        $Property->saveOrFail();

        return $Property;
    }
}
