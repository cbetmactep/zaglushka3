<?php

namespace Modules\Rin\Http\Controllers\Content;

use Cache;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Modules\Rin\Models\City;
use Validator;

class CityController extends BaseController
{

    /**
     * @return City[]
     */
    public function getList()
    {

        return City::all();
    }

    /**
     * @param int                      $id
     * @param \Illuminate\Http\Request $Request
     *
     * @return City
     */
    public function update($id, Request $Request)
    {
        Validator::make($Request->all(), [
            'title'               => 'required',
        ])->validate();

        /** @var City $City */
        $City = City::findOrFail($id);

        $City->fill($Request->all());

        $City->phone = $City->phone ?: '';

        $City->saveOrFail();

        return $City;
    }

    public function delete($id)
    {
        $City = City::findOrFail($id);

        $City->delete();
    }

    /**
     * @param \Illuminate\Http\Request $Request
     *
     * @return City
     */
    public function add(Request $Request)
    {
        Validator::make($Request->all(), [
            'title'               => 'required',
        ])->validate();

        $City = new City($Request->all());

        $City->saveOrFail();

        return $City;
    }
}
