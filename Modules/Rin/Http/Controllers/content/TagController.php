<?php

namespace Modules\Rin\Http\Controllers\Content;

use Modules\Rin\Enum\Subjects;
use Illuminate\Http\Request;
use Modules\Rin\Http\Controllers\TagController as BaseTagController;
use Modules\Rin\Models\Tag;
use Slug;
use Modules\Rin\Models\Slug as ModelSlug;

class TagController extends BaseTagController
{
    /**
     * @param Request $Request
     *
     * @return bool
     */
    public function add(Request $Request)
    {
        $Tag = new Tag();

        $Tag->fill($Request->all());

        $Tag->translit = str_slug(Slug::make($Tag->title));
        $Tag->meta_title       = '';
        $Tag->meta_description = '';
        $Tag->meta_keywords    = '';

        $Tag->validate();
        $Tag->saveOrFail();

        // генерация slug

        $Slug = new ModelSlug();

        $Slug->subject_id = Subjects::SUBJECT_TAG;
        $Slug->object_id  = $Tag->id;

        $Slug->slug    = str_slug(Slug::make($Tag->title));
        $Slug->current = true;

        $Slug->saveOrFail();
    }

    /**
     * @param int     $id
     * @param Request $Request
     *
     * @return bool
     */
    public function update($id, Request $Request)
    {
        /** @var Tag $Tag */
        $Tag = Tag::findOrFail($id);

        $Tag->fill($Request->all());

        $Tag->validate();
        $Tag->saveOrFail();
    }

    /**
     * @param Request $Request
     *
     * @return bool
     */
    public function order(Request $Request)
    {
        $orders = $Request->all();

        $Tags = Tag::all();

        foreach ($Tags as $Tag) {
            if (isset($orders[$Tag->id])) {
                $Tag->ord = $orders[$Tag->id];

                $Tag->save();
            }
        }
    }
}
