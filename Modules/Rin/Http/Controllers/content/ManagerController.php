<?php

namespace Modules\Rin\Http\Controllers\Content;

use Modules\Rin\Models\Manager;
use Illuminate\Http\Request;
use Modules\Rin\Http\Controllers\TagController;
use Validator;

class ManagerController extends TagController
{

    /**
     * @return Manager[]
     */
    public function getList()
    {

        return Manager::all();
    }

    /**
     * @param int                      $id
     * @param \Illuminate\Http\Request $Request
     *
     * @return Manager
     */
    public function update($id, Request $Request)
    {
        Validator::make($Request->all(), [
            'title'   => 'required',
            'phone'   => 'required',
            'email'   => 'required',
            'enabled' => 'required',
            'ord'     => 'required',
        ])->validate();

        /** @var Manager $Manager */
        $Manager = Manager::findOrFail($id);

        $Manager->fill($Request->all());

        $Manager->saveOrFail();

        return $Manager;
    }

    public function delete($id)
    {
        $Manager = Manager::findOrFail($id);

        $Manager->delete();
    }

    /**
     * @param \Illuminate\Http\Request $Request
     *
     * @return Manager
     */
    public function add(Request $Request)
    {
        Validator::make($Request->all(), [
            'title'   => 'required',
            'phone'   => 'required',
            'email'   => 'required',
            'enabled' => 'required',
            'ord'     => 'required',
        ])->validate();

        $Manager = new Manager($Request->all());

        $Manager->saveOrFail();

        return $Manager;
    }
}
