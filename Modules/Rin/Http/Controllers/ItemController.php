<?php

namespace Modules\Rin\Http\Controllers;

use App\Helpers\Tools;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Modules\Rin\Enum\Subjects;
use Modules\Rin\Models\Category;
use Modules\Rin\Models\Item;
use Modules\Rin\Models\Slug as ModelSlug;

class ItemController extends BaseController
{

    /**
     * Show the profile for the given user.
     *
     * @param $id
     *
     * @return Item
     */
    public function get($id)
    {
        /** @var Item $Item */
        $Item = Item::findOrFail($id);

        return $Item;
    }

    /**
     * Show the profile for the given user.
     *
     * @param $categoryId
     *
     * @return Item[]
     */
    public function getList($categoryId)
    {
        /** @var Category $Category */
        $Category = Category::findOrFail($categoryId);

        $wedge = 'custom_cat_position';

        /** @var Item[] $Items */
        $Items = $Category->items($wedge)->paginate(100);

        $Items->setCollection(Tools::collectionCustomPosition($Items->getCollection(), $wedge));

        foreach ($Items as $Item) {
            $ModelSlug = ModelSlug::where([
                'object_id'  => $Item->id,
                'subject_id' => Subjects::SUBJECT_ITEM,
                'current'    => true,
            ])->first();

            $Item->slug = $ModelSlug ? $ModelSlug->slug : null;
        }

        return $Items;
    }
}
