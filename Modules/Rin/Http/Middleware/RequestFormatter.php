<?php

namespace Modules\Rin\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class RequestFormatter
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $values = $request->input();

        $this->filterValue($values);

        $request->merge($values);

        return $next($request);
    }

    /**
     *
     */
    public function filterValue(&$values)
    {
        foreach ($values as $inputKey => &$inputValue) {
            if (is_array($inputValue)) {
                $this->filterValue($inputValue);
            } else {
                if ($inputValue === 'undefined') {
                    $inputValue = null;
                } else if ($inputValue === 'true') {
                    $inputValue = true;
                } else if ($inputValue === 'false') {
                    $inputValue = false;
                }
            }
        }
    }
}
