<?php

namespace Modules\Rin\Http;

class Response extends \Response
{
    public static function error($code, $message = '')
    {
        switch ($code) {
            case 404:
                $message = (!$message ? $message = 'the requested resource was not found' : $message);
                break;
        }

        return Response::json([
            'code'    => $code,
            'message' => $message,
        ], $code);
    }
}
