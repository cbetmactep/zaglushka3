<?php
declare(strict_types=1);

namespace App\Traits;

use App;
use App\Enum\Subjects;
use App\Helpers\Formatter;
use App\Helpers\Tools;
use App\Models\Category;
use App\Models\City;
use App\Models\Meta;
use Cache;
use Config;
use morphos\Russian\GeographicalNamesInflection;

trait MetaAble
{
    /**
     * @return Meta
     */
    public function getMeta() : ?Meta
    {
        $subdomain = 'www'; // todo Config::get('app.subdomain') ?: 'www'
        $language  = App::getLocale();

        $Meta = Cache::remember(
            'meta_' . $this->getType() . '_' . $this->id . '_' . $subdomain . '_' . $language,
            self::CACHE_TIME,
            function () use ($subdomain, $language) {
                $Meta = Meta::where([
                    'subject_id' => self::getType(),
                    'object_id'  => $this->id,
                    'subdomain'  => $subdomain,
                    'language'   => $language,
                ])->first();

                return $Meta ?: false;
            }
        );

        if ($Meta === false) {
            $Meta = Cache::remember(
                'meta_' . $this->getType() . '_' . 0 . '_' . $subdomain . '_' . $language,
                self::CACHE_TIME,
                function () use ($subdomain, $language) {
                    // пробуем взять родительскую для всего сабжекта
                    $Meta = Meta::where([
                        'subject_id' => self::getType(),
                        'object_id'  => 0,
                        'subdomain'  => $subdomain,
                        'language'   => $language,
                    ])->first();

                    return $Meta ?: false;
                }
            );
        }

        return $Meta === false ? null : $Meta;
    }

    /**
     * @param null|Category $Category
     */
    public function getMetaTitle(Category $Category = null) : string
    {
        if ($this->getMeta()) {
            return $this->replaceMetaReplacements($this->getMeta()->title, $Category);
        }

        if (App::isLocale('ru')) {
            if (false && Config::get('app.subdomain')) { // todo false
                $minPrice = $this->getMinPrice($Category);
                $minLabel = Formatter::getNumEnding($minPrice, ['рубля', 'рублей', 'рублей']);

                if ($minPrice < 1) {
                    $minPrice = (int) ($minPrice * 100);
                    $minLabel = Formatter::getNumEnding($minPrice, ['копейки', 'копеек', 'копеек']);
                }

                $minLabel = ' от ' . $minPrice . ' ' . $minLabel;

                if ($minPrice == 0) {
                    $minLabel = '';
                }

                $cityName = GeographicalNamesInflection::getCase(City::cachedOne((int) session('current_city'))->title, 'предложный');

                if ($this->getType() == Subjects::SUBJECT_TAG) {
                    $title = 'Купить ' . mb_strtolower($this->getTitle() . ' ' . $Category->title) . ' в ' . $cityName . $minLabel;
                } else {
                    $title = 'Купить ' . mb_strtolower($this->getTitle()) . ' в  ' . $cityName . $minLabel;
                }

                return preg_replace('#[\\\/]#', ' ', $title);
            }

            if ($this->meta_title) {
                if (Config::get('app.subdomain')) {
                    return $this->replaceCityFromSpb($this->meta_title);
                }

                return $this->meta_title;
            }
        }

        return $this->getTitle(true);
    }

    /**
     * @param null|Category $Category
     */
    public function getMetaDescription(Category $Category = null) : string
    {
        if ($this->getMeta()) {
            return $this->replaceMetaReplacements($this->getMeta()->description, $Category);
        }

        if (App::isLocale('ru')) {
            if (false && Config::get('app.subdomain')) { // todo false
                if ($this->getType() == Subjects::SUBJECT_ITEM) {
                    return $this->yandex_name;
                }

                return '';
            }

            if ($this->getType() == Subjects::SUBJECT_ITEM) {
                return "{$Category->title} {$this->getTitle()}: подробные характеристики, информация о наличии, цветах, вариантах доставки и способах оплаты";
            }

            if ($this->meta_description) {
                if (Config::get('app.subdomain')) {
                    return $this->replaceCityFromSpb($this->meta_description);
                }

                return $this->meta_description;
            }
        }

        return $this->getTitle(true);
    }

    /**
     * @param Category $Category
     *
     * @return string
     */
    public function getMetaKeywords(Category $Category = null) : string
    {
        if ($this->getMeta()) {
            return $this->replaceMetaReplacements($this->getMeta()->keywords, $Category);
        }

        if (App::isLocale('ru')) {
            if (false && Config::get('app.subdomain')) { // todo false
                return '';
            }

            if ($this->getType() == Subjects::SUBJECT_ITEM) {
                return "{$Category->title}, {$this->title}, купить, цена, интернет-магазин";
            }

            if ($this->meta_keywords) {
                return $this->meta_keywords;
            }
        }

        return $this->getTitle(true);
    }

    private function replaceCityFromSpb(string $string)
    {
        $cityName = City::cachedOne((int) session('current_city'))->title;

        $string = Tools::mb_ucfirst(preg_replace(
            '/спб/ui',
            GeographicalNamesInflection::getCase($cityName, 'предложный'),
            $string
        ));

        $string = Tools::mb_ucfirst(preg_replace(
            '/Санкт-Петербурге/ui',
            GeographicalNamesInflection::getCase($cityName, 'предложный'),
            $string
        ));

        $string = Tools::mb_ucfirst(preg_replace(
            '/Санкт-Петербург/ui',
            GeographicalNamesInflection::getCase($cityName, 'именительный'),
            $string
        ));

        return $string;
    }
}
