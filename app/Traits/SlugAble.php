<?php
declare(strict_types=1);

namespace App\Traits;

use App\Models\Slug as ModelSlug;
use Cache;

trait SlugAble
{
    /**
     * @return string
     */
    public function slug() : string
    {
        $Slug = Cache::remember(
            'slug_by_id_' . self::getType() . '_' . $this->id,
            self::CACHE_TIME,
            function () : ?ModelSlug {
                return ModelSlug::where([
                    'object_id'  => $this->id,
                    'subject_id' => self::getType(),
                    'current'    => true,
                ])->first();
            }
        );
        //todo
        if (!$Slug) {
            return '/404';
        }

        return $Slug->slug;
    }
}
