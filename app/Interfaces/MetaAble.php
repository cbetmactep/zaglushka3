<?php
declare(strict_types=1);

namespace App\Interfaces;

interface MetaAble
{
    public function getMeta();

    /**
     * @return int тип текущей модели
     */
    public static function getType() : int;

    public function replaceMetaReplacements(string $string) : string;

    public function getTitle() : string;
}
