<?php
declare(strict_types=1);

namespace App\Interfaces;

interface SlugAble
{
    /**
     * @return int тип текущей модели
     */
    public static function getType() : int;

    public function getTitle() : string;

    /**
     * @return mixed
     */
    public function slug() : string;
}
