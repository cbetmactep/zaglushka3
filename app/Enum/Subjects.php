<?php
declare(strict_types=1);

namespace App\Enum;

class Subjects
{
    const SUBJECT_ORDER = 1;
    const SUBJECT_CATEGORY = 2;
    const SUBJECT_TAG = 3;
    const SUBJECT_ITEM = 4;
    const SUBJECT_COLOR = 5;
    const SUBJECT_CITY = 6;
    const SUBJECT_ITEM_HOT = 7;
    const SUBJECT_MANAGER = 8;
    const SUBJECT_PROPERTY = 9;
    const SUBJECT_SLUG = 9;
}
