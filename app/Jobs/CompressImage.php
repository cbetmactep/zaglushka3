<?php
declare(strict_types=1);

namespace App\Jobs;

use File;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Log;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;

class CompressImage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $url;

    /**
     * Create a new job instance.
     *
     * @param string $url
     */
    public function __construct($url)
    {
        $this->url = $url;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $Monolog = Log::getMonolog();

        $Monolog->setHandlers([$handler = new StreamHandler(storage_path('/logs/items:compressimages.log'))]);

        $handler->setFormatter(new LineFormatter(null, null, true, true));

        $lastModified    = File::lastModified(public_path($this->url));
        $cachedFileName     = '/cached/' . md5($this->url . $lastModified) . '.' . File::extension($this->url);

        $compressionsThisMonth = \Tinify\compressionCount();

        if ($compressionsThisMonth >= 490) {
            return;
        }

        if (!File::exists(public_path($cachedFileName))) {
            try {
                \Tinify\setKey(getenv('TINY_PNG_KEY'));
                $source = \Tinify\fromFile(public_path($this->url));
                $source->toFile(public_path($cachedFileName));
            } catch (\Tinify\ClientException $e) {
                // забиваем на этот файл
                File::copy(public_path($this->url), public_path($cachedFileName));
            }
        }
    }
}
