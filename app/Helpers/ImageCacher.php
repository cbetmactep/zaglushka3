<?php
declare(strict_types=1);

namespace App\Helpers;

use App;
use App\Jobs\CompressImage;
use File;

/**
 * Class Price.
 */
class ImageCacher
{
    /**
     * @param string $url
     *
     * @return string
     */
    public static function cache(string $url) : string
    {
        if (!File::exists(public_path($url))) {
            return $url;
        }

        $lastModified   = File::lastModified(public_path($url));
        $cachedFileName = '/cached/' . md5($url . $lastModified) . '.' . File::extension($url);

        if (File::exists(public_path($cachedFileName))) {
            return $cachedFileName;
        }

        if (App::environment() === 'production' && (File::extension($url) == 'png' || File::extension($url) == 'jpg')) {
            $job = (new CompressImage($url));
            dispatch($job);
        }

        return $url;
    }
}
