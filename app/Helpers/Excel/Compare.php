<?php
declare(strict_types=1);

namespace App\Helpers\Excel;

use App\Helpers\Formatter;
use App\Models\Color;
use App\Models\Manager;
use Auth;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_RichText;
use PHPExcel_Style_Alignment;
use PHPExcel_Style_Border;
use PHPExcel_Style_Color;
use PHPExcel_Style_Fill;
use PHPExcel_Worksheet_Drawing;
use PHPExcel_Worksheet_PageSetup;
use App\Models\Item as ItemModel;

/**
 * @deprecated 
 */
class Compare
{
    /**
     * @param \App\Models\ItemColor[] $Colors
     */
    public static function generate($Colors): void
    {
        $Items = [];
        $ItemColors = [];
        foreach ($Colors as $Color) {
            if (!isset($Items[$Color->item_id])) {
                $Items[$Color->item_id] = $Color->item;
                $ItemColors[$Color->item_id] = [];
            }

            $ItemColors[$Color->item_id][] = $Color;
        }

        if (Auth::check() && Auth::user()->isManager()) {
            $Managers = Manager::where(['email' => Auth::user()->email])->get();
        }

        if (!isset($Managers) || count($Managers) === 0) {
            $Managers = Manager::where(['enabled' => 1])->get();
        }

        $cExcel = new PHPExcel();
        $cExcel->setActiveSheetIndex(0);
        $cWorksheet = $cExcel->getActiveSheet();
        $cExcel->getDefaultStyle()->getAlignment()->setWrapText(true);
        $cExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $cExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $cWorksheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $cWorksheet->setShowGridlines(true);

        $cDrawing = new PHPExcel_Worksheet_Drawing();
        $cDrawing->setPath(public_path('/images/excel/logo.png'));
        $cDrawing->setWidthAndHeight(260, 260);
        $cDrawing->setOffsetX(10);
        $cDrawing->setOffsetY(15);
        $cDrawing->setWorksheet($cWorksheet);
        $cDrawing->setCoordinates('A1');

        if (count($Managers) === 1) {
            $cWorksheet->mergeCellsByColumnAndRow(3, 1, 6, 1);
            $cWorksheet->setCellValueByColumnAndRow(3, 1, "АДРЕС СКЛАДА:");
            $cWorksheet->mergeCellsByColumnAndRow(3, 2, 6, 2);
            $cWorksheet->setCellValueByColumnAndRow(3, 2, "г. Санкт-Петербург, п. Парголово");
            $cWorksheet->mergeCellsByColumnAndRow(3, 3, 6, 3);
            $cWorksheet->setCellValueByColumnAndRow(3, 3, "ул. Подгорная, дом 6.");
            $cWorksheet->mergeCellsByColumnAndRow(3, 4, 6, 4);
            $cWorksheet->setCellValueByColumnAndRow(3, 4, "Въезд с ул. Железнодорожная, дом 11");
            $cWorksheet->mergeCellsByColumnAndRow(3, 5, 6, 5);
            $cWorksheet->setCellValueByColumnAndRow(3, 5, "На территории АНГАР № 12");
            $cWorksheet->mergeCellsByColumnAndRow(3, 6, 6, 6);
            $cWorksheet->setCellValueByColumnAndRow(3, 6, "График работы: Пн-Пт, 9:00 - 18:00");

            $cWorksheet->mergeCellsByColumnAndRow(7, 1, 10, 1);
            $cWorksheet->setCellValueByColumnAndRow(7, 1, "ОТДЕЛ ПРОДАЖ");
            $cWorksheet->mergeCellsByColumnAndRow(7, 2, 10, 2);
            $cWorksheet->setCellValueByColumnAndRow(7, 2, "тел. 7 (812) 242-80-99");
            $cWorksheet->mergeCellsByColumnAndRow(7, 3, 10, 3);
            $cWorksheet->setCellValueByColumnAndRow(7, 3, "тел. 7 (800) 555-04-99");

            $cWorksheet->mergeCellsByColumnAndRow(7, 6, 10, 6);
            $cWorksheet->setCellValueByColumnAndRow(7, 6, "МЕНЕДЖЕР ПО РАБОТЕ С КЛИЕНТАМИ:");

            $cWorksheet->mergeCellsByColumnAndRow(7, 7, 8, 7);
            $cWorksheet->setCellValueByColumnAndRow(7, 7, "доб. " . $Managers[0]->phone);
            $cWorksheet->mergeCellsByColumnAndRow(9, 7, 10, 7);
            $cWorksheet->setCellValueByColumnAndRow(9, 7, $Managers[0]->title);

            $cWorksheet->mergeCellsByColumnAndRow(7, 8, 8, 8);
            $cWorksheet->setCellValueByColumnAndRow(7, 8, "e-mail");
            $cWorksheet->mergeCellsByColumnAndRow(9, 8, 10, 8);
            $cWorksheet->setCellValueByColumnAndRow(9, 8, $Managers[0]->email);

            $cWorksheet->mergeCellsByColumnAndRow(0, 14, 10, 14);
            $cWorksheet->setCellValueByColumnAndRow(0, 14, "Коммерческое предложение");
            $cWorksheet->getRowDimension(14)->setRowHeight(25);

            $cWorksheet->getStyle('H1:H6')->getFont()->setBold(true);
            $cWorksheet->getStyle('D1')->getFont()->setBold(true);
            $cWorksheet->getStyle('A14')->getFont()->setBold(true)->setSize(16);

            $r = 16;
        } else {
            $cWorksheet->mergeCellsByColumnAndRow(3, 1, 6, 1);
            $cWorksheet->setCellValueByColumnAndRow(3, 1, "ОТДЕЛ ПРОДАЖ");
            $cWorksheet->mergeCellsByColumnAndRow(3, 2, 6, 2);
            $cWorksheet->setCellValueByColumnAndRow(3, 2, "тел. 7 (812) 242-80-99");
            $cWorksheet->mergeCellsByColumnAndRow(3, 3, 6, 3);
            $cWorksheet->setCellValueByColumnAndRow(3, 3, "тел. 7 (800) 555-04-99");
            $cWorksheet->mergeCellsByColumnAndRow(3, 4, 6, 4);
            $cWorksheet->mergeCellsByColumnAndRow(3, 5, 6, 5);

            $cWorksheet->mergeCellsByColumnAndRow(3, 6, 6, 6);
            $cWorksheet->setCellValueByColumnAndRow(3, 6, "АДРЕС СКЛАДА:");
            $cWorksheet->mergeCellsByColumnAndRow(3, 7, 6, 7);
            $cWorksheet->setCellValueByColumnAndRow(3, 7, "г. Санкт-Петербург, п. Парголово");
            $cWorksheet->mergeCellsByColumnAndRow(3, 8, 6, 8);
            $cWorksheet->setCellValueByColumnAndRow(3, 8, "ул. Подгорная, дом 6.");
            $cWorksheet->mergeCellsByColumnAndRow(3, 9, 6, 9);
            $cWorksheet->setCellValueByColumnAndRow(3, 9, "Въезд с ул. Железнодорожная, дом 11");
            $cWorksheet->mergeCellsByColumnAndRow(3, 10, 6, 10);
            $cWorksheet->setCellValueByColumnAndRow(3, 10, "На территории АНГАР № 12");
            $cWorksheet->mergeCellsByColumnAndRow(3, 11, 6, 11);
            $cWorksheet->setCellValueByColumnAndRow(3, 11, "График работы: Пн-Пт, 9:00 - 18:00");
            $cWorksheet->mergeCellsByColumnAndRow(3, 12, 6, 12);
            $cWorksheet->mergeCellsByColumnAndRow(3, 13, 6, 13);

            $cWorksheet->getStyle('D1:D6')->getFont()->setBold(true);
            $cWorksheet->getStyle('H1')->getFont()->setBold(true);

            $cWorksheet->mergeCellsByColumnAndRow(7, 1, 10, 1);

            $cWorksheet->setCellValueByColumnAndRow(7, 1, "МЕНЕДЖЕРЫ ПО РАБОТЕ С КЛИЕНТАМИ:");

            foreach ($Managers as $iNum => $Manager) {
                $r = ($iNum + 1) * 2;
                $cWorksheet->mergeCellsByColumnAndRow(7, $r, 8, $r);
                $cWorksheet->setCellValueByColumnAndRow(7, $r, 'доб. '.$Manager->phone);
                $cWorksheet->mergeCellsByColumnAndRow(9, $r, 10, $r);
                $cWorksheet->setCellValueByColumnAndRow(9, $r, $Manager->title);
                $cWorksheet->mergeCellsByColumnAndRow(7, $r + 1, 8, $r + 1);
                $cWorksheet->setCellValueByColumnAndRow(7, $r + 1, 'e-mail');
                $cWorksheet->mergeCellsByColumnAndRow(9, $r + 1, 10, $r + 1);
                $cWorksheet->setCellValueByColumnAndRow(9, $r + 1, $Manager->email);
                $cWorksheet->getStyle('H'.$r.':K'.$r)->getBorders()->applyFromArray([
                    'allborders' => [
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                    ],
                ]);
                $cWorksheet->getStyle('H'.$r.':K'.($r + 1))->getBorders()->applyFromArray([
                    'left' => [
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    ],
                    'right' => [
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    ],
                    'top' => [
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    ],
                    'bottom' => [
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    ],
                ]);
            }

            for ($r = 1; $r <= count($Managers) * 2 + 1; $r++) {
                $cWorksheet->getRowDimension($r)->setRowHeight(17);
            }
        }


        /** @var \App\Models\Item $Item */
        foreach ($Items as $Item) {
            $bWithImages = false;
            $cWorksheet->getRowDimension($r)->setRowHeight(130);
            if ($Item->isImageExists(ItemModel::IMAGE_TYPE_1)) {
                $cDrawing = new PHPExcel_Worksheet_Drawing();
                $cDrawing->setPath(public_path($Item->getImageLink(ItemModel::IMAGE_TYPE_1)));
                $cDrawing->setWidthAndHeight(160, 160);
                $cDrawing->setOffsetX(5);
                $cDrawing->setOffsetY(5);
                $cDrawing->setWorksheet($cWorksheet);
                $cDrawing->setCoordinates('A'.$r);
                $bWithImages = true;
            }
            if ($Item->isImageExists(ItemModel::IMAGE_TYPE_2)) {
                $cDrawing = new PHPExcel_Worksheet_Drawing();
                $cDrawing->setPath(public_path($Item->getImageLink(ItemModel::IMAGE_TYPE_2)));
                $cDrawing->setWidthAndHeight(160, 160);
                $cDrawing->setOffsetX(0);
                $cDrawing->setOffsetY(5);
                $cDrawing->setWorksheet($cWorksheet);
                $cDrawing->setCoordinates('C'.$r);
                $bWithImages = true;
            }
            if ($Item->isImageExists(ItemModel::IMAGE_TYPE_3)) {
                $cDrawing = new PHPExcel_Worksheet_Drawing();
                $cDrawing->setPath(public_path($Item->getImageLink(ItemModel::IMAGE_TYPE_3)));
                $cDrawing->setWidthAndHeight(160, 160);
                $cDrawing->setOffsetX(60);
                $cDrawing->setOffsetY(5);
                $cDrawing->setWorksheet($cWorksheet);
                $cDrawing->setCoordinates('D'.$r);
                $bWithImages = true;
            }
            if ($Item->isImageExists(ItemModel::IMAGE_TYPE_4)) {
                $cDrawing = new PHPExcel_Worksheet_Drawing();
                $cDrawing->setPath(public_path($Item->getImageLink(ItemModel::IMAGE_TYPE_4)));
                $cDrawing->setWidthAndHeight(160, 160);
                $cDrawing->setOffsetX(35);
                $cDrawing->setOffsetY(5);
                $cDrawing->setWorksheet($cWorksheet);
                $cDrawing->setCoordinates('G'.$r);
                $bWithImages = true;
            }
            if ($Item->isImageExists(ItemModel::IMAGE_TYPE_9)) {
                $cDrawing = new PHPExcel_Worksheet_Drawing();
                $cDrawing->setPath(public_path($Item->getImageLink(ItemModel::IMAGE_TYPE_9)));
                $cDrawing->setWidthAndHeight(160, 160);
                $cDrawing->setOffsetX(10);
                $cDrawing->setOffsetY(5);
                $cDrawing->setWorksheet($cWorksheet);
                $cDrawing->setCoordinates('J'.$r);
                $bWithImages = true;
            }
            $r++;

            $cWorksheet->mergeCellsByColumnAndRow(0, $r, 10, $r);
            $cWorksheet->getRowDimension($r)->setRowHeight(30);
            $cWorksheet->setCellValueByColumnAndRow(0, $r, html_entity_decode($Item->short_descr, ENT_COMPAT, 'UTF-8'));
            $r++;

            $iFirstRow = $r;

            $cWorksheet->mergeCellsByColumnAndRow(3, $r, 8, $r);
            $cWorksheet->mergeCellsByColumnAndRow(0, $r, 0, $r + 1);
            $cWorksheet->mergeCellsByColumnAndRow(1, $r, 1, $r + 1);
            $cWorksheet->mergeCellsByColumnAndRow(2, $r, 2, $r + 1);
            $cWorksheet->mergeCellsByColumnAndRow(9, $r, 9, $r + 1);
            $cWorksheet->mergeCellsByColumnAndRow(10, $r, 10, $r + 1);

            $aHeaders = [
                0 => ['title' => '№', 'width' => 4],
                1 => ['title' => 'Артикул', 'width' => 20],
                2 => ['title' => 'Цвет', 'width' => 15],
                3 => ['title' => 'Цена, руб. за 1 шт.'],
                9 => ['title' => 'Упаковка', 'width' => 12],
                10 => ['title' => 'Наличие на складе '.Formatter::date(), 'width' => 13],
            ];
            $cWorksheet->getRowDimension($r)->setRowHeight(20);
            foreach ($aHeaders as $iCell => $aItem) {
                $cWorksheet->setCellValueByColumnAndRow($iCell, $r, $aItem['title']);
                if (isset($aItem['width'])) {
                    $cWorksheet->getColumnDimensionByColumn($iCell)->setWidth($aItem['width']);
                }
            }

            $aHeaders = [
                3 => ['title' => 'более 10000', 'width' => 9],
                4 => ['title' => 'от 5000 до 10000', 'width' => 9],
                5 => ['title' => 'от 1000 до 5000', 'width' => 9],
                6 => ['title' => 'от 500 до 1000', 'width' => 9],
                7 => ['title' => 'от 100 до 500', 'width' => 9],
                8 => ['title' => 'розница до 100', 'width' => 9],
            ];

            $r++;
            $cWorksheet->getRowDimension($r)->setRowHeight(30);
            foreach ($aHeaders as $iCell => $aItem) {
                $cWorksheet->setCellValueByColumnAndRow($iCell, $r, $aItem['title']);
                if ($aItem['width']) {
                    $cWorksheet->getColumnDimensionByColumn($iCell)->setWidth($aItem['width']);
                }
            }

            $r++;
            $k = 1;
            /** @var \App\Models\ItemColor $Color */
            foreach ($ItemColors[$Item->id] as $Color) {
                $cWorksheet->getRowDimension($r)->setRowHeight(-1);
                $cWorksheet->setCellValueByColumnAndRow(0, $r, $k++);
                $cWorksheet->setCellValueByColumnAndRow(1, $r, $Color->code);
                $cWorksheet->setCellValueByColumnAndRow(2, $r, str_replace('/', "/\r\n", _t(Color::cachedOne($Color->color)->name, 'colors')));
                if ($Item->price5) {
                    $objRichText = new PHPExcel_RichText();

                    if ($Item->special && $Color->main && $Item->getSpecialDiscount(5)) {
                        $objRichText->createTextRun(Formatter::price($Color->getPrice($Item->price5), true) . "\n")
                            ->getFont()->setStrikethrough(true);

                        $objRichText->createTextRun('-' . $Item->getSpecialDiscount(5) . '%' . "\n")
                            ->getFont()->setColor(new PHPExcel_Style_Color(($Item->getSpecialDiscount(5) === 50) ? PHPExcel_Style_Color::COLOR_RED : 'FF7F7F7F'));

                        $objRichText->createTextRun(Formatter::price($Item->getDiscountPrice(5), true));
                    } else {
                        $objRichText->createTextRun(Formatter::price($Color->getPrice($Item->price5), true));
                    }

                    $cWorksheet->setCellValueByColumnAndRow(3, $r, $objRichText);
                }
                if ($Item->price4) {
                    $objRichText = new PHPExcel_RichText();

                    if ($Item->special && $Color->main && $Item->getSpecialDiscount(4)) {
                        $objRichText->createTextRun(Formatter::price($Color->getPrice($Item->price4), true) . "\n")
                            ->getFont()->setStrikethrough(true);

                        $objRichText->createTextRun('-' . $Item->getSpecialDiscount(4) . '%' . "\n")
                            ->getFont()->setColor(new PHPExcel_Style_Color(($Item->getSpecialDiscount(4) === 50) ? PHPExcel_Style_Color::COLOR_RED : 'FF7F7F7F'));

                        $objRichText->createTextRun(Formatter::price($Item->getDiscountPrice(4), true));
                    } else {
                        $objRichText->createTextRun(Formatter::price($Color->getPrice($Item->price4), true));
                    }

                    $cWorksheet->setCellValueByColumnAndRow(4, $r, $objRichText);
                }
                if ($Item->price3) {
                    $objRichText = new PHPExcel_RichText();

                    if ($Item->special && $Color->main && $Item->getSpecialDiscount(3)) {
                        $objRichText->createTextRun(Formatter::price($Color->getPrice($Item->price3), true) . "\n")
                            ->getFont()->setStrikethrough(true);

                        $objRichText->createTextRun('-' . $Item->getSpecialDiscount(3) . '%' . "\n")
                            ->getFont()->setColor(new PHPExcel_Style_Color(($Item->getSpecialDiscount(3) === 50) ? PHPExcel_Style_Color::COLOR_RED : 'FF7F7F7F'));

                        $objRichText->createTextRun(Formatter::price($Item->getDiscountPrice(3), true));
                    } else {
                        $objRichText->createTextRun(Formatter::price($Color->getPrice($Item->price3), true));
                    }

                    $cWorksheet->setCellValueByColumnAndRow(5, $r, $objRichText);
                }
                if ($Item->price2) {
                    $objRichText = new PHPExcel_RichText();

                    if ($Item->special && $Color->main && $Item->getSpecialDiscount(2)) {
                        $objRichText->createTextRun(Formatter::price($Color->getPrice($Item->price2), true) . "\n")
                            ->getFont()->setStrikethrough(true);

                        $objRichText->createTextRun('-' . $Item->getSpecialDiscount(2) . '%' . "\n")
                            ->getFont()->setColor(new PHPExcel_Style_Color(($Item->getSpecialDiscount(2) === 50) ? PHPExcel_Style_Color::COLOR_RED : 'FF7F7F7F'));

                        $objRichText->createTextRun(Formatter::price($Item->getDiscountPrice(2), true));
                    } else {
                        $objRichText->createTextRun(Formatter::price($Color->getPrice($Item->price2), true));
                    }

                    $cWorksheet->setCellValueByColumnAndRow(6, $r, $objRichText);
                }
                if ($Item->price1) {
                    $objRichText = new PHPExcel_RichText();

                    if ($Item->special && $Color->main && $Item->getSpecialDiscount(1)) {
                        $objRichText->createTextRun(Formatter::price($Color->getPrice($Item->price1), true) . "\n")
                            ->getFont()->setStrikethrough(true);

                        $objRichText->createTextRun('-' . $Item->getSpecialDiscount(1) . '%' . "\n")
                            ->getFont()->setColor(new PHPExcel_Style_Color(($Item->getSpecialDiscount(1) === 50) ? PHPExcel_Style_Color::COLOR_RED : 'FF7F7F7F'));

                        $objRichText->createTextRun(Formatter::price($Item->getDiscountPrice(1), true));
                    } else {
                        $objRichText->createTextRun(Formatter::price($Color->getPrice($Item->price1), true));
                    }

                    $cWorksheet->setCellValueByColumnAndRow(7, $r, $objRichText);
                }
                if ($Item->price0) {
                    $objRichText = new PHPExcel_RichText();

                    if ($Item->special && $Color->main && $Item->getSpecialDiscount(0)) {
                        $objRichText->createTextRun(Formatter::price($Color->getPrice($Item->price0), true) . "\n")
                            ->getFont()->setStrikethrough(true);

                        $objRichText->createTextRun('-' . $Item->getSpecialDiscount(0) . '%' . "\n")
                            ->getFont()->setColor(new PHPExcel_Style_Color(($Item->getSpecialDiscount(0) === 50) ? PHPExcel_Style_Color::COLOR_RED : 'FF7F7F7F'));

                        $objRichText->createTextRun(Formatter::price($Item->getDiscountPrice(0), true));
                    } else {
                        $objRichText->createTextRun(Formatter::price($Color->getPrice($Item->price0), true));
                    }

                    $cWorksheet->setCellValueByColumnAndRow(8, $r, $objRichText);
                }
                $cWorksheet->setCellValueByColumnAndRow(9, $r, $Item->pack_size ? $Item->pack_size . ' ' . _t('шт.', 'common') : '');
                $cWorksheet->setCellValueByColumnAndRow(10, $r, $Color->remains ? number_format($Color->remains, 0, ',', ' ') : '―');
                $r++;
            }

            $cWorksheet->getStyle('A'.$iFirstRow.':K'.($iFirstRow + 1))->getFont()->setBold(true);
            $cWorksheet->getStyle('A'.$iFirstRow.':K'.($iFirstRow + 1))->getFill()->applyFromArray([
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => ['rgb' => 'CECECE'],
            ]);
            $cWorksheet->getStyle('A'.$iFirstRow.':K'.($r - 1))->getBorders()->applyFromArray([
                'allborders' => [
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ],
            ]);

            $cWorksheet->getStyle('A'.($iFirstRow - ($bWithImages?2:1)).':K'.($r - 1))->getBorders()->applyFromArray([
                'left' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                ],
                'right' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                ],
                'top' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                ],
                'bottom' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                ],
            ]);

            $r++;
            $r++;

            //$cWorksheet->setBreak('A'.($r - 1) , PHPExcel_Worksheet::BREAK_ROW);
        }

        $cWriter = PHPExcel_IOFactory::createWriter($cExcel, 'Excel2007');
        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.‌sheet');//vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename=Zaglushka.Ru_Sravnenie.xlsx');
        $cWriter->save('php://output');

        die();
    }
}
