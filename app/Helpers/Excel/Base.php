<?php
declare(strict_types=1);

namespace App\Helpers\Excel;

use App\Helpers\Formatter;
use App\Models\Category;
use App\Models\Item;
use App\Models\Manager;
use Auth;
use PHPExcel;
use PHPExcel_Cell;
use PHPExcel_IOFactory;
use PHPExcel_RichText;
use PHPExcel_Style_Alignment;
use PHPExcel_Style_Border;
use PHPExcel_Style_Color;
use PHPExcel_Style_Fill;
use PHPExcel_Worksheet;
use PHPExcel_Worksheet_Drawing;
use PHPExcel_Worksheet_PageSetup;

class Base
{
    /**
     * @param Item[]  $Items
     * @param bool    $colors
     * @param array[] $SimilarItems
     * @param bool    $preview
     */
    public static function generate($Items, $colors = false, $SimilarItems = [], $preview = false): void
    {
        if (Auth::check() && Auth::user()->isManager()) {
            $Managers = Manager::where(['email' => Auth::user()->email])->get();
        }

        if (!isset($Managers) || count($Managers) === 0) {
            $Managers = Manager::where(['enabled' => 1])->get();
        }

        $cExcel = new PHPExcel();
        $cExcel->setActiveSheetIndex(0);
        $cWorksheet = $cExcel->getActiveSheet();
        $cExcel->getDefaultStyle()->getAlignment()->setWrapText(true);
        $cExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $cExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $cWorksheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $cWorksheet->setShowGridlines(true);

        $height = 50 + (count($Managers) * 30);

        $cWorksheet->getRowDimension(1)->setRowHeight($height >= 172 ? $height : 172);

        $cDrawing = new PHPExcel_Worksheet_Drawing();
        $cDrawing->setPath(public_path() . '/images/excel/logo.png');
        $cDrawing->setWidthAndHeight(200, 200);
        $cDrawing->setOffsetX(10);
        $cDrawing->setOffsetY(10);
        $cDrawing->setWorksheet($cWorksheet);
        $cDrawing->setCoordinates('A1');

        $cWorksheet->mergeCellsByColumnAndRow(3, 1, 6, 1);
        $cWorksheet->setCellValueByColumnAndRow(
            3,
            1,
            "г. Санкт-Петербург, п. Парголово,\r\n" .
            "ул. Подгорная, д. 6. (въезд с Железнодорожной, 11) АНГАР №12\r\n" .
            "график работы: Пн-Пт, 9.00-18.00\r\n\r\n" .
            "8 (812) 242 80 99 (доб. 160)\r\n" .
            "8 (812) 640 91 99 (факс)\r\n" .
            "info@zaglushka.ru (секретарь)\r\n\r\n" .
            (
                count($Managers) === 1 ?
                $Managers[0]->title . "   (доб. " . $Managers[0]->phone . ")\r\n" .
                $Managers[0]->email . "\r\n"
            :
                    ("руководитель отдела продаж\r\n" .
                    "Соколов Юрий Сергеевич (доб. 111)\r\n" .
                    "y.sokolov@zaglushka.ru")
            )
        );
        $cWorksheet->getStyle('D1:G1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

        $cWorksheet->mergeCellsByColumnAndRow(7, 1, 11, 1);
        $sLabel = "8 (812) 242 80 99 (многоканальный)\r\n" .
            "8 (800) 555 04 99 (бесплатный звонок по России)\r\n\r\n";
/*
        if (count($Managers) !== 1) {
            foreach ($Managers as $Manager) {
                $sLabel .= $Manager->title . "   (доб. " . $Manager->phone . ")\r\n" . $Manager->email . "\r\n";
            }
        }
*/
        $cWorksheet->setCellValueByColumnAndRow(7, 1, $sLabel);
        $cWorksheet->getStyle('H1:L1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

        $r = 2;
        if ($Items) {
            self::excelList($cWorksheet, $r, $Items, $colors);

            if (count($SimilarItems) > 0) {
                $cWorksheet->getRowDimension($r)->setRowHeight(25);
                $cWorksheet->mergeCellsByColumnAndRow(0, $r, 11, $r);
                $cWorksheet->getStyle('A' . $r . ':L' . $r)->getFont()->setBold(true);
                $cWorksheet->getStyle('A' . $r . ':L' . $r)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $cWorksheet->setCellValueByColumnAndRow(0, $r, _t('Под выбранный размер также подходят:', 'catalog'));
                $r++;

                foreach ($SimilarItems as $parentCategoryId => $GroupItems) {
                    $catItems = new \Illuminate\Database\Eloquent\Collection();
                    foreach ($GroupItems as $categoryId => $CategoryItems) {
                        $catItems = $catItems->merge($CategoryItems);
                    }

                    $cWorksheet->getRowDimension($r)->setRowHeight(25);
                    $cWorksheet->mergeCellsByColumnAndRow(0, $r, 11, $r);
                    $cWorksheet->getStyle('A' . $r . ':L' . $r)->getFont()->setBold(true);
                    $cWorksheet->getStyle('A' . $r . ':L' . $r)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                    $cWorksheet->setCellValueByColumnAndRow(0, $r, Category::cachedOne($parentCategoryId)->getTitle());
                    $r++;

                    self::excelList($cWorksheet, $r, $catItems, $colors);
                }
            }


            $cWorksheet->getStyle('A2:L' . ($r - 1))->getBorders()->applyFromArray([
                'allborders' => [
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ],
            ]);
        }

        $cWriter = PHPExcel_IOFactory::createWriter($cExcel, 'Excel2007');
        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.‌sheet');//vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename=Zaglushka.Ru.xlsx');
        $cWriter->save('php://output');

        die();
    }

    /**
     * @param \PHPExcel_Worksheet $cWorksheet
     * @param int                 $r
     * @param Item[]              $Items
     * @param bool                $colors
     */
    private static function excelList(PHPExcel_Worksheet &$cWorksheet, &$r, $Items, $colors = false): void
    {
        $cWorksheet->mergeCellsByColumnAndRow(4, $r, 9, $r);
        $cWorksheet->mergeCellsByColumnAndRow(0, $r, 0, $r + 1);
        $cWorksheet->mergeCellsByColumnAndRow(1, $r, 1, $r + 1);
        $cWorksheet->mergeCellsByColumnAndRow(2, $r, 2, $r + 1);
        $cWorksheet->mergeCellsByColumnAndRow(3, $r, 3, $r + 1);
        $cWorksheet->mergeCellsByColumnAndRow(10, $r, 10, $r + 1);
        $cWorksheet->mergeCellsByColumnAndRow(11, $r, 11, $r + 1);

        $cWorksheet->getStyle('A' . $r . ':L' . ($r + 1))->getFont()->setBold(true);
        $cWorksheet->getStyle('A' . $r . ':L' . ($r + 1))->getFill()->applyFromArray([
            'type'       => PHPExcel_Style_Fill::FILL_SOLID,
            'startcolor' => ['rgb' => 'CECECE'],
        ]);

        $aHeaders = [
            0  => ['title' => '№', 'width' => 4],
            1  => ['title' => 'Артикул', 'width' => 20],
            4  => ['title' => 'Цена, руб. за 1 шт.'],
            10 => ['title' => 'Упаковка', 'width' => 10],
            11 => ['title' => 'Наличие на складе ' . Formatter::date(), 'width' => 11],
        ];

        $aHeaders[2] = ['title' => 'Схема', 'width' => 13];
        $aHeaders[3] = ['title' => 'Фото', 'width' => 13];

        $cWorksheet->getRowDimension($r)->setRowHeight(20);
        foreach ($aHeaders as $iCell => $aItem) {
            $cWorksheet->setCellValueByColumnAndRow($iCell, $r, $aItem['title']);
            if (isset($aItem['width'])) {
                $cWorksheet->getColumnDimensionByColumn($iCell)->setWidth($aItem['width']);
            }
        }
        $r++;

        $aHeaders = [
            4 => ['title' => 'более 10000', 'width' => 9],
            5 => ['title' => 'от 5000 до 10000', 'width' => 9],
            6 => ['title' => 'от 1000 до 5000', 'width' => 9],
            7 => ['title' => 'от 500 до 1000', 'width' => 8],
            8 => ['title' => 'от 100 до 500', 'width' => 8],
            9 => ['title' => 'розница до 100', 'width' => 9],
        ];

        $cWorksheet->getRowDimension($r)->setRowHeight(30);
        foreach ($aHeaders as $iCell => $aItem) {
            $cWorksheet->setCellValueByColumnAndRow($iCell, $r, $aItem['title']);
            if (isset($aItem['width'])) {
                $cWorksheet->getColumnDimensionByColumn($iCell)->setWidth($aItem['width']);
            }
        }
        $r++;

        foreach ($Items as $iNum => $Item) {
            if ($colors) {
                $Color = $Item;
                $Item  = $Item->item;
            }

            $cWorksheet->getRowDimension($r)->setRowHeight(75);
            $cWorksheet->setCellValueByColumnAndRow(0, $r, $iNum + 1);
            $cWorksheet->setCellValueByColumnAndRow(1, $r, $colors ? $Color->code : $Item->title);
            if ($Item->isImageExists(Item::IMAGE_TYPE_18)) {
                $cDrawing = new PHPExcel_Worksheet_Drawing();
                $cDrawing->setPath(public_path($Item->getImageLink(Item::IMAGE_TYPE_18)));
                $cDrawing->setWidthAndHeight(80, 80);
                $cDrawing->setOffsetX(5);
                $cDrawing->setOffsetY(5);
                $cDrawing->setWorksheet($cWorksheet);
                $cDrawing->setCoordinates(PHPExcel_Cell::stringFromColumnIndex(2) . $r);
            }
            if ($Item->isImageExists(Item::IMAGE_TYPE_5)) {
                $cDrawing = new PHPExcel_Worksheet_Drawing();
                $cDrawing->setPath(public_path($Item->getImageLink(Item::IMAGE_TYPE_5)));
                $cDrawing->setWidthAndHeight(80, 80);
                $cDrawing->setOffsetX(5);
                $cDrawing->setOffsetY(5);
                $cDrawing->setWorksheet($cWorksheet);
                $cDrawing->setCoordinates(PHPExcel_Cell::stringFromColumnIndex(3) . $r);
            }
            if (!empty($Item->price5)) {
                $objRichText = new PHPExcel_RichText();

                if ($colors) {
                    $objRichText->createTextRun(Formatter::price($Color->getPrice($Item->price5), true));
                } else {
                    if ($Item->getSpecialDiscount(5)) {
                        $objRichText->createTextRun(Formatter::price($Item->price5, true) . "\n")
                            ->getFont()->setStrikethrough(true);

                        $objRichText->createTextRun('-' . $Item->getSpecialDiscount(5) . '%' . "\n")
                            ->getFont()->setColor(new PHPExcel_Style_Color(($Item->getSpecialDiscount(5) === 50) ? PHPExcel_Style_Color::COLOR_RED : 'FF7F7F7F'));

                        $objRichText->createTextRun(Formatter::price($Item->getDiscountPrice(5), true));
                    } else {
                        $objRichText->createTextRun(Formatter::price($Item->price5, true));
                    }
                }

                $cWorksheet->setCellValueByColumnAndRow(4, $r, $objRichText);
            }
            if (!empty($Item->price4)) {
                $objRichText = new PHPExcel_RichText();

                if ($colors) {
                    $objRichText->createTextRun(Formatter::price($Color->getPrice($Item->price4), true));
                } else {
                    if ($Item->getSpecialDiscount(4)) {
                        $objRichText->createTextRun(Formatter::price($Item->price4, true) . "\n")
                            ->getFont()->setStrikethrough(true);

                        $objRichText->createTextRun('-' . $Item->getSpecialDiscount(4) . '%' . "\n")
                            ->getFont()->setColor(new PHPExcel_Style_Color(($Item->getSpecialDiscount(4) === 50) ? PHPExcel_Style_Color::COLOR_RED : 'FF7F7F7F'));

                        $objRichText->createTextRun(Formatter::price($Item->getDiscountPrice(4), true));
                    } else {
                        $objRichText->createTextRun(Formatter::price($Item->price4, true));
                    }
                }

                $cWorksheet->setCellValueByColumnAndRow(5, $r, $objRichText);
            }
            if (!empty($Item->price3)) {
                $objRichText = new PHPExcel_RichText();

                if ($colors) {
                    $objRichText->createTextRun(Formatter::price($Color->getPrice($Item->price3), true));
                } else {
                    if ($Item->getSpecialDiscount(3)) {
                        $objRichText->createTextRun(Formatter::price($Item->price3, true) . "\n")
                            ->getFont()->setStrikethrough(true);

                        $objRichText->createTextRun('-' . $Item->getSpecialDiscount(3) . '%' . "\n")
                            ->getFont()->setColor(new PHPExcel_Style_Color(($Item->getSpecialDiscount(3) === 50) ? PHPExcel_Style_Color::COLOR_RED : 'FF7F7F7F'));

                        $objRichText->createTextRun(Formatter::price($Item->getDiscountPrice(3), true));
                    } else {
                        $objRichText->createTextRun(Formatter::price($Item->price3, true));
                    }
                }

                $cWorksheet->setCellValueByColumnAndRow(6, $r, $objRichText);
            }
            if (!empty($Item->price2)) {
                $objRichText = new PHPExcel_RichText();

                if ($colors) {
                    $objRichText->createTextRun(Formatter::price($Color->getPrice($Item->price2), true));
                } else {
                    if ($Item->getSpecialDiscount(2)) {
                        $objRichText->createTextRun(Formatter::price($Item->price2, true) . "\n")
                            ->getFont()->setStrikethrough(true);

                        $objRichText->createTextRun('-' . $Item->getSpecialDiscount(2) . '%' . "\n")
                            ->getFont()->setColor(new PHPExcel_Style_Color(($Item->getSpecialDiscount(2) === 50) ? PHPExcel_Style_Color::COLOR_RED : 'FF7F7F7F'));

                        $objRichText->createTextRun(Formatter::price($Item->getDiscountPrice(2), true));
                    } else {
                        $objRichText->createTextRun(Formatter::price($Item->price2, true));
                    }
                }

                $cWorksheet->setCellValueByColumnAndRow(7, $r, $objRichText);
            }
            if (!empty($Item->price1)) {
                $objRichText = new PHPExcel_RichText();

                if ($colors) {
                    $objRichText->createTextRun(Formatter::price($Color->getPrice($Item->price1), true));
                } else {
                    if ($Item->getSpecialDiscount(1)) {
                        $objRichText->createTextRun(Formatter::price($Item->price1, true) . "\n")
                            ->getFont()->setStrikethrough(true);

                        $objRichText->createTextRun('-' . $Item->getSpecialDiscount(1) . '%' . "\n")
                            ->getFont()->setColor(new PHPExcel_Style_Color(($Item->getSpecialDiscount(1) === 50) ? PHPExcel_Style_Color::COLOR_RED : 'FF7F7F7F'));

                        $objRichText->createTextRun(Formatter::price($Item->getDiscountPrice(1), true));
                    } else {
                        $objRichText->createTextRun(Formatter::price($Item->price1, true));
                    }
                }

                $cWorksheet->setCellValueByColumnAndRow(8, $r, $objRichText);
            }
            if (!empty($Item->price0)) {
                $objRichText = new PHPExcel_RichText();

                if ($colors) {
                    $objRichText->createTextRun(Formatter::price($Color->getPrice($Item->price0), true));
                } else {
                    if ($Item->getSpecialDiscount(0)) {
                        $objRichText->createTextRun(Formatter::price($Item->price0, true) . "\n")
                            ->getFont()->setStrikethrough(true);

                        $objRichText->createTextRun('-' . $Item->getSpecialDiscount(0) . '%' . "\n")
                            ->getFont()->setColor(new PHPExcel_Style_Color(($Item->getSpecialDiscount(0) === 50) ? PHPExcel_Style_Color::COLOR_RED : 'FF7F7F7F'));

                        $objRichText->createTextRun(Formatter::price($Item->getDiscountPrice(0), true));
                    } else {
                        $objRichText->createTextRun(Formatter::price($Item->price0, true));
                    }
                }

                $cWorksheet->setCellValueByColumnAndRow(9, $r, $objRichText);
            }
            $cWorksheet->setCellValueByColumnAndRow(10, $r, $Item->pack_size);

            $remains = $colors ? $Color->remains : $Item->remains();

            $cWorksheet->setCellValueByColumnAndRow(11, $r, $remains ? $remains : '―');
            $r++;
        }
    }
}
