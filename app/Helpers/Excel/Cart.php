<?php
declare(strict_types=1);

namespace App\Helpers\Excel;

use App;
use App\Helpers\Formatter;
use App\Models\City;
use App\Models\Color;
use App\Models\Manager;
use App\Models\Order;
use App\Models\OrderItem;
use Auth;
use PHPExcel;
use PHPExcel_Cell;
use PHPExcel_IOFactory;
use PHPExcel_Style_Alignment;
use PHPExcel_Style_Border;
use PHPExcel_Style_Fill;
use PHPExcel_Worksheet_Drawing;

class Cart
{
    /**
     * @param OrderItem[] $OrderItems
     * @param bool        $freeDeliveryIsImpossible
     */
    public static function generate($OrderItems, $freeDeliveryIsImpossible): void
    {
        /* TODO: Лишний запрос */
        $CurrentCity = App::isLocale('ru') && session('current_city') ? City::cachedOne(session('current_city')) : null;

        $request = request();
        $step = (int) request()->step;

        if (Auth::check() && Auth::user()->isManager()) {
            $Managers = Manager::where(['email' => Auth::user()->email])->get();
        }

        if (!isset($Managers) || count($Managers) === 0) {
            $Managers = Manager::where(['enabled' => 1])->get();
        }

        $cExcel = new PHPExcel();
        $cExcel->setActiveSheetIndex(0);
        $cWorksheet = $cExcel->getActiveSheet();

        $cExcel->getDefaultStyle()->getAlignment()->setWrapText(true);
        $cExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $cExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $sOrderNumDate = '';

        $cDrawing = new PHPExcel_Worksheet_Drawing();
        $cDrawing->setPath(public_path('/images/excel/logo.png'));
        $cDrawing->setWidthAndHeight(200, 200);
        $cDrawing->setOffsetX(10);
        $cDrawing->setOffsetY(10);
        $cDrawing->setWorksheet($cWorksheet);
        $cDrawing->setCoordinates('B1');

        $height = 50 + (count($Managers) * 30);

        $cWorksheet->getRowDimension(1)->setRowHeight($height >= 172 ? $height : 172);

        $cWorksheet->mergeCellsByColumnAndRow(3, 1, 4, 1);
        $cWorksheet->setCellValueByColumnAndRow(
            3,
            1,
            "г. Санкт-Петербург, п. Парголово,\r\n" .
            "ул. Подгорная, д. 6. (въезд с Железнодорожной, 11) АНГАР №12\r\n" .
            "график работы: Пн-Пт, 9.00-18.00\r\n\r\n" .
            "8 (812) 242 80 99 (доб. 160)\r\n" .
            "8 (812) 640 91 99 (факс)\r\n" .
            "info@zaglushka.ru (секретарь)\r\n\r\n" .
            (
                count($Managers) === 1 ?
                    $Managers[0]->title . "   (доб. " . $Managers[0]->phone . ")\r\n" .
                    $Managers[0]->email . "\r\n"
                :
                    ("руководитель отдела продаж\r\n" .
                    "Соколов Юрий Сергеевич (доб. 111)\r\n" .
                    "y.sokolov@zaglushka.ru")
            )
        );
        $cWorksheet->getStyle('D1:E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

        $cWorksheet->mergeCellsByColumnAndRow(5, 1, 8, 1);
        $sLabel = "8 (812) 242 80 99 (многоканальный)\r\n" .
            "8 (800) 555 04 99 (бесплатный звонок по России)\r\n\r\n";
/*
        if (count($Managers) !== 1) {
            foreach ($Managers as $Manager) {
                $sLabel .= $Manager->title . "   (доб. " . $Manager->phone . ")\r\n" . $Manager->email . "\r\n";
            }
        }
*/
        $cWorksheet->setCellValueByColumnAndRow(5, 1, $sLabel);
        $cWorksheet->getStyle('F1:I1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

        $r = 2;
        $cWorksheet->mergeCellsByColumnAndRow(0, $r, 8, $r);
        $cWorksheet->setCellValueByColumnAndRow(0, $r, 'Заказ клиента' . $sOrderNumDate);
        $cWorksheet->getStyle('A' . $r . ':I' . $r)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $cWorksheet->getStyle('A' . $r . ':I' . $r)->getFont()->setBold(true)->setSize(16);
        $cWorksheet->getRowDimension($r)->setRowHeight(25);
        $r++;
        $iStart = $r;

        $aHeaders = [
            0 => ['title' => '№', 'width' => 4],
            1 => ['title' => 'Фото', 'width' => 13],
            2 => ['title' => 'Артикул', 'width' => 22],
            3 => ['title' => 'Описание', 'width' => 25],
            4 => ['title' => 'Цвет', 'width' => 15],
            5 => ['title' => 'Кол-во штук', 'width' => 10],
            6 => ['title' => 'Оптовая группа', 'width' => 12],
            7 => ['title' => 'Цена за 1 штуку', 'width' => 9],
            8 => ['title' => 'Сумма', 'width' => 11],
        ];
        foreach ($aHeaders as $iCell => $aItem) {
            if ($aItem['width']) {
                $cWorksheet->getColumnDimensionByColumn($iCell)->setWidth($aItem['width']);
            }
        }

        $totalPrice = 0;
        if (!empty($OrderItems)) {
            foreach ($aHeaders as $iCell => $aItem) {
                $cWorksheet->setCellValueByColumnAndRow($iCell, $r, $aItem['title']);
            }
            $cWorksheet->getStyle('A' . $r . ':I' . $r)->getFont()->setBold(true);
            $cWorksheet->getStyle('A' . $r . ':I' . $r)->getFill()->applyFromArray([
                'type'       => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => ['rgb' => 'CECECE'],
            ]);
            $cWorksheet->getRowDimension($r)->setRowHeight(40);
            $r++;

            $k = 1;
            foreach ($OrderItems as $OrderItem) {
                $cWorksheet->setCellValueByColumnAndRow(0, $r, $k++);
                if ($OrderItem->image) {
                    $cDrawing = new PHPExcel_Worksheet_Drawing();
                    $cDrawing->setPath(public_path($OrderItem->image));
                    $cDrawing->setWidthAndHeight(80, 80);
                    $cDrawing->setOffsetX(5);
                    $cDrawing->setOffsetY(5);
                    $cDrawing->setWorksheet($cWorksheet);
                    $cDrawing->setCoordinates(PHPExcel_Cell::stringFromColumnIndex(1) . $r);
                }
                $cWorksheet->setCellValueByColumnAndRow(2, $r, $OrderItem->title);
                $cWorksheet->setCellValueByColumnAndRow(3, $r, html_entity_decode($OrderItem->item->short_descr, ENT_COMPAT, 'UTF-8'));
                $cWorksheet->setCellValueByColumnAndRow(4, $r, Color::cachedOne($OrderItem->color)->name);

                if ($OrderItem->promo) {
                    $cWorksheet->setCellValueByColumnAndRow(5, $r, 'Бесплатные образцы');
                    $cWorksheet->mergeCells('F' . ($r) . ':I' . ($r));
                } else {
                    $cWorksheet->setCellValueByColumnAndRow(5, $r, $OrderItem->quant);
                    $cWorksheet->setCellValueByColumnAndRow(6, $r, preg_replace(['#<br />#', '#&nbsp;#'], ["\n", ' '], $OrderItem->getWholeSaleGroupText($OrderItem->getWholeSaleGroup())));
                    $cWorksheet->setCellValueByColumnAndRow(7, $r, Formatter::price($OrderItem->price, true));
                    $cWorksheet->setCellValueByColumnAndRow(8, $r, Formatter::price(round($OrderItem->price, 1) * $OrderItem->quant, true));
                }


                $cWorksheet->getRowDimension($r)->setRowHeight(80);
                $r++;

                $totalPrice += (round($OrderItem->price, 1) * $OrderItem->quant);
            }

            $cWorksheet->getStyle('A' . $iStart . ':I' . ($r - 1))->getBorders()->applyFromArray([
                'allborders' => [
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ],
            ]);

            $cWorksheet->mergeCellsByColumnAndRow(0, $r, 8, $r);
            $cWorksheet->setCellValueByColumnAndRow(
                0,
                $r,
                'Стоимость продукции: ' . $totalPrice . ' рублей'
            );
            $cWorksheet->getStyle('A' . $r . ':I' . $r)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $cWorksheet->getStyle('A' . $r . ':I' . $r)->getFont()->setBold(true)->setSize(12);
            $cWorksheet->getRowDimension($r)->setRowHeight(20);

            if (!$freeDeliveryIsImpossible && $CurrentCity && $CurrentCity->free_delivery_price) {
                if ($totalPrice >= $CurrentCity->free_delivery_price) {
                    $r++;
                    $cWorksheet->mergeCellsByColumnAndRow(0, $r, 8, $r);
                    $cWorksheet->setCellValueByColumnAndRow(
                        0,
                        $r,
                        'Ваш заказ на сумму более ' . number_format($CurrentCity->free_delivery_price, 0, ',', ' ') . ' рублей. ' .
                        'Доставка "до дверей" в городе ' . $CurrentCity->title . ' бесплатно!'
                    );
                    $cWorksheet->getStyle('A' . $r . ':I' . $r)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                    $cWorksheet->getStyle('A' . $r . ':I' . $r)->getFont()->setSize(12);
                    $cWorksheet->getRowDimension($r)->setRowHeight(20);
                } else {
                    $r++;
                    $cWorksheet->mergeCellsByColumnAndRow(0, $r, 8, $r);
                    $cWorksheet->setCellValueByColumnAndRow(
                        0,
                        $r,
                        'До бесплатной доставки "до дверей" в городе ' . $CurrentCity->title . ' в заказе не хватает ' .
                        'товаров на сумму ' . number_format($CurrentCity->free_delivery_price - $totalPrice, 0, ',', ' ') . ' рублей.'
                    );
                    $cWorksheet->getStyle('A' . $r . ':I' . $r)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                    $cWorksheet->getStyle('A' . $r . ':I' . $r)->getFont()->setSize(12);
                    $cWorksheet->getRowDimension($r)->setRowHeight(20);
                }
            }
            $r++;
            $cWorksheet->mergeCellsByColumnAndRow(0, $r, 8, $r);
            $cWorksheet->setCellValueByColumnAndRow(
                0,
                $r,
                'Бесплатная доставка не распространяется на горки, бухты каната и цепи, сиденья и каркасы для стульев.'
            );
            $cWorksheet->getStyle('A' . $r . ':I' . $r)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $cWorksheet->getStyle('A' . $r . ':I' . $r)->getFont()->setSize(12);
            $cWorksheet->getRowDimension($r)->setRowHeight(36);
        } else {
            $cWorksheet->mergeCellsByColumnAndRow(0, $r, 8, $r);
            $cWorksheet->setCellValueByColumnAndRow(0, $r, 'Вы не добавили в заказ ни одного товара');
            $cWorksheet->getStyle('A' . $r . ':I' . $r)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        }

        if ($step > 1) {
            $r++;
            $r++;

            $cWorksheet->mergeCellsByColumnAndRow(0, $r, 8, $r);
            $cWorksheet->setCellValueByColumnAndRow(0, $r, 'Информация о доставке');
            $cWorksheet->getStyle('A' . $r . ':I' . $r)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $cWorksheet->getStyle('A' . $r . ':I' . $r)->getFont()->setBold(true)->setSize(16);
            $cWorksheet->getRowDimension($r)->setRowHeight(25);
            $r++;
            $iStart = $r;
            $cWorksheet->mergeCellsByColumnAndRow(0, $r, 2, $r);
            $cWorksheet->mergeCellsByColumnAndRow(3, $r, 4, $r);
            $cWorksheet->mergeCellsByColumnAndRow(5, $r, 8, $r);
            $cWorksheet->setCellValueByColumnAndRow(0, $r, 'Способ доставки');
            $cWorksheet->setCellValueByColumnAndRow(3, $r, 'Комментарии');
            if ($request['delivery_company'] !== 'Самовывоз') {
                $cWorksheet->setCellValueByColumnAndRow(5, $r, 'Адрес доставки');
            }
            $cWorksheet->getRowDimension($r)->setRowHeight(30);
            $cWorksheet->getStyle('A' . $r . ':I' . $r)->getFont()->setBold(true);
            $cWorksheet->getStyle('A' . $r . ':I' . $r)->getFill()->applyFromArray([
                'type'       => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => ['rgb' => 'CECECE'],
            ]);
            $r++;

            $cWorksheet->mergeCellsByColumnAndRow(0, $r, 2, $r);
            $cWorksheet->mergeCellsByColumnAndRow(3, $r, 4, $r);
            $cWorksheet->mergeCellsByColumnAndRow(5, $r, 8, $r);
            $cWorksheet->setCellValueByColumnAndRow(0, $r, $request['delivery_company'] ?? '');
            $cWorksheet->setCellValueByColumnAndRow(3, $r, $request['comments'] ?? '');
            if ($request['delivery_company'] !== 'Самовывоз') {
                $cWorksheet->setCellValueByColumnAndRow(5, $r, ($request['city'] ?? '') . ' ' . ($request['address'] ?? ''));
            }
            $cWorksheet->getRowDimension($r)->setRowHeight(30);
            $r++;


            $cWorksheet->getStyle('A' . $iStart . ':I' . ($r - 1))->getBorders()->applyFromArray([
                'allborders' => [
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ],
            ]);
            if (isset($request['company']) && $request['company'] != 'Не выбрано') {
                $cWorksheet->mergeCellsByColumnAndRow(0, $r, 8, $r);
                $cWorksheet->setCellValueByColumnAndRow(
                    0,
                    $r,
                    'Общая стоимость: ' . Formatter::price($totalPrice + (int) $request['price']) . ' рублей'
                );
                $cWorksheet->getRowDimension($r)->setRowHeight(20);
                $cWorksheet->getStyle('A' . $r . ':I' . $r)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $cWorksheet->getStyle('A' . $r . ':I' . $r)->getFont()->setBold(true)->setSize(12);
            }
        }

        if ($step > 0) {
            $r++;
            $r++;

            $cWorksheet->mergeCellsByColumnAndRow(0, $r, 8, $r);
            $cWorksheet->setCellValueByColumnAndRow(0, $r, 'Данные о клиенте');
            $cWorksheet->getStyle('A' . $r . ':I' . $r)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $cWorksheet->getStyle('A' . $r . ':I' . $r)->getFont()->setBold(true)->setSize(16);
            $cWorksheet->getRowDimension($r)->setRowHeight(25);
            $r++;
            $cWorksheet->mergeCellsByColumnAndRow(0, $r, 8, $r);
            $cWorksheet->getStyle('A' . $r . ':I' . $r)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $cWorksheet->getStyle('A' . $r . ':I' . $r)->getFont()->setBold(true);
            $cWorksheet->getRowDimension($r)->setRowHeight(20);
            if ($request['type'] == Order::TYPE_COMPANY) {
                $cWorksheet->getRowDimension($r)->setRowHeight(20);
                $cWorksheet->setCellValueByColumnAndRow(0, $r, 'Организация (юр.лицо)');
                $r++;
                $iStart = $r;
                if (isset($request['company_person_name'])) {
                    $cWorksheet->setCellValueByColumnAndRow(0, $r, 'Фамилия Имя Отчество');
                    $cWorksheet->setCellValueByColumnAndRow(3, $r, $request['company_person_name']);
                    $r++;
                }
                if (isset($request['company_phone'])) {
                    $cWorksheet->setCellValueByColumnAndRow(0, $r, 'Телефон / Факс');
                    $cWorksheet->setCellValueByColumnAndRow(3, $r, $request['company_phone']);
                    $r++;
                }
                if (isset($request['company_email'])) {
                    $cWorksheet->setCellValueByColumnAndRow(0, $r, 'E-mail для связи');
                    $cWorksheet->setCellValueByColumnAndRow(3, $r, $request['company_email']);
                    $r++;
                }
                if (isset($request['company_name'])) {
                    $cWorksheet->setCellValueByColumnAndRow(0, $r, 'Название компании');
                    $cWorksheet->setCellValueByColumnAndRow(3, $r, $request['company_name']);
                    $r++;
                }
                if (isset($request['company_address'])) {
                    $cWorksheet->setCellValueByColumnAndRow(0, $r, 'Юридический адрес');
                    $cWorksheet->setCellValueByColumnAndRow(3, $r, $request['company_address']);
                    $r++;
                }
                if (isset($request['company_inn'])) {
                    $cWorksheet->setCellValueByColumnAndRow(0, $r, 'ИНН/КПП');
                    $cWorksheet->setCellValueByColumnAndRow(3, $r, $request['company_inn'] . '/' . $request['company_kpp']);
                    $r++;
                }
                if (isset($request['company_bik'])) {
                    $cWorksheet->setCellValueByColumnAndRow(0, $r, 'БИК');
                    $cWorksheet->setCellValueByColumnAndRow(3, $r, $request['company_bik']);
                    $r++;
                }
                if (isset($request['company_bik'])) {
                    $cWorksheet->setCellValueByColumnAndRow(0, $r, 'БИК');
                    $cWorksheet->setCellValueByColumnAndRow(3, $r, $request['company_bik']);
                    $r++;
                }
                if (isset($request['company_rs'])) {
                    $cWorksheet->setCellValueByColumnAndRow(0, $r, 'Расчетный счет');
                    $cWorksheet->setCellValueByColumnAndRow(3, $r, $request['company_rs']);
                    $r++;
                }
                if (isset($request['company_file']) && $request['company_file']) {
                    $cWorksheet->setCellValueByColumnAndRow(0, $r, 'Файл');
                    $cWorksheet->setCellValueByColumnAndRow(3, $r, $request['company_file']);
                    $r++;
                }
            } else {
                $cWorksheet->setCellValueByColumnAndRow(0, $r, 'Физ. лицо');
                $r++;
                $iStart = $r;
                $cWorksheet->setCellValueByColumnAndRow(0, $r, 'Фамилия Имя Отчество');
                $cWorksheet->setCellValueByColumnAndRow(3, $r, $request['person_name']);
                $r++;
                $cWorksheet->setCellValueByColumnAndRow(0, $r, 'Телефон для связи');
                $cWorksheet->setCellValueByColumnAndRow(3, $r, $request['person_phone']);
                $r++;
                $cWorksheet->setCellValueByColumnAndRow(0, $r, 'E-mail для связи');
                $cWorksheet->setCellValueByColumnAndRow(3, $r, $request['person_email']);
            }
            for ($c = $iStart; $c <= $r; $c++) {
                $cWorksheet->mergeCellsByColumnAndRow(0, $c, 2, $c);
                $cWorksheet->mergeCellsByColumnAndRow(3, $c, 8, $c);
                $cWorksheet->getRowDimension($c)->setRowHeight(20);
            }
            $cWorksheet->getStyle('A' . $iStart . ':I' . $r)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $cWorksheet->getStyle('A' . $iStart . ':I' . $r)->getBorders()->applyFromArray([
            'allborders' => [
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            ],
        ]);
        }

        $cWriter = PHPExcel_IOFactory::createWriter($cExcel, 'Excel2007');
        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.‌sheet');//vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename=Zaglushka.Ru_korzina.xlsx');
        $cWriter->save('php://output');

        die();
    }
}
