<?php
declare(strict_types=1);

namespace App\Helpers;

use File;

class CityFileParser
{
    /**
     * @param string $cityTitle
     *
     * @return array
     */
    public static function cityFileParser(string $cityTitle): array
    {
        $tks = json_decode(File::get(storage_path('app/tk.json')));

        $result = [];
        foreach ($tks as $key => $value) {
            if (trim(mb_strtolower($key)) === trim(mb_strtolower($cityTitle))) {
                foreach ($value->terminals as $tk) {
                    if ($tk->type === 'pecom') {
                        continue;
                    }
                    $result[$tk->type][] = [
                        'name'   => $tk->address,
                        'coords' => [$tk->latitude, $tk->longitude],
                    ];
                }
            }
        }

        return $result;
    }
}
