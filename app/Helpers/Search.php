<?php
declare(strict_types=1);

namespace App\Helpers;

use App\Models\Category;
use App\Models\Item as ModelItem;
use App\Models\Item;
use App\Models\ItemColor;
use App\Models\ItemSize;
use DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Request;

class Search
{
    /**
     * @param int $a
     * @param int $b
     *
     * @return int
     */
    protected static function sortfnc(int $a, int $b) : int
    {
        $CategoryA = Category::cachedOne($a);
        $CategoryB = Category::cachedOne($b);

        if ($CategoryA->ord == $CategoryB->ord) {
            return 0;
        }

        return ($CategoryA->ord > $CategoryB->ord) ? -1 : 1;
    }

    /**
     * @return Collection
     */
    public static function findItems() : Collection
    {
        $Items = ModelItem::orderByRaw('-custom_search_position DESC')
            ->orderBy('custom_search_position', 'ASC');

        if (Request::get('type')) {
            $Items = self::filterItemsBySize($Items);
        }

        if (Request::get('special')) {
            $Items = self::filterSpecial($Items);
        }

        if (Request::get('exist') && Request::get('transit')) {
            $Items = self::filterExistsOrTransit($Items);
        } else {
            if (Request::get('exist')) {
                $Items = self::filterExists($Items);
            }

            if (Request::get('transit')) {
                $Items = self::filterTransit($Items);
            }
        }

        $Items = $Items
            ->where(['enabled' => true])
            ->limit(100);

        /** @var Builder $Items */
        $Items = $Items->selectRaw('*, IF (`price5` > 0 and `special` = 1 and ((SELECT remains FROM items_colors WHERE item_id = items.id AND main = 1) >= 10000), ROUND(price5 / 2, 1), price5) price_sort');

        $Items = $Items
            ->orderByRaw('if (price_sort = "" or price_sort is null, 1, 0), price_sort asc');

        return $Items->get();
    }

    /**
     * @param $Items
     *
     * @return array
     */
    public static function groupItems($Items) : array // todo type hint
    {
        if (count($Items) == 0) {
            return [];
        }

        $GroupItems = [];

        foreach ($Items as $Item) {
            $Category = $Item->category;

            if (!isset($GroupItems[$Category->parent_id][$Category->id])) {
                $GroupItems[$Category->parent_id][$Category->id] = new Collection();
            }

            $GroupItems[$Category->parent_id][$Category->id]->push($Item);
        }

        uksort($GroupItems, ['self', 'sortfnc']);

        foreach ($GroupItems as $Categories) {
            foreach ($Categories as $key => $Items) {
                $Categories[$key] = Tools::collectionCustomPosition($Items, 'custom_search_position');
            }
        }

        return $GroupItems;
    }

    /**
     * @param $Items
     *
     * @return mixed
     */
    public static function filterItemsBySize(Builder $Items)
    {
        // todo ;дец
        $filters = [];

        for ($i = 1; $i <= 6; $i++) {
            if (Request::get('size_' . $i)) {
                $filters['size_' . $i] = htmlentities(Request::get('size_' . $i));
                $filters['size_' . $i] = str_replace(',', '.', $filters['size_' . $i]);

                // вводимые значения
                if (in_array(Request::get('type'), ItemSize::$inputs)) {
                    $filters['size_' . $i] = preg_replace('#[^\d\.]#', '', $filters['size_' . $i]);
                }
            }
        }

        if (Request::get('size')) {
            $filters['size'] = (int) Request::get('size');
        }

        if (empty($filters)) {
            return false;
        }

        if (Request::get('type') == ItemSize::FILTER_HOLE) {
            if (Request::get('size_1')) {
                $ItemSize = ItemSize::where(['type' => Request::get('type')])
                    ->groupBy('item_id')
                    ->havingRaw('MIN(size_1 * 1) <= ' . (float) $filters['size_1'])
                    ->havingRaw('MAX(size_1 * 1) >= ' . (float) $filters['size_1']);
            }
        } else {
            if (in_array(Request::get('type'), [
                ItemSize::FILTER_RECTANGLE,
                ItemSize::FILTER_OVAL,
                ItemSize::FILTER_POLUOVAL,
                ItemSize::FILTER_ELLIPSE,
                ItemSize::FILTER_WASHER,
            ])) {
                if (Request::get('size_1') || Request::get('size_2')) {
                    $ItemSize = ItemSize::where(['type' => Request::get('type')]);

                    if (Request::get('size_1') == Request::get('size_2')) {
                        $ItemSize = $ItemSize->where(function (Builder $Builder) use ($filters): void {
                            $Builder->where('size_1', $filters['size_1'])
                                ->Where('size_2', $filters['size_2']);
                        });
                    } else {
                        if (Request::get('size_1')) {
                            $ItemSize = $ItemSize->where(function (Builder $Builder) use ($filters): void {
                                $Builder->where('size_1', $filters['size_1'])
                                    ->orWhere('size_2', $filters['size_1']);
                            });
                        }
                        if (Request::get('size_2')) {
                            $ItemSize = $ItemSize->where(function (Builder $Builder) use ($filters): void {
                                $Builder->where('size_1', $filters['size_2'])
                                    ->orWhere('size_2', $filters['size_2']);
                            });
                        }
                    }
                }
            } else {
                $ItemSize = ItemSize::where(['type' => Request::get('type')]);

                if (Request::get('size')) {
                    if ($filters['size'] == 1) { // M
                        $ItemSize = $ItemSize->whereNotNull('size_1');
                    } elseif ($filters['size'] == 3) { // GAS/BSP
                        $ItemSize = $ItemSize->whereNotNull('size_3');
                    } elseif ($filters['size'] == 5) { // UNF/JIC
                        $ItemSize = $ItemSize->whereNotNull('size_5');
                    }
                }

                for ($i = 1; $i <= 6; $i++) {
                    if (Request::get('size_' . $i)) {
                        $ItemSize = $ItemSize->where(['size_' . $i => $filters['size_' . $i]]);
                    }
                }
            }
        }

        $ItemSize = $ItemSize->select('item_id')->distinct()->get()->pluck('item_id')->toArray();

        return $Items->whereIn('id', $ItemSize);
    }

    /**
     * @param Builder $Items
     *
     * @return Builder
     */
    public static function filterExists(Builder $Items) : Builder
    {
        $Items = $Items->whereExists(function (\Illuminate\Database\Query\Builder $Builder): void {
            $Builder->select(DB::raw(1))
                ->from('items_colors')
                ->whereRaw('items_colors.remains > 0 AND items_colors.item_id = items.id');
        });

        return $Items;
    }

    /**
     * @param Builder $Items
     *
     * @return Builder
     */
    public static function filterTransit(Builder $Items) : Builder
    {
        $Items = $Items->whereExists(function (\Illuminate\Database\Query\Builder $Builder): void {
            $Builder->select(DB::raw(1))
                ->from('items_transit')
                ->whereRaw('items_transit.quant > 0 AND items_transit.item_id = items.id');
        });

        return $Items;
    }

    /**
     * @param Builder $Items
     *
     * @return Builder
     */
    public static function filterExistsOrTransit(Builder $Items) : Builder
    {
        $Items = $Items->where(function (Builder $Builder) : void {
            $Builder->whereExists(function (\Illuminate\Database\Query\Builder $Builder): void {
                $Builder->select(DB::raw(1))
                    ->from('items_colors')
                    ->whereRaw('items_colors.remains > 0 AND items_colors.item_id = items.id');
            })->orWhereExists(function (\Illuminate\Database\Query\Builder $Builder): void {
                $Builder->select(DB::raw(1))
                    ->from('items_transit')
                    ->whereRaw('items_transit.quant > 0 AND items_transit.item_id = items.id');
            });
        });

        return $Items;
    }

    /**
     * @param Builder $Items
     *
     * @return Builder
     */
    public static function filterSpecial(Builder $Items) : Builder
    {
        return $Items->where(['special' => true]);
    }

    /**
     * @param array $ids
     *
     * @return Builder
     */
    public static function findItemsByIds(array $ids) : Builder
    {
        $idsOrdered = implode(',', $ids);

        $Items = Item::where(['enabled' => true])
            ->limit(100);

        $Items = $Items->orderByRaw('ISNULL(price5)');

        $Items = $Items->orderByRaw(DB::raw("FIELD(id, $idsOrdered)"));

        $Items = $Items->whereIn('id', $ids);

        return $Items;
    }

    /**
     * @param string $article
     * @param bool   $strict
     *
     * @return mixed
     */
    public static function findItemsByArticle(string $article, bool $strict = false) : Builder
    {
        $Items = Item::where(['enabled' => true])
            ->limit(100);

        $Items = $Items->selectRaw('*, IF (`price5` > 0 and `special` = 1 and ((SELECT remains FROM items_colors WHERE item_id = items.id AND main = 1) >= 10000), ROUND(price5 / 2, 1), price5) price_sort');

        $Items = $Items
            ->orderByRaw('if (price_sort = "" or price_sort is null, 1, 0), price_sort asc');

        if ($strict) {
            $Items = $Items->where(function (Builder $Builder) use ($article): void {
                $Builder->where(['title' => str_replace(',', '.', $article)])
                    ->orWhere(['title' => str_replace('.', ',', $article)]);
            });
        } else {
            $Items = $Items->where(function (Builder $Builder) use ($article): void {
                $Builder->where('title', 'like', '%' . str_replace(',', '.', $article) . '%')
                    ->orWhere('title', 'like', '%' . str_replace('.', ',', $article) . '%');
            });
        }

        return $Items;
    }

    /**
     * @param string $article
     * @param bool   $strict
     *
     * @return mixed
     */
    public static function findColorsByArticle(string $article, bool $strict = false) : Builder
    {
        $Colors = ItemColor::whereExists(function (\Illuminate\Database\Query\Builder $Builder): void {
            $Builder->select(DB::raw(1))
                ->from('items')
                ->whereRaw('items.enabled = 1 AND items_colors.item_id = items.id');
        })
            ->orderBy('code', 'ASC')
            ->limit(100);

        if ($strict) {
            $Colors = $Colors->where(function (Builder $Builder) use ($article): void {
                $Builder->where(['code' => str_replace(',', '.', $article)])
                    ->orWhere(['code' => str_replace('.', ',', $article)]);
            });
        } else {
            $Colors = $Colors->where(function (Builder $Builder) use ($article): void {
                $Builder->where('code', 'like', '%' . str_replace(',', '.', $article) . '%')
                    ->orWhere('code', 'like', '%' . str_replace('.', ',', $article) . '%');
            });
        }

        return $Colors;
    }
}
