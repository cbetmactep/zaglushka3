<?php
//declare(strict_types=1); // todo

namespace App\Helpers;

use App\Models\I10n;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Str;

class Tools
{
    /**
     * @param Collection $Collection
     * @param string     $wedge
     *
     * @return Collection
     */
    public static function collectionCustomPosition(Collection $Collection, string $wedge) : Collection
    {
        $wedges = [];

        foreach ($Collection as $key => $Item) {
            if ($Item->$wedge) {
                $wedges[] = $Item;
                $Collection->forget($key);
            } else {
                break;
            }
        }

        foreach ($wedges as $WedgedItem) {
            $Collection->splice($WedgedItem->$wedge - 1, 0, [$WedgedItem]);
        }

        return $Collection;
    }

    /**
     * @param $string
     *
     * @return mixed
     */
    public static function correctString(string $string) : string
    {
        $search = [
            'q','w','e','r','t','y','u','i','o','p','[',']',
            'a','s','d','f','g','h','j','k','l',';','\'',
            'z','x','c','v','b','n','m',',','.'
        ];
        $replace = [
            'й','ц','у','к','е','н','г','ш','щ','з','х','ъ',
            'ф','ы','в','а','п','р','о','л','д','ж','э',
            'я','ч','с','м','и','т','ь','б','ю'
        ];

        return str_replace($search, $replace, $string);
    }

    /**
     * первая буква заглавная (mb).
     *
     * @param string $text
     *
     * @return string
     */
    public static function mb_ucfirst(string $text) : string
    {
        return mb_strtoupper(mb_substr($text, 0, 1)) . mb_substr($text, 1);
    }

    /**
     * @param string $s        строка для перевода
     * @param string $category категория или страница
     * @param array  $args     подставляемые значения
     *
     * @throws Exception
     *
     * @return string
     */
    public static function translate(string $s, string $category = 'all', array $args = [])
    {
        $lang = app('translator')->getLocale();

        $I10n = I10n::cachedOne($category, md5($s));

        if (!$I10n) {
            $I10nAll = I10n::cachedOne('all', md5($s));

            $I10n = new I10n();

            $I10n->category = $category;
            $I10n->key      = md5($s);
            $I10n->ru       = $s;

            if ($I10nAll) {
                $I10n->en = $I10nAll->en;
                $I10n->it = $I10nAll->it;
            }

            $I10n->save();
        }

        if (trim($I10n->$lang)) {
            $s = $I10n->$lang;
        }
        try {
            return vsprintf($s, $args);
        } catch (Exception $Exception) {
            return $s; // todo экранирование js ?
        }
    }
}
