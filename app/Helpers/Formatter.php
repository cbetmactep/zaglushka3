<?php
//declare(strict_types=1); // todo

namespace App\Helpers;

class Formatter
{
    /**
     * @param string $date
     *
     * @return string
     */
    public static function date(string $date = '') : string
    {
        $date = ($date !== '') ? strtotime($date) : false;

        return date('d.m.Y', $date ?: time());
    }

    /**
     * @param float $price
     * @param bool  $currency
     * @param bool  $showZero
     *
     * @return string
     */
    public static function price(float $price, bool $currency = false, bool $showZero = false) : string
    {
        $formatted = '';

        if ($price || $showZero) {
            if ($price >= 1000 && ($price - floor($price)) == 0) {
                $formatted = number_format($price, 0, ',', ' ');
            } else {
                $formatted = number_format($price, 1, ',', ' ');
            }

            if ($currency) {
                $formatted .= ' ' . _t('р.');
            }
        }

        return $formatted;
    }

    /**
     * @param int  $number
     * @param bool $showPostfix
     *
     * @return string
     */
    public static function quantity(int $number, bool $showPostfix = true) : string
    {
        $formatted = number_format($number, 0, ',', ' ') . ($showPostfix ? ' ' . _t('шт.', 'common') : '');

        return $formatted;
    }

    /**
     * @param int $daysMin
     * @param int $daysMax
     *
     * @return string
     */
    public static function preOrderAvailability(int $daysMin, int $daysMax) : string
    {
        if ($daysMin == $daysMax) {
            $daysMin = 0;
        }

        // пробуем получить месяцы
        if ((($daysMin * 2) % 30) == 0 && (($daysMax * 2) % 30) == 0) {
            $words = ['месяц', 'месяца', 'месяцев'];

            if ((!$daysMin || !$daysMax)) {
                $value2 = (($daysMin + $daysMax) / 30);

                return '~ ' .
                    (is_int($value2) ? $value2 : number_format($value2, 1, ',', ' '))
                    . ' ' . _t(self::getNumEnding($value2, $words));
            }

            $value1 = ($daysMin / 30);
            $value2 = ($daysMax / 30);

            return (is_int($value1) ? $value1 : number_format($value1, 1, ',', ' '))
                . ' - ' .
                (is_int($value2) ? $value2 : number_format($value2, 1, ',', ' '))
                . ' ' . _t(self::getNumEnding($value2, $words));
        }

        // пробуем получить недели
        if (($daysMin % 7) == 0 && ($daysMax % 7) == 0) {
            $words = ['неделя', 'недели', 'недель'];

            if (!$daysMin || !$daysMax) {
                $value2 = (int) (($daysMin + $daysMax) / 7);

                return '~ ' . $value2 . ' ' . _t(self::getNumEnding($value2, $words));
            }

            $value1 = (int) ($daysMin / 7);
            $value2 = (int) ($daysMax / 7);

            return $value1 . ' - ' . $value2 . ' ' . _t(self::getNumEnding($value2, $words));
        }

        // дни

        $words = ['день', 'дня', 'дней'];

        if (!$daysMin || !$daysMax) {
            $value2 = (int) ($daysMin + $daysMax);

            return '~ ' . $value2 . ' ' . _t(self::getNumEnding($value2, $words));
        }

        $value1 = (int) $daysMin;
        $value2 = (int) $daysMax;

        return ($value1) . ' - ' . ($value2) . ' ' . _t(self::getNumEnding($value2, $words));
    }

    /**
     * Функция возвращает окончание для множественного числа слова на основании числа и массива окончаний.
     *
     * @param float $number      Число на основе которого нужно сформировать окончание
     * @param array $endingArray Массив слов или окончаний для чисел (1, 4, 5),
     *                           например array('яблоко', 'яблока', 'яблок')
     *
     * @return string
     */
    public static function getNumEnding(float $number, array $endingArray) : string
    {
        if (!is_int($number) && $number > 1 && $number < 2) {
            return $endingArray[1];
        }

        $number = $number % 100;
        if ($number >= 11 && $number <= 19) {
            $ending = $endingArray[2];
        } else {
            $i = $number % 10;
            switch ($i) {
                case (1):
                    $ending = $endingArray[0];

                    break;
                case (2):
                case (3):
                case (4):
                    $ending = $endingArray[1];

                    break;
                default:
                    $ending = $endingArray[2];
            }
        }

        return $ending;
    }

    /**
     * @param string $phone
     *
     * @return string
     */
    public static function phone(string $phone) : string
    {
        $phone = trim($phone);

        $phone = preg_replace('/[\+\ \-\(\)]/', '', $phone);

        if (!is_numeric($phone)) {
            return $phone;
        }

        //  короткий номер
        if (mb_strlen($phone) < 10) {
            return $phone;
        }

        // нет кода страны todo: 7 - только россия
        if (mb_strlen($phone) == 10) {
            $phone = '7' . $phone;
        }

        if (mb_substr($phone, 0, 1) == '8') {
            $phone = '7' . mb_substr($phone, 1);
        }

        return $phone;
    }

    /**
     * @param float $minPrice
     *
     * @return string
     */
    public static function getPriceLabel(float $minPrice) : string
    {
        $minLabel = '';

        if ($minPrice > 0) {
            if ($minPrice < 1) {
                $minPrice = ($minPrice * 100);
                $minLabel = Formatter::getNumEnding($minPrice, ['копейки', 'копеек', 'копеек']);
            } else {
                $minLabel = Formatter::getNumEnding($minPrice, ['рубля', 'рублей', 'рублей']);
            }

            $minLabel = ' от ' .
                ((floor($minPrice) === $minPrice) ? (int) $minPrice : $minPrice) .
                ' ' . $minLabel;
        }

        return $minLabel;
    }
}
