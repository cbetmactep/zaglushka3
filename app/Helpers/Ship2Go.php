<?php
declare(strict_types=1);

namespace App\Helpers;

use App\Models\Redis\Cart;
use Cache;
use Exception;
use GuzzleHttp\Client;
use Log;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;

class Ship2Go
{
    const CACHE_TIME = 60 * 24;

    /**
     * @param string $cityTo
     * @param string $box
     *
     * @return int
     */
    public static function calc(string $cityTo, string $box = '10,0.4,0.4,0.4') : int
    {
        $OrderItems = Cart::items();

        $weight = 0;
        $volume = 0;

        /** @var \App\Models\OrderItem $OrderItem */
        foreach ($OrderItems as $OrderItem) {
            $Item = $OrderItem->item;

            if ($Item->width && $Item->length && $Item->height && $Item->weight) {
                $weight += $OrderItem->quant * ($Item->weight / 1000);
                $volume += ($OrderItem->quant * ($Item->length / 1000 * $Item->height / 1000 * $Item->weight / 1000));
            } elseif ($Item->pack_size && $Item->prop_pack_weight_gross && $Item->prop_pack_volume) {
                $weight += $OrderItem->quant * $Item->prop_pack_weight_gross / $Item->pack_size;
                $volume += $OrderItem->quant * $Item->prop_pack_volume / $Item->pack_size;
            } else {
                $weight = 0;
                $volume = 0;

                break;
            }
        }

        if ($weight && $volume) {
            $size = round($volume**(1 / 3), 3);
            $box  = $weight . ',' . $size . ',' . $size . ',' . $size;
        }

        try {
            // кешируем ответ
            return (int) Cache::remember(
                'ship2go_' . md5($cityTo . '_' . $box),
                self::CACHE_TIME,
                function () use ($cityTo, $box) : int {
                    // кеш
                    $Client = new Client(); //GuzzleHttp\Client

                    $result = $Client->get('https://api.ship2go.ru/price', [
                        'query' => [
                            'city_from' => '190000',
                            'city_to'   => $cityTo,
                            'box'       => $box,
                        ],
						'connect_timeout' => 20,
						'timeout' => 60
                    ]);

                    $result   = json_decode($result->getBody()->getContents())->result;
                    $minPrice = 0;

                    foreach ($result as $r) {
                        if (isset($r->price)) {
                            if ($minPrice === 0) {
                                $minPrice = (int) $r->price;
                            } else {
                                $minPrice = min($minPrice, $r->price);
                            }
                        }
                    }

                    return (int) $minPrice;
                }
            );
        } catch (Exception $Exception) {
            $Monolog = Log::getMonolog();
            $Monolog->setHandlers([$handler = new StreamHandler(storage_path('/logs/delivery:ship2go.log'))]);
            $handler->setFormatter(new LineFormatter(null, null, true, true));
            $Monolog->error($Exception->getMessage());

            return 0;
        }
    }
}
