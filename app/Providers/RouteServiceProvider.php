<?php
declare(strict_types=1);

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     */
    public function boot(): void
    {
        Route::pattern('id', '[0-9]+');
        Route::pattern('cityId', '[0-9]+');
        Route::pattern('catalogId', '[0-9]+');
        Route::pattern('categoryId', '[0-9]+');
        Route::pattern('tagId', '[0-9]+'); // todo в админку
        Route::pattern('itemId', '[0-9]+');
        Route::pattern('color', '[0-9]+');
        Route::pattern('quantity', '[0-9]+');
        Route::pattern('slug', '[a-z0-9\-\_]+');
        Route::pattern('tagSlug', '[a-z0-9\-\_]+');
        Route::pattern('categorySlug', '[a-z0-9\-\_]+');
        Route::pattern('lang', '[a-z]{2}');

        parent::boot();
    }

    /**
     * Define the routes for the application.
     */
    public function map(): void
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     */
    protected function mapWebRoutes(): void
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     */
    protected function mapApiRoutes(): void
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}
