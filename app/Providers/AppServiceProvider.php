<?php
declare(strict_types=1);

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Krlove\EloquentModelGenerator\Provider\GeneratorServiceProvider;
use Laravel\Tinker\TinkerServiceProvider;
use Monolog\Handler\SlackWebhookHandler;
use Monolog\Logger;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
    }

    /**
     * Register any application services.
     */
    public function register(): void
    {
        if ($this->app->environment() === 'production') {
            $monolog = \Log::getMonolog();
            $slackHandler = new SlackWebhookHandler(
                getenv('UPTIME_MONITOR_SLACK_WEBHOOK_URL'), // $webhookUrl
                null, // $channel
                null, // $username
                true, // $useAttachment
                null, // $iconEmoji
                false, // $useShortAttachment
                false, // $includeContextAndExtra
                Logger::ERROR, // $level
                true, // $bubble
                [] //$excludeFields
            );
            $monolog->pushHandler($slackHandler);
        } elseif ($this->app->environment() !== 'testing') {
            $this->app->register(\Barryvdh\Debugbar\ServiceProvider::class);
            $this->app->register(IdeHelperServiceProvider::class);
            $this->app->register(GeneratorServiceProvider::class);
            $this->app->register(TinkerServiceProvider::class);
        }
    }
}
