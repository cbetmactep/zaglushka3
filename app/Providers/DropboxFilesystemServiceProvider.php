<?php
declare(strict_types=1);

namespace App\Providers;

use Illuminate\Contracts\Foundation\Application;
use Spatie\FlysystemDropbox\DropboxAdapter;
use Storage;
use League\Flysystem\Filesystem;
use Illuminate\Support\ServiceProvider;
use Spatie\Dropbox\Client as DropboxClient;

class DropboxFilesystemServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        Storage::extend('dropbox', function (Application $App, array $config) {
            $client = new DropboxClient(
                $config['authorizationToken']
            );

            return new Filesystem(new DropboxAdapter($client));
        });
    }

    public function register(): void
    {
    }
}
