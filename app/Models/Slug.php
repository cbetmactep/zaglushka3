<?php
declare(strict_types=1);

namespace App\Models;

use App\Enum\Subjects;
use Cache;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int     $id
 * @property int     $subject_id
 * @property int     $object_id
 * @property string  $slug
 * @property bool $current
 * @property string  $created_at
 * @property string  $updated_at
 */
class Slug extends Model
{
    const CACHE_TIME = 10080;

    protected $casts = [
        'current' => 'boolean',
    ];

    /**
     * @param string $slug
     * @param int    $subjectId
     *
     * @return null|Slug
     */
    public static function getSlug(string $slug, int $subjectId = 0) : ?self
    {
        $Slug = Cache::remember(
            'slug_by_string_' . $slug . '_' . $subjectId,
            self::CACHE_TIME,
            function () use ($slug, $subjectId) {
                $model = self::where(['slug' => $slug]);

                if ($subjectId) {
                    $model = $model->where(['subject_id' => $subjectId]);
                }

                return $model->first();
            }
        );

        return $Slug;
    }

    /**
     * @return int тип текущей модели
     */
    public static function getType() : int
    {
        return Subjects::SUBJECT_SLUG;
    }
}
