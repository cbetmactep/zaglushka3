<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ItemSame.
 *
 * @property int $id
 * @property int $item_id
 * @property int $same_item_id
 */
class ItemSame extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'items_same_items';
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['item_id', 'same_item_id'];

    /**
     * @param int $id
     *
     * @return Item[]
     */
    public static function getItems(int $id) : Collection
    {
        $SameItems = self::where(['item_id' => $id])->orWhere(['same_item_id' => $id])->get();

        $ids = [];
        foreach ($SameItems as $SameItem) {
            $ids[] = ($SameItem->item_id == $id) ? $SameItem->same_item_id : $SameItem->item_id;
        }

        /** @var Item[] $Items */
        $Items = Item::whereIn('id', $ids)
            ->where(['enabled' => 1]);

        $Items = $Items->selectRaw('*, IF (`price5` > 0 and `special` = 1 and ((SELECT remains FROM items_colors WHERE item_id = items.id AND main = 1) >= 10000), ROUND(price5 / 2, 1), price5) price_sort');

        $Items = $Items
            ->orderByRaw('if (price_sort = "" or price_sort is null, 1, 0), price_sort asc');

        return $Items->get();
    }
}
