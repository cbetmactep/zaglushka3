<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ItemComplex.
 *
 * @property int $id
 * @property int $complex_item_id
 * @property int $component_item_id
 * @property int $complex_item_color
 * @property int $component_item_color
 */
class ItemComplexColor extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'items_complex_colors';
    public $timestamps = false;

    public static function updateComplexItem(Item $Item) : void
    {
        $ComplexItems = $Item->ItemComplexColors()
            ->distinct()
            ->get();

        $Item->price0 = array_sum($ComplexItems->pluck('price0')->toArray());
        $Item->price1 = array_sum($ComplexItems->pluck('price1')->toArray());
        $Item->price2 = array_sum($ComplexItems->pluck('price2')->toArray());
        $Item->price3 = array_sum($ComplexItems->pluck('price3')->toArray());
        $Item->price4 = array_sum($ComplexItems->pluck('price4')->toArray());
        $Item->price5 = array_sum($ComplexItems->pluck('price5')->toArray());

        $Item->save();

        $Colors = $Item->itemColors;

        foreach ($Colors as $Color) {
            $tmpColors = [];
            foreach ($ComplexItems as $ComplexItem) {
                $color = ItemComplexColor::where([
                    'complex_item_id' => $Item->id,
                    'component_item_id' => $ComplexItem->id,
                    'complex_item_color' => $Color->color,
                ])->pluck('component_item_color');

                $ComplexItemColor = $ComplexItem->itemColors()->where(['color' => $color])->first();

                $tmpColors[] = $ComplexItemColor ? $ComplexItemColor->remains : 0;
            }

            $Color->remains = min($tmpColors);
            $Color->updated = 2;

            $Color->save();
        }
    }
}
