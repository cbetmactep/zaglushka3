<?php
declare(strict_types=1);

namespace App\Models;

use App\Enum\Subjects;
use App\Helpers\Formatter;
use App\Helpers\Tools;
use App\Interfaces\MetaAble;
use App\Interfaces\SlugAble;
use Cache;
use Config;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use File as FileHelper;
use morphos\Russian\GeographicalNamesInflection;

/**
 * App\Models\Tag.
 *
 * @property int $id
 * @property string  $title
 * @property string  $title_h1
 * @property bool $top_level
 * @property string  $descr
 * @property int $ord
 * @property string  $meta_title
 * @property string  $meta_description
 * @property string  $meta_keywords
 * @property string  $slug
 * @property Item[]  $items
 */
class Tag extends Model implements MetaAble, SlugAble
{
    use \App\Traits\MetaAble;
    use \App\Traits\SlugAble;

    const CACHE_TIME = 10800;

    public $timestamps = false;

    /**
     * @param Category $Category
     * @param string   $sort
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items(Category $Category, $sort = null)
    {
        return self::getItems($Category, [$this->id], $sort);
    }

    /**
     * @param Category $Category
     * @param array    $tagIds
     * @param string   $sort
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     *
     * @internal param string $sort
     */
    public static function getItems(Category $Category, array $tagIds, $sort = null)
    {
        // todo кешировать эту дичь
        $Items = Item::where(['enabled' => 1])
            ->where(function (Builder $Builder) use ($tagIds): void {
                $Builder->whereExists(function (\Illuminate\Database\Query\Builder $Builder) use ($tagIds): void {
                    $Builder->select(DB::raw(1))
                    ->from('items_tags')
                    ->whereIn('items_tags.tag_id', $tagIds)
                    ->whereRaw('items_tags.item_id = items.id');
                })
                ->orWhereExists(function (\Illuminate\Database\Query\Builder $Builder) use ($tagIds): void {
                    $Builder->select(DB::raw(1))
                        ->from('items_add_tags')
                        ->whereIn('items_add_tags.tag_id', $tagIds)
                        ->whereRaw('items_add_tags.item_id = items.id');
                });
            });

        $categories = $Category->parent_id ? [$Category->id] : $Category->getSubcategoriesIds();
        $Items      = $Items->where(function (Builder $Builder) use ($categories): void {
            $Builder->whereIn('cat_id', $categories)
                ->orWhereIn('add_cat_id', $categories);
        });

        if ($sort) {
            $Items = $Items
                ->orderByRaw('-' . $sort . ' DESC')
                ->orderBy($sort, 'ASC');
        }

        $Items = $Items->selectRaw('*, IF (`price5` > 0 and `special` = 1 and ((SELECT remains FROM items_colors WHERE item_id = items.id AND main = 1) >= 10000), ROUND(price5 / 2, 1), price5) price_sort');

        return $Items
            ->orderByRaw('if (price_sort = "" or price_sort is null, 1, 0), price_sort asc');
    }

    /**
     * @param Category $Category
     *
     * @return bool
     */
    public function isBreadcrumbsImageExists(Category $Category)
    {
        return FileHelper::exists(public_path($this->getBreadcrumbsImage($Category)));
    }

    /**
     * @param Category $Category
     *
     * @return string
     */
    public function getBreadcrumbsImage(Category $Category)
    {
        return '/images/breadcrumbs/tags/' . $Category->id . '_' . $this->id . '.jpg';
    }

    /**
     * @param Category $Category
     * @param string   $sort
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function additionalItems(Category $Category = null, $sort = null)
    {
        // todo кешировать эту дичь
        $ItemAddTag = new ItemHasAddTag();

        return $this->belongsToMany(Item::class, $ItemAddTag->getTable())->where(['enabled' => 1]);
    }

    /**
     * @param bool $h1
     *
     * @return string title
     */
    public function getTitle(bool $h1 = false) : string
    {
        return $h1 ? $this->title_h1 : $this->title;
    }

    /**
     * @return int тип текущей модели
     */
    public static function getType() : int
    {
        return Subjects::SUBJECT_TAG;
    }

    /**
     * @param \App\Models\Category $Category
     *
     * @return float
     */
    public function getMinPrice(Category $Category = null) : float
    {
        return (float) Cache::remember('min_price_' . $this->getType() . '_' . $this->id, self::CACHE_TIME, function () use ($Category) {
            return $this->items($Category)->min('price5');
        });
    }

    /**
     * @param mixed $id
     *
     * @return null|Tag
     */
    public static function cachedOne(int $id) : ?Tag
    {
        return  Cache::remember('tag_' . $id, self::CACHE_TIME, function () use ($id) {
            return self::find($id);
        });
    }

    /**
     * @param string   $string
     * @param Category $Category
     *
     * @throws \Exception
     *
     * @return string
     */
    public function replaceMetaReplacements(string $string, Category $Category = null): string
    {
        if ($Category === null) {
            throw new \Exception();
        }

        return Tools::mb_ucfirst(preg_replace(
            [
                '#\[НАЗВАНИЕ_ТЕГА\]#',
                '#\[НАЗВАНИЕ_КАТЕГОРИИ\]#',
                '#\[ОТ_МИНИМАЛЬНАЯ_ЦЕНА\]#',
                '#\[ГОРОДЕ\]#',
            ],
            [
                mb_strtolower($this->getTitle()),
                mb_strtolower($Category->getTitle()),
                Formatter::getPriceLabel($this->getMinPrice($Category)),
                Config::get('app.subdomain') ?
                    GeographicalNamesInflection::getCase(
                        City::cachedOne((int) session('current_city'))->title,
                        'предложный'
                    ) :
                    GeographicalNamesInflection::getCase(City::cachedOne(25)->title, 'предложный'), // Санкт-Петербург
            ],
            $string
        ));
    }
}
