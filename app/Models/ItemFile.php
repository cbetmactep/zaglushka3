<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int    $id
 * @property int    $item_id
 * @property string $title
 * @property string $ext
 * @property int    $ord
 *
 * todo выпилить
 */
class ItemFile extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'items_files';

    /**
     * @var array
     */
    protected $fillable = ['item_id', 'title', 'ext', 'ord'];

    /**
     * @return string path to image
     */
    public function getImageLink() : string
    {
        return '/files/item/' . ($this->id % 100) . '/' . $this->id . '.' . $this->ext;
    }
}
