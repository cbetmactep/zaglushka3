<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductionItems extends Model {
    
    protected $table='production_items';
    
}