<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\OrderItem.
 *
 * @property int $id
 * @property int $order_id
 * @property int $item_id
 * @property string  $title
 * @property string  $image
 * @property string  $color
 * @property float   $price
 * @property float   $original_price
 * @property int $quant
 * @property int $promo
 * @property int $discount
 */
class OrderItem extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orders_items';
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['order_id', 'item_id', 'title', 'image', 'color', 'price', 'quant', 'discount', 'original_price'];

    /**
     * @return string path to image
     */
    public function getImageLink() : string
    {
        return \Request::root() . $this->image;
    }

    /**
     * @return Item
     */
    public function item() : BelongsTo
    {
        return $this->belongsTo(Item::class)->where(['enabled' => 1]);
    }

    /**
     * @return Item
     */
    public function getColor() : ItemColor
    {
        return ItemColor::where(['color' => $this->color, 'item_id' => $this->item_id])->first();
    }

    /**
     * @param int $quantity
     *
     * @return int
     */
    public function getWholeSaleGroup(int $quantity = null) : int
    {
        $quantity = (int) (($quantity === null) ? $this->quant : $quantity);

        //todo constant
        switch (true) {
            case ($quantity >= 30000) && ($this->item->price6 > 0):
                return 6;
            case $quantity >= 10000:
                return 5;
            case $quantity >= 5000:
                return 4;
            case $quantity >= 1000:
                return 3;
            case $quantity >= 500:
                return 2;
            case $quantity >= 100:
                return 1;
            default:
                return 0;
        }
    }

    /**
     * @param int $wholeSaleGroupId
     *
     * @return string
     */
    public static function getWholeSaleGroupText(int $wholeSaleGroupId) : string
    {
        $texts = [
            0 => 'розница до 100',
            1 => 'от 100 до 500',
            2 => 'от 500 до 1 000',
            3 => 'от 1 000 до 5 000',
            4 => 'от 5 000 до 10 000',
            5 => 'более 10 000',
            6 => 'более 30 000',
        ];

        return _t($texts[$wholeSaleGroupId], 'item');
    }

    /**
     * @param mixed $value
     *
     * @return int
     */
    public function getColorAttribute($value) : int
    {
        return (int) $value;
    }

    /**
     * @param mixed $value
     *
     * @return float
     */
    public function getPriceAttribute($value) : float
    {
        return (float) $value;
    }

    /**
     * @param mixed $value
     *
     * @return int
     */
    public function getQuantAttribute($value) : int
    {
        return (int) $value;
    }

}
