<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $item_id
 * @property int $tag_id
 * @property Item $item
 * @property Tag $tag
 */
class ItemHasAddTag extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'items_add_tags';
    public $timestamps = false;
}
