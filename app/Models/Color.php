<?php
declare(strict_types=1);

namespace App\Models;

use App\Enum\Subjects;
use Cache;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $hex
 */
class Color extends Model
{
    const CACHE_TIME = 10080; // week неделя это 60*60*24*7 а здесь не более 2- х часов !!!

    public $timestamps = false;

    /**
     * @param int $id
     *
     * @return null|Color
     *
     * @internal param array $columns
     */
    public static function cachedOne(int $id) : ?Color
    {
        return  Cache::remember('color_' . $id, self::CACHE_TIME, function () use ($id) {
            return self::find($id);
        });
    }

    /**
     * @return int тип текущей модели
     */
    public static function getType() : int
    {
        return Subjects::SUBJECT_COLOR;
    }
}
