<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $subject_id
 * @property int $object_id
 * @property string $subdomain
 * @property string $language
 * @property string $title
 * @property string $description
 * @property string $keywords
 */
class Meta extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'meta';
    public $timestamps = false;
}
