<?php
declare(strict_types=1);

namespace App\Models;

use App\Enum\Subjects;
use App\Helpers\Formatter;
use App\Helpers\ImageCacher;
use App\Helpers\Tools;
use App\Interfaces\MetaAble;
use App\Interfaces\SlugAble;
use Cache;
use Config;
use DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use File as FileHelper;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Files\Interfaces\Attachable;
use morphos\Russian\GeographicalNamesInflection;
use App\Models\Production;
use App\Models\ProductionItems;

/**
 * App\Models\Item.
 *
 * @property int       $id
 * @property int       $cat_id
 * @property int       $add_cat_id
 * @property string        $code
 * @property string        $title
 * @property float         $price0
 * @property float         $price1
 * @property float         $price2
 * @property float         $price3
 * @property float         $price4
 * @property float         $price5
 * @property float         $price6
 * @property int       $pack_size
 * @property string        $descr
 * @property float         $weight
 * @property float         $volume
 * @property string        $production
 * @property string        $short_descr
 * @property int       $prop_matherial
 * @property int       $prop_package
 * @property string        $prop_pipes
 * @property float         $prop_weight
 * @property int       $prop_pack_width
 * @property int       $prop_pack_length
 * @property int       $prop_pack_height
 * @property int       $prop_pack_quant
 * @property int       $prop_pellet_width
 * @property int       $prop_pellet_length
 * @property int       $prop_pellet_height
 * @property int       $prop_pellet_quant_pack
 * @property float         $prop_pack_weight
 * @property string        $meta_title
 * @property string        $meta_description
 * @property string        $meta_keywords
 * @property int       $enabled
 * @property string        $under_title
 * @property string        $under_title_color
 * @property int       $special
 * @property float         $prop_pack_weight_gross
 * @property float         $prop_pack_weight_net
 * @property float         $prop_pack_volume
 * @property int       $prop_fit_pipe
 * @property int       $prop_features
 * @property int       $logo_image
 * @property string        $logo_tab_content
 * @property int       $no_free_delivery
 * @property int       $custom_search_position
 * @property int       $custom_cat_position
 * @property int       $custom_tag_position
 * @property string        $yandex_name
 * @property int       $recommend
 * @property Category      $category
 * @property ItemColor[]   $itemColors
 * @property ItemColor[]   $itemColorsExists
 * @property Item[]        $items
 * @property ItemSize[]    $itemSizes
 * @property Tag[]         $tags
 * @property Tag[]         $addTags
 * @property ItemTransit[] $itemTransits
 * @property float         $width
 * @property float         $length
 * @property float         $height
 * @property int           $preorder_availability_days_min
 * @property int           $preorder_availability_days_max
 * @property string        $slug
 * @property int           $promo
 * @property string        $name
 * @property boolean       $only_two_prices
 */
class Item extends Model implements MetaAble, Attachable, SlugAble
{
    use \App\Traits\MetaAble;
    use \App\Traits\SlugAble;
    use \Modules\Files\Traits\Attachable;
    public $storeFolder = 'items';

    const CACHE_TIME = 60 * 24 * 7;

    public $timestamps = false;

    /**
     *                      ____
     *                   .'* *.'
     *                __/_*_*(_
     *               / _______ \
     *              _\_)/___\(_/_
     *             / _((\- -/))_ \
     *             \ \())(-)(()/ /
     *              ' \(((()))/ '
     *             / ' \)).))/ ' \
     *            / _ \ - | - /_  \
     *           (   ( .;''';. .'  )
     *           _\"__ /    )\ __"/_
     *             \/  \   ' /  \/
     *              .'  '...' ' )
     *               / /  |  \ \
     *              / .   .   . \
     *             /   .     .   \
     *            /   /   |   \   \
     *          .'   /    b    '.  '.
     *      _.-'    /     Bb     '-. '-._
     *  _.-'       |      BBb       '-.  '-.
     * (________mrf\____.dBBBb.________)____)
     */
    protected $casts = [
        'price0' => 'float',
        'price1' => 'float',
        'price2' => 'float',
        'price3' => 'float',
        'price4' => 'float',
        'price5' => 'float',
        'price6' => 'float',
    ];

    /**
     * 1 - 3 крупные фотки
     * 5 - 7 мелкие фотки
     * 4 - крупная схема
     * 8 - мелкая схема
     * 9 - крупная картинка применения
     * 10 - мелкая картинка применения
     * 11-13 - крупные фотки
     * 14-16 - мелкие фотки
     * 17 - мелкая картинка для видео
     * 18 - мелкая схема для списков
     * 19-23 - крупные фотки
     * 24-28 - мелкие фотки
     * 29 - крупная схема 2
     * 30 - мелкая схема 2.
     */
    const IMAGE_TYPE_1 = '1.jpg';
    const IMAGE_TYPE_2 = '2.jpg';
    const IMAGE_TYPE_3 = '3.jpg';

    const IMAGE_TYPE_4 = '4.jpg';

    const IMAGE_TYPE_5 = '5.jpg';
    const IMAGE_TYPE_6 = '6.jpg';
    const IMAGE_TYPE_7 = '7.jpg';

    const IMAGE_TYPE_8 = '8.jpg';
    const IMAGE_TYPE_9 = '9.jpg';
    const IMAGE_TYPE_10 = '10.jpg';

    const IMAGE_TYPE_11 = '11.jpg';
    const IMAGE_TYPE_12 = '12.jpg';
    const IMAGE_TYPE_13 = '13.jpg';

    const IMAGE_TYPE_14 = '14.jpg';
    const IMAGE_TYPE_15 = '15.jpg';
    const IMAGE_TYPE_16 = '16.jpg';

    const IMAGE_TYPE_17 = '17.jpg';
    const IMAGE_TYPE_18 = '18.jpg';

    const IMAGE_TYPE_19 = '19.jpg';
    const IMAGE_TYPE_20 = '20.jpg';
    const IMAGE_TYPE_21 = '21.jpg';
    const IMAGE_TYPE_22 = '22.jpg';
    const IMAGE_TYPE_23 = '23.jpg';

    const IMAGE_TYPE_24 = '24.jpg';
    const IMAGE_TYPE_25 = '25.jpg';
    const IMAGE_TYPE_26 = '26.jpg';
    const IMAGE_TYPE_27 = '27.jpg';
    const IMAGE_TYPE_28 = '28.jpg';

    const IMAGE_TYPE_29 = '29.jpg';
    const IMAGE_TYPE_30 = '30.jpg';

    const IMAGE_TYPE_31 = '31.jpg';
    const IMAGE_TYPE_32 = '32.jpg';
    const IMAGE_TYPE_33 = '32.jpg';
    const IMAGE_TYPE_34 = '34.jpg';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category() : BelongsTo
    {
        return $this->belongsTo(Category::class, 'cat_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function fullImages() : HasMany
    {
        return $this->hasMany(ItemFile::class)->orderBy('ord', 'ASC');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function itemColors() : HasMany
    {
        return $this->hasMany(ItemColor::class)->orderBy('ord', 'ASC');
    }

    /**
     * @return ItemColor[]
     */
    public function itemCachedColors() : Collection
    {
        return Cache::remember('item_colors_' . $this->id, self::CACHE_TIME, function () {
            return $this->itemColors()->get();
        });
    }

    /**
     * @return Collection
     */
    public function itemColorsExists() : Collection
    {
        return Cache::remember('item_colors_exists_' . $this->id, self::CACHE_TIME, function () {
            return $this->itemColors()->where('remains', '>', 0)->get();
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function mainColor() : ?ItemColor
    {
        return Cache::remember('item_main_color_' . $this->id, self::CACHE_TIME, function () {
            return ItemColor::where(['item_id' => $this->id, 'main' => 1])->first();
        });
    }

    /**
     * @return Collection|null
     */
    public function itemTransits() : ?Collection
    {
        return Cache::remember('items_transit_' . $this->id, self::CACHE_TIME, function () {
            $ItemTransits = $this->hasMany(ItemTransit::class)
                ->select(['code', 'item_id'])
                ->selectRaw('SUM(quant) as quant')
                ->whereExists(function ($Builder): void { // builder
                    $Builder->select(DB::raw(1))
                        ->from('items_colors')
                        ->whereRaw('items_colors.code = items_transit.code');
                })
                ->groupBy('item_id', 'code')
                ->get();

            return $ItemTransits;
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function itemSizes() : HasMany
    {
        return $this->hasMany(ItemSize::class);
    }

    
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags() : BelongsToMany
    {
        return $this->belongsToMany(Tag::class, 'items_tags');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function addTags() : BelongsToMany
    {
        return $this->belongsToMany(Tag::class, 'items_add_tags');
    }

    /**
     * @return int
     */
    public function remains() : int
    {
        $remains = 0;

        // todo кеш
        foreach ($this->itemColorsExists() as $ItemColor) {
            $remains += $ItemColor->remains;
        }

        return $remains;
    }

    /**
     * @return int
     */
    public function transit() : int
    {
        return $this->itemTransits()->sum('quant');
    }

    /**
     * @param string $type
     *
     * @return string
     */
    public function getImagePath(string $type) : string
    {
        return '/images/item/' . ($this->id % 100) . '/' . $this->id . '/' . $type;
    }

    /**
     * @param   $type
     *
     * @return string path to image
     */
    public function getImageLink(string $type) : string
    {
        if ($type == self::IMAGE_TYPE_18) {
            return $this->getImagePath($type);
        }

        return ImageCacher::cache($this->getImagePath($type));
    }

    /**
     * @param string $type
     *
     * @return bool
     */
    public function isImageExists(string $type) : bool
    {
        return FileHelper::exists(public_path($this->getImagePath($type)));
    }

    /**
     * @return array
     */
    public function getImages() : array
    {
        $imageTypes = [
            self::IMAGE_TYPE_5  => self::IMAGE_TYPE_1,
            self::IMAGE_TYPE_6  => self::IMAGE_TYPE_2,
            self::IMAGE_TYPE_7  => self::IMAGE_TYPE_3,
            self::IMAGE_TYPE_8  => self::IMAGE_TYPE_4,
            self::IMAGE_TYPE_30 => self::IMAGE_TYPE_29,
            self::IMAGE_TYPE_10 => self::IMAGE_TYPE_9,
            self::IMAGE_TYPE_14 => self::IMAGE_TYPE_11,
            self::IMAGE_TYPE_15 => self::IMAGE_TYPE_12,
            self::IMAGE_TYPE_16 => self::IMAGE_TYPE_13,
            self::IMAGE_TYPE_24 => self::IMAGE_TYPE_19,
            self::IMAGE_TYPE_25 => self::IMAGE_TYPE_20,
            self::IMAGE_TYPE_26 => self::IMAGE_TYPE_21,
            self::IMAGE_TYPE_27 => self::IMAGE_TYPE_22,
            self::IMAGE_TYPE_28 => self::IMAGE_TYPE_23,
            self::IMAGE_TYPE_34 => self::IMAGE_TYPE_32,
        ];

        $Images = [];

        foreach ($imageTypes as $thumb => $image) {
            if ($this->isImageExists($thumb)) {
                if (!$this->isImageExists($image)) {
                    $image = $thumb;
                }

                $Images[] = [
                    'thumb' => $this->getImageLink($thumb),
                    'image' => $this->getImageLink($image),
                ];
            }
        }

        return $Images;
    }

    /**
     * @return array доступные цвета
     */
    public function getAvailableColors() : array
    {
        return Cache::remember('item_colors_available_' . $this->id, self::CACHE_TIME, function () {
            return $this->itemColors()->distinct()->get()->pluck('color')->toArray();
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function material() : ?Property
    {
        return Property::find($this->prop_matherial);
    }

    /**
     * @return Property
     */
    public function fit() : ?Property
    {
        return Property::find($this->prop_fit_pipe);
    }

    /**
     * @return Property
     */
    public function package() : ?Property
    {
        return Property::find($this->prop_package);
    }

    /**
     * @return float
     */
    public function getPalletVolumeM() : float
    {
        return ($this->prop_pellet_width / 1000) * ($this->prop_pellet_length / 1000) * ($this->prop_pellet_height / 1000);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany дочерние итемы
     */
    public function ItemComplex() : BelongsToMany
    {
        return $this->belongsToMany(Item::class, 'items_complect_items', 'item_id', 'complect_item_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany дочерние итемы (цветные)
     */
    public function ItemComplexColors() : BelongsToMany
    {
        return $this->belongsToMany(Item::class, 'items_complex_colors', 'complex_item_id', 'component_item_id');
    }

    /**
     * @param bool $h1
     *
     * @return string title
     */
    public function getTitle(bool $h1 = false) : string
    {
        return $this->title;
    }

    /**
     * @return int тип текущей модели
     */
    public static function getType() : int
    {
        return Subjects::SUBJECT_ITEM;
    }

    /**
     * @param $wholeSaleGroup
     *
     * @return int
     */
    public function getSpecialDiscount(int $wholeSaleGroup) : int
    {
        if (!$this->special) {
            return 0;
        }

        $MainColor = $this->mainColor();

        if (!$MainColor || !($remains = $MainColor->remains)) {
            return 0;
        }

        //todo constant
        switch (true) {
            case $remains >= 30000 && $this->price6 > 0:
                $discountWholesaleNumber = 6;

                break;
            case $remains >= 10000:
                $discountWholesaleNumber = 5;

                break;
            case $remains >= 5000:
                $discountWholesaleNumber = 4;

                break;
            case $remains >= 1000:
                $discountWholesaleNumber = 3;

                break;
            case $remains >= 500:
                $discountWholesaleNumber = 2;

                break;
            case $remains >= 100:
                $discountWholesaleNumber = 1;

                break;
            default:
                $discountWholesaleNumber = 0;
        }

        if ($wholeSaleGroup > $discountWholesaleNumber) {
            return 0;
        }

        if ($wholeSaleGroup == $discountWholesaleNumber) {
            return 50;
        }

        return 30;
    }

    public function getMinPrice() : float
    {
        $prices = [];
        for ($i = 0; $i <= 6; $i++) {
            $price = $this->getDiscountPrice($i);

            if ($price > 0) {
                $prices[] = $price;
            }
        }

        return count($prices) > 0 ? min($prices) : 0;
    }

    /**
     * @param int $wholeSaleGroup
     *
     * @return float
     */
    public function getDiscountPrice(int $wholeSaleGroup) : float
    {
        $discount = $this->getSpecialDiscount($wholeSaleGroup);
        $priceGroup = 'price' . $wholeSaleGroup;

        return $this->$priceGroup * (1 - ($discount / 100));
    }

    /**
     * @param string   $string
     * @param Category $Category
     *
     * @throws \Exception
     *
     * @return string
     */
    public function replaceMetaReplacements(string $string, Category $Category = null): string
    {
        if ($Category === null) {
            throw new \Exception();
        }

        return Tools::mb_ucfirst(preg_replace(
            [
                '#\[НАЗВАНИЕ_КАТЕГОРИИ\]#',
                '#\[ОТ_МИНИМАЛЬНАЯ_ЦЕНА\]#',
                '#\[НАЗВАНИЕ_ТОВАРА\]#',
                '#\[НАЗВАНИЕ_ТОВАРА_ДЛЯ_ЯНДЕКСА\]#',
                '#\[ГОРОДЕ\]#',
            ],
            [
                mb_strtolower($Category->getTitle()),
                Formatter::getPriceLabel($this->getMinPrice()),
                $this->getTitle(),
                $this->yandex_name,
                Config::get('app.subdomain') ?
                    GeographicalNamesInflection::getCase(
                        City::cachedOne((int) session('current_city'))->title,
                        'предложный'
                    ) :
                    GeographicalNamesInflection::getCase(City::cachedOne(25)->title, 'предложный'), // Санкт-Петербург
            ],
            $string
        ));
    }

    /**
     * @param mixed $value
     *
     * @return float
     */
    public function getPrice0Attribute($value) : float
    {
        return (float) $value;
    }

    /**
     * @param mixed $value
     *
     * @return float
     */
    public function getPrice1Attribute($value) : float
    {
        return (float) $value;
    }

    /**
     * @param mixed $value
     *
     * @return float
     */
    public function getPrice2Attribute($value) : float
    {
        return (float) $value;
    }

    /**
     * @param mixed $value
     *
     * @return float
     */
    public function getPrice3Attribute($value) : float
    {
        return (float) $value;
    }

    /**
     * @param mixed $value
     *
     * @return float
     */
    public function getPrice4Attribute($value) : float
    {
        return (float) $value;
    }

    /**
     * @param mixed $value
     *
     * @return float
     */
    public function getPrice5Attribute($value) : float
    {
        return (float) $value;
    }

    /**
     * @param mixed $value
     *
     * @return float
     */
    public function getPrice6Attribute($value) : float
    {
        return (float) $value;
    }
}
