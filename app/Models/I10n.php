<?php
declare(strict_types=1);

namespace App\Models;

use Cache;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $category
 * @property string $key
 * @property string $ru
 * @property string $en
 * @property string $it
 * @property string $de
 * @property string $es
 * @property string $created_at
 * @property string $updated_at
 */
class I10n extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'i10n';

    const CACHE_TIME = 10080;

    /**
     * @param $category
     * @param $key
     *
     * @return null|\App\Models\I10n
     */
    public static function cachedOne($category, $key) : ?I10n
    {
        $I10n = Cache::remember('i10n_' . $category . '_' . $key, self::CACHE_TIME, function () use ($category, $key) {
            return self::where(['category' => $category, 'key' => $key])->first();
        });

        if ($I10n && (mt_rand(0, 1000) === 5)) {
            $I10n->touch();
        }

        return $I10n;
    }
}
