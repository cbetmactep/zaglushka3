<?php
declare(strict_types=1);

namespace App\Models;

use App\Enum\Subjects;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Manager.
 *
 * @property int $id
 * @property string $title
 * @property string $phone
 * @property string $email
 * @property bool $enabled
 * @property int $ord
 */
class Manager extends Model
{
    public $timestamps = false;

    /**
     * @return int тип текущей модели
     */
    public static function getType() : int
    {
        return Subjects::SUBJECT_MANAGER;
    }
}
