<?php
declare(strict_types=1);

namespace App\Models\Redis;

use App\Models\ItemColor;
use Illuminate\Support\Facades\Redis;
use Session;

/**
 * @deprecated
 */
class Compare
{
    const REDIS_KEY_PREFIX = 'zaglushka_compare';

    const LIMIT = 1000;

    private static function getKey()
    {
        return self::REDIS_KEY_PREFIX . '_' . Session::getId();
    }

    /**
     * Get all.
     *
     * @return array
     */
    public static function range() : array
    {
        return Redis::zRange(self::getKey(), 0, -1);
    }

    /**
     * Add to compare.
     *
     * @param int $id
     * @param int $color
     *
     * @return int count
     */
    public static function add(int $id, int $color) : int
    {
        $count = Redis::zCard(self::getKey());

        if ($count < self::LIMIT) {
            if (Redis::zAdd(self::getKey(), time(), serialize(['id' => (string) $id, 'color' => (string) $color]))) {
                Redis::expire(self::getKey(), 24 * 60 * 60 * 7);

                $count++;
            }
        }

        return $count;
    }

    /**
     * Remove from compare.
     *
     * @param int $id
     * @param int $color
     *
     * @return int count
     */
    public static function remove(int $id, int $color) : int
    {
        Redis::zRem(self::getKey(), serialize(['id' => (string) $id, 'color' => (string) $color]));

        return Redis::zCard(self::getKey());
    }

    /**
     * Flush by key.
     *
     * @return int count
     */
    public static function flush() : int
    {
        return Redis::del(self::getKey());
    }

    /**
     * @return []
     */
    public static function items() : array
    {
        $compared = self::range();

        if (count($compared) > 0) {
            $comparedItems = array_map(function (string $compare) {
                return unserialize($compare);
            }, $compared);

            $Colors = [];
            foreach ($comparedItems as $Item) {
                $Color = ItemColor::where(['item_id' => $Item['id'], 'color' => $Item['color']])->first();

                if ($Color) {
                    $Colors[] = $Color;
                }
            }

            return $Colors;
        }

        return [];
    }
}
