<?php
declare(strict_types=1);

namespace App\Models\Redis;

use App\Models\Item;
use App\Models\ItemColor;
use App\Models\OrderItem;
use Illuminate\Support\Facades\Redis;
use Session;

/**
 * App\Models\Redis\File.
 */
class Cart
{
    const REDIS_KEY_PREFIX = 'zaglushka_cart';

    const LIMIT = 1000;

    private static function getKey()
    {
        return self::REDIS_KEY_PREFIX . '_' . Session::getId();
    }

    /**
     * Get all.
     *
     * @return array
     */
    public static function all() : array
    {
        return Redis::hGetAll(self::getKey());
    }

    /**
     * Get all.
     *
     * @return int
     */
    public static function count() : int
    {
        $cart  = self::all();
        $count = 0;

        foreach ($cart as $elem) {
            $count += (int) unserialize($elem)['quantity'];
        }

        return $count;
    }

    /**
     * Get item from cart.
     *
     * @param int $id
     * @param int $color
     *
     * @return null|int
     */
    public static function get(int $id, int $color) : ?int
    {
        $Current = Redis::hGet(self::getKey(), $id . '_' . $color);

        if ($Current) {
            return (int) unserialize($Current)['quantity'];
        }

        return null;
    }

    /**
     * Set cart color quantity.
     *
     * @param int $id
     * @param int $color
     * @param int $quantity
     *
     * @return int
     */
    public static function set(int $id, int $color, int $quantity) : int
    {
        if ((int) $quantity <= 0) {
            return self::remove($id, $color);
        }

        // can change old or add now
        if (self::get($id, $color) !== null || Redis::hLen(self::getKey()) < self::LIMIT) {
            Redis::hSet(
                self::getKey(),
                $id . '_' . $color,
                serialize(['id' => (int) $id, 'color' => (int) $color, 'quantity' => (int) $quantity])
            );
            Redis::expire(self::getKey(), 24 * 60 * 60 * 7);
        }

        return self::count();
    }

    /**
     * Add to compare.
     *
     * @param int $id
     * @param int $color
     *
     * @return int
     */
    public static function promo(int $id, int $color) : int
    {
        Redis::hSet(self::getKey(), $id . '_' . $color, serialize(['id' => (int) $id, 'color' => (int) $color, 'quantity' => 1, 'promo' => 1]));
        Redis::expire(self::getKey(), 24 * 60 * 60 * 7);

        return self::count();
    }

    /**
     * Remove from cart.
     *
     * @param int $id
     * @param int $color
     *
     * @return int count
     */
    public static function remove(int $id, int $color) : int
    {
        Redis::hDel(self::getKey(), $id . '_' . $color);

        return self::count();
    }

    /**
     * Flush by key.
     *
     * @return int count
     */
    public static function flush() : int
    {
        return Redis::del(self::getKey());
    }

    /**
     * @return OrderItem[]
     */
    public static function items() : array
    {
        $cart = self::all();

        if (count($cart) > 0) {
            $cartItems = array_map(function (string $cartString) {
                return unserialize($cartString);
            }, $cart);

            $OrderItems = [];
            foreach ($cartItems as $cartItem) {
                $Color = ItemColor::where(['item_id' => $cartItem['id'], 'color' => $cartItem['color']])->first();

                if (!$Color) {
                    continue;
                }

                $Color->quantity = $cartItem['quantity'];

                $OrderItem = new OrderItem([
                    'item_id'        => $cartItem['id'],
                    'title'          => $Color->code,
                    'image'          => $Color->item->isImageExists(Item::IMAGE_TYPE_5) ? $Color->item->getImagePath(Item::IMAGE_TYPE_5) : '',
                    'color'          => $Color->color,
                    'quant'          => $cartItem['quantity'],
                    'discount'       => 0,
                    'original_price' => 0,
                ]);

                $wholeSalePrice   = 'price' . $OrderItem->getWholeSaleGroup();
                $OrderItem->price = $Color->getPrice($Color->item->$wholeSalePrice);

                if (isset($cartItem['promo']) && $cartItem['promo'] && $cartItem['quantity'] == 1 && $Color->item->promo) {
//                    $OrderItem->original_price = $OrderItem->price;
                    $OrderItem->price = 0;
                    $OrderItem->promo = true;
                } elseif ($Color->main && $Color->item->special) {
                    if ($cartItem['quantity'] > $Color->remains) {
                        $OrderItem->quant = $Color->remains;

                        $NoSaleOrderItem        = new OrderItem($OrderItem->getAttributes());
                        $NoSaleOrderItem->quant = $cartItem['quantity'] - $OrderItem->quant;

                        $wholeSalePrice         = 'price' . $NoSaleOrderItem->getWholeSaleGroup();
                        $NoSaleOrderItem->price = $Color->getPrice($Color->item->$wholeSalePrice);
                    }

                    $wholeSalePrice            = 'price' . $OrderItem->getWholeSaleGroup();
                    $OrderItem->original_price = $Color->getPrice($Color->item->$wholeSalePrice);
                    $OrderItem->discount       = $Color->item->getSpecialDiscount($OrderItem->getWholeSaleGroup());
                    $OrderItem->price          = $Color->item->getDiscountPrice($OrderItem->getWholeSaleGroup());
                }

                $OrderItems[] = $OrderItem;

                if (isset($NoSaleOrderItem)) {
                    $OrderItems[] = $NoSaleOrderItem;
                    unset($NoSaleOrderItem);
                }
            }

            return $OrderItems;
        }

        return [];
    }
}
