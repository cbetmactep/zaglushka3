<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

/**
 * App\User.
 *
 * @property int            $id
 * @property string         $name
 * @property string         $email
 * @property string         $password
 * @property string         $remember_token
 * @property int            $role
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    const ROLE_USER = 1;
    const ROLE_ADMIN = 2;

    /**
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role', // todo -
    ];

    public function isHaveRinAccess() : bool
    {
        return $this->hasAnyRole('admin|content|manager|seo');
    }

    public function isAdmin() : bool
    {
        return $this->hasRole('admin');
    }

    public function isContent() : bool
    {
        return $this->hasAnyRole('admin|content');
    }

    public function isSeo() : bool
    {
        return $this->hasAnyRole('admin|seo');
    }

    public function isManager() : bool
    {
        return $this->hasAnyRole('admin|manager');
    }
}
