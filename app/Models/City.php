<?php
declare(strict_types=1);

namespace App\Models;

use App\Enum\Subjects;
use Cache;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\City.
 *
 * @property int $id
 * @property string  $title
 * @property string  $post_index
 * @property int $free_delivery_price
 * @property string  $phone
 * @mixin \Eloquent
 */
class City extends Model
{
    const CITY_PERM = 43;
    const CITY_SPB = 25;
    const CITY_EKB = 26;
    const CITY_VORONEZH = 40;

    const COUNTRY_RUSSIA = 1;
    const COUNTRY_KAZAKHSTAN = 2;
    const COUNTRY_BELARUS = 3;
    const COUNTRY_UKRAINE = 4;

    const CACHE_TIME = 60 * 24 * 7;
    public $timestamps = false;

    /**
     * @param int $countryId
     *
     * @return string
     */
    public static function getCountryName(int $countryId) : string
    {
        switch ($countryId) {
            case self::COUNTRY_RUSSIA: return 'Россия';
            case self::COUNTRY_KAZAKHSTAN: return 'Казахстан';
            case self::COUNTRY_BELARUS: return 'Беларусь';
            case self::COUNTRY_UKRAINE: return 'Украина';
        }
    }

    /**
     * @param mixed $id
     *
     * @return null|City
     *
     * @internal param array $columns
     */
    public static function cachedOne(int $id) : ?City
    {
        return Cache::remember('city_' . $id, self::CACHE_TIME, function () use ($id) {
            return self::find($id);
        });
    }

    /**
     * @param string $title
     *
     * @return null|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|static|static[]
     *
     * @internal param array $columns
     */
    public static function cachedByTitle(string $title) : ?City
    {
        return Cache::remember('city_by_title_' . $title, self::CACHE_TIME, function () use ($title) {
            return self::where(['title' => $title])->first();
        });
    }

    /**
     * Get all of the models from the database.
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function cachedAll() : Collection
    {
        return Cache::remember('cities', self::CACHE_TIME, function () {
            return self::orderBy('title', 'ASC')->get();
        });
    }

    /**
     * @return int тип текущей модели
     */
    public static function getType() : int
    {
        return Subjects::SUBJECT_CITY;
    }
}
