<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ItemComplex.
 *
 * @property int $id
 * @property int $item_id
 * @property int $complect_item_id
 */
class ItemComplex extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'items_complect_items';
    public $timestamps = false;

    /**
     * @param Item       $Item
     * @param Collection $ComplexItems
     *
     * @return bool
     */
    public static function updateComplexItem(Item $Item, Collection $ComplexItems) : void
    {
        $Item->price0 = array_sum($ComplexItems->pluck('price0')->toArray()) * 0.95;
        $Item->price1 = array_sum($ComplexItems->pluck('price1')->toArray()) * 0.95;
        $Item->price2 = array_sum($ComplexItems->pluck('price2')->toArray()) * 0.95;
        $Item->price3 = array_sum($ComplexItems->pluck('price3')->toArray()) * 0.95;
        $Item->price4 = array_sum($ComplexItems->pluck('price4')->toArray()) * 0.95;
        $Item->price5 = array_sum($ComplexItems->pluck('price5')->toArray()) * 0.95;

        $Item->save();

        $Colors = $Item->itemColors;

        foreach ($Colors as $Color) {
            $tmpColors = [];
            foreach ($ComplexItems as $ComplexItem) {
                $ComplexItemColor = $ComplexItem->itemColors()->where(['color' => $Color->color])->first();

                if (!$ComplexItemColor) {
                    $ComplexItemColor = $ComplexItem->itemColors()->where(['color' => 6])->first(); // металл
                }

                $tmpColors[] = $ComplexItemColor ? $ComplexItemColor->remains : 0;
            }

            $Color->remains = min($tmpColors);
            $Color->updated = 2;
            $Color->save();
        }
    }
}
