<?php
declare(strict_types=1);

namespace App\Models;

use Cache;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\ItemColor.
 *
 * @property int $id
 * @property int $item_id
 * @property int $color
 * @property string $code
 * @property int $remains
 * @property bool $main
 * @property bool $add_price
 * @property bool $updated
 * @property int $ord
 * @property Item $item
 * @property Item $prop_features
 */
class ItemColor extends Model
{
    const CACHE_TIME = 10080;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'items_colors';
    public $timestamps = false;

    public static $additionalPrices = [
        0 => 0,
        1 => 20,
        2 => 30,
        7 => 35,
        8 => 40,
        3 => 50,
        4 => 70,
        5 => 100,
        6 => 200
    ];

    public function getTransitItems()
    {
        return Cache::remember('items_transit_color_' . $this->id, self::CACHE_TIME, function () {
            return ItemTransit::where(['item_id' => $this->item_id, 'code' => $this->code])->get();
        });
    }

    /**
     * @param float $price
     *
     * @return float
     */
    public function getPrice(float $price) : float
    {
        return round(((self::$additionalPrices[$this->add_price] / 100) + 1) * $price, 1);
    }
    
    public function features() : ?Property
    {
        return Property::find($this->prop_features);
    }

    /**
     * @return Item
     */
    public function item() : BelongsTo
    {
        return $this->belongsTo(Item::class)->where(['enabled' => 1]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany дочерние итемы (цветные)
     */
    public function complexColors() : Collection
    {
        return ItemComplexColor::where(['complex_item_id' => $this->item_id, 'complex_item_color' => $this->color])->get();
    }

    /**
     * @param int $wholeSaleGroup
     *
     * @return int
     */
    public function getSpecialDiscount(int $wholeSaleGroup) : int
    {
        if (!$this->main) {
            return 0;
        }

        return $this->item->getSpecialDiscount($wholeSaleGroup);
    }
}
