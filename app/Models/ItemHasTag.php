<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ItemHasTag.
 *
 * @property int $id
 * @property int $item_id
 * @property int $tag_id
 */
class ItemHasTag extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'items_tags';
    public $timestamps = false;
}
