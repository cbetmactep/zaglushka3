<?php
declare(strict_types=1);

namespace App\Models;

use App\Enum\Subjects;
use Cache;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Files\Interfaces\Attachable;

/**
 * @property int $id
 * @property string $title
 * @property string $text
 * @property string $add_text_1
 * @property string $add_text_2
 * @property int $subject_id
 * @property int $object_id
 * @property int $size
 * @property int $created_at
 * @property int $updated_at
 * @property int $deleted_at
 * @property int $category_id
 * @property int $tag_category_id
 * @property int icon_id_1
 * @property int icon_id_2
 * @property string additional_info
 */
class ItemHot extends Model implements Attachable
{
    use \Modules\Files\Traits\Attachable;
    public $storeFolder = 'hotitems';

    use SoftDeletes;

    const CACHE_TIME = 60 * 24 * 7;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hot_items';

    public static function cachedAll() : Collection
    {
        return Cache::remember('hot_items', self::CACHE_TIME, function () {
            return self::all();
        });
    }

    /**
     * @return int тип текущей модели
     */
    public static function getType() : int
    {
        return Subjects::SUBJECT_ITEM_HOT;
    }

    /**
     * @return string slug
     */
    public function getObjectUrl() : string
    {
        // todo Cache

        switch ($this->subject_id) {
            case Subjects::SUBJECT_CATEGORY:
                $Object = Category::cachedOne($this->object_id);

                if ($Object) {
                    return route('slug', $Object->slug());
                }
            case Subjects::SUBJECT_ITEM:
                $Object = Item::find($this->object_id);

                if ($Object) {
                    return route('slug', $Object->slug());
                }
            case Subjects::SUBJECT_TAG:
                $Object = Tag::cachedOne($this->object_id);
                $Category = Category::cachedOne($this->tag_category_id);

                if ($Object && $Category) {
                    return route('tags', ['categorySlug' => $Category->slug(), 'tagSlug' => $Object->slug()]);
                }
        }

        return '';
    }

    /**
     * @param int $categoryId
     *
     * @return array banners
     */
    public static function getBanners(int $categoryId) : array
    {
        return (array) Cache::remember('banner_' . $categoryId, self::CACHE_TIME, function () use ($categoryId) {

            /** @var Collection $ItemHots */
            $ItemHots = ItemHot::where(['category_id' => $categoryId])->inRandomOrder()->get();

            $Banners = [];

            foreach ($ItemHots as $itemHot) {
                if ($itemHot->size) {
                    $Banners[] = [$itemHot];
                } else {
                    $inserted = false;
                    foreach ($Banners as &$Banner) {
                        if (count($Banner) === 1 && !$Banner[0]->size) {
                            $Banner[] = $itemHot;
                            $inserted = true;
                            break;
                        }
                    }
                    if (!$inserted) {
                        $Banners[] = [$itemHot];
                    }
                }
            }

            return $Banners;
        });
    }


    /**
     * @return int
     */
    public function getTagCategoryIdAttribute() : int
    {
        $categoryId = 0;

        if ($this->additional_info && isset(json_decode($this->additional_info)->tag_category_id)) {

            $categoryId = json_decode($this->additional_info)->tag_category_id;
        }

        return (int) $categoryId;
    }
}
