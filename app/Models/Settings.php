<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int    $id
 * @property string $name
 * @property string $value
 *
 * todo выпилить
 */
class Settings extends Model
{
    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'setts';

    /**
     * @var array
     */
    protected $fillable = ['name', 'value'];

    /**
     * @param string $setting
     *
     * @return array
     */
    public static function getSetting(string $setting): array
    {
        $Settings = self::where(['name' => $setting])->pluck('value')->toArray();

        if (count($Settings) == 0) {
            $Setting = new Settings([
                'key'   => $setting,
                'value' => '',
            ]);

            $Setting->save();
        }

        return $Settings;
    }
}
