<?php
//declare(strict_types=1); // todo

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\ItemSize.
 *
 * @property int $id
 * @property int $item_id
 * @property bool $type
 * @property string  $size_1
 * @property string  $size_2
 * @property string  $size_3
 * @property string  $size_4
 * @property string  $size_5
 * @property string  $size_6
 * @property Item    $item
 */
class ItemSize extends Model
{
    const FILTER_SIZE_OR_ROD = 1;
    const FILTER_SQUARE = 2;
    const FILTER_RECTANGLE = 3;
    const FILTER_OVAL = 4;
    const FILTER_POLUOVAL = 5;
    const FILTER_SCREW_PILLAR = 6;
    const FILTER_OTHER = 7;
    const FILTER_BOLT_OR_NUT = 8;
    const FILTER_ELLIPSE = 9;
    const FILTER_NOMINAL_DIAMETER = 10;
    const FILTER_HOLE = 11;
    const FILTER_FOR_EXTERNAL_THREAD = 12;
    const FILTER_FOR_INTERNAL_THREAD = 13;
    const FILTER_HANDLE_SCREW_ = 14;
    const FILTER_PEN_NUT = 15;

    const FILTER_WASHER = 17;
    const FILTER_SLAT_HOLDER = 18;
    const FILTER_SLAT_HOLDER_FOR_PIPE = 19;

    public static $filterTypes = [
        1  => 'M',
        3  => 'GAS/BSP',
        5  => 'UNF/JIC',
    ];


    public static $types = [
        1  => 'круг/пруток',
        2  => 'квадрат',
        3  => 'прямоугольник',
        4  => 'овал',
        5  => 'полуовал',
        6  => 'винтовая опора',
        7  => 'другое',
        8  => 'болт/гайка',
        9  => 'эллипс',
        10 => 'ДУ',
        11 => 'отверстие',
        12 => 'под наружную резьбу',
        13 => 'под внутреннюю резьбу',
        14 => 'ручка-винт (фиксатор)',
        15 => 'ручка-гайка (барашек)',
//        16 => 'фиксатор сквозной',// 15
        17 => 'шайбы',
        18 => 'латодержатель',
        19 => 'латодержатель под трубу',
//        20 => 'колпачок ПВХ', // 12
    ];

    public static $inputs = [
        1,
        2,
        3,
        4,
        5,
        9,
        11,
        17
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'items_sizes';
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function item() : BelongsTo
    {
        return $this->belongsTo(Item::class);
    }

    /**
     * @param int   $type
     * @param int   $size
     * @param array $params
     *
     * @return array
     */
    public static function getSizes($type, $size, array $params = []) : array // todo strict types
    {
        if ($size < 1 || $size > 6) {
            return [];
        }

        // todo cache
        $itemSizes = ItemSize::where(['type' => $type])
            ->whereNotNull('size_' . $size);

        foreach ($params as $key => $value) {
            if ($value) {
                $itemSizes = $itemSizes->where([$key => $value]);
            }
        }

        $itemSizes = $itemSizes
            ->select('size_' . $size)
            ->whereExists(function ($Builder): void {  // todo type hint $Builder
                $Builder->select(DB::raw(1))
                    ->from('items')
                    ->whereRaw('items.enabled = 1 AND items_sizes.item_id = items.id');
            })
            ->distinct()
            ->get()
            ->pluck('size_' . $size)
            ->toArray();

        // sorting keys
        $itemValues = array_map(function (string $itemSize) {
            if (mb_strpos($itemSize, '/') !== false) {
                // todo facepalm
                $itemSize = eval('return('. preg_replace('/[^0-9\/]/', '+', preg_replace('/[^0-9\/\. ]/', '', $itemSize)).');');
            }

            return preg_replace('#^[^\d]+#', '', $itemSize);
        }, $itemSizes);

        $itemSizes = array_combine($itemSizes, $itemValues);

        asort($itemSizes, SORT_NUMERIC);

        $itemSizes = array_keys($itemSizes);


        $prefix = '';
        if (($type == 6 || $type == 8 || $type == 12 || $type == 13 || $type == 14 || $type == 15) && ($size == 1)) {
            $prefix = 'M';
        }

        $itemValues = array_map(function (string $itemsize) use ($prefix, $type, $size) {
            if (($type == 10) && ($size == 1)) {
                $prefix = _t('ДУ', 'search') . ' ';
                $itemsize = preg_replace('#\&quot\;#', '″', $itemsize);
            }

            if ((($type == 12) && ($size == 3 || $size == 5)) || (($type == 13) && ($size == 3 || $size == 5))) {
                $itemsize .= '″';
            }

            if (($type == 6) && ($size == 3) && mb_substr($itemsize, 0, 1) == 'D') {
                $itemsize = 'Ø' . mb_substr($itemsize, 1);
            }

            return $prefix . $itemsize;
        }, $itemSizes);

        return array_combine($itemSizes, $itemValues);
    }

    /**
     * @param int    $type
     * @param int    $size
     * @param string $query
     * @param mixed  $filters
     *
     * @return array
     */
    public static function getAutocomplete($type, $size, $query, $filters)  // todo strict types
    {
        // todo cache
        $itemSizes = ItemSize::where(['type' => $type])
            ->whereExists(function ($Builder): void { // todo type hint $Builder
                $Builder->select(DB::raw(1))
                    ->from('items')
                    ->whereRaw('items.enabled = 1 AND items_sizes.item_id = items.id');
            })
            ->whereNotNull('size_' . $size);


        if ($type == ItemSize::FILTER_HOLE) {
            foreach ($filters as $key => $value) {
                if ($value) {
                    $itemSizes = $itemSizes->where([$key => $value]);
                }
            }

            $itemSizes = $itemSizes
                ->selectRaw('MIN(size_1 * 1) as min')
                ->selectRaw('MAX(size_1 * 1) as max')
                ->groupBy('item_id')
                ->get()
                ->toArray();

            $result = [];
            foreach ($itemSizes as $itemSize) {
                $result = array_merge($result, range($itemSize['min'] * 10, $itemSize['max'] * 10));
            }

            $result = array_unique($result);

            $result = array_map(function (int $elem) {
                return $elem / 10;
            }, $result);

            $result = array_filter($result, function (string $elem) use ($query) {
                return mb_strrpos($elem, $query) === 0;
            });
        } else {
            if (in_array($type, [
                ItemSize::FILTER_RECTANGLE,
                ItemSize::FILTER_OVAL,
                ItemSize::FILTER_POLUOVAL,
                ItemSize::FILTER_ELLIPSE,
                ItemSize::FILTER_WASHER,
            ])) {
                $itemSizes2 = clone $itemSizes;

                $result = $itemSizes
                    ->where('size_1', 'like', $query . '%')
                    ->select('size_1')
                    ->distinct()
                    ->get()
                    ->pluck('size_1')
                    ->toArray();


                $result2 = $itemSizes2
                    ->where('size_2', 'like', $query . '%')
                    ->select('size_2')
                    ->distinct()
                    ->get()
                    ->pluck('size_2')
                    ->toArray();

                $result= array_unique(array_merge($result, $result2));
            } else {
                foreach ($filters as $key => $value) {
                    if ($value) {
                        $itemSizes = $itemSizes->where([$key => $value]);
                    }
                }

                $result = $itemSizes
                    ->where('size_' . $size, 'like', $query . '%')
                    ->select('size_' . $size)
                    ->distinct()
                    ->get()
                    ->pluck('size_' . $size)
                    ->toArray();
            }
        }

        sort($result);

        return $result;
    }
}
