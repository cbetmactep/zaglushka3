<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\ItemTransit.
 *
 * @property int $id
 * @property int $item_id
 * @property bool $source
 * @property string $code
 * @property string $date
 * @property int $quant
 * @property Item $item
 */
class ItemTransit extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'items_transit';
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['item_id', 'source', 'code', 'date', 'quant'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function item() : BelongsTo
    {
        return $this->belongsTo(Item::class);
    }

    /**
     * todo кеш.
     *
     * @return int
     */
    public function getColor() : ?int
    {
        $ItemColor = ItemColor::where(['item_id' => $this->item_id, 'code' => $this->code])
            ->select('color')->first();

        return $ItemColor? $ItemColor->color : null;
    }
}
