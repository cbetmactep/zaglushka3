<?php
declare(strict_types=1);

namespace App\Models;

use App\Enum\Subjects;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Property.
 *
 * @property int $id
 * @property int $type
 * @property string  $title
 */
class Property extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'props';
    public $timestamps = false;

    const TYPE_MATERIAL = 1;
    const TYPE_PACKAGE = 2;
    const TYPE_FIT = 3;
    const TYPE_FUT = 4;

    public static $aPropTitles = [ // todo не используется
        self::TYPE_MATERIAL => 'Материал', // Material          Materiale
        self::TYPE_PACKAGE => 'Тип упаковки', // Package type           Tipo di  imballaggio
        self::TYPE_FIT => 'Подходит для труб', // Suitable for pipes            Adatto per tubi
    ];

    /**
     * @return int тип текущей модели
     */
    public static function getType() : int
    {
        return Subjects::SUBJECT_PROPERTY;
    }
}
