<?php
declare(strict_types=1);

namespace App\Models;

use App;
use App\Enum\Subjects;
use App\Helpers\Formatter;
use App\Helpers\ImageCacher;
use App\Helpers\Tools;
use App\Interfaces\MetaAble;
use App\Interfaces\SlugAble;
use Cache;
use Config;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use File as FileHelper;
use Illuminate\Database\Eloquent\Relations\HasMany;
use morphos\Russian\GeographicalNamesInflection;

/**
 * App\Models\Category.
 *
 * @property int        $id
 * @property int        $parent_id
 * @property string     $title
 * @property int        $ord
 * @property string     $descr
 * @property bool    $type
 * @property string     $sort
 * @property string     $filter_title
 * @property string     $filter_descr
 * @property bool    $filter_hide
 * @property bool    $enabled
 * @property string     $title_h1
 * @property string     $meta_title
 * @property string     $meta_description
 * @property string     $meta_keywords
 * @property string     $slug
 * @property Builder    $items
 * @property Category[] $subcategories
 * @property bool    $description_acceptable_for_subdomains
 */
class Category extends Model implements MetaAble, SlugAble
{
    use App\Traits\MetaAble;
    use App\Traits\SlugAble;

    const IMAGE_PATH_LIST = '_list.jpg';
//    const IMAGE_PATH_THUMB = '_thumb.jpg';
    const IMAGE_PATH_MENU = '_menu.jpg'; // страница продукции
//     const IMAGE_PATH_MENU_LEFT = '_menu_left.svg';

    const CACHE_TIME = 60 * 24 * 7;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cats';
    public $timestamps = false;

    protected $casts = [
        'parent_id' => 'integer',
    ];

    /**
     * @return bool
     */
    public function isBreadcrumbsImageExists() : bool
    {
        return FileHelper::exists(public_path($this->getBreadcrumbsImage()));
    }

    /**
     * @return string
     */
    public function getBreadcrumbsImage() : string
    {
        return '/images/breadcrumbs/categories/' . $this->id . '.jpg';
    }

    /**
     * @return Category[]
     */
    public static function getTopCategories() : Collection
    {
        return Cache::remember('topcategories', self::CACHE_TIME, function () {
            return self::where(['parent_id' => null, 'enabled' => 1])
                ->orderBy('ord', 'DESC')
                ->get();
        });
    }

    /**
     * @param string $sort
     *
     * @return Builder
     */
    public function items($sort = null) : Builder
    {
        $Items = Item::where(['enabled' => 1])
            ->where(function (Builder $Builder): void {
                $Builder->where('cat_id', $this->id)
                    ->orWhere('add_cat_id', $this->id);
            });

        if ($sort) {
            $Items = $Items
                ->orderByRaw('-' . $sort . ' DESC')
                ->orderBy($sort, 'ASC');
        }

        $Items = $Items->selectRaw('*, IF (`price5` > 0 and `special` = 1 and ((SELECT remains FROM items_colors WHERE item_id = items.id AND main = 1) >= 10000), ROUND(price5 / 2, 1), price5) price_sort');

        return $Items
            ->orderByRaw('if (price_sort = "" or price_sort is null, 1, 0), price_sort asc');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subcategories() : HasMany
    {
        return $this->hasMany(Category::class, 'parent_id')
            ->where(['enabled' => 1])
            ->orderBy('ord', 'DESC');
    }

    /**
     * @return array
     */
    public function getSubcategoriesIds() : array
    {
        return Cache::remember('subcategories_ids_' . $this->id, self::CACHE_TIME, function () {
            return $this->subcategories()->select('id')->get()->pluck('id')->toArray();
        });
    }

    /**
     * @param string $type
     *
     * @return string
     */
    protected function getImagePath(string $type) : string
    {
        return '/images/cat/' . $this->id . $type;
    }

    /**
     * @param string $type
     *
     * @return string path to image
     */
    public function getImageLink(string $type) : string
    {
        return ImageCacher::cache($this->getImagePath($type));
    }

    /**
     * @param string $type
     *
     * @return bool
     */
    public function isImageExists(string $type) : bool
    {
        return FileHelper::exists(public_path($this->getImagePath($type)));
    }

    /**
     * @param bool $h1
     *
     * @return string translated title
     */
    public function getTitle(bool $h1 = false) : string
    {
        $title =  App::isLocale('ru') ? ($h1 ? $this->title_h1 : $this->title) : _t(
            $this->id,
            'categories_title'
        );

        return (($this->id == $title) ? ($h1 ? $this->title_h1 : $this->title) : $title);
    }

    /**
     * @return string translated title
     */
    public function getDescription() : string
    {
        $description =  App::isLocale('ru') ? $this->descr : _t($this->id, 'categories_desc');

        return (string) (($this->id == $description) ? $this->descr : $description); // todo remove cast
    }

    /**
     * @return Builder
     */
    public function getTags() : Builder
    {
        // todo кешировать эту дичь
        $categories = $this->parent_id ? [$this->id] : $this->getSubcategoriesIds();

        $itemsId = Item::where(['enabled' => 1])
            ->whereIn('cat_id', $categories)
            ->select('id')
            ->get()
            ->pluck('id')
            ->toArray();

        $itemsIdAdditional = Item::where(['enabled' => 1])
            ->whereIn('add_cat_id', $categories)
            ->select('id')
            ->get()
            ->pluck('id')
            ->toArray();

        $ItemsTags = ItemHasTag::whereIn('item_id', $itemsId)
            ->select('tag_id')
            ->distinct()
            ->get()
            ->pluck('tag_id')
            ->toArray();

        $ItemsTagsAdditional = ItemHasAddTag::whereIn('item_id', $itemsIdAdditional)
            ->select('tag_id')
            ->distinct()
            ->get()
            ->pluck('tag_id')
            ->toArray();

        $Tags = Tag::whereIn('id', array_unique(array_merge($ItemsTags, $ItemsTagsAdditional)))->orderBy('ord', 'ASC');

        return $Tags;
    }

    /**
     * @return Tag[] Tags
     */
    public function getTopTags() : Collection
    {
        return Cache::remember('top_tags_' . $this->id, self::CACHE_TIME, function () {

            /** @var Tag[] $Tags */
            return $this->getTags()->where(['top_level' => 1])->get();
        });
    }

    /**
     * @return Tag[] Tags
     */
    public function getLowTags() : Collection
    {
        return Cache::remember('low_tags_' . $this->id, self::CACHE_TIME, function () {

            /** @var Tag[] $Tags */
            return $this->getTags()->where(['top_level' => 0])->get();
        });
    }

    /**
     * @return int category id
     */
    public function getFilterId() : int
    {
        /** @var Tag[] $Tags */
        switch ($this->id) {
            case 76:
            case 132:
            case 513:
            case 489:
                return 1;
            case 77:
                return 2;
            case 78:
                return 3;
            case 79:
                return 4;
            case 80:
                return 5;
            case 93:
                return 6;
            case 37:
                return 8;
            case 522:
                return 23; // 8
            case 581:
                return 24; // 8
            case 582:
                return 25; // 8
            case 580:
                return 26; // 8
            case 464:
                return 9;
            case 366:
            case 511:
                return 10;
            case 494:
            case 247:
            case 564:
            case 565:
            case 566:
            case 567:
            case 561:
                return 11;
            case 521:
                return 12;
            case 520:
                return 13;
            case 142:
                return 14;
            case 99:
                return 15;
            case 221:
                return 16; // 15
            case 242:
                return 17;
            case 158:
            case 159:
            case 34:
                return 18;
            case 162:
                return 19;
            case 539:
                return 20; // 12
            case 562:
                return 21; // 14
            case 568:
                return 22; // 15
            default:
                return 0;
        }
    }

    /**
     * @return int тип текущей модели
     */
    public static function getType() : int
    {
        return Subjects::SUBJECT_CATEGORY;
    }

    /**
     * @return mixed
     */
    public function getMinPrice() : float
    {
        // todo 25
        return (float) Cache::remember('min_price_' . $this->getType() . '_' . $this->id, 25, function () {
            if ($this->parent_id) {
                return $this->items()->min('price5');
            }

            $prices = [];
            foreach ($this->subcategories as $Subcategory) {
                $prices[] = $Subcategory->items()->min('price5');
            }

            return min($prices);
        });
    }

    /**
     * @param mixed $id
     *
     * @return null|self
     */
    public static function cachedOne(int $id) : ?Category
    {
        return  Cache::remember('category_' . $id, self::CACHE_TIME, function () use ($id) {
            return self::where(['id' => $id, 'enabled' => 1])->first();
        });
    }

    /**
     * @param string $string
     *
     * @return string
     */
    public function replaceMetaReplacements(string $string): string
    {
        return Tools::mb_ucfirst(preg_replace(
            [
                '#\[НАЗВАНИЕ_КАТЕГОРИИ\]#',
                '#\[ОТ_МИНИМАЛЬНАЯ_ЦЕНА\]#',
                '#\[ГОРОДЕ\]#',
            ],
            [
                mb_strtolower($this->getTitle()),
                Formatter::getPriceLabel($this->getMinPrice()),
                Config::get('app.subdomain') ?
                    GeographicalNamesInflection::getCase(
                        City::cachedOne((int) session('current_city'))->title,
                        'предложный'
                    ) :
                    GeographicalNamesInflection::getCase(City::cachedOne(25)->title, 'предложный'), // Санкт-Петербург
            ],
            $string
        ));
    }

    /**
     * @return int
     */
    public function itemsCount() : int
    {
        return (int) Cache::remember('category_items_count_' . $this->id, 60 * 24, function () {

            // todo кешировать эту дичь
            $categories = $this->parent_id ? [$this->id] : $this->getSubcategoriesIds();

            $itemsId = Item::where(['enabled' => 1])
                ->whereIn('cat_id', $categories)
                ->select('id')
                ->get()
                ->pluck('id')
                ->toArray();

            $itemsIdAdditional = Item::where(['enabled' => 1])
                ->whereIn('add_cat_id', $categories)
                ->select('id')
                ->get()
                ->pluck('id')
                ->toArray();

            $itemsIds = array_unique(array_merge($itemsId, $itemsIdAdditional));

            $Quantity = (int) ItemColor::whereIn('item_id', $itemsIds)
                ->sum('remains');

            return $Quantity;
        });
    }
}
