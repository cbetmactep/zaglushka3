<?php
declare(strict_types=1);

namespace App\Models;

use App\Enum\Subjects;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use Modules\Files\Interfaces\Attachable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Order.
 *
 * @property int $id
 * @property string  $date
 * @property bool $type
 * @property string  $name
 * @property string  $zip
 * @property string  $city
 * @property string  $country
 * @property string  $address
 * @property string  $phone
 * @property string  $email
 * @property string  $person_passport_series
 * @property string  $person_passport_number
 * @property string  $company_name
 * @property string  $company_inn
 * @property string  $company_kpp
 * @property string  $company_bik
 * @property string  $company_rs
 * @property string  $comments
 * @property bool $free_delivery
 * @property string  $delivery_company
 * @property string  $delivery_type
 * @property string  $delivery_price
 * @property float   $total_price
 * @property string  $company_file
 * @property string  $payment_id
 * @property int  $status
 *
 * @property-read OrderItem[]|Collection $items
 */
class Order extends Model implements Attachable
{
    use \Modules\Files\Traits\Attachable;
    public $storeFolder = 'orders';

    public $timestamps = false;

    const TYPE_COMPANY = 1;
    const TYPE_INDIVIDUAL = 2;

    const STATUS_NEW = 1;
    const STATUS_WAITING_FOR_PAYMENT = 2;
    const STATUS_PROCESSING = 3;
    const STATUS_COMPLETED = 4;
    const STATUS_CANCELED = 5;
    const STATUS_RETURNED = 7;

    public static $deliveryPrice = [
        1  => 'круг/пруток',
        2  => 'квадрат',
        3  => 'прямоугольник',
        4  => 'овал',
        5  => 'полуовал',
        6  => 'винтовая опора',
        7  => 'другое',
        8  => 'болт/гайка',
        9  => 'эллипс',
        10 => 'ДУ',
        11 => 'отверстие',
        12 => 'под наружную резьбу',
        13 => 'под внутреннюю резьбу',
        14 => 'ручка-винт (фиксатор)',
        15 => 'ручка-гайка (барашек)',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'date',
        'type',
        'name',
        'zip',
        'city',
        'country',
        'address',
        'phone',
        'email',
        'person_passport_series',
        'person_passport_number',
        'company_name',
        'company_inn',
        'company_kpp',
        'company_bik',
        'company_rs',
        'comments',
        'free_delivery',
        'delivery_company',
        'delivery_type',
        'delivery_price',
        'total_price',
        'company_file',
        'payment_id',
    ];

    public function getLockKey(): string
    {
        return 'order_' . $this->id;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items() : HasMany
    {
        return $this->hasMany(OrderItem::class);
    }

    /**
     * @return int тип текущей модели
     */
    public static function getType() : int
    {
        return Subjects::SUBJECT_ORDER;
    }

    /**
     * @param \App\Models\OrderItem[] $OrderItems
     *
     * @return bool
     */
    public static function isFreeDeliveryImpossible(array $OrderItems)
    {
        $itemsId = [];
        foreach ($OrderItems as $OrderItem) {
            $itemsId[] = $OrderItem->item_id;
        }

        return Item::where(['no_free_delivery' => true])
            ->whereIn('id', array_unique($itemsId))
            ->exists();
    }

    /**
     * @param mixed $value
     *
     * @return string
     */
    public function getCommentsAttribute($value) : string
    {
        return (string) $value;
    }

    /**
     * @param mixed $value
     *
     * @return float
     */
    public function getTotalPriceAttribute($value) : float
    {
        return (float) $value;
    }

}
