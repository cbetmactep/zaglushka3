<?php
declare(strict_types=1);

namespace App\Console\Commands;

use App\Helpers\Formatter;
use App\Models\Category;
use Illuminate\Console\Command;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Style_Alignment;
use PHPExcel_Style_Border;
use PHPExcel_Style_Fill;
use Storage;

class CreatePricelist extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'items:createpricelist';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update pricelist';

    protected $errors = [];

    /**
     * Execute the console command.
     */
    public function handle() : void
    {
        $cExcel = new PHPExcel();
        $cExcel->setActiveSheetIndex(0);
        $cWorksheet = $cExcel->getActiveSheet();
        $cExcel->getDefaultStyle()->getAlignment()->setWrapText(true);
        $cExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $aHeaders = [
            0 => ['title' => 'Артикул', 'width' => 20],
            1 => ['title' => 'Краткое описание', 'width' => 40],
            2 => ['title' => 'Цена, руб. за 1 шт. при заказе от'],
            7 => ['title' => 'Цена в розницу', 'width' => 10],
            8 => ['title' => "Кол-во в\nуп., шт", 'width' => 10],
        ];

        $cWorksheet->getRowDimension(1)->setRowHeight(20);
        foreach ($aHeaders as $iCell => $aItem) {
            $cWorksheet->setCellValueByColumnAndRow($iCell, 1, $aItem['title']);
            if (isset($aItem['width'])) {
                $cWorksheet->getColumnDimensionByColumn($iCell)->setWidth($aItem['width']);
            }
        }

        $aHeaders = [
            2 => ['title' => '10000', 'width' => 7],
            3 => ['title' => '5000', 'width' => 7],
            4 => ['title' => '1000', 'width' => 6],
            5 => ['title' => '500', 'width' => 6],
            6 => ['title' => '100', 'width' => 6],
        ];

        $cWorksheet->getRowDimension(2)->setRowHeight(20);
        foreach ($aHeaders as $iCell => $aItem) {
            $cWorksheet->setCellValueByColumnAndRow($iCell, 2, $aItem['title']);
            if ($aItem['width']) {
                $cWorksheet->getColumnDimensionByColumn($iCell)->setWidth($aItem['width']);
            }
        }

        $cWorksheet->mergeCells('A1:A2');
        $cWorksheet->mergeCells('B1:B2');
        $cWorksheet->mergeCells('H1:H2');
        $cWorksheet->mergeCells('I1:I2');

        $cWorksheet->mergeCells('C1:G1');
        $cWorksheet->getStyle('A1:I2')->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()->setARGB('FFE8E5E5');
        $cWorksheet->getStyle('A1:I2')->getFont()->setBold(true);

        $TopLevelCategories = Category::getTopCategories();

        $r = 3;
        foreach ($TopLevelCategories as $TopLevelCategory) {
            $cWorksheet->mergeCells('A' . $r . ':I' . $r);
            $cWorksheet->getRowDimension($r)->setRowHeight(20);
            $cWorksheet->setCellValueByColumnAndRow(0, $r, $TopLevelCategory->getTitle());
            $cWorksheet->getStyle('A' . $r . ':A' . $r)->getAlignment()
                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $cWorksheet->getStyle('A' . $r . ':A' . $r)->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB('FFE8E5E5');
            $cWorksheet->getStyle('A' . $r . ':A' . $r)->getFont()->setBold(true);
            $r++;

            $Subcategories = $TopLevelCategory->subcategories;

            foreach ($Subcategories as $Subcategory) {
                $cWorksheet->mergeCells('A' . $r . ':I' . $r);
                $cWorksheet->getRowDimension($r)->setRowHeight(20);
                $cWorksheet->setCellValueByColumnAndRow(0, $r, $Subcategory->getTitle());
                $cWorksheet->getStyle('A' . $r . ':A' . $r)->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $cWorksheet->getStyle('A' . $r . ':A' . $r)->getFill()
                    ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('FFE8E5E5');
                $cWorksheet->getStyle('A' . $r . ':A' . $r)->getFont()->setBold(true);
                $r++;

                $Items = $Subcategory->items()->get();

                $sr = $r;
                foreach ($Items as $Item) {
                    $cWorksheet->getRowDimension($r)->setRowHeight(80);
                    $cWorksheet->setCellValueByColumnAndRow(0, $r, $Item->title);
                    $cWorksheet->setCellValueByColumnAndRow(
                        1,
                        $r,
                        html_entity_decode($Item->short_descr, ENT_COMPAT, 'UTF-8')
                    );
                    $cWorksheet->setCellValueByColumnAndRow(2, $r, Formatter::price($Item->price5));
                    $cWorksheet->setCellValueByColumnAndRow(3, $r, Formatter::price($Item->price4));
                    $cWorksheet->setCellValueByColumnAndRow(4, $r, Formatter::price($Item->price3));
                    $cWorksheet->setCellValueByColumnAndRow(5, $r, Formatter::price($Item->price2));
                    $cWorksheet->setCellValueByColumnAndRow(6, $r, Formatter::price($Item->price1));
                    $cWorksheet->setCellValueByColumnAndRow(7, $r, Formatter::price($Item->price0));
                    $cWorksheet->setCellValueByColumnAndRow(8, $r, $Item->pack_size);
                    $r++;
                }
                $cWorksheet->getRowDimension($r)->setRowHeight(30);
                $r++;

                $cWorksheet->getStyle('A' . $sr . ':B' . ($r - 1))->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $cWorksheet->getStyle('C' . $sr . ':I' . ($r - 1))->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            }
        }

        $cWorksheet->getStyle('A1:I' . ($r - 1))->getBorders()
            ->getAllBorders()
            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $cWorksheet->freezePane('A3');
        $cWorksheet->setSelectedCell('A1');

        $temp = sys_get_temp_dir() . '/price.xlsx';

        $cWriter = PHPExcel_IOFactory::createWriter($cExcel, 'Excel2007');
        $cWriter->save($temp);

        Storage::disk('public_files')->put('price.xlsx', file_get_contents($temp));
    }
}
