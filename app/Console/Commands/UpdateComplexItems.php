<?php
declare(strict_types=1);

namespace App\Console\Commands;

use App\Models\Item;
use App\Models\ItemComplex;
use App\Models\ItemComplexColor;
use Illuminate\Console\Command;

class UpdateComplexItems extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'items:updatecomplex';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update complex items';

    /**
     * Execute the console command.
     */
    public function handle() : void
    {
        $ItemsIds = ItemComplex::select(['item_id'])->pluck('item_id');
        $Items = Item::whereIn('id', $ItemsIds)->get();

        foreach ($Items as $Item) {
            ItemComplex::updateComplexItem($Item, $Item->ItemComplex);
        }

        $ItemsIds = ItemComplexColor::select(['complex_item_id'])->pluck('complex_item_id');
        $Items = Item::whereIn('id', $ItemsIds)->get();

        foreach ($Items as $Item) {
            ItemComplexColor::updateComplexItem($Item);
        }
    }
}
