<?php
declare(strict_types=1);

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Mail\Message;
use Mail;
use Spatie\Activitylog\Models\Activity;
use Storage;
use App\Models\Settings;

class SpecialItemsChangeAutoReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:specials_items_change_auto';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Specials items change auto report';

    /**
     * Execute the console command.
     */
    public function handle() : void
    {
        $dateFrom = new Carbon();
        $dateFrom->subDay()->startOfDay();

        $dateTo = new Carbon();
        $dateTo->subDay()->endOfDay();

        /** @var Activity[] $Items */
        $Items = Activity::where('subject_type', \Modules\Rin\Models\Item::class)
            ->where(['description' => 'updated'])
            ->whereNull('causer_id')
            ->whereBetween('created_at', [$dateFrom->subHours(3)->toDateTimeString(), $dateTo->subHours(3)->toDateTimeString()])
            ->get();

        $SpecialsChangesItems = [];

        foreach ($Items as $Item) {
            if (isset($Item->properties['attributes']['special'])) {
                if ($Item->subject->enabled) {
                    $SpecialsChangesItems[] = $Item;
                }
            }
        }

        if (count($SpecialsChangesItems) > 0) {
            //if (app()->environment() === 'production') {
                $emails = Settings::getSetting('report_specials_items_change_auto');

                Mail::send(
                    'manager.emails.specials_changes_auto',
                    ['SpecialsChangesItems' => $SpecialsChangesItems, 'date' => $dateFrom->addHours(3)->toDateString()],
                    function (Message $Message) use ($emails) : void {
                        $Message->to($emails ?: 'content@zaglushka.ru')->subject('Отчет по автоматически отключенным специальным предложениям с сайта Zaglushka.ru');
                    }
                );
            //}
        }
    }
}
