<?php
declare(strict_types=1);

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:create {name} {email} {role}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create admin';

    /**
     * Execute the console command.
     */
    public function handle() : void
    {
        $name     = $this->argument('name');
        $email    = $this->argument('email');
        $role     = $this->argument('role');
        $password = str_random(8);

        $User = User::create([
            'name'     => trim($name),
            'email'    => trim($email),
            'password' => bcrypt($password),
            'role' => 2, // todo -
        ]);

        $User->assignRole($role);

        // todo send email to user instead
        var_dump($password);
    }
}
