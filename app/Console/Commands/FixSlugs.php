<?php
declare(strict_types=1);

namespace App\Console\Commands;

use App\Enum\Subjects;
use Cache;
use Exception;
use Illuminate\Console\Command;
use App\Models\Slug as ModelSlug;
use Log;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Slug;

class FixSlugs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'slugs:fix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fix slugs';

    protected $errors = [];

    /**
     * Execute the console command.
     */
    public function handle() : void
    {
        $Monolog = Log::getMonolog();
        $Monolog->setHandlers([$handler = new StreamHandler(storage_path('/logs/slugs.log'))]);
        $handler->setFormatter(new LineFormatter(null, null, true, true));


        $redis = Cache::getRedis();
        $keys1 = $redis->keys(Cache::getPrefix() . 'slug_by_id_*');
        $keys2 = $redis->keys(Cache::getPrefix() . 'slug_by_string_*');

        foreach (array_merge($keys1, $keys2) as $key) {
            $redis->del($key);
        }

        $Objects = \App\Models\Category::all();

        foreach ($Objects as $Object) {
            $Slug = ModelSlug::where(['subject_id' => Subjects::SUBJECT_CATEGORY, 'object_id' => $Object->id])->first();
            if (!$Slug) {
                $Slug = new ModelSlug();

                $Slug->subject_id = Subjects::SUBJECT_CATEGORY;
                $Slug->object_id = $Object->id;

                $Slug->slug = str_slug(Slug::make(html_entity_decode($Object->title)));
                $Slug->current = true;

                try {
                    if (!$Slug->save()) {
                        $Monolog->error('cant save slug for category ' . $Object->id);
                    } else {
                        $this->info('fixed category ' . $Object->id);
                    }
                } catch (Exception $E) {
                    $Monolog->error('cant save slug for category ' . $Object->id);
                }
            }
        }

        $Objects = \App\Models\Tag::all();

        foreach ($Objects as $Object) {
            $Slug = ModelSlug::where(['subject_id' => Subjects::SUBJECT_TAG, 'object_id' => $Object->id])->first();

            if (!$Slug) {
                $Slug = new ModelSlug();

                $Slug->subject_id = Subjects::SUBJECT_TAG;
                $Slug->object_id = $Object->id;

                $Slug->slug = str_slug(Slug::make(html_entity_decode($Object->title)));
                $Slug->current = true;

                try {
                    if (!$Slug->save()) {
                        $Monolog->error('cant save slug for tag ' . $Object->id);
                    } else {
                        $this->info('fixed tag ' . $Object->id);
                    }
                } catch (Exception $E) {
                    $Monolog->error('cant save slug for tag ' . $Object->id);
                }
            }
        }

        $Objects = \App\Models\Item::all();

        foreach ($Objects as $Object) {
            $Slug = ModelSlug::where(['subject_id' => Subjects::SUBJECT_ITEM, 'object_id' => $Object->id])->first();

            if (!$Slug) {
                $Slug = new ModelSlug();

                $Slug->subject_id = Subjects::SUBJECT_ITEM;
                $Slug->object_id = $Object->id;

                $Slug->slug = str_slug(Slug::make(html_entity_decode(($Object->yandex_name ? $Object->yandex_name . '-' : '') . $Object->title)));
                $Slug->current = true;

                try {
                    if (!$Slug->save()) {
                        $Monolog->error('cant save slug for item ' . $Object->id);
                    } else {
                        $this->info('fixed item ' . $Object->id);
                    }
                } catch (Exception $E) {
                    $Monolog->error('cant save slug for item ' . $Object->id);
                }
            }
        }
    }
}
