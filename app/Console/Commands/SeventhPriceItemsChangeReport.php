<?php
declare(strict_types=1);

namespace App\Console\Commands;

use App\Models\Settings;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Mail\Message;
use Mail;
use Spatie\Activitylog\Models\Activity;
use Storage;

class SeventhPriceItemsChangeReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:seventh_price_items_change';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seventh price items change report';

    /**
     * Execute the console command.
     */
    public function handle() : void
    {
        $dateFrom = new Carbon();
        $dateFrom->subDay()->startOfDay();

        $dateTo = new Carbon();
        $dateTo->subDay()->endOfDay();

        /** @var Activity[] $Items */
        $Items = Activity::where('subject_type', \Modules\Rin\Models\Item::class)
            ->where(['description' => 'updated'])
            ->whereBetween('created_at', [$dateFrom->subHours(3)->toDateTimeString(), $dateTo->subHours(3)->toDateTimeString()])
            ->get();

        $Price6ChangesItems = [];

        foreach ($Items as $Item) {
            if (isset($Item->properties['attributes']['price6'])) {
                if ($Item->subject->enabled) {
                    $Price6ChangesItems[] = $Item;
                }
            }
        }

        if (count($Price6ChangesItems) > 0) {
            //if (app()->environment() === 'production') {
                $emails = Settings::getSetting('report_7th_price_change');

                Mail::send(
                    'manager.emails.7th_price',
                    ['Price6ChangesItems' => $Price6ChangesItems, 'date' => $dateFrom->addHours(3)->toDateString()],
                    function (Message $Message) use ($emails) : void {
                        $Message->to($emails ?: 'content@zaglushka.ru')->subject('Отчет по измениям седьмой цены с сайта Zaglushka.ru');
                    }
                );
            //}
        }

    }
}
