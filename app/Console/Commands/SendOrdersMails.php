<?php
declare(strict_types=1);

namespace App\Console\Commands;

use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Settings;
use Illuminate\Console\Command;
use Illuminate\Mail\Message;
use Mail;
use Storage;

class SendOrdersMails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:mail {from_num} {to_num}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Email sales report';

    protected $errors = [];

    /**
     * Execute the console command.
     */
    public function handle() : void
    {
        $Orders = Order::whereBetween('id', [(int) $this->argument('from_num'), (int) $this->argument('to_num')])->get();

//        $salesEmail = Settings::where(['name' => 'sender_email'])->first()->value;

        foreach ($Orders as $Order) {
            $OrderItems = OrderItem::where(['order_id' => $Order->id])->get();

            Mail::send(
                'order.emails.order',
                ['Order' => $Order, 'OrderItems' => $OrderItems],
                function (Message $Message) use ($Order): void {
                    $Message->to('s.kirienko@zaglushka.ru')->subject('Заказ с сайта Zaglushka.ru');
                }
            );
        }
    }
}
