<?php
declare(strict_types=1);

namespace App\Console\Commands;

use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Mail\Message;
use Mail;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Style_Alignment;
use Storage;

class WeeklySalesReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:report_weekly';

    protected $email = 'yv@zaglushka.ru'; // todo

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Email sales report';

    protected $errors = [];

    /**
     * Execute the console command.
     */
    public function handle() : void
    {
        $cExcel = new PHPExcel();
        $cExcel->setActiveSheetIndex(0);
        $cWorksheet = $cExcel->getActiveSheet();
        $cExcel->getDefaultStyle()->getAlignment()->setWrapText(true);
        $cExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $aHeaders = [
            0 => ['title' => 'Дата заказа', 'width' => 12],
            1 => ['title' => '№ заказа', 'width' => 10],
            2 => ['title' => 'Клиент', 'width' => 40],
            3 => ['title' => 'Сумма', 'width' => 10],
        ];

        $cWorksheet->getRowDimension(1)->setRowHeight(20);
        foreach ($aHeaders as $iCell => $aItem) {
            $cWorksheet->setCellValueByColumnAndRow($iCell, 1, $aItem['title']);
            if (isset($aItem['width'])) {
                $cWorksheet->getColumnDimensionByColumn($iCell)->setWidth($aItem['width']);
            }
        }

        // todo заголовок заказы с дат  по

        $dateFrom = new Carbon();
        $dateFrom->subDay()->startOfWeek();

        $dateTo = new Carbon();
        $dateTo->subDay()->endOfWeek();

        /** @var \App\Models\Order[] $Orders */
        $Orders = Order::whereBetween('date', [$dateFrom->subHours(3)->toDateTimeString(), $dateTo->subHours(3)->toDateTimeString()])
            ->whereRaw('name NOT LIKE "%test%" and name not like "тест%" and email NOT LIKE "test@%" and email NOT LIKE "content@%"')
            ->get();

        $r = 2;
        foreach ($Orders as $Order) {
            $orderDate = new Carbon($Order->date);
            $orderDate->addHours(3);

            $cWorksheet->setCellValueByColumnAndRow(0, $r, $orderDate->format('d.m.Y H:i'));
            $cWorksheet->setCellValueByColumnAndRow(1, $r, $Order->id);
            $cWorksheet->setCellValueByColumnAndRow(2, $r, ($Order->company_name ? $Order->name . ' / ' . $Order->company_name : $Order->name));
            $cWorksheet->setCellValueByColumnAndRow(3, $r, $Order->total_price);
            $r++;
        }

        $cWorksheet->freezePane('A2');
        $cWorksheet->setSelectedCell('A1');

        $temp = sys_get_temp_dir() . '/orders.xlsx';

        $cWriter = PHPExcel_IOFactory::createWriter($cExcel, 'Excel2007');
        $cWriter->save($temp);

        Storage::put('orders.xlsx', file_get_contents($temp));

        //if (app()->environment() === 'production') {
            Mail::raw(
                '',
                function (Message $Message) use ($dateFrom, $dateTo): void {
                    $Message->to('yv@zaglushka.ru')->subject('Отчет по заказам с сайта Zaglushka.ru');
                    $Message->cc('s.kirienko@zaglushka.ru')->subject('Отчет по заказам с сайта Zaglushka.ru');

                    $Message->attach(Storage::path('orders.xlsx'), [
                        'as'   => 'Отчет с сайта Заглушка.ру ' . $dateFrom->addHours(3)->toDateString() .' - ' . $dateTo->addHours(3)->toDateString() . '.xlsx',
                        'mime' => 'xlsx',
                    ]);
                }
            );
        //}
    }
}
