<?php
declare(strict_types=1);

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Spatie\ArrayToXml\ArrayToXml;
use Storage;

class CreateYML extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'items:createyml';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update yml';

    protected $errors = [];

    /**
     * Execute the console command.
     */
    public function handle() : void
    {
        $content = [
            'shop' => [
                'name' => 'Заглушка.Ру',
                'company' => 'Заглушка.Ру',
                'url' => 'https://zaglushka.ru/',
                'email' => 'root@zaglushka.ru',
                'currencies' => [
                    'currency' => [
                        '_attributes' => [
                            'id' => 'RUR',
                            'rate' => '1'
                        ]
                    ]
                ]
            ]
        ];

        foreach (\App\Models\Category::getTopCategories() as $Category) {
            $content['shop']['categories']['category'][] = [
                '_attributes' => ['id' => $Category->id],
                '_value' => $Category->getTitle()
            ];

            foreach ($Category->subcategories as $SubCategory) {
                $content['shop']['categories']['category'][] = [
                    '_attributes' => [
                        'id' => $SubCategory->id,
                        'parentId' => $Category->id
                    ],
                    '_value' => $SubCategory->getTitle()
                ];
            }
        }

        $Items = \App\Models\Item::where(['enabled' => true])->get();

        foreach ($Items as $Item) {
            $price = $Item->price0 ?: ($Item->getSpecialDiscount(5) ?: $Item->price5);
            
            if ($Item->yandex_name && $price && $price>0 && $Item->remains() && $Item->remains()>0) {
                $sales_notes='Все способы оплаты. Отгрузка товара в день заказа.';
                $maf_cats=['253','412','436','444','499','500','501','502','503','504','510','528','529','530','531','533','534','535','535','536','537','540','541','553','554','555','556','557','592','604','609','610','611','614','615','616','617','518','620'];
                if(in_array($maf_cats, $Item->cat_id)) {
                    $sales_notes='100% предоплата';
                    $delivery_options='';
                } else {
                    
                }
                $item = [
                    '_attributes' => [
                        'id' => $Item->id,
                        'available' => $Item->remains() ? 'true' : 'false',
                    ],
                    'name' => $Item->yandex_name,
                    'url' => route('slug', ['slug' => $Item->slug()]),
                    'currencyId' => 'RUR',
                    'categoryId' => $Item->cat_id,
                    'store' => 'true',
                    'pickup' => 'true',
                    'local_delivery_cost' => '200',
                    'price' => [
                        /*'_attributes' => [
                            'from' => 'true',
                        ],*/
                        '_value' => (string) $price,
                    ],
                    'delivery' => 'true',
                    'sales_notes' => 'Все способы оплаты. Отгрузка товара в день заказа.',
                ];

                if ($Item->production) {
                    if($Item->production=='BOHAI') {
                        $item['vendor'] = "Zaglushka";
                    } else {
                        $item['vendor'] = $Item->production;
                    }
                }

                if ($Item->descr) {
                    $item['description'] = $Item->descr;
                }

                foreach ($Item->getImages() as $index => $image) {
                    if ($index < 10) {
                        $item['picture'][] = asset($image['image']);
                    }
                }

                $content['shop']['offers']['offer'][] = $item;
            }
        }

        Storage::disk('public_files')->put('price.yml', ArrayToXml::convert($content, ['rootElementName' => 'yml_catalog', '_attributes' => ['date' => date("Y-m-d H:i")]], null, 'UTF-8'));
    }
}
