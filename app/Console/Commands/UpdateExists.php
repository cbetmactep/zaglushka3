<?php
declare(strict_types=1);

namespace App\Console\Commands;

use App\Models\ItemColor;
use Cache;
use DB;
use File;
use Illuminate\Console\Command;
use Log;
use Modules\Rin\Models\Item;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Spatie\ArrayToXml\ArrayToXml;
use Storage;
use Illuminate\Mail\Message;
use Mail;

class UpdateExists extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'items:exists';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update exists items';

    protected $errors = [];

    private $cFtp;

    /**
     * Execute the console command.
     */
    public function handle() : void
    {
        File::delete(storage_path('/logs/items:exists.log'));
        $notFound = [];

        $Monolog = Log::getMonolog();

        $Monolog->setHandlers([$handler = new StreamHandler(storage_path('/logs/items:exists.log'))]);

        $handler->setFormatter(new LineFormatter(null, null, true, true));

        $this->errors = [];

        $this->cFtp = @ftp_connect(getenv('FTP_HOST'), (int) getenv('FTP_PORT'));
        if (empty($this->cFtp)) {
            $this->errors[] = 'ftp open error';
        }
        if (empty($this->errors) && !@ftp_login($this->cFtp, getenv('FTP_USERNAME'), getenv('FTP_PASSWORD'))) {
            $this->errors[] = 'ftp auth error';
        }
        if (empty($this->errors) && !@ftp_pasv($this->cFtp, true)) {
            $this->errors[] = 'ftp set passive mode error';
        }

        $ostatki         = [];
        $OstatkiMAF      = [];
		$itemsInfoErrors = [];

        if (empty($this->errors)) {
            $ostatki    = $this->parseFile('ostatki.xml');
            $OstatkiMAF = $this->parseFile('OstatkiMAF.xml');
        }

        if (empty($this->errors)) {
            $remains   = [];
			$itemsInfo = [];

            foreach ($ostatki as $article => $info) {
                if (isset($remains[$article])) {
                    $remains[$article] += $info['quantity'];
                } else {
                    $remains[$article] = $info['quantity'];
                }
            }

            foreach ($OstatkiMAF as $article => $info) {
                if (isset($remains[$article])) {
                    $remains[$article] += $info['quantity'];
                } else {
                    $remains[$article] = $info['quantity'];
                }
            }

            ItemColor::where(['updated' => 1])->update(['updated' => 0]);

            $itemIdsForCleanCache = [];
            foreach ($remains as $article => $quantity) {
                $ItemColors = ItemColor::where(['code' => $article])
                    ->orWhere(['code' => str_replace(',', '.', $article)])
                    ->orWhere(['code' => str_replace('.', ',', $article)])
                    ->get();

                if (count($ItemColors) === 0) {
                    $Monolog->warning('not found: ' . $article);
                    $notFound[] = $article;

                    continue;
                }

                foreach ($ItemColors as $ItemColor) {
                    $ItemColor->remains = $quantity;
                    $ItemColor->updated = 1;

                    $ItemColor->save();

                    $itemIdsForCleanCache[] = $ItemColor->item_id;

					if (isset($ostatki[$article]) && $ostatki[$article]['props'] !== []) {
						if (!isset($itemsInfo[$ItemColor->item_id])) {
							$itemsInfo[$ItemColor->item_id] = $ostatki[$article];
						} elseif ($itemsInfo[$ItemColor->item_id]['props'] !== $ostatki[$article]['props']) {
							if (!isset($itemsInfoErrors[$ItemColor->item_id])) {
								$itemsInfoErrors[$ItemColor->item_id] = $itemsInfo[$ItemColor->item_id]['article'] . ' ' .
										json_encode($itemsInfo[$ItemColor->item_id]['props']) . "\r\n";
							}
							$itemsInfoErrors[$ItemColor->item_id] .= $article . ' ' . json_encode($ostatki[$article]['props']) . "\r\n";

							$Monolog->warning('props mismatch: ' . $article . ' (' . $ItemColor->id . ', ' .
									$ItemColor->item_id . ')' . json_encode($ostatki[$article]['props']));
						}
					}
                }
            }

            $OldColorsItemIds = ItemColor::where(['updated' => 0])->where('remains', '>', 0)->distinct()->pluck('item_id')->toArray();

            ItemColor::where(['updated' => 0])->where('remains', '>', 0)->update(['remains' => 0]);

			foreach ($itemsInfo as $itemId => $itemInfo) {
				if (!isset($itemsInfoErrors[$itemId])) {
					Item::where(['id' => $itemId])->update($itemInfo['props']);
				}
			}

            $itemIdsForCleanCache = array_unique(array_merge($itemIdsForCleanCache, $OldColorsItemIds));

            foreach ($itemIdsForCleanCache as $itemId) {
                Cache::delete('item_colors_exists_' . $itemId);
                Cache::delete('item_main_color_' . $itemId);
                Cache::delete('item_colors_available_' . $itemId);
                Cache::delete('item_colors_' . $itemId);
            }

            $Items = Item::whereNotExists(function (\Illuminate\Database\Query\Builder $Builder): void {
                $Builder->select(DB::raw(1))
                    ->from('items_colors')
                    ->whereRaw('items_colors.remains > 0 AND items_colors.item_id = items.id');
            })->where(['special' => 1])->get();

            foreach ($Items as $Item) {
                $Item->special = 0;
                $Item->save();
            }

            $this->call('items:updatecomplex');
        }

        @ftp_close($this->cFtp);

        foreach ($this->errors as $error) {
            $Monolog->error($error);
        }

        if (count($notFound) > 0) {
            $content = [];
            foreach ($notFound as $nf) {
                $content['code'][] = $nf;
            }

            Storage::put('codes.xml', ArrayToXml::convert($content, 'root', null, 'UTF-8'));
        }

		if (count($itemsInfoErrors) > 0) {
			//if (app()->environment() === 'production') {
				Mail::raw(
					join("\r\n", $itemsInfoErrors),
					function (Message $Message) use ($itemsInfoErrors): void {
						$Message->to('content@zaglushka.ru')->subject('Ошибки в данных при обновлении количества на сайте Zaglushka.ru');
						//$Message->to('andrew.diablo@gmail.com')->subject('Ошибки в данных при обновлении количества на сайте Zaglushka.ru');
					}
				);
			//}
		}
    }

    /**
     * @param string $filename
     *
     * @return float[]
     */
    private function parseFile(string $filename) : array
    {
        if (!@ftp_get($this->cFtp, storage_path('tmp/' . $filename), './' . $filename, FTP_BINARY)) {
            $this->errors[] = 'ftp download error ' . $filename;

            return [];
        }

        $cXml = simplexml_load_file(storage_path('tmp/' . $filename));

        $aRemains = [];
        foreach ($cXml->Nomenklatura as $cItem) {
            $sArticle = (string) $cItem->Attributes()->Art;

            if (empty($sArticle)) {
                continue;
            }

            $iQuantity = floor((float) str_replace(
                ',',
                '.',
                preg_replace('/[^\d,]/is', '', (string) $cItem->Attributes()->Kolvo)
            ));
			if (isset($cItem->Attributes()->KolVKor)) {
				$fPackSize = floor((float) str_replace(
					',',
					'.',
					preg_replace('/[^\d,]/is', '', (string) $cItem->Attributes()->KolVKor)
				));
			} else {
				$fPackSize = 0;
			}
			if (isset($cItem->Attributes()->GrossWeight)) {
				$fPropPackWeightGross = (float) str_replace(
					',',
					'.',
					preg_replace('/[^\d,]/is', '', (string) $cItem->Attributes()->GrossWeight)
				);
			} else {
				$fPropPackWeightGross = 0;
			}
			if (isset($cItem->Attributes()->Amount)) {
				$fPropPackVolume = (float) str_replace(
					',',
					'.',
					preg_replace('/[^\d,]/is', '', (string) $cItem->Attributes()->Amount)
				);
			} else {
				$fPropPackVolume = 0;
			}
			if (isset($cItem->Attributes()->Weight)) {
				$fPropWeight = (float) str_replace(
					',',
					'.',
					preg_replace('/[^\d,]/is', '', (string) $cItem->Attributes()->Weight)
				);
			} else {
				$fPropWeight = 0;
			}

            if (!isset($aRemains[$sArticle])) {
                $aRemains[$sArticle] = [
					'article' => $sArticle,
					'quantity' => $iQuantity,
					'props' => []
				];
				if ($fPackSize > 0) {
					$aRemains[$sArticle]['props']['pack_size'] = $fPackSize;
				}
				if ($fPropPackWeightGross > 0) {
					$aRemains[$sArticle]['props']['prop_pack_weight_gross'] = $fPropPackWeightGross;
				}
				if ($fPropPackVolume > 0) {
					$aRemains[$sArticle]['props']['prop_pack_volume'] = $fPropPackVolume;
				}
				if ($fPropWeight > 0) {
					$aRemains[$sArticle]['props']['prop_weight'] = $fPropWeight;
				}
            } else {
                $aRemains[$sArticle]['quantity'] += $iQuantity;
            }
        }

        return $aRemains;
    }
}
