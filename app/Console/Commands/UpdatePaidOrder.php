<?php
declare(strict_types=1);

namespace App\Console\Commands;

use App\Models\ItemColor;
use App\Models\ItemTransit;
use Cache;
use File;
use Illuminate\Console\Command;
use Log;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;

class UpdatePaidOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'items:paidorder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update transit items';

    protected $errors = [];

    private $cFtp;

    /**
     * Execute the console command.
     */
    public function handle() : void
    {
        /*File::delete(storage_path('/logs/items:transit.log'));

        $Monolog = Log::getMonolog();

        $Monolog->setHandlers([$handler = new StreamHandler(storage_path('/logs/items:transit.log'))]);

        $handler->setFormatter(new LineFormatter(null, null, true, true));*/

        $this->errors = [];

        $this->cFtp = @ftp_connect(getenv('FTP_HOST'), (int) getenv('FTP_PORT'));
        if (empty($this->cFtp)) {
            $this->errors[] = 'ftp open error';
        }
        if (empty($this->errors) && !@ftp_login($this->cFtp, getenv('FTP_USERNAME'), getenv('FTP_PASSWORD'))) {
            $this->errors[] = 'ftp auth error';
        }
        if (empty($this->errors) && !@ftp_pasv($this->cFtp, true)) {
            $this->errors[] = 'ftp set passive mode error';
        }

        if (empty($this->errors)) {
            $transit = $this->parseFile('z.xml');
        }

        /*if (empty($this->errors)) {
            ItemTransit::query()->delete();

            foreach ($transit as $transitItem) {
                $article = $transitItem['code'];

                $ItemColors = ItemColor::where(['code' => $article])
                    ->orWhere(['code' => str_replace(',', '.', $article)])
                    ->orWhere(['code' => str_replace('.', ',', $article)])
                    ->get();

                if (count($ItemColors) === 0) {
                    $Monolog->warning('not found: ' . $article);
                    $notFound[] = $article;

                    continue;
                }

                foreach ($ItemColors as $ItemColor) {
                    $params = [
                        'item_id' => $ItemColor->item_id,
                        'source'  => 0,
                        'code'    => $ItemColor->code,
                        'quant'   => $transitItem['quantity'],
                    ];

                    if ($transitItem['date']) {
                        $params['date'] = $transitItem['date'];
                    }

                    if (!ItemTransit::insert($params)) {
                        $Monolog->warning('Can\'t insert: ' . $ItemColor->code);
                    }
                }
            }

            $redis = Cache::getRedis();
            $keys1 = $redis->keys(Cache::getPrefix() . 'items_transit_*');
            $keys2 = $redis->keys(Cache::getPrefix() . 'items_transit_color_*');

            foreach (array_merge($keys1, $keys2) as $key) {
                $redis->del($key);
            }
        }*/

        @ftp_close($this->cFtp);

        /*foreach ($this->errors as $error) {
            $Monolog->error($error);
        }*/
    }

    /**
     * @param string $filename
     *
     * @return mixed[]
     */
    private function parseFile(string $filename) : array
    {
        if (!@ftp_get($this->cFtp, storage_path('tmp/' . $filename), './' . $filename, FTP_BINARY)) {
            $this->errors[] = 'ftp download error ' . $filename;

            return [];
        }
        $transit = [];
        /*$cXml = simplexml_load_file(storage_path('tmp/' . $filename));

        if (!count($cXml->Nomenklatura)) {
            $this->errors[] = 'nothing found in xml file ' . $filename . '.xml';

            return [];
        }

        $transit = [];
        foreach ($cXml->Nomenklatura as $cItem) {
            $sArticul = (string) $cItem->Attributes()->Art;

            if (empty($sArticul)) {
                continue;
            }

            $quantity = floor((float) str_replace(
                ',',
                '.',
                preg_replace('/[^\d,]/is', '', (string) $cItem->Attributes()->Kolvo)
            ));
            $date     = strtotime((string) $cItem->Attributes()->Data);

            $transit[] = [
                'code'     => $sArticul,
                'date'     => $date ? date('Y-m-d', $date) : date('Y-m-d'),
                'quantity' => $quantity,
            ];
        }*/

        return $transit;
    }
}
