<?php
declare(strict_types=1);

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Mail\Message;
use Mail;
use Spatie\Activitylog\Models\Activity;
use Storage;
use App\Models\Settings;

class SpecialItemsChangeReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:specials_items_change';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Specials items change report';

    /**
     * Execute the console command.
     */
    public function handle() : void
    {
        $dateFrom = new Carbon();
        $dateFrom->subDay()->startOfDay();

        $dateTo = new Carbon();
        $dateTo->subDay()->endOfDay();

        /** @var Activity[] $Items */
        $Items = Activity::where('subject_type', \Modules\Rin\Models\Item::class)
            ->where(['description' => 'updated'])
            ->whereBetween('created_at', [$dateFrom->subHours(3)->toDateTimeString(), $dateTo->subHours(3)->toDateTimeString()])
            ->get();

        $SpecialsChangesItemsAdded = [];
        $SpecialsChangesItemsRemoved = [];

        foreach ($Items as $Item) {
            if (isset($Item->properties['attributes']['special'])) {
                if ($Item->subject->enabled) {
                    if ($Item->properties['attributes']['special']) {
                        $SpecialsChangesItemsAdded[] = $Item;
                    } else {
                        $SpecialsChangesItemsRemoved[] = $Item;
                    }
                }
            }
        }

        if ((count($SpecialsChangesItemsAdded) > 0) || (count($SpecialsChangesItemsRemoved) > 0)) {
            //if (app()->environment() === 'production') {
                $emails = Settings::getSetting('report_specials_items_change');

                Mail::send(
                    'manager.emails.specials_changes',
                    ['SpecialsChangesItemsAdded' => $SpecialsChangesItemsAdded, 'SpecialsChangesItemsRemoved' => $SpecialsChangesItemsRemoved, 'date' => $dateFrom->addHours(3)->toDateString()],
                    function (Message $Message) use ($emails) : void {
                        $Message->to($emails ?: 'content@zaglushka.ru')->subject('Отчет по специальным предложениям с сайта Zaglushka.ru');
                    }
                );
            //}
        }

    }
}
