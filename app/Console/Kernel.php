<?php
declare(strict_types=1);

namespace App\Console;

use App\Console\Commands\CreatePricelist;
use App\Console\Commands\CreateUser;
use App\Console\Commands\CreateYML;
use App\Console\Commands\FixSlugs;
use App\Console\Commands\MonthlySalesReport;
use App\Console\Commands\OrderStatus;
use App\Console\Commands\SeventhPriceItemsChangeReport;
use App\Console\Commands\SpecialItemsChangeAutoReport;
use App\Console\Commands\SpecialItemsChangeReport;
use App\Console\Commands\UpdateComplexItems;
use App\Console\Commands\UpdateExists;
use App\Console\Commands\UpdateTransit;
use App\Console\Commands\WeeklySalesReport;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**r application.
     *
     * The Artisan commands provided by you
     * @var array
     */
    protected $commands = [
        UpdateExists::class,
        UpdateTransit::class,
        UpdateComplexItems::class,
        CreatePricelist::class,
        CreateYML::class,
        CreateUser::class,
        FixSlugs::class,
        WeeklySalesReport::class,
        MonthlySalesReport::class,

        SeventhPriceItemsChangeReport::class,
        SpecialItemsChangeAutoReport::class,
        SpecialItemsChangeReport::class,

        OrderStatus::class,
        // cleanup old files1
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     */
    protected function schedule(Schedule $schedule): void
    {
        $schedule->command('items:exists')->everyTenMinutes();
        $schedule->command('items:transit')->everyThirtyMinutes();
        $schedule->command('items:createpricelist')->everyThirtyMinutes();
//        $schedule->command('items:updatecomplex')->hourly();
        $schedule->command('backup:clean')->daily()->at('00:00');
        $schedule->command('backup:run')->daily()->at('01:00');

        $schedule->command('items:createyml')->hourly();

        $schedule->command('activitylog:clean')->daily();

        $schedule->command('order:report_weekly')->mondays()->at('05:00');
        $schedule->command('order:report_monthly')->monthly()->at('06:00');

        $schedule->command('report:seventh_price_items_change')->daily()->at('05:10');
        $schedule->command('report:specials_items_change_auto')->daily()->at('05:20');
        $schedule->command('report:specials_items_change')->daily()->at('05:30');

        $schedule->command('order:status')->withoutOverlapping()->everyMinute()->environments(['production']);
    }

    /**
     * Register the commands for the application.
     */
    protected function commands(): void
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
