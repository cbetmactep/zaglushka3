<?php
declare(strict_types=1);

namespace App\Http\Middleware;

use App\Models\City;
use Config;
use Exception;
use Redirect;

/**
 * Class Subdomains.
 */
class Subdomains
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param                          $next
     *
     * @return mixed
     */
    public function handle($request, $next)
    {
        if (Config::get('app.subdomain')) {
            if ($request->getHost() != Config::get('app.subdomain') . '.' . getenv('APP_HOST')) {
                return Redirect::to('https://' . Config::get('app.subdomain') . '.' . getenv('APP_HOST'));
            }

            foreach (Config::get('app.subdomains') as $key => $value) {
                if (Config::get('app.subdomain') == $value) {
                    $City = City::cachedOne($key);

                    if ($City) {
                        if (session('current_city') != $City->id) {
                            session(['current_city' => $City->id]);
                            session(['supposed_city' => $City->id]);
                        }

                        return $next($request);
                    }
                }
            }

            return Redirect::to('https://' . getenv('APP_HOST'));
        }

        if (!session('current_city') && session('supposed_city') === null) {
            $City = null;

            try {
                $geoip = geoip($request->getClientIp());

                $City  = City::where(['title' => $geoip->city, 'region' => $geoip->state_name])->first();

                if (!$City) {
                    $City  = City::where(['title' => $geoip->city])->first();
                }
            } catch (Exception $Exception) {
                // already logged by vendor
            }

            if ($City) {
                if (!$request->routeIs('change-city')) {
                    if (isset(Config::get('app.subdomains')[$City->id]) && Config::get('app.subdomain') != Config::get('app.subdomains')[$City->id]) {
                        session(['supposed_city' => 0]);

                        return Redirect::to('https://' . Config::get('app.subdomains')[$City->id] . '.' . getenv('APP_HOST'));
                    }
                }

                session(['supposed_city' => $City->id]);
                session(['current_city' => $City->id]);
            } else {
                session(['supposed_city' => 0]);
            }
        }

//        session(['current_city' => 0]);

        return $next($request);
    }
}
