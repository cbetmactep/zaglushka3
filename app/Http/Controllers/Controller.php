<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App;
use App\Models\City;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $CurrentCity = app()->isLocale('ru') && session('current_city') ? City::cachedOne((int) session('current_city')) : null;

        view()->share('CurrentCity', $CurrentCity);
    }
}
