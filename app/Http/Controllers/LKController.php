<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use Response;

class LKController extends Controller
{
    /**
     * Show the profile for the given user.
     *page__title
     * @return Response
     */
    public function index()
    {
        return response()->view('lk.index');
    }

    /**
    * Show the profile for the given user.
    *page__title
    * @return Response
    */
    public function order()
    {
        return response()->view('lk.order');
    }

    /**
     * Show the profile for the given user.
     *page__title
     * @return Response
     */
    public function search()
    {
        return response()->view('lk.search');
    }

    /**
     * Show the profile for the given user.
     *page__title
     * @return Response
     */
    public function favorites()
    {
        return response()->view('lk.favorites');
    }

    /**
     * Show the profile for the given user.
     *page__title
     * @return Response
     */
    public function popular()
    {
        return response()->view('lk.popular');
    }

    /**
     * Show the profile for the given user.
     *page__title
     * @return Response
     */
    public function requisites()
    {
        return response()->view('lk.requisites');
    }

    /**
     * Show the profile for the given user.
     *page__title
     * @return Response
     */
    public function feedback()
    {
        return response()->view('lk.feedback');
    }

    /**
     * Show the profile for the given user.
     *page__title
     * @return Response
     */
    public function settings()
    {
        return response()->view('lk.settings');
    }
}
