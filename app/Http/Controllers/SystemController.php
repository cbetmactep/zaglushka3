<?php
//declare(strict_types=1); todo

namespace App\Http\Controllers;

use App;
use App\Models\Item;
use File;
use Response;
use Storage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Http\Request;
use Symfony\Component\Process\Process;

class SystemController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param \Illuminate\Http\Request $Request
     *
     * @return Response
     */
    public function tameraUpdate(Request $Request) // todo Response
    {
        $userName = $Request->get('user_name');
        $token       = $Request->get('token');

        if ($token != '4CcpzNgVZi8buIMR27TmQvkV' || App::environment() == 'production') {
            throw new NotFoundHttpException();
        }

        if ($userName != 'root' && $userName != 'v.poluhin') {
            return response()->json([
                'text' => 'You\'re not my master, ' . $userName,
            ]);
        }

        $process = new Process('sh deploy_dev.sh');
        $process->setWorkingDirectory('/var/www/zaglushka');
        $process->run();

        return response()->json([
            'text' => $process->getOutput(),
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $Request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getArticleImages(Request $Request)
    {
        $token    = $Request->get('token');

        if ($token != '9cz2SafNvZX3k5CUFns9RuBrFPEs6jdt3Ehx') {
            throw new NotFoundHttpException();
        }

        $Items  = Item::where(['enabled' => 1])->get();
        $result = [];

        foreach ($Items as $Item) {
            if ($Item->isImageExists(Item::IMAGE_TYPE_5)) {
                foreach ($Item->itemCachedColors() as $Color) {
                    $result[] = [
                        'article' => $Color->code,
                        'image'   => $Item->getImagePath(Item::IMAGE_TYPE_5),
                        'date'    => date('d.m.Y H:i:s', File::lastModified(public_path($Item->getImagePath(Item::IMAGE_TYPE_5)))),
                    ];
                }
            }
        }

        return Response::json($result);
    }

    /**
     * @param \Illuminate\Http\Request $Request
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function getAllArticleCodes(Request $Request)
    {
        $token = $Request->get('token');

        if ($token != '9cz2SafNvZX3k5CUFns9RuBrFPEs6jdt3Ehx') {
            throw new NotFoundHttpException();
        }

        return Response::download(Storage::path('codes.xml'));
    }
}
