<?php
declare(strict_types=1);

namespace App\Http\Controllers\User\Manager;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Request;
use Response;
use App\Models\Redis\Compare as RedisCompare;

/**
 * @deprecated
 */
class CompareController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        parent::__construct();

        $this->middleware('manager');
    }

    /**
     * Show the profile for the given user.
     *
     * @return Response
     */
    public function index()
    {
        $Colors = RedisCompare::items();

        if (Request::get('excel')) {
            \App\Helpers\Excel\Compare::generate($Colors);
        }

        if (Request::get('print')) {
            return response()->view('user.compare.print', [
                'Colors' => $Colors,
            ]);
        }

        return response()->view('user.compare.index', [
            'Items' => $Colors,
        ]);
    }

    /**
     * Show the profile for the given user.
     *
     * @return Response
     */
    public function get() : JsonResponse
    {
        return response()->json([
            'Items' => RedisCompare::items(),
        ]);
    }

    /**
     * Show the profile for the given user.
     *
     * @param int $id
     * @param int $color
     *
     * @return Response
     */
    public function add(int $id, int $color) : JsonResponse
    {
        return response()->json([
            'length' => RedisCompare::add($id, $color),
        ]);
    }

    /**
     * Show the profile for the given user.
     *
     * @param int $id
     * @param int $color
     *
     * @return Response
     */
    public function remove(int $id, int $color) : JsonResponse
    {
        return response()->json([
            'length' => RedisCompare::remove($id, $color),
        ]);
    }

    public function flush()
    {
        RedisCompare::flush();

        return Redirect::route('catalog');
    }
}
