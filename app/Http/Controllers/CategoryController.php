<?php
//declare(strict_types=1); // todo

namespace App\Http\Controllers;

use App\Helpers\Tools;
use App\Models\Category;
use App\Models\Tag;
use App\Models\Production;
use App\Models\ProductionItems;
use App\Models\Item;
use Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Meta;
use Request;
use App\Helpers\Search as HelperSearch;

class CategoryController extends Controller
{
    /**.
     * Show the profile for the given user.
     *
     * @see http://zaglushka.ru/catalog.html
     *
     * @return \Illuminate\Http\Response
     */
    public function index() : \Illuminate\Http\Response
    {
		throw new NotFoundHttpException();

        return response()->view('category.catalog', [
            'TopCategories' => Category::getTopCategories(),
        ]);
    }
    
    public function test() {
        $Item=Item::where('id',1146)->get();
        $ItemProduction=ProductionItems::where('id_item',1146)->first();
        $production=Production::where('id',$ItemProduction->id_production)->first();
        echo $Item->production;
        
    }
    
    /**
     * Show the profile for the given user.
     *
     * @see ttp://zaglushka.ru/catalog/60.html
     *
     * @param int $categoryId
     *
     * @return \Illuminate\Http\Response
     */
    public function category(int $categoryId) // todo type hint response
    {
        $Category = Category::cachedOne($categoryId);

        if (!$Category) {
            throw new NotFoundHttpException();
        }

        $viewParams['Category'] = $Category;

        if (!$Category->parent_id) {
            $viewParams['Subcategories'] = $Category->subcategories;
            $viewParams['TopTags'] = $Category->getTopTags();

            return response()->view('category.subcategories', $viewParams);
        }

        if (Request::get('type')) {
            $wedge = 'custom_search_position';
            $viewParams['Items'] = HelperSearch::filterItemsBySize($Category->items($wedge));
        }

        if (!Request::get('type') || $viewParams['Items'] === false) {
            $wedge = 'custom_cat_position';
            $viewParams['Items']  = $Category->items($wedge);
            $viewParams['SimilarItems'] = [];
        } else {
            $viewParams['SimilarItems'] = HelperSearch::groupItems(HelperSearch::findItems());
            foreach ($viewParams['SimilarItems'] as $GroupCategoryId => $Group) {
                foreach ($Group as $SearchCategoryId => $value) {
                    if ($SearchCategoryId == $Category->id) {
                        unset($viewParams['SimilarItems'][$GroupCategoryId][$SearchCategoryId]);
                        if (empty($viewParams['SimilarItems'][$GroupCategoryId])) {
                            unset($viewParams['SimilarItems'][$GroupCategoryId]);
                        }

                        break;
                    }
                }
            }
        }

        if (Request::get('special')) {
            $viewParams['Items'] = HelperSearch::filterSpecial($viewParams['Items']);
        }

        if (Request::get('exist') && Request::get('transit')) {
            $viewParams['Items'] = HelperSearch::filterExistsOrTransit($viewParams['Items']);
        } else {
            if (Request::get('exist')) {
                $viewParams['Items'] = HelperSearch::filterExists($viewParams['Items']);
            }

            if (Request::get('transit')) {
                $viewParams['Items'] = HelperSearch::filterTransit($viewParams['Items']);
            }
        }

        if (Request::ajax() && Request::get('count')) {
            return $viewParams['Items']->count();
        }

        if (Request::get('excel') || Request::get('print')) {
            $viewParams['Items'] = $viewParams['Items']->get();
            $viewParams['Items'] = Tools::collectionCustomPosition($viewParams['Items'], $wedge);
        } else {
            $viewParams['Items'] = $viewParams['Items']->paginate(100);

            if (Request::get('page', 1) > $viewParams['Items']->lastPage()) {
                return \Redirect::to(request()->fullUrlWithQuery(["page" => "1"]));
            }

            $Collection          = $viewParams['Items']->getCollection();
            $viewParams['Items']->setCollection(Tools::collectionCustomPosition($Collection, $wedge));
        }

        if (Request::get('excel')) {
            \App\Helpers\Excel\Base::generate($viewParams['Items'], false, $viewParams['SimilarItems']);
        }

        if (Request::get('print')) {
            return response()->view('category.itemlist_print', $viewParams);
        }

        if (Request::ajax()) {
            return response()->view('category.itemlist_inner', [
                'Category'     => $viewParams['Category'],
                'Items'        => $viewParams['Items'],
                'SimilarItems' => $viewParams['SimilarItems'],
            ]);
        }

//        $viewParams['LowTags']        = $Category->getLowTags();
        $viewParams['ParentCategory'] = Category::cachedOne($Category->parent_id);
        $viewParams['TopTags'] = $Category->getTopTags();

        return response()->view('category.itemlist', $viewParams);
    }

    /**
     * Show the profile for the given user.
     *
     * @see http://zaglushka.ru/catalog.html
     *
     * @param int $categoryId
     * @param int $tagId
     *
     * @return Response
     *
     * todo выпилить этот метод
     */
    public function tag($categoryId, $tagId)
    {
        $Category = Category::cachedOne($categoryId);

        $Tag = Tag::cachedOne($tagId);

        if (!$Category || !$Tag) {
            throw new NotFoundHttpException();
        }

        $tagIds = array_map(function (string $elem) {
            return (int) $elem;
        }, explode(',', Request::get('tags', '')));

        array_push($tagIds, $Tag->id);

        $viewParams['Category'] = $Category;

        if (Request::get('type')) {
            $wedge = 'custom_search_position';
            $viewParams['Items'] = HelperSearch::filterItemsBySize(Tag::getItems($Category, $tagIds, $wedge));
        }

        if (!Request::get('type') || $viewParams['Items'] === false) {
            $wedge = 'custom_tag_position';
            $viewParams['Items'] = Tag::getItems($Category, $tagIds, $wedge);
        }

        if (Request::get('special')) {
            $viewParams['Items'] = HelperSearch::filterSpecial($viewParams['Items']);
        }

        if (Request::get('exist') && Request::get('transit')) {
            $viewParams['Items'] = HelperSearch::filterExistsOrTransit($viewParams['Items']);
        } else {
            if (Request::get('exist')) {
                $viewParams['Items'] = HelperSearch::filterExists($viewParams['Items']);
            }

            if (Request::get('transit')) {
                $viewParams['Items'] = HelperSearch::filterTransit($viewParams['Items']);
            }
        }

        if (Request::ajax() && Request::get('count')) {
            return $viewParams['Items']->count();
        }

        if (Request::get('excel') || Request::get('print')) {
            $viewParams['Items'] = $viewParams['Items']->get();
            $viewParams['Items'] = Tools::collectionCustomPosition($viewParams['Items'], $wedge);
        } else {
            $viewParams['Items'] = $viewParams['Items']->paginate(100);

            if (Request::get('page', 1) > $viewParams['Items']->lastPage()) {
                return \Redirect::to(request()->fullUrlWithQuery(["page" => "1"]));
            }

            $Collection          = $viewParams['Items']->getCollection();
            $viewParams['Items']->setCollection(Tools::collectionCustomPosition($Collection, $wedge));
        }

        if (Request::get('excel')) {
            \App\Helpers\Excel\Base::generate($viewParams['Items']);
        }

        Meta::set('title', $Tag->getMetaTitle($Category));
        Meta::set('description', $Tag->getMetaDescription($Category));
        Meta::set('keywords', $Tag->getMetaKeywords($Category));

        if ($Category->isImageExists(Category::IMAGE_PATH_LIST)) {
            Meta::set('image', asset($Category->getImageLink(Category::IMAGE_PATH_LIST)));
        }

        if (Request::get('print')) {
            return response()->view('category.itemlist_print', $viewParams);
        }

        $viewParams['TopTags']        = $Category->getTopTags();
        $viewParams['CurrentTag']     = $Tag;
        $viewParams['TopCategories']  = Category::getTopCategories();

        if ($Category->parent_id === null) {
            $viewParams['Subcategories'] = $Category->subcategories;

            return response()->view('category.tags_category', $viewParams);
        }

        $viewParams['ParentCategory'] = Category::cachedOne($Category->parent_id);
//        $viewParams['LowTags']        = $Category->getLowTags();
        $viewParams['Subcategories']  = $viewParams['ParentCategory']->subcategories;

        if (Request::ajax()) {
            return response()->view('components.tags_subcategory', ['CurrentTag' => $viewParams['CurrentTag'], 'Items' => $viewParams['Items']]);
        }

        return response()->view('category.tags_subcategory', $viewParams);
    }
}
