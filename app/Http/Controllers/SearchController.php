<?php
//declare(strict_types=1);  // todo

namespace App\Http\Controllers;

use App\Helpers\Excel;
use App\Models\ItemColor;
use App\Models\ItemSize;
use App\Models\Item as ModelItem;
use Cache;
use Exception;
use Illuminate\Http\Request;
use Log;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Response;
use App\Helpers\Search as HelperSearch;
use GuzzleHttp\Client;

class SearchController extends Controller
{
    const CACHE_TIME = 60 * 24 * 7;

    /**
     * Show the profile for the given user.
     *
     * @param Request $Request
     *
     * @return Response
     */
    public function index(Request $Request)
    {
        $article = $Request->get('article');

        if ($article) {
            $Items = HelperSearch::findItemsByArticle($article)->get();

            if (count($Items) == 0) {
                $Colors = HelperSearch::findColorsByArticle($article)->get();

                if ($Colors) {
                    $Items = collect($Colors->map(function (ItemColor $Color, $key) {
                        // todo ¯\_(ツ)_/¯
                        $Color->item->color = $Color;
                        $Color->item->title = $Color->code;

                        for ($i = 0; $i <= 6; $i++) {
                            $Color->item['price' . $i] = $Color->getPrice($Color->item['price' . $i]);
                        }

                        if (!$Color->main) {
                            $Color->item->special = false;
                        }

                        return $Color->item;
                    }))->unique();
                }
            }

            if (count($Items) == 0) {
                try {
                    // пробуем яндекс
                    $text = trim($article);

                    // кешируем ответ
                    $ids = Cache::remember('yandex_search_' . md5($text), self::CACHE_TIME, function () use ($text) {
                        $Client = new Client(); //GuzzleHttp\Client
                        $result = $Client->get(getenv('YANDEX_SEARCH_URL'), [
                            'query' => [
                                'apikey'   => getenv('YANDEX_SEARCH_APIKEY'),
                                'searchid' => getenv('YANDEX_SEARCH_SEARCHID'),
                                'text'     => $text,
                                'per_page' => '100',
                            ],
                        ]);

                        $result = json_decode($result->getBody()->getContents());

                        if (!isset($result->documents)) {
                            return false;
                        }

                        $ids = [];
                        foreach ($result->documents as $document) {
                            $ids[] = $document->id;
                        }

                        return $ids;
                    });

                    if ($ids && count($ids) > 0) {
                        $Items = HelperSearch::findItemsByIds($ids)->get();
                    }
                } catch (Exception $Exception) {
                    $Monolog = Log::getMonolog();

                    $Monolog->setHandlers([$handler = new StreamHandler(storage_path('/logs/search:yandex.log'))]);

                    $handler->setFormatter(new LineFormatter(null, null, true, true));

                    $Monolog->error($Exception->getMessage());
                }
            }
        } else {
            $Items = null;

            $possibles = [
                'size',
                'size_1',
                'size_2',
                'size_3',
                'size_4',
                'size_5',
                'size_6',
                'special',
                'exist',
            ];

            $req = false;
            foreach ($possibles as $possible) {
                $req = $Request->get($possible) || $req;
            }

            if ($req) {
                $Items = HelperSearch::findItems();
            }
        }

        if ($Items === null) {
            if ($Request->ajax()) {
                return Response::json([
                    'status' => 302,
                    'url' => route('search'),
                ]);
            }

            return response()->view('search.index');
        }

        $Result = HelperSearch::groupItems($Items);

        if ($Request->get('excel')) {
            Excel\Search::generate($Result);
        }

        if ($Request->get('print')) {
            return response()->view('search.result_print', ['Items' => $Result]);
        }

        if ($Request->ajax()) {
            return response()->view('components.search', ['Items' => $Result]);
        }

        return response()->view('search.result', ['Items' => $Result]);
    }

    /**
     * @param \Illuminate\Http\Request $Request
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function sizeTooltip(Request $Request)
    {
        $type  = (int) $Request->get('type');
        $size1 = (int) $Request->get('size_1');
        $size2 = (int) $Request->get('size_2');
        $size3 = (int) $Request->get('size_3');
        $size4 = (int) $Request->get('size_4');
        $size5 = (int) $Request->get('size_5');
        $size6 = (int) $Request->get('size_6');

        $response['size_1'] = ItemSize::getSizes(
            $type,
            1,
            ['size_2' => $size2, 'size_3' => $size3, 'size_4' => $size4, 'size_5' => $size5, 'size_6' => $size6]
        );

        $response['size_2'] = (($type != 12 && $type != 13) || (($type == 12 || $type == 13) && $size1)) ?
            ItemSize::getSizes($type, 2, [
                'size_1' => $size1,
                'size_3' => $size3,
                'size_4' => $size4,
                'size_5' => $size5,
                'size_6' => $size6,
            ]) : [];

        $response['size_3'] = ItemSize::getSizes(
            $type,
            3,
            ['size_1' => $size1, 'size_2' => $size2, 'size_4' => $size4, 'size_5' => $size5, 'size_6' => $size6]
        );

        $response['size_4'] = (($type != 12 && $type != 13) || (($type == 12 || $type == 13) && $size3)) ?
            ItemSize::getSizes($type, 4, [
                'size_1' => $size1,
                'size_2' => $size2,
                'size_3' => $size3,
                'size_5' => $size5,
                'size_6' => $size6,
            ]) : [];

        $response['size_5'] = ItemSize::getSizes(
            $type,
            5,
            ['size_1' => $size1, 'size_2' => $size2, 'size_3' => $size3, 'size_4' => $size4, 'size_6' => $size6]
        );

        $response['size_6'] = (($type != 12 && $type != 13) || (($type == 12 || $type == 13) && $size5)) ?
            ItemSize::getSizes($type, 6, [
                'size_1' => $size1,
                'size_2' => $size2,
                'size_3' => $size3,
                'size_4' => $size4,
                'size_5' => $size5,
            ]) : [];

        // костыли для js (аякс)
        $response2 = [];
        foreach ($response as $key1 => $value1) {
            $response2[$key1] = [];
            foreach ($response[$key1] as $key => $value) {
                $response2[$key1][] = [$key => $value];
            }
        }

        return response()->json($response2);
    }

    /**
     * @param \Illuminate\Http\Request $Request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function sizeAutocomplete(Request $Request)
    {
        $type  = (int) $Request->get('type');
        $query = $Request->get('query');

        $filters = [
            'size_1' => $Request->get('size_1'),
            'size_2' => $Request->get('size_2'),
            'size_3' => $Request->get('size_3'),
            'size_4' => $Request->get('size_4'),
            'size_5' => $Request->get('size_5'),
            'size_6' => $Request->get('size_6'),
        ];

        unset($filters['size_' . $query]);

        $response = [];

        $response['size_' . $query] = ItemSize::getAutocomplete(
            $type,
            $query,
            str_replace(',', '.', $Request->get('size_' . $query)),
            $filters
        );

        return response()->json($response);
    }

    /**
     * @param \Illuminate\Http\Request $Request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function articleAutocomplete(Request $Request)
    {
        $article = $Request->get('article');

        $Items = ModelItem::where(['enabled' => 1])
            ->where('title', 'LIKE', $article . '%')
            ->limit(50)
            ->orderBy('title', 'ASC')
            ->select('title')
            ->get()
            ->pluck('title')
            ->toArray();

        return $Items;
    }
}
