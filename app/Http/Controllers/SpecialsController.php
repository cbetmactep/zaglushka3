<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Item as ItemModel;
use Request;
use Response;

class SpecialsController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @return Response
     */
    public function index()
    {
        $Items = ItemModel::where(['special' => true, 'enabled' => 1]);

        $Items = $Items->selectRaw('*, IF (`price5` > 0 and `special` = 1 and ((SELECT remains FROM items_colors WHERE item_id = items.id AND main = 1) >= 10000), ROUND(price5 / 2, 1), price5) price_sort');

        $Items = $Items
            ->orderByRaw('if (price_sort = "" or price_sort is null, 1, 0), price_sort asc');

        if (Request::get('excel')) {
            \App\Helpers\Excel\Base::generate($Items->get());
        }

        if (Request::get('print')) {
            return response()->view('category.itemlist_print', [
                'Items' => $Items->get(),
                'search' => false,
            ]);
        }

        return response()->view('specials.index', [
            'Items' => $Items->paginate(100),
        ]);
    }
}
