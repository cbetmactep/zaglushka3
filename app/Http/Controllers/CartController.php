<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Order;
use Illuminate\Http\JsonResponse;
use App\Models\Redis\Cart as RedisCart;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CartController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @return Response
     */
    public function index(Request $Request) : Response
    {
        $OrderItems = RedisCart::items();

        $viewParams = [
            'OrderItems'               => $OrderItems,
            'freeDeliveryIsImpossible' => Order::isFreeDeliveryImpossible($OrderItems),
        ];

        if ($Request->get('excel')) {
            \App\Helpers\Excel\Cart::generate($OrderItems, $viewParams['freeDeliveryIsImpossible']);
        }

        if ($Request->get('print')) {
            return response()->view('cart.print', $viewParams);
        }

        return response()->view('cart.index', $viewParams)->header('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
    }

    /**
     * Get quantity.
     *
     * @param int $id
     * @param int $color
     *
     * @return JsonResponse
     */
    public function get(int $id, int $color) : JsonResponse
    {
        return response()->json([
            'quantity' => RedisCart::get($id, $color),
        ]);
    }

    /**
     * Set quantity.
     *
     * @param int $id
     * @param int $color
     *
     * @return JsonResponse
     */
    public function promo(int $id, int $color) : JsonResponse
    {
        return response()->json([
            'quantity' => RedisCart::promo($id, $color),
        ]);
    }

    /**
     * Show the profile for the given user.
     *
     * @param int $id
     * @param int $color
     * @param int $quantity
     *
     * @return JsonResponse
     */
    public function set(int $id, int $color, int $quantity) : JsonResponse
    {
        $quantity = RedisCart::set($id, $color, $quantity);

        $OrderItems               = RedisCart::items();
        $isFreeDeliveryImpossible = Order::isFreeDeliveryImpossible($OrderItems);

        $totalPrice = 0;
        foreach ($OrderItems as $OrderItem) {
            $totalPrice += ($OrderItem->price * $OrderItem->quant);
        }

        /** @var \App\Models\City $City */
        $City = City::cachedOne((int) session('current_city'));

        return response()->json([
            'quantity'       => $quantity,
            'isFreeDelivery' => $City && $City->free_delivery_price && (!$isFreeDeliveryImpossible) && ($totalPrice >= $City->free_delivery_price),
            'notEnough'      => $City && $City->free_delivery_price && (($City->free_delivery_price - $totalPrice) > 0) ? round($City->free_delivery_price - $totalPrice) : 0,
        ]);
    }

    /**
     * @return JsonResponse
     */
    public function flush() : JsonResponse
    {
        return response()->json([
            'success' => (RedisCart::flush() >= 0),
        ]);
    }
}
