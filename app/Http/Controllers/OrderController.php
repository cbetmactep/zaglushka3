<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App;
use App\Helpers\Ship2Go;
use App\Models\City;
use App\Models\Item as ModelItem;
use App\Models\Order;
use App\Models\Settings;
use App\Services\YandexMoneyService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Mail;
use App\Models\Redis\Cart as RedisCart;
use Redirect;
use Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class OrderController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @return Response
     */
    public function index(Request $Request)
    {
        $OrderItems = RedisCart::items();

        if (count($OrderItems) == 0) {
            return Redirect::route('cart');
        }

        $Order = new Order();

        foreach ($OrderItems as $OrderItem) {
            $Order->total_price += ($OrderItem->price * $OrderItem->quant);
        }

        $viewParams = [
            'Order'                    => $Order,
            'OrderItems'               => $OrderItems,
            'freeDeliveryIsImpossible' => Order::isFreeDeliveryImpossible($OrderItems),
        ];

        if ($Request->get('excel')) {
            \App\Helpers\Excel\Cart::generate($OrderItems, $viewParams['freeDeliveryIsImpossible']);
        }

        if ($Request->get('print')) {
            return response()->view('order.print', $viewParams);
        }

        return response()->view('order.index', $viewParams)->header(
            'Cache-Control',
            'no-store, no-cache, must-revalidate, post-check=0, pre-check=0'
        );
    }

    /**
     * Show the profile for the given user.
     *
     * @param \Illuminate\Http\Request $Request
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function send(Request $Request)
    {
        $File        = null;
        $Order       = new Order();
        $Order->date = date("Y-m-d H:i:s");
        $Order->type = (int) $Request->get('type');
        $payOnline   = $Request->get('person_pay_online');

        // адрес
        $Order->delivery_type = $Request->get('address');
        //тип
        $Order->delivery_company = $Request->get('delivery_company');
        $Order->delivery_price   = 'Расчет менеджером';

        if ($Order->type === Order::TYPE_COMPANY) { // company
            $File = $Request->file('company_file');

            $orderAttributes = [
                'name'  => $Request->get('company_person_name'),
                'phone' => $Request->get('company_phone'),
                'email' => $Request->get('company_email'),

                'company_name' => $Request->get('company_name'),
                'company_inn'  => $Request->get('company_inn'),
                'company_kpp'  => $Request->get('company_kpp'),
                'company_bik'  => $Request->get('company_bik'),
                'company_rs'   => $Request->get('company_rs'),

                'country' => $Request->get('country'),
                'city'    => $Request->get('city'),
                'zip'     => $Request->get('index'),
                'address' => $Request->get('company_address'),

                'comments' => $Request->get('comments'),
            ];
        } elseif ($Order->type === Order::TYPE_INDIVIDUAL) { // физическое лицо
            $orderAttributes = [
                'name'     => $Request->get('person_name'),
                'country'  => $Request->get('country'),
                'city'     => $Request->get('city'),
                'zip'      => $Request->get('index'),
                'address'  => $Request->get('address'),
                'phone'    => $Request->get('person_phone'),
                'email'    => $Request->get('person_email'),
                'comments' => $Request->get('comments'),
            ];
        } else {
            throw new Exception('Unknown type');
        }

        $Order->fill($orderAttributes);

        $OrderItems = RedisCart::items();

        if (count($OrderItems) === 0) {
            return Redirect::route('cart');
        }

        /** @var ModelItem $Item */
        /** @var \App\Models\ItemColor $Color */
        $Order->save();

        if ($File) {
            if (!$Order->addFile($File)) {
                $File = null;
            }
        }

        $totalPrice = 0;
        foreach ($OrderItems as $OrderItem) {
            $OrderItem->order_id = $Order->id;
            $totalPrice          += ($OrderItem->price * $OrderItem->quant);
            $OrderItem->save();
        }

        $Order->total_price = $totalPrice;

        /** @var \App\Models\City $City */
        $City = City::where(['title' => $Order->city])->first();
        if ($City) {
            $totalPriceWithoutDiscount = 0;
            foreach ($OrderItems as $OrderItem) {
                $totalPriceWithoutDiscount += ($OrderItem->discount == 0) ? ($OrderItem->price * $OrderItem->quant) : 0;
            }

            if ($City->free_delivery_price && $totalPriceWithoutDiscount >= $City->free_delivery_price && !Order::isFreeDeliveryImpossible($OrderItems)) {
                $Order->free_delivery  = true;
                $Order->delivery_price = 'Бесплатно';
            }
        }

        $Order->save();

        RedisCart::flush();

        try {
            $salesEmails = [];
            //if (App::environment() === 'production') {
                $salesEmails = Settings::getSetting('sender_email');
                $salesEmails[] = 'content@zaglushka.ru';
                $salesEmails = array_unique($salesEmails);
            //}

            Mail::send(
                'order.emails.order',
                ['Order' => $Order, 'OrderItems' => $OrderItems],
                function (Message $Message) use ($File, $Order, $salesEmails): void {
                    $Message->to($Order->email)->subject('Заказ с сайта Zaglushka.ru');
                    $Message->bcc($salesEmails)->subject('Заказ с сайта Zaglushka.ru');

                    if ($File) {
                        $Message->attach($File->getRealPath(), [
                            'as'   => 'attached_file.' . $File->getClientOriginalExtension(),
                            'mime' => $File->getMimeType(),
                        ]);
                    }
                }
            );
        } catch (Exception $Exception) {
            //todo log
        }

        //оплата через як
        if ($Order->type === Order::TYPE_INDIVIDUAL && $payOnline) {
            /** @var YandexMoneyService $yandexService */
            $yandexService = app(App\Services\YandexMoneyService::class);

            $payment = $yandexService->createPaymentFromOrder($Order);

            $Order->status = Order::STATUS_WAITING_FOR_PAYMENT;
            $Order->payment_id = $payment['id'];
            if ($Order->saveOrFail()) {
                return redirect($payment['confirmation_url']);
            }
        }

        return redirect()->route('result', ['OrderId' => $Order->id, 'Date' => $Order->date]);
    }

    /**
     * @return array
     */
    public function calcDelivery(Request $Request)
    {
        $cityTo     = (string) $Request->get('city_to');
        $OrderItems = RedisCart::items();

		if ($cityTo === '' || !is_numeric($cityTo)) {
			throw new NotFoundHttpException();
		}

        $isFreeDeliveryImpossible = Order::isFreeDeliveryImpossible($OrderItems);
        $freeDelivery             = false;

        if (!$isFreeDeliveryImpossible) {
            $totalPriceWithoutDiscount = 0;
            foreach ($OrderItems as $OrderItem) {
                $totalPriceWithoutDiscount += ($OrderItem->discount == 0) ? ($OrderItem->price * $OrderItem->quant) : 0;
            }

            /** @var \App\Models\City $City */
            $City = City::where(['post_index' => $cityTo])->first();
            if ($City && ($totalPriceWithoutDiscount >= $City->free_delivery_price)) {
                $freeDelivery = true;
            }
        }

        return [
            'minDeliveryPrice'         => (int) (($isFreeDeliveryImpossible || $freeDelivery) ? 0 : Ship2Go::calc($cityTo)),
            'freeDeliveryIsImpossible' => $isFreeDeliveryImpossible,
            'freeDelivery'             => (int) $freeDelivery,
        ];
    }

    /**
     * Show the profile for the given user.
     *
     * @param \Illuminate\Http\Request $Request
     *
     * @return \Response
     */
    public function result(Request $Request)
    {
        if (!$Request->get('OrderId')) {
            abort(404);
        }

        return response()->view('order.result');
    }

    public function resultPayment(int $orderId)
    {
        $order = Order::findOrFail($orderId);

        /** @var App\Services\OrderService $orderService */
        $orderService = app(App\Services\OrderService::class);

        $order = $orderService->checkPayment($order);

        return response()->view('order.result_payment', ['order' => $order]);
    }
}
