<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Category;
use Meta;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Models\Item as ModelItem;
use Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use App\Models\ItemColor;
use App\Models\Color;

class ItemController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @see http://www.zaglushka.ru/catalog/76/1192.html
     *
     * @param int $id
     *
     * @return \Response
     */
    public function index($id)
    {
        $Item = ModelItem::where(['id' => $id, 'enabled' => true])->first();

        if (!$Item) {
            throw new NotFoundHttpException();
        }

        foreach ($Item->getImages() as $image) {
            Meta::set('image', asset($image['image']));
        }

        $Colors = $Item->itemCachedColors();

        // todo сортировка?

        $viewParams = [
            'Item'       => $Item,
            'images'     => $Item->getImages(),
            'ItemColors' => $Colors,
        ];

        if (Request::get('excel')) {
            \App\Helpers\Excel\Item::generate($Item);
        }

        /** @var Category $Category */
        $Category = $Item->category;

        Meta::set('title', $Item->getMetaTitle($Category));
        Meta::set('description', $Item->getMetaDescription($Category));
        Meta::set('keywords', $Item->getMetaKeywords($Category));

        if (Request::get('print')) {
            return response()->view('item.print', $viewParams);
        }

        $viewParams['Category']       = $Category;
        $viewParams['TopCategories']  = Category::getTopCategories();
        $viewParams['Files']          = $Item->fullImages;
        $viewParams['ParentCategory'] = Category::cachedOne($Category->parent_id);
        $viewParams['TopTags']        = $Category->getTopTags();

        return view('item.index', $viewParams);
    }
    
    public function get($id) {
        $Item = ModelItem::where(['id' => $id, 'enabled' => true])->first();
        $images = $Item->getImages();
        $Colors = $Item->itemCachedColors();
        $item_color=[];
        foreach($Colors as $color) {
            $row=[];
            $row['dataarticle']=$color->code;
            $row['value']=$color->color;
            $row['dataprice0']=$color->getPrice($Item->price0);
            $row['dataprice1']=$color->getPrice($Item->price1);
            $row['dataprice2']=$color->getPrice($Item->price2);
            $row['dataprice3']=$color->getPrice($Item->price3);
            $row['dataprice4']=$color->getPrice($Item->price4);
            $row['dataprice5']=$color->getPrice($Item->price5);
            $row['dataprice6']=$color->getPrice($Item->price6);
            $row['name']=Color::cachedOne($color->color)->name;
            $row['dataremains']=$color->remains;
            $item_color[]=$row;
            unset($row);
        }
        return response()->json([
            'title'         => $Item->title,
            'short_descr'   => $Item->short_descr,
            'photo'         => $images,
            'colors'        => $item_color,
        ]);
    }
}
