<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use Response;

class PageController extends Controller
{
    /*
     * Show page about company new version
     */
    public function aboutnew() {
        return response()->view('pages.aboutnew');
    }
    /**
     * Show the profile for the given user.
     *
     * @return Response
     */
    public function about()
    {
        return response()->view('pages.about');
    }
    /**
     * Show the profile for the given user.
     *
     * @return Response
     */
    public function pay()
    {
        return response()->view('pages.pay');
    }

    /**
     * Show the profile for the given user.
     *
     * @return Response
     */
    public function contacts()
    {
        return response()->view('pages.contacts');
    }

    /**
     * Show the profile for the given user.
     *
     * @return Response
     */
    public function delivery()
    {
        return response()->view('pages.delivery');
    }
}
