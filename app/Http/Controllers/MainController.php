<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\City;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class MainController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @return Response
     */
    public function index() : Response
    {
        return response()->view('pages.main', [
            'categories' => Category::getTopCategories(),
        ]);
    }

    /**
     * @param                          $cityId
     * @param \Illuminate\Http\Request $Request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changeCity(int $cityId, Request $Request) : RedirectResponse
    {
        session(['supposed_city' => 0]);

        $City = City::cachedOne($cityId);

        if ($City) {
            if (isset(config()->get('app.subdomains')[$City->id])) {
                if (config()->get('app.subdomain') != config()->get('app.subdomains')[$City->id]) {
                    return redirect()->to('https://' . config()->get('app.subdomains')[$City->id] . '.' . getenv('APP_HOST') . '/change-city/' . $City->id);
                }
            } else {
                if (config()->get('app.subdomain')) {
                    return redirect()->to('https://' . getenv('APP_HOST') . '/change-city/' . $City->id);
                }
            }

            session(['current_city' => $City ? $City->id : 0]);
        }

        $redirect = '/';
        if (Request::create(url()->previous())->getHttpHost() == $Request->getHttpHost()) {
            $redirect = url()->previous();
        }

        return redirect()->to($redirect);
    }
}
