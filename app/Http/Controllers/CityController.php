<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Helpers\Tools;
use App\Models\City;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CityController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $Request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function autocomplete(Request $Request) : JsonResponse
    {
        $county  = (int) $Request->get('country');
        $query = $Request->get('query', '');

        $Cities = City::where(['country' => $county])
            ->where('title', 'LIKE', Tools::correctString($query) . '%')
            ->get();

        return response()->json($Cities);
    }
}
