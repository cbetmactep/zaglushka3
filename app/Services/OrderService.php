<?php

namespace App\Services;

use App\Models\Order;
use App\Models\Settings;
use Cache;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;

class OrderService
{
    protected $yandexMoneyService;

    public function __construct(
        YandexMoneyService $yandexMoneyService
    )
    {
        $this->yandexMoneyService = $yandexMoneyService;
    }

    public function bought(Order $order): void
    {
        $order->status = $order::STATUS_PROCESSING;
        $order->saveOrFail();

        //if (App::environment() === 'production') {
            $salesEmails = Settings::getSetting('sender_email');
            $salesEmails[] = 'content@zaglushka.ru';
            $salesEmails = array_unique($salesEmails);

            Mail::send(
                'order.emails.order',
                ['Order' => $order, 'OrderItems' => $order->items],
                function (Message $Message) use ($order, $salesEmails): void {
                    $Message->to($salesEmails)->subject('Оплачен заказ с сайта Zaglushka.ru');
                }
            );
        //}
    }

    public function checkPayment(Order $order): Order
    {
        if ($order->status !== Order::STATUS_WAITING_FOR_PAYMENT) {
            return $order;
        }

        if (Cache::lock($order->getLockKey(), 60 * 10)->block(5)) {
            try {
                /** @var Order $order */
                $order = $order->fresh();

                if ($order->status === Order::STATUS_WAITING_FOR_PAYMENT) {
                    $paymentInfo = $this->yandexMoneyService->getPaymentInfo($order->payment_id);

                    if ($paymentInfo) {
                        switch ($paymentInfo->getStatus()) {
                            case $this->yandexMoneyService::STATUS_PENDING:
                                break;
                            case $this->yandexMoneyService::STATUS_WAITING_FOR_CAPTURE:
                                if ($this->yandexMoneyService->capturePayment($paymentInfo)) {
                                    $this->bought($order);
                                }
                                break;
                            case $this->yandexMoneyService::STATUS_SUCCEEDED:
                                $this->bought($order);
                                break;
                            case $this->yandexMoneyService::STATUS_CANCELED:
                                $order->status = Order::STATUS_CANCELED;
                                $order->saveOrFail();
                                break;
                        }
                    }
                }
            } finally {
                Cache::lock($order->getLockKey())->release();
            }
        }

        return $order;
    }
}
