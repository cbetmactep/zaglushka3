<?php

namespace App\Services;

use App\Models\Order;
use Exception;
use YandexCheckout\Client;
use YandexCheckout\Common\Exceptions\InternalServerError;
use YandexCheckout\Model\Confirmation\ConfirmationExternal;
use YandexCheckout\Model\Confirmation\ConfirmationRedirect;
use YandexCheckout\Model\PaymentInterface;

class YandexMoneyService
{
    const STATUS_PENDING = 'pending';
    const STATUS_WAITING_FOR_CAPTURE = 'waiting_for_capture';
    const STATUS_SUCCEEDED = 'succeeded';
    const STATUS_CANCELED = 'canceled';

    protected $client;

    public function __construct()
    {
        $this->client = new Client();
        $this->setAuth();
    }

    protected function setAuth(int $login = null, string $password = null)
    {
        $login = $login ?? getenv('YANDEX_SHOP_ID');
        $password = $password ?? getenv('YANDEX_SECRET_KEY');
        $this->client->setAuth($login, $password);
    }

    protected function createPayment(array $data): array
    {
        $idempotenceKey = uniqid('', true);

        $response = $this->client->createPayment($data, $idempotenceKey);

        return self::getConfirmation($response);
    }

    public function createPaymentFromOrder(Order $order): array
    {
        $data = [
            'amount' => [
                'value' => (string) $order->total_price,
                'currency' => 'RUB',
            ],
            'description' => 'Оплата заказа № ' . $order->id . ' для ' . $order->email,
            'confirmation' => [
                'type' => 'redirect',
                'return_url' => getenv('YANDEX_REDIRECT_URL') . $order->id,
            ],
            'receipt' => [
                'email' => $order->email,
                'items' => self::buildItems($order),
            ],
        ];

        return $this->createPayment($data);
    }

    public function getPaymentInfo(string $id): ?PaymentInterface
    {
        try {
            return $this->client->getPaymentInfo($id);
        } catch (InternalServerError $exception) { // InternalServerError usually
      //      app('sentry')->captureException($exception, ['level' => Raven_Client::WARNING]);
        } catch (Exception $exception) {
     //       app('sentry')->captureException($exception);
        }

        return null;
    }

    public function capturePayment(PaymentInterface $paymentInfo): bool
    {
        try {
            $idempotenceKey = uniqid('', true);

            $response = $this->client->capturePayment(
                ['amount' => $paymentInfo->getAmount()],
                $paymentInfo->getId(),
                $idempotenceKey
            );

            if ($response->getStatus() === self::STATUS_SUCCEEDED) {
                return true;
            }
        } catch (InternalServerError $exception) { // InternalServerError usually
    //        app('sentry')->captureException($exception, ['level' => Raven_Client::WARNING]);
        } catch (Exception $exception) {
    //        app('sentry')->captureException($exception);
        }

        return false;
    }

    public static function getConfirmation(PaymentInterface $paymentInfo): array
    {
        $confirmation = $paymentInfo->getConfirmation();
        $confirmationUrl = '';
        $message = '';

        if ($confirmation instanceof ConfirmationRedirect) {
            $confirmationUrl = $confirmation->getConfirmationUrl();
        } elseif ($confirmation instanceof ConfirmationExternal) {
            $message = 'Повторить оплату невозможно: в настоящее время платежная система ожидает от вас подтверждения через SMS или другим способом; следуйте полученным инструкциям от провайдера выбранного вами способа оплаты';
        } else {
            if ($confirmation) {
     //           app('sentry')->captureMessage('unknown confirmation class ' . get_class($confirmation));
            }
        }

        return [
            'id' => $paymentInfo->getId(),
            'confirmation_url' => $confirmationUrl,
            'message' => $message,
        ];
    }

    protected static function buildItems(Order $order): array
    {
        $items = [];

        foreach ($order->items as $orderItem) {
            if ($orderItem->price > 0) {
                $items[] = [
                    'description' => $orderItem->title . ' ' . $orderItem->color,
                    'quantity' => (string) $orderItem->quant,
                    'amount' => [
                        'value' => (string) $orderItem->price,
                        'currency' => 'RUB',
                    ],
                    'vat_code' => '1', // без ндс
                    'payment_mode' => 'full_payment',
                    'payment_subject' => 'commodity',
                ];
            }
        }

        return $items;
    }
}
