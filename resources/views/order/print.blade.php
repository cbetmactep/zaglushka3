@php
    declare(strict_types=1);

    use App\Models\Order;
    use App\Models\OrderItem;
    use App\Models\Color;

    $request = request();
    $step = Request::get('step');

    $totalPrice = 0;

    Meta::set('title', _t('Оформление заказа', 'order'));
@endphp

@extends('layouts.print')

@section('content')
    @if(count($OrderItems))
        <table class="list">
            <thead>
            <tr>
                <th>№</th>
                <th>{!! _t('Фото', 'order')  !!}</th>
                <th>{!! _t('Артикул', 'order')  !!}</th>
                <th>{!! _t('Описание', 'order') !!}</th>
                <th>{!! _t('Цвет', 'order') !!}</th>
                <th class="nowrap">{!! _t('Кол-во штук', 'order') !!}</th>
                <th>{!! _t('Оптовая группа', 'order') !!}</th>
                <th class="nowrap">{!! _t('Цена за 1 штуку', 'order') !!}</th>
                <th>{!! _t('Сумма', 'order') !!}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($OrderItems as $OrderItem)
                <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>
                        @if($OrderItem->image)
                            <img src="{{ $OrderItem->image }}" alt="{{ $OrderItem->title }}" width="90">
                        @endif
                    </td>
                    <td>{{ $OrderItem->title }}</td>
                    <td>{{ $OrderItem->item->short_descr }}</td>
                    <td>{{ _t(Color::cachedOne($OrderItem->color)->name, 'colors') }}</td>
                    @if($OrderItem->promo)
                        <td colspan="4">{!! _t('Бесплатные образцы', 'order') !!}</td>
                    @else
                        <td>{{ $OrderItem->quant }}</td>
                        <td>{!! OrderItem::getWholeSaleGroupText($OrderItem->getWholeSaleGroup()) !!}</td>
                        <td class="nowrap">{{ Formatter::price($OrderItem->price, true) }}</td>
                        <td class="nowrap">{{ Formatter::price(round($OrderItem->price, 1) * $OrderItem->quant, true) }}</td>
                    @endif
                    @php
                        $totalPrice += round($OrderItem->price, 1) * $OrderItem->quant;
                    @endphp
                </tr>
            @endforeach
            </tbody>
        </table>
        <p class="total">
            {{ _t('Стоимость продукции:', 'order') }} {{ _t('%s руб.', 'order', [Formatter::price($totalPrice)]) }}
        </p>
        @if(!$freeDeliveryIsImpossible && $CurrentCity)
            @if($totalPrice >= $CurrentCity->free_delivery_price)
                <p>
                    {!! _t('Ваш заказ на сумму более %s рублей. Доставка "до дверей" в городе %s бесплатно!', 'order', [number_format($CurrentCity->free_delivery_price, 0, ',', ' '), $CurrentCity->title]) !!}
                </p>
            @else
                <p>
                    {!! _t('До бесплатной доставки "до дверей" в городе %s в заказе не хватает товаров на сумму %s рублей.', 'order', [$CurrentCity->title, number_format($CurrentCity->free_delivery_price - $totalPrice, 0, ',', ' ')]) !!}
                </p>
            @endif
        @endif
        <p>{!! _t('Бесплатная доставка не распространяется на горки, канаты "Викинг", стальные цепи, сиденья и каркасы для стульев.', 'order') !!}</p>

        @if($step > 1 && isset($request['delivery_company']))
            <h1>{{ _t('Информация о доставке', 'order') }}</h1>
            <table class="list">
                <thead>
                    <tr>
                        <th>{{ _t('Способ доставки', 'order') }}</th>
                        @if($request['delivery_company'] !== 'Самовывоз')
                            <th>{{ _t('Адрес доставки', 'order') }}</th>
                        @endif
                        <th>{{ _t('Комментарии', 'order') }}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ isset($request['delivery_company']) ? $request['delivery_company'] : '' }}</td>
                        @if($request['delivery_company'] !== 'Самовывоз')
                            <td>{{ isset($request['city']) ? $request['city'] : '' }} {{ isset($request['address']) ? $request['address'] : '' }} </td>
                        @endif
                        <td>{{ isset($request['comments']) ? $request['comments'] : '' }}</td>
                    </tr>
                </tbody>
            </table>
        @endif

        @if($step > 0)
            <h1>{{ _t('Данные о клиенте', 'order') }}</h1>
            @if($request['type'] == Order::TYPE_COMPANY)
                <h2>{{ _t('Организация (Юр. лицо)', 'order') }}</h2>
                <table class="info">
                    <tbody>
                    @isset($request['company_person_name'])
                        <tr>
                            <td>{{ _t('Фамилия Имя Отчество', 'order') }}</td>
                            <td>{{ $request['company_person_name'] }}</td>
                        </tr>
                    @endif
                    @isset($request['company_phone'])
                        <tr>
                            <td>{{ _t('Телефон', 'order') }}</td>
                            <td>{{ $request['company_phone'] }}</td>
                        </tr>
                    @endif
                    @isset($request['company_email'])
                        <tr>
                            <td>{{ _t('E-mail для связи', 'order') }}</td>
                            <td>{{ $request['company_email'] }}</td>
                        </tr>
                    @endif
                    @isset($request['company_name'])
                        <tr>
                            <td>{{ _t('Название компании', 'order') }}</td>
                            <td>{{ $request['company_name'] }}</td>
                        </tr>
                    @endif
                    @isset($request['company_address'])
                        <tr>
                            <td>{{ _t('Юридический адрес', 'order') }}</td>
                            <td>{{ $request['company_address'] }}</td>
                        </tr>
                    @endif
                    @isset($request['company_inn'])
                        <tr>
                            <td>{{ _t('ИНН/КПП', 'order') }}</td>
                            <td>{{ $request['company_inn']}}/{{$request['company_kpp'] }}</td>
                        </tr>
                    @endif
                    @isset($request['company_bik'])
                        <tr>
                            <td>{{ _t('БИК', 'order') }}</td>
                            <td>{{ $request['company_bik'] }}</td>
                        </tr>
                    @endif
                    @isset($request['company_rs'])
                        <tr>
                            <td>{{ _t('Расчетный счет', 'order') }}</td>
                            <td>{{ $request['company_rs'] }}</td>
                        </tr>
                    @endif
                    @if(isset($request['company_file']) && $request['company_file'])
                        <tr>
                            <td>{{ _t('Файл с реквизитами', 'order') }}</td>
                            <td>{{ $request['company_file'] }}</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            @else
                <h2>{{ _t('Физ. лицо', 'order') }}</h2>
                <table class="info">
                    <tbody>
                        @isset($request['person_name'])
                            <tr>
                                <td>{{ _t('Фамилия Имя Отчество', 'order') }}</td>
                                <td>{{ $request['person_name'] }}</td>
                            </tr>
                        @endif
                        @isset($request['person_phone'])
                            <tr>
                                <td>{{ _t('Телефон для связи', 'order') }}</td>
                                <td>{{ $request['person_phone'] }}</td>
                            </tr>
                        @endif
                        @isset($request['person_email'])
                            <tr>
                                <td>{{ _t('E-mail для связи', 'order') }}</td>
                                <td>{{ $request['person_email'] }}</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            @endif
        @endif
    @endif
@endsection