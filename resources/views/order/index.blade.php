@php
    declare(strict_types=1);

    use App\Models\City;

    Meta::set('title', _t('Оформление заказа', 'order'));
@endphp

@extends('layouts.master')

@section('content')
    <div class="container">
        {{--TODO:--}}
        {{--{!! Breadcrumbs::render('order') !!}--}}
        <div class="page-header">
            <div class="page-header__title-wrap">
                <h1 class="page__title">{!! _t('Оформление заказа', 'order') !!}</h1>
            </div>
            <div class="page-header__actions">
                @component('includes.page_actions')@endcomponent
            </div>
        </div>
        <div class="page">
            <div class="page__content">
                <div class="ordering__row">
                    <div class="ordering__left">
                        <div class="ordering__block ordering__block_step">
                            {{ csrf_field() }}
                            <div class="ordering-step">
                                <div class="ordering-step-item ordering-step-item_active">
                                    <div class="ordering-step-item__header">
                                        <div class="ordering-step-item__icon">
                                            @component('components.icon', ['name' => 'cart-personal'])])@endcomponent
                                        </div>
                                        <div class="ordering-step-item__title">
                                            {!! _t('Контактная информация', 'order') !!}
                                        </div>
                                        @component('components.icon', ['name' => 'arrow-thin', 'attributes' => ['class' => 'ordering-step-item__arrow']])])@endcomponent
                                    </div>
                                    <div class="ordering-step-item__body" style="display: block;">
                                        <div class="ordering-tab-header">
                                            <div data-type="2" class="ordering-tab-header__item ordering-tab-header__item_active">{!! _t('Физическое лицо', 'order') !!}</div>
                                            <div data-type="1" class="ordering-tab-header__item">{!! _t('Юридическое лицо', 'order') !!}</div>
                                        </div>
                                        <form class="ordering-tab js-ordering-contacts-form">
                                            <input type="hidden" name="type" value="2" class="js-ordering-contacts-type">
                                            <div class="ordering-tab__item ordering-tab__item_active js-ordering-contacts-form">
                                                <div class="ordering-form ordering-form_there-text">
                                                    <label class="ordering-form__label" for="person_name">
                                                        <span>
                                                            {!! _t('ФИО:', 'order') !!}
                                                        </span>
                                                    </label>
                                                    <div class="ordering-form__col">
                                                        <div class="ordering-form__field">
                                                            <input class="ordering-form__input" name="person_name" id="person_name">
                                                        </div>
                                                        <div class="ordering-form__text">{!! _t('Представьтесь, пожалуйста.', 'order') !!}</div>
                                                    </div>
                                                </div>
                                                <div class="ordering-form ordering-form_there-text">
                                                    <label class="ordering-form__label" for="person_email">
                                                        <span>
                                                            {!! _t('Эл. почта:', 'order') !!}
                                                        </span>
                                                    </label>
                                                    <div class="ordering-form__col">
                                                        <div class="ordering-form__field">
                                                            <input class="ordering-form__input" name="person_email" id="person_email" type="email">
                                                        </div>
                                                        <div class="ordering-form__text">{!! _t('Пришлем письмо с информацией о заказе.', 'order') !!}</div>
                                                    </div>
                                                </div>
                                                <div class="ordering-form ordering-form_there-text">
                                                    <label class="ordering-form__label" for="person_phone">
                                                        <span>
                                                            {!! _t('Телефон:', 'order') !!}
                                                        </span>
                                                    </label>
                                                    <div class="ordering-form__col">
                                                        <div class="ordering-form__field">
                                                            <input class="ordering-form__input ordering-form__input_phone" name="person_phone" id="person_phone" type="tel">
                                                        </div>
                                                        <div class="ordering-form__text">{!! _t('Перезвоним, чтобы подтвердить заказ.', 'order') !!}</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ordering-tab__item">
                                                <div class="ordering-form ordering-form_there-text">
                                                    <label class="ordering-form__label" for="company_person_name">
                                                        <span>
                                                            {!! _t('ФИО:', 'order') !!}
                                                        </span>
                                                    </label>
                                                    <div class="ordering-form__col">
                                                        <div class="ordering-form__field">
                                                            <input class="ordering-form__input" name="company_person_name" id="company_person_name">
                                                        </div>
                                                        <div class="ordering-form__text">{!! _t('Представьтесь, пожалуйста.', 'order') !!}</div>
                                                    </div>
                                                </div>
                                                <div class="ordering-form ordering-form_there-text">
                                                    <label class="ordering-form__label" for="company_email">
                                                        <span>
                                                            {!! _t('Эл. почта:', 'order') !!}
                                                        </span>
                                                    </label>
                                                    <div class="ordering-form__col">
                                                        <div class="ordering-form__field">
                                                            <input class="ordering-form__input" name="company_email" id="company_email" type="email">
                                                        </div>
                                                        <div class="ordering-form__text">{!! _t('На этот адрес мы пришлем счет.', 'order') !!}</div>
                                                    </div>
                                                </div>
                                                <div class="ordering-form ordering-form_there-text">
                                                    <label class="ordering-form__label" for="company_phone">
                                                        <span>
                                                            {!! _t('Телефон:', 'order') !!}
                                                        </span>
                                                    </label>
                                                    <div class="ordering-form__col">
                                                        <div class="ordering-form__field">
                                                            <input class="ordering-form__input ordering-form__input_phone" name="company_phone" id="company_phone" type="tel">
                                                        </div>
                                                        <div class="ordering-form__text">{!! _t('Перезвоним, чтобы подтвердить заказ.', 'order') !!}</div>
                                                    </div>
                                                </div>
                                                <div class="ordering-form">
                                                    <label class="ordering-form__label"></label>
                                                    <div class="ordering-form__col">
                                                        <label class="ordering-form__file" tabindex="0">
                                                            <span class="ordering-form__file-placeholder">
                                                                {!! _t('Прикрепить файл с реквизитами', 'order') !!}
                                                            </span>
                                                            <span class="ordering-form__file-name"></span>
                                                            @component('components.icon', ['name' => 'close-thin', 'attributes' => ['class' => 'ordering-form__file-icon']])])@endcomponent
                                                            <input type="file" name="company_file" class="js-ordering-delivery-input-company-file">
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="ordering-form js-ordering-requisites-checkbox-wrap">
                                                    <label class="ordering-form__label"></label>
                                                    <div class="ordering-form__col">
                                                        <label class="ordering-form__checkbox" tabindex="0">
                                                            <input type="checkbox" name="company" class="js-ordering-requisites-checkbox">
                                                            <span>
                                                                @component('components.icon', ['name' => 'check'])@endcomponent
                                                                {!! _t('Ввести реквизиты вручную', 'order') !!}
                                                            </span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="ordering-form__entity">
                                                    <div class="ordering-form">
                                                        <label class="ordering-form__label" for="company_name">
                                                            <span>
                                                                {!! _t('Название компании:', 'order') !!}
                                                            </span>
                                                        </label>
                                                        <div class="ordering-form__col">
                                                            <div class="ordering-form__field">
                                                                <input class="ordering-form__input" name="company_name" id="company_name">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="ordering-form">
                                                        <label class="ordering-form__label" for="company_address">
                                                            <span>
                                                                {!! _t('Юр. адрес:', 'order') !!}
                                                            </span>
                                                        </label>
                                                        <div class="ordering-form__col">
                                                            <div class="ordering-form__field">
                                                                <input class="ordering-form__input" name="company_address" id="company_address">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="ordering-form">
                                                        <label class="ordering-form__label" for="company_inn">
                                                            <span>
                                                                {!! _t('ИНН:', 'order') !!}
                                                            </span>
                                                        </label>
                                                        <div class="ordering-form__col">
                                                            <div class="ordering-form__field">
                                                                <input class="ordering-form__input ordering-form__input_inn" name="company_inn" id="company_inn">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="ordering-form">
                                                        <label class="ordering-form__label" for="company_kpp">
                                                            <span>
                                                                {!! _t('КПП:', 'order') !!}
                                                            </span>
                                                        </label>
                                                        <div class="ordering-form__col">
                                                            <div class="ordering-form__field">
                                                                <input class="ordering-form__input ordering-form__input_inn" name="company_kpp" id="company_kpp">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="ordering-form">
                                                        <label class="ordering-form__label" for="company_bik">
                                                            <span>
                                                                {!! _t('БИК:', 'order') !!}
                                                            </span>
                                                        </label>
                                                        <div class="ordering-form__col">
                                                            <div class="ordering-form__field">
                                                                <input class="ordering-form__input ordering-form__input_inn" name="company_bik" id="company_bik">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="ordering-form">
                                                        <label class="ordering-form__label" for="company_rs">
                                                            <span>
                                                                {!! _t('Расчетный счет:', 'order') !!}
                                                            </span>
                                                        </label>
                                                        <div class="ordering-form__col">
                                                            <div class="ordering-form__field">
                                                                <input class="ordering-form__input ordering-form__input_invoice" name="company_rs" id="company_rs">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ordering-form__btns">
                                                <button class="ordering-form__btn">{!! _t('Продолжить', 'order') !!}</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="ordering-step-item">
                                    <div class="ordering-step-item__header">
                                        <div class="ordering-step-item__icon">
                                            @component('components.icon', ['name' => 'cart-car'])])@endcomponent
                                        </div>
                                        <div class="ordering-step-item__title">
                                            {!! _t('Варианты получения', 'order') !!}
                                        </div>
                                        @component('components.icon', ['name' => 'arrow-thin', 'attributes' => ['class' => 'ordering-step-item__arrow']])])@endcomponent
                                    </div>
                                    <div class="ordering-step-item__body">
                                        <form class="js-ordering-delivery-form">
                                            <!--<input class="js-ordering-delivery-input-post-index" type="hidden" value="{{ $CurrentCity && $CurrentCity->post_index ? $CurrentCity->post_index : '' }}" name="carrier_post_index">
                                            <div class="ordering-delivery">
                                                <div class="ordering-delivery__header">
                                                    <div class="ordering-form">
                                                        <label class="ordering-form__radio" tabindex="0">
                                                            <input type="radio" name="carrier_type" value="pickup" class="js-ordering-delivery-input">
                                                            <span></span>
                                                            <span>{!! _t('Самовывоз', 'order') !!} — {!! _t('бесплатно', 'order') !!}</span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="ordering-delivery__body">
                                                    <div class="ordering-delivery__text">
                                                        {!! _t('Со склада в г. Санкт-Петербург, ул. Железнодорожная 11, пос. Парголово - Схема проезда', 'order', [asset('/files/map.jpg')]) !!}
                                                    </div>
                                                    <div class="ordering-delivery__comment">
                                                        <div class="ordering-form ordering-form_horizontal">
                                                            <label class="ordering-form__label" for="pickup_comment">
                                                                <span>
                                                                    {!! _t('Комментарий к заказу:', 'order') !!}
                                                                </span>
                                                            </label>
                                                            <div class="ordering-form__col">
                                                                <textarea class="ordering-form__textarea js-ordering-delivery-pickup-comment" rows="3" id="pickup_comment" name="pickup_comment"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <div class="ordering-delivery">
                                                <div class="ordering-delivery__header">
                                                    <div class="ordering-form ordering-form_horizontal">
                                                        <label class="ordering-form__radio" tabindex="0">
                                                            <input type="radio" name="carrier_type" value="carrier" class="js-ordering-delivery-input" id="delivery_carrier">
                                                            <span></span>
                                                            <span>
                                                                {!! _t('Доставка', 'order') !!}
                                                                <span class="js-ordering-delivery-chargeable" style="display: none;">— {{ _t('от', 'order')  }} <span class="js-ordering-delivery-price"></span> {{ _t('р.', 'order')  }}</span>
                                                                <span class="js-ordering-delivery-free" style="display: none;">— {!! _t('бесплатно', 'order') !!}</span>
                                                                <span class="js-ordering-delivery-manager" style="display: none;">— {!! _t('рассчитывается менеджером', 'order') !!}</span>
                                                                <span class="js-ordering-delivery-loader" style="display: none;">
                                                                    <span class="ordering-delivery__loader">
                                                                        <svg class="ordering-delivery__loader-circular" viewBox="25 25 50 50">
                                                                            <circle class="ordering-delivery__loader-path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle>
                                                                        </svg>
                                                                    </span>
                                                                    <span class="ordering-delivery__loader-text">
                                                                        {!! _t('Рассчитывается...', 'order') !!}
                                                                    </span>
                                                                </span>
                                                            </span>
                                                        </label>
                                                        <div class="ordering-delivery__popover" style="display: none;">
                                                            {!! _t('Примерная стоимость доставки. Менеджер свяжется с вами и уточнит стоимость и сроки.', 'order') !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="ordering-delivery__body">
                                                    <div class="ordering-delivery__comment">
                                                        <div class="ordering-form ordering-delivery__comment-field">
                                                            <label class="ordering-form__label" for="carrier_country">
                                                                <span>
                                                                    {!! _t('Страна:', 'order') !!}
                                                                </span>
                                                            </label>
                                                            <div class="ordering-form__col">
                                                                <input class="ordering-form__input js-ordering-delivery-input-country" value="{{ $CurrentCity ? _t(City::getCountryName($CurrentCity->country), 'country') : _t('Россия', 'country') }}" id="carrier_country" name="carrier_country">
                                                            </div>
                                                        </div>
                                                        <div class="ordering-form ordering-delivery__comment-field">
                                                            <label class="ordering-form__label" for="carrier_city">
                                                                <span>
                                                                    {!! _t('Город:', 'order') !!}
                                                                </span>
                                                            </label>
                                                            <div class="ordering-form__col">
                                                                <input class="ordering-form__input js-ordering-delivery-input-city" value="{{ $CurrentCity ? $CurrentCity->title : '' }}" id="carrier_city" name="carrier_city">
                                                            </div>
                                                        </div>
                                                        <div class="ordering-form ordering-delivery__comment-field">
                                                            <label class="ordering-form__label" for="carrier_address">
                                                                <span>
                                                                    {!! _t('Адрес:', 'order') !!}
                                                                </span>
                                                            </label>
                                                            <div class="ordering-form__col">
                                                                <input class="ordering-form__input js-ordering-delivery-input-address" id="carrier_address" name="carrier_address">
                                                            </div>
                                                        </div>
                                                        <div class="ordering-form ordering-form_horizontal">
                                                            <label class="ordering-form__label" for="carrier_comment">
                                                                <span>
                                                                    {!! _t('Комментарий к заказу:', 'order') !!}
                                                                </span>
                                                            </label>
                                                            <div class="ordering-form__col">
                                                                <textarea class="ordering-form__textarea js-ordering-delivery-carrier-comment" rows="3" id="carrier_comment" name="carrier_comment"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="js-ordering-delivery-info">
                                                    <div class="ordering-delivery__text ordering-delivery__text-list">{!! _t('Сотрудничаем с более чем %s транспортными компаниями', 'order', ['40']) !!} @component('components.icon', ['name' => 'arrow-thin', 'attributes' => ['class' => 'ordering-delivery__text-list-icon']])@endcomponent</div>
                                                    <div class="ordering-delivery__list" style="display: none;">
                                                        <img src="{{ asset('/images/delivery.png') }}">
                                                        {!! _t('Если у вас есть пожелания касательно транспортной компании, укажите их в комментариях к заказу.', 'order') !!}
                                                    </div>
                                                    </div>
                                                    <div class="js-ordering-delivery-info-spb ordering-delivery__text" style="display: none;">
                                                        {!! _t('Доставим заказ сегодня, если заявка сформирована до 15:00. Заказы, поступившие после 15:00, будут доставлены на следующий день.', 'order') !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ordering-form__btns">
                                                <button class="ordering-form__btn js-ordering-delivery-btn" disabled>
                                                    <span class="js-ordering-delivery-btn-individual">
                                                        {!! _t('Продолжить', 'order') !!}
                                                    </span>
                                                    <span class="js-ordering-delivery-btn-entity" style="display: none;">
                                                        {!! _t('Оформить заказ', 'order') !!}
                                                    </span>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="ordering-step-item ordering-step-item_payment ordering-step-item_last">
                                    <div class="ordering-step-item__header">
                                        <div class="ordering-step-item__icon">
                                            @component('components.icon', ['name' => 'cart-wallet'])])@endcomponent
                                        </div>
                                        <div class="ordering-step-item__title">
                                            {!! _t('Способы оплаты', 'order') !!}
                                        </div>
                                        @component('components.icon', ['name' => 'arrow-thin', 'attributes' => ['class' => 'ordering-step-item__arrow']])])@endcomponent
                                    </div>
                                    <form class="ordering-step-item__body js-ordering-payment-form">
                                        <div class="ordering-delivery__body-payment">
                                            <!--<div class="ordering-form">
                                                <label class="ordering-form__radio" tabindex="0">
                                                    <input class="js-ordering-form-payment" type="radio" name="payment" value="cash">
                                                    <span></span>
                                                    <span>{!! _t('Наличными', 'order') !!}</span>
                                                </label>
                                            </div>-->
                                            <div class="ordering-form">
                                                <label class="ordering-form__radio" tabindex="0">
                                                    <input class="js-ordering-form-payment" type="radio" name="payment" value="bank">
                                                    <span></span>
                                                    <span>{!! _t('Онлайн банковской картой', 'order') !!}
                                                        <span class="ordering-form__radio-images">
                                                            <img src="{{ asset('/images/payment/visa.svg') }}" alt="Visa"> <img src="{{ asset('/images/payment/mastercard.svg') }}" alt="MasterCard"> <img src="{{ asset('/images/payment/mir.svg') }}" alt="Мир">
                                                        </span>
                                                    </span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="ordering-form__btns">
                                            <button class="ordering-form__btn js-ordering-payment-btn" disabled>
                                                <span class="js-ordering-payment-btn-cash">
                                                    {!! _t('Оформить заказ', 'order') !!}
                                                </span>
                                                <span class="js-ordering-payment-btn-bank" style="display: none;">
                                                    {!! _t('Перейти к оплате', 'order') !!}
                                                </span>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ordering__right">
                        <div class="ordering__block">
                            <div class="ordering-list-header">
                                <div class="ordering-list-header__left">
                                    <a class="ordering-list-header__link" href="{{ route('cart') }}">{!! _t('Ваш заказ', 'order') !!}</a>
                                    @if(min(array_map(function($OrderItem) {  return $OrderItem->getColor()->remains; }, $OrderItems)) == 0)
                                        @component('components.icon', ['name' => 'attention', 'attributes' => ['class' => 'js-cart-not-available-btn ordering-list-not-available__btn']])@endcomponent
                                        <div class="ordering-list-not-available js-cart-not-available">
                                            {!! _t('Напоминаем, что в корзине есть товар, которого нет в наличии. Срок его поставки может достигать 3-х месяцев. Можете выбрать аналогичные товары в наличии.', 'order') !!}
                                            <br/>
                                            <div class="ordering-list-not-available__link js-cart-not-available-close">{!! _t('Спасибо, не нужно', 'order') !!}</div>
                                        </div>
                                    @endif
                                </div>
                                <div class="ordering-list-header__slide">
                                    <span class="ordering-list-header__slide-active">
                                        {!! _t('Развернуть', 'order') !!}
                                    </span>
                                    <span class="ordering-list-header__slide-disabled">
                                        {!! _t('Свернуть', 'order') !!}
                                    </span>
                                    @component('components.icon', ['name' => 'arrow-thin', 'attributes' => ['class' => 'ordering-list-header__slide-icon']])])@endcomponent
                                </div>
                            </div>
                            <div class="ordering-list">
                                <table>
                                    @foreach($OrderItems as $OrderItem)
                                        <tr class="ordering-list__item">
                                            <td>{{ $OrderItem->title }}</td>
                                            @if($OrderItem->promo)
                                                <td class="ordering-list__item-right" colspan="2">{!! _t('Бесплатные образцы', 'order') !!}</td>
                                            @else
                                                <td class="ordering-list__item-right">{{ Formatter::quantity($OrderItem->quant) }}</td>
                                                <td class="ordering-list__item-right">{{ Formatter::price($OrderItem->price * $OrderItem->quant, true) }}</td>
                                            @endif
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                            <div class="ordering-list__footer">
                                <span class="ordering-list__amount">{!! _t('Итого:', 'order') !!} {{ Formatter::price($Order->total_price, true, true) }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection