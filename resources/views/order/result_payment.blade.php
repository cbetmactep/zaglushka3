@php
    declare(strict_types=1);

    use App\Models\Order;

    Meta::set('title', _t('Корзина', 'order'));
@endphp

@extends('layouts.master')

@section('content')
    {{--TODO:--}}
    {{--{!! Breadcrumbs::render('order') !!}--}}
    <div class="container">
        <div class="page-header">
            <div class="page-header__title-wrap">
                <h1 class="page__title">{!! _t('Корзина', 'order') !!}</h1>
            </div>
        </div>
        <div class="page">
            <div class="page__content page__content_order page__content_background page__content_text">
                @if($order->status == Order::STATUS_PROCESSING)
                    <div class="order-complete__title">
                        {!! _t('Ваш заказ №%d оплачен.', 'order', [$order->id]) !!}
                    </div>
                    <div class="order-complete__text">
                        {!! _t('В ближайшее время менеджер свяжется с вами по указаным контактам.', 'order') !!}
                    </div>
                @else
                    <div class="order-complete__title">
                        {!! _t('Ваш заказ №%d оформлен, но платеж еще не поступил.', 'order', [$order->id]) !!}
                    </div>
                    <div class="order-complete__text">
                        {!! _t('В ближайшее время менеджер свяжется с вами по указаным контактам.', 'order') !!}
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
