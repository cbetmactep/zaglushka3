@php
    declare(strict_types=1);

    use App\Models\Item;
    use App\Models\Color;
@endphp

@component('components.modal', ['name' => 'order'])
    <form class="order-popup__form js-order-modal-form">
        <input type="hidden" name="id" class="js-order-modal-id" value="">
        <div class="order-popup-table__wrap">
            <table class="order-popup-table">
                <thead class="order-popup-table__thead">
                    <tr>
                        <th class="order-popup-table__th order-popup-table__th_image">{!! _t('Фото', 'order') !!}</th>
                        <th class="order-popup-table__th order-popup-table__th_article">{!! _t('Артикул', 'order') !!}</th>
                        <th class="order-popup-table__th order-popup-table__th_description">{!! _t('Описание', 'order') !!}</th>
                        <th class="order-popup-table__th">{!! _t('Цвет', 'order') !!}</th>
                        <th class="order-popup-table__th order-popup-table__th_small">{!! _t('Количество, штук', 'order') !!}</th>
                        <th class="order-popup-table__th order-popup-table__th_small">{!! _t('Цена за штуку', 'order') !!}</th>
                        <th class="order-popup-table__th order-popup-table__th_small">{!! _t('Сумма', 'order') !!}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="order-popup-table__tr">
                        <td class="order-popup-table__td order-popup-table__td_image">
                            <span>
                                <img width="86" alt="" src="">
                            </span>
                        </td>
                        <td class="order-popup-table__td order-popup-table__td_article">
                            <span class="js-order-modal-article order-popup-table__article"></span>
                        </td>
                        <td class="order-popup-table__td order-popup-table__td_description" style="vertical-align: middle;padding-top:0px;">
                            <span></span>
                        </td>
                        <td class="order-popup-table__td order-popup-table__td_color">
                            <span class="order-popup-table__mob-title">{!! _t('Цвет', 'order') !!}</span>
                            <div class="order-popup-table__select">
                                @component('components.order-popup-select')
                                    <select name="color" id="js-order-modal-color" class="js-order-modal-color">
                                    </select>
                                @endcomponent
                            </div>
                            <div class="order-popup-table__tooltip order-popup-table__tooltip_is-stock js-order-modal-remains-block">
                                {{ _t('В наличии', 'order') }}
                                <br/>
                                <b><span class="js-order-modal-remains"></span> {!! _t('шт.', 'order') !!}</b>
                            </div>
                        </td>
                        <td class="order-popup-table__td order-popup-table__td_quantity">
                            <span class="order-popup-table__mob-title">{!! _t('Количество, штук', 'order') !!}</span>
                            <input name="quantity" class="order-popup-table__input js-order-modal-quantity" autocomplete="off" type="tel">
                            <template class="js-order-modal-table-price">
                                <div class="order-popup-table-prices__row">
                                    <div class="order-popup-table-prices__key"></div>
                                    <div class="order-popup-table-prices__value"></div>
                                </div>
                            </template>
                            <div class="order-popup-table-prices js-order-modal-table-prices"></div>
                        </td>
                        <td class="order-popup-table__td js-order-popup-price-td order-popup-table__td_price">
                            <span class="order-popup-table__mob-title">{!! _t('Цена за штуку', 'order') !!}</span>
                            <span style="position: relative;">
                                <b class="nowrap"><span class="js-order-modal-price js-order-modal-show-is-price"></span> <span class="js-order-modal-show-is-price">{!! _t('р.', 'order') !!}</span></b>
                                <div class="js-order-modal-show-is-middle-price" style="display: none;">
                                    <div class="order-popup-table__middle-price">{{ _t('Средняя цена с учетом скидки', 'order') }}</div>
                                    <div class="order-popup-table__tooltip order-popup-table__tooltip_middle-price">
                                        <div class="order-popup-table__tooltip-text">
                                            {!! _t('Скидка распространяется только на изделия в наличии.', 'order') !!}
                                        </div>
                                        <b><span class="js-order-modal-middle-quantity-discount"></span> {!! _t('шт.', 'common') !!} {{ _t('по цене', 'order') }} <span class="js-order-modal-middle-price-discount"></span> {!! _t('р.', 'order') !!}</b>
                                        <br/>
                                        <b><span class="js-order-modal-middle-quantity"></span> {!! _t('шт.', 'common') !!} {{ _t('по цене', 'order') }} <span class="js-order-modal-middle-price"></span> {!! _t('р.', 'order') !!}</b>
                                    </div>
                                </div>
                            </span>
                        </td>
                        <td class="order-popup-table__td order-popup-table__td_price-total">
                            <span class="order-popup-table__mob-title">{!! _t('Сумма', 'order') !!}</span>
                            <b class="js-order-modal-price-total-block nowrap"><span class="js-order-modal-price-total"></span> {!! _t('р.', 'order') !!}</b>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="order-popup__footer">
            <button class="order-popup__btn" data-id="" data-name="" data-brand="ZAGLUSHKA.RU" >{{ _t('Добавить в корзину', 'order') }}</button>
        </div>
    </form>
@endcomponent