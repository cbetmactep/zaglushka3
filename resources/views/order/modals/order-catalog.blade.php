@php
    declare(strict_types=1);

    use App\Models\Item;
    use App\Models\Color;
@endphp

@component('components.modal', ['name' => 'order-catalog'])
    <form class="order-popup__form js-order-modal-form">
        <input type="hidden" name="id" class="js-order-modal-id" value="">
        <div class="order-popup-table__wrap">
            <table class="order-popup-table">
                <thead class="order-popup-table__thead">
                    <tr>
                        <th class="order-popup-table__th order-popup-table__th_image">{!! _t('Фото', 'order') !!}</th>
                        <th class="order-popup-table__th order-popup-table__th_article">{!! _t('Артикул', 'order') !!}</th>
                        <th class="order-popup-table__th order-popup-table__th_description">{!! _t('Описание', 'order') !!}</th>
                        <th class="order-popup-table__th">{!! _t('Цвет', 'order') !!}</th>
                        <th class="order-popup-table__th order-popup-table__th_small">{!! _t('Количество, штук', 'order') !!}</th>
                        <th class="order-popup-table__th order-popup-table__th_small">{!! _t('Цена за штуку', 'order') !!}</th>
                        <th class="order-popup-table__th order-popup-table__th_small">{!! _t('Сумма', 'order') !!}</th>
                    </tr>
                </thead>
                <tbo
            </table>
        </div>
        <div class="order-popup__footer">
            
        </div>
    </form>
@endcomponent