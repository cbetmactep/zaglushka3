@php
    declare(strict_types=1);

    use App\Models\Order;
    use App\Models\Color;

    /** @var \App\Models\Order $Order */
@endphp
<html>
    <body>
        <table cellspacing="0" border="1" cellpadding="2">
            <tbody>
                <tr>
                    <td nowrap>{!! _t('Номер заказа', 'order') !!}</td>
                    <td>{{ $Order->id }}</td>
                </tr>
                <tr>
                    <td nowrap>{!! _t('Дата заказа', 'order') !!}</td>
                    <td>{{ Formatter::date($Order->date) }}</td>
                </tr>
                <tr>
                    <td>{!! _t('Тип клиента', 'order') !!}</td>
                    <td>{{ $Order->type == Order::TYPE_COMPANY ? _t('Компания', 'order') : _t('Частное лицо', 'order') }}</td>
                </tr>
                @if($Order->type == Order::TYPE_COMPANY)
                    <tr>
                        <td>{!! _t('ФИО', 'order') !!}</td>
                        <td>{{ $Order->name }}</td>
                    </tr>
                    <tr>
                        <td>{!! _t('Телефон', 'order') !!}</td>
                        <td>{{ $Order->phone }}</td>
                    </tr>
                    <tr>
                        <td>E-Mail</td>
                        <td>{{ $Order->email }}</td>
                    </tr>
                    <tr>
                        <td nowrap>{!! _t('Название компании', 'order') !!}</td>
                        <td>{{ $Order->company_name }}</td>
                    </tr>
                    <tr>
                        <td>{!! _t('Индекс', 'order') !!}</td>
                        <td>{{ $Order->zip }}</td>
                    </tr>
                    <tr>
                        <td>{!! _t('Страна', 'order') !!}</td>
                        <td>{{ $Order->country }}</td>
                    </tr>
                    <tr>
                        <td>{!! _t('Город', 'order') !!}</td>
                        <td>{{ $Order->city }}</td>
                    </tr>
                    <tr>
                        <td>{!! _t('Адрес', 'order') !!}</td>
                        <td>{{ $Order->address }}</td>
                    </tr>
                    <tr>
                        <td>{!! _t('ИНН', 'order') !!}</td>
                        <td>{{ $Order->company_inn }}</td>
                    </tr>
                    <tr>
                        <td>{!! _t('КПП', 'order') !!}</td>
                        <td>{{ $Order->company_kpp }}</td>
                    </tr>
                    <tr>
                        <td>{!! _t('БИК', 'order') !!}</td>
                        <td>{{ $Order->company_bik }}</td>
                    </tr>
                    <tr>
                        <td nowrap>{!! _t('Расчетный счет', 'order') !!}</td>
                        <td>{{ $Order->company_rs }}</td>
                    </tr>
                @else
                    <tr>
                        <td>{!! _t('ФИО', 'order') !!}</td>
                        <td>{{ $Order->name }}</td>
                    </tr>
                    <tr>
                        <td>{!! _t('Индекс', 'order') !!}</td>
                        <td>{{ $Order->zip }}</td>
                    </tr>
                    <tr>
                        <td>{!! _t('Страна', 'order') !!}</td>
                        <td>{{ $Order->country }}</td>
                    </tr>
                    <tr>
                        <td>{!! _t('Город', 'order') !!}</td>
                        <td>{{ $Order->city }}</td>
                    </tr>
                    <tr>
                        <td>{!! _t('Телефон', 'order') !!}</td>
                        <td>{{ $Order->phone }}</td>
                    </tr>
                    <tr>
                        <td>E-mail:</td>
                        <td>{{ $Order->email }}</td>
                    </tr>
                @endif
                <tr>
                    <td>{!! _t('Комментарии', 'order') !!}</td>
                    <td>{{ nl2br($Order->comments) }}</td>
                </tr>
                <tr>
                    <td nowrap>{!! _t('Способ доставки', 'order') !!}</td>
                    <td>
                        {{ $Order->delivery_company }}
                    </td>
                </tr>
                @if($Order->delivery_company !== 'Самовывоз')
                    <tr>
                        <td nowrap>{!! _t('Адрес доставки', 'order') !!}</td>
                        <td>
                            {{ $Order->delivery_type }}
                        </td>
                    </tr>
                @endif
                @if($Order->free_delivery)
                    <tr>
                        <td nowrap>{!! _t('Стоимость доставки', 'order') !!}</td>
                        <td>{!! _t('Бесплатная доставка', 'order') !!}</td>
                    </tr>
                @endif
                <tr>
                    <td>{!! _t('Вариант оплаты', 'order') !!}</td>
                    <td>
                        @if($Order->type == Order::TYPE_COMPANY)
                            {{ _t('По счёту', 'order') }}
                        @elseif($Order->payment_id)
                            {{ _t('Онлайн', 'order') }}
                        @else
                            {{ _t('Наличными', 'order') }}
                        @endif
                    </td>
                </tr>
            </tbody>
        </table>
        <br />
        <table cellspacing="0" cellpadding="2" border="1">
            <tr>
                <th>№</th>
                <th>{!! _t('Артикул', 'order') !!}</th>
                <th>{!! _t('Фото', 'order') !!}</th>
                <th>{!! _t('Цвет', 'order') !!}</th>
                <th>{!! _t('Цена, руб. за 1 шт.', 'order') !!}</th>
                <th>{!! _t('Кол-во, шт.', 'order') !!}</th>
                <th>{!! _t('Сумма, руб.', 'order') !!}</th>
            </tr>
            @foreach($OrderItems as $OrderItem)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td><a href="{{ route('slug', ['slug' => $OrderItem->item->slug()]) }}">{{ $OrderItem->title }}</a></td>
                    <td>
                        @if($OrderItem->image)
                            <img src="{{ $OrderItem->getImageLink() }}" alt="{{ $OrderItem->title }}" height="95" width="95">
                        @endif
                    </td>

                    @if($OrderItem->promo)
                        <td colspan="4">{!! _t('Бесплатные образцы', 'order') !!}</td>
                    @else
                        <td>{{ Color::cachedOne($OrderItem->color)->name }}</td>
                        <td>{{ Formatter::price($OrderItem->price) }}</td>
                        <td>{{ $OrderItem->quant }}</td>
                        <td>{{ Formatter::price($OrderItem->price * $OrderItem->quant) }}</td>
                    @endif
                </tr>
            @endforeach
            <tr>
                <td colspan="5">&nbsp;</td>
                <td><strong>{!! _t('Итого', 'order') !!}</strong></td>
                <td><strong>{{ Formatter::price($Order->total_price) }}</strong></td>
            </tr>
        </table>
        <br />
        <p>
            {!! _t('Это письмо сформировано автоматически, отвечать на него не нужно.', 'order') !!}
        </p>
    </body>
</html>