@php
    declare(strict_types=1);

    Meta::set('title', _t('Оформление заказа', 'order'));
@endphp

@extends('layouts.master')

@section('content')
    {{--TODO:--}}
    {{--{!! Breadcrumbs::render('order') !!}--}}
    <div class="container">
        <div class="page-header">
            <div class="page-header__title-wrap">
                <h1 class="page__title">{!! _t('Оформление заказа', 'order') !!}</h1>
            </div>
        </div>
        <div class="page">
            <div class="page__content page__content_order page__content_background page__content_text">
                <div class="order-complete__title">
                    {{--TODO: мб стоит вынести формат даты в глобальный метод--}}
                    {!! _t('Ваш заказ №%s от %s передан в отдел продаж.', 'order', [Request::get('OrderId'), Formatter::date(Request::get('Date', ''))]) !!} {!! _t('Спасибо что выбрали нас.', 'order') !!}
                </div>
                <div class="order-complete__text">
                    {!! _t('В ближайшее время менеджер свяжется с вами по указаным телефонам. Счет будет выставлен после подтверждения заказа.', 'order') !!}
                </div>
            </div>
        </div>
    </div>
@endsection
