@php
    declare(strict_types=1);
@endphp

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="{{ mix('css/manager.css') }}"/>
<script src="{{ mix('js/manager.js') }}"></script>

<div class="manager-panel">
    <div class="manager-panel__item">
        <div class="manager-panel__name">
            {{ Auth::user()->name }}
        </div>
    </div>
    <a href="{{ route('compare') }}" class="manager-panel__item">
        <div class="manager-panel__compare">
            {{ _t('В сравнении:', 'favorites') }} <span class="js-manager-panel-compare-count">0</span>
        </div>
    </a>
    <div class="manager-panel__item manager-panel__item_link compare-flush" title="{{ _t('Удалить сравнение', 'favorites') }}">
        <i class="fa fa-trash"></i>
    </div>
    @if(isset($CurrentCity) && $CurrentCity->free_delivery_price)
        <div class="manager-panel__item">
            Сумма для бесплатной доставки в город {{ $CurrentCity->title }}: {{ Formatter::price($CurrentCity->free_delivery_price, true) }}
        </div>
    @endif
</div>
