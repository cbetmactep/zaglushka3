@php
    declare(strict_types=1);

    use App\Models\Order;
    use App\Models\Color;
    use App\Models\Item;

    /** @var \App\Models\Order $Order */
@endphp
<html>
<body>
@if (count($SpecialsChangesItemsAdded) > 0)
    <b>Добавленные товары в специальные предложение за {{ $date }}</b>
    <table cellspacing="0" cellpadding="2" border="1">
        <tr>
            <th rowspan="2">{!! _t('№', 'order') !!}</th>
            <th rowspan="2">{!! _t('Артикул', 'order') !!}</th>
            <th rowspan="2">{!! _t('Фото', 'order') !!}</th>
            <th rowspan="2">{!! _t('Описание', 'order') !!}</th>
            <th colspan="7">Цена за 1 шт.</th>
        </tr>
        <tr>
            <th>{!! _t('от 30000', 'order') !!}</th>
            <th>{!! _t('от 10000', 'order') !!}</th>
            <th>{!! _t('от 5000', 'order') !!}</th>
            <th>{!! _t('от 1000', 'order') !!}</th>
            <th>{!! _t('от 500', 'order') !!}</th>
            <th>{!! _t('от 100', 'order') !!}</th>
            <th>{!! _t('Розница', 'order') !!}</th>
        </tr>
        @foreach($SpecialsChangesItemsAdded as $SpecialsChangesItem)
            @if (!$SpecialsChangesItem->properties['attributes']['special'])
                @continue
            @endif

            @php
                $Item = $SpecialsChangesItem->subject;
            @endphp
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td><a href="{{ route('slug', ['slug' => $Item->slug()]) }}">{{ $Item->title }}</a></td>
                <td>
                    @if($Item->isImageExists(Item::IMAGE_TYPE_5))
                        <img src="{{ Request::root() . $Item->getImageLink(Item::IMAGE_TYPE_5) }}" alt="Схема {{ $Item->title }}">
                    @endif
                </td>
                <td>{{ $Item->short_descr }}</td>


                @for($i = 6; $i >= 0; $i--)
                    <td @if($Item->special && $Item->getSpecialDiscount(5))class="price-specials" @endif>
                        @if ($i != 6 && $Item->getSpecialDiscount($i))
                            <div style="white-space: nowrap"><s>{{ Formatter::price($Item['price' . $i], true) }}</s></div>
                            <div style="white-space: nowrap" class="price-discount @if($Item->getSpecialDiscount($i) === 50)price-discount_hot @endif">−{{ $Item->getSpecialDiscount($i) }} %</div>
                            <div style="white-space: nowrap">{{ Formatter::price($Item->getDiscountPrice($i), true) }}</div>
                        @else
                            <span style="white-space: nowrap">{{ Formatter::price($Item['price' . $i], true) }}</span>
                        @endif
                    </td>
                @endfor
            </tr>

        @endforeach
    </table>
@endif
@if (count($SpecialsChangesItemsRemoved) > 0)
    <br>
    <b>Удаленные товары из специальных предложений за {{ $date }}</b>
    <table cellspacing="0" cellpadding="2" border="1">
        <tr>
            <th rowspan="2">{!! _t('№', 'order') !!}</th>
            <th rowspan="2">{!! _t('Артикул', 'order') !!}</th>
            <th rowspan="2">{!! _t('Фото', 'order') !!}</th>
            <th rowspan="2">{!! _t('Описание', 'order') !!}</th>
            <th colspan="7">Цена за 1 шт.</th>
        </tr>
        <tr>
            <th>{!! _t('от 30000', 'order') !!}</th>
            <th>{!! _t('от 10000', 'order') !!}</th>
            <th>{!! _t('от 5000', 'order') !!}</th>
            <th>{!! _t('от 1000', 'order') !!}</th>
            <th>{!! _t('от 500', 'order') !!}</th>
            <th>{!! _t('от 100', 'order') !!}</th>
            <th>{!! _t('Розница', 'order') !!}</th>
        </tr>
        @foreach($SpecialsChangesItemsRemoved as $SpecialsChangesItem)
            @if ($SpecialsChangesItem->properties['attributes']['special'])
                @continue
            @endif

            @php
                $Item = $SpecialsChangesItem->subject;
            @endphp
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td><a href="{{ route('slug', ['slug' => $Item->slug()]) }}">{{ $Item->title }}</a></td>
                <td>
                    @if($Item->isImageExists(Item::IMAGE_TYPE_5))
                        <img src="{{ Request::root() . $Item->getImageLink(Item::IMAGE_TYPE_5) }}" alt="Схема {{ $Item->title }}">
                    @endif
                </td>
                <td>{!! $Item->short_descr !!}</td>


                @for($i = 6; $i >= 0; $i--)
                    <td @if($Item->special && $Item->getSpecialDiscount(5))class="price-specials" @endif>
                        @if ($i != 6 && $Item->getSpecialDiscount($i))
                            <div style="white-space: nowrap"><s>{{ Formatter::price($Item['price' . $i], true) }}</s></div>
                            <div style="white-space: nowrap" class="price-discount @if($Item->getSpecialDiscount($i) === 50)price-discount_hot @endif">−{{ $Item->getSpecialDiscount($i) }} %</div>
                            <div style="white-space: nowrap">{{ Formatter::price($Item->getDiscountPrice($i), true) }}</div>
                        @else
                            <span style="white-space: nowrap">{{ Formatter::price($Item['price' . $i], true) }}</span>
                        @endif
                    </td>
                @endfor
            </tr>
        @endforeach
    </table>
@endif


<style>
    .price-discount {
        color: rgba(0, 0, 0, .5);
    }

    .price-discount_hot {
        color: red;
    }
</style>
</body>
</html>
