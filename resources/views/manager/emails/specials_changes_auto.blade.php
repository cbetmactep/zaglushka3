@php
    declare(strict_types=1);

    use App\Models\Order;
    use App\Models\Color;
    use App\Models\Item;

    /** @var \App\Models\Order $Order */
@endphp
<html>
<body>
<b>{{ $date }}. Следующие товары были автоматически удалены из специальных предложений за неимением остатков:</b>
<table cellspacing="0" cellpadding="2" border="1">
    <tr>
        <th rowspan="2">{!! _t('№', 'order') !!}</th>
        <th rowspan="2">{!! _t('Артикул', 'order') !!}</th>
        <th rowspan="2">{!! _t('Фото', 'order') !!}</th>
        <th rowspan="2">{!! _t('Описание', 'order') !!}</th>
        <th colspan="7">Цена за 1 шт.</th>
    </tr>
    <tr>
        <th>{!! _t('от 30000', 'order') !!}</th>
        <th>{!! _t('от 10000', 'order') !!}</th>
        <th>{!! _t('от 5000', 'order') !!}</th>
        <th>{!! _t('от 1000', 'order') !!}</th>
        <th>{!! _t('от 500', 'order') !!}</th>
        <th>{!! _t('от 100', 'order') !!}</th>
        <th>{!! _t('Розница', 'order') !!}</th>
    </tr>
    @foreach($SpecialsChangesItems as $SpecialsChangesItem)
        @php
            $Item = $SpecialsChangesItem->subject;
        @endphp
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td><a href="{{ route('slug', ['slug' => $Item->slug()]) }}">{{ $Item->title }}</a></td>
            <td>
                @if($Item->isImageExists(Item::IMAGE_TYPE_5))
                    <img src="{{ Request::root() . $Item->getImageLink(Item::IMAGE_TYPE_5) }}" alt="Схема {{ $Item->title }}">
                @endif
            </td>
            <td>{{ $Item->short_descr }}</td>


            @for($i = 6; $i >= 0; $i--)
                <td @if($Item->special && $Item->getSpecialDiscount(5))class="price-specials" @endif>
                    @if ($i != 6 && $Item->getSpecialDiscount($i))
                        <div style="white-space: nowrap"><s>{{ Formatter::price($Item['price' . $i], true) }}</s></div>
                        <div style="white-space: nowrap" class="price-discount @if($Item->getSpecialDiscount($i) === 50)price-discount_hot @endif">−{{ $Item->getSpecialDiscount($i) }} %</div>
                        <div style="white-space: nowrap">{{ Formatter::price($Item->getDiscountPrice($i), true) }}</div>
                    @else
                        <span style="white-space: nowrap">{{ Formatter::price($Item['price' . $i], true) }}</span>
                    @endif
                </td>
            @endfor
        </tr>

    @endforeach
</table>

<style>
    .price-discount {
        color: rgba(0, 0, 0, .5);
    }

    .price-discount_hot {
        color: red;
    }
</style>
</body>
</html>
