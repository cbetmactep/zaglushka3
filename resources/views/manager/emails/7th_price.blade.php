@php
    declare(strict_types=1);

    use App\Models\Order;
    use App\Models\Color;
    use App\Models\Item;
    use App\Helpers\Formatter;

    /** @var \App\Models\Order $Order */
@endphp
<html>
<body>
<b>Изменение седьмой цены за {{ $date }}</b>
<table cellspacing="0" cellpadding="2" border="1">
    <tr>
        <th rowspan="2">{!! _t('№', 'order') !!}</th>
        <th rowspan="2">{!! _t('Артикул', 'order') !!}</th>
        <th rowspan="2">{!! _t('Фото', 'order') !!}</th>
        <th rowspan="2">{!! _t('Описание', 'order') !!}</th>
        <th colspan="7">Цена за 1 шт.</th>
    </tr>
    <tr>
        <th>{!! _t('от 30000', 'order') !!}</th>
        <th>{!! _t('от 10000', 'order') !!}</th>
        <th>{!! _t('от 5000', 'order') !!}</th>
        <th>{!! _t('от 1000', 'order') !!}</th>
        <th>{!! _t('от 500', 'order') !!}</th>
        <th>{!! _t('от 100', 'order') !!}</th>
        <th>{!! _t('Розница', 'order') !!}</th>
    </tr>
    @foreach($Price6ChangesItems as $Price6ChangesItem)
        @php
            $Item = $Price6ChangesItem->subject;
        @endphp
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td><a href="{{ route('slug', ['slug' => $Item->slug()]) }}">{{ $Item->title }}</a></td>
            <td>
                @if($Item->isImageExists(Item::IMAGE_TYPE_5))
                    <img src="{{ Request::root() . $Item->getImageLink(Item::IMAGE_TYPE_5) }}" alt="Схема {{ $Item->title }}">
                @endif
            </td>
            <td>{{ $Item->short_descr }}</td>

            @php
                for ($i = 6; $i >= 0; $i--) {
                $price = 'price' . $i;
                    $oldPrice = isset($Price6ChangesItem->properties['old'][$price]) ? (float) $Price6ChangesItem->properties['old'][$price] : 0;
                    @endphp
                        <td>
                            <span style="white-space: nowrap">{!! $oldPrice ? Formatter::price((float) $oldPrice, false, true) : '' !!}</span>
                            <br>
                            <span style="white-space: nowrap">{{ Formatter::price($Item->$price, true) }}</span>
                        </td>
                    @php
                }
            @endphp

            <td>
                <span style="white-space: nowrap">старая цена</span> <br>
                <span style="white-space: nowrap">новая цена</span>
            </td>
        </tr>
    @endforeach
</table>
</body>
</html>