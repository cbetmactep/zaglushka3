@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="page-header">
            <div class="page-header__title-wrap">
                <h1 class="page__title">Сброс пароля</h1>
            </div>
        </div>
        <div class="page">
            <div class="page__content page__content_text page__content_background">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">Email адрес</label>
                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <button class="btn btn_outline_primary">
                        Отправить ссылку сброса пароля
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection
