@php
	declare(strict_types=1);

	use App\Models\Category;
	use App\Models\City;
	use morphos\Russian\GeographicalNamesInflection;

	/**
	 * @var Category $category
	 * @var Category $subcategory
	 */

    if (Config::get('app.subdomain')) {
		$City = City::cachedOne((int) session('current_city'));
	} else {
		$City = City::cachedOne(25);
	}

	Meta::set('title', 'Интернет магазин заглушек в ' . GeographicalNamesInflection::getCase($City->title, 'предложный') . ' | ' . $City->phone);
	Meta::set('description', 'Интернет-магазин пластиковых заглушек и прочей пластиковой фурнитуры в '
	. GeographicalNamesInflection::getCase($City->title, 'предложный')
	. ' ЗАГЛУШКА.РУ приглашает на свой официальный сайт. Доставка по всей России. Доставим заказ бесплатно: подробности на сайте или звоните по телефону ' . $City->phone .'.');

	Meta::set('keywords', _t('магазин заглушек, интернет магазин заглушек, заглушка пластиковая магазин, магазин заглушек для труб', 'pages'));

@endphp

@extends('layouts.master')

@section('content')
	<div class="container">
		<div class="page">
			<img class="banner" src="/images/zaglushkaru.jpg" alt="" style="margin: 20px auto 0; max-width:1140px;">
			@if(!Config::get('app.subdomain'))
				<h1 class="page__title p-main__title">{!! _t('Изготовление пластиковых заглушек', 'pages') !!}</h1>
			@endif
			<div class="page__content">
				<div class="p-main-catalog__wrap">
					@foreach($categories as $category)
                                            @if($category->id!=40 and $category->id!=39 and $category->id!=210)
						<section class="p-main-catalog">
							<div class="p-main-catalog__row">
								<h2 class="p-main-catalog__title">
									<a class="p-main-catalog__link" href="{{ route('slug', ['slug' => $category->slug()]) }}">{{ $category->getTitle() }}</a>
								</h2>
							</div>
							<div class="catalog">
							@foreach($category->subcategories as $subcategory)
								<div class="catalog-item__wrap">
									<a class="catalog-item" href="{{ route('slug', ['slug' => $subcategory->slug()]) }}">
										<div class="catalog-item__image">
											@if($subcategory->isImageExists(Category::IMAGE_PATH_LIST))
												<img src="{{ $subcategory->getImageLink(Category::IMAGE_PATH_LIST) }}" width="160" height="160" alt="{{ $subcategory->getTitle() }}" loading="lazy">
											@endif
										</div>
										<div class="catalog-item__name">
										<span>
											{{ $subcategory->getTitle() }}
										</span>
									</div>
									<div class="catalog-item__counter">
										@component('components.icon', ['name' => 'checkcart', 'attributes' => ['class' => 'catalog-item__counter-icon']])@endcomponent
										{!! _t('в наличии', 'common') !!} {{ Formatter::quantity($subcategory->itemsCount()) }}
									</div>

								</a>
							</div>
						@endforeach
						</div>
					</section>
                                            @endif
				@endforeach</div>
				<div class="p-main-catalog__wrap_mob">
					<section class="p-main-catalog">
						<div class="catalog">
							@foreach($categories as $category)
								<div class="catalog-item__wrap">
									<a class="catalog-item" href="{{ route('slug', ['slug' => $category->slug()]) }}">
										<div class="catalog-item__image">
											@if($category->isBreadcrumbsImageExists())
												<img src="{{ asset('images/dummy_95.png') }}" data-src="{{ $category->getBreadcrumbsImage() }}" height="160" alt="{{ $category->getTitle() }}">
											@elseif ($category->isImageExists(Category::IMAGE_PATH_LIST))
												<img src="{{ asset('images/dummy_95.png') }}" data-src="{{ $category->getImageLink(Category::IMAGE_PATH_LIST) }}" height="160" alt="{{ $category->getTitle() }}">
											@endif
										</div>
										<div class="catalog-item__name">{{ $category->getTitle() }}</div>
									</a>
								</div>
							@endforeach
						</div>
					</section>
				</div>
			</div>
			<div class="p-main-epilog">
				@if(App::isLocale('ru'))
					@if(!Config::get('app.subdomain'))
						<section class="p-main-epilog__block">
							<h2 class="p-main-epilog__title">Компания «Заглушка.Ру» – крупнейший производитель и поставщик пластиковой фурнитуры</h2>
							<p class="p-main-epilog__paragraph">Мы производим и продаём заглушки, опоры, фиксаторы, колпачки и другую пластиковую фурнитуру уже более 25 лет. «Заглушка.Ру» – это более 50'000'000 изделий в наличии на складе в Санкт-Петербурге, более 20'000 товаров в каталоге, около 2'500 наименований производимой продукции, общая площадь складских помещений – 7'500 м², около 30'000 довольных клиентов ежегодно. Сегодня с уверенностью можно сказать, что наша компания занимает лидирующие позиции на российском рынке пластиковой фурнитуры.</p>
							<p class="p-main-epilog__paragraph">Пластиковые заглушки составляют основу нашего ассортимента. Они имеют ряд преимуществ: устойчивы к коррозии, воздействию солнечных лучей и перепадам температур, просто и легко монтируются, служат долгие годы. Пластиковые заглушки, представленные в нашем каталоге, универсальны в использовании и применяются в различных сферах производства.</p>
						</section>
						<section class="p-main-epilog__block">
							<h2 class="p-main-epilog__title">Пластиковые заглушки в широком ассортименте</h2>
							<p class="p-main-epilog__paragraph">Наша компания предлагает <a class="link" href="{{ route('slug', ['slug' => Category::cachedOne(60)->slug()]) }}">заглушки для труб различных размеров</a> и форм в разнообразной цветовой палитре. Мы постоянно работаем над расширением ассортимента: разрабатываем новые изделия, в том числе изготавливаем продукцию под заказ по индивидуальным запросам, проектируем и производим новые пресс-формы, заключаем договоры с российскими и зарубежными фирмами-поставщиками.
							</p>
							<p class="p-main-epilog__paragraph">Собственное производство полипропиленовых заглушек, опор, фиксаторов и другой фурнитуры постоянно обеспечивает склад продукцией, поэтому 90% представленных в каталоге изделий всегда в наличии. Сотрудничая с нами, вы получаете продукцию по конкурентоспособной цене в кратчайшие сроки.</p>
							<p class="p-main-epilog__paragraph">Изготовление пластиковых заглушек – основное, но не единственное направление деятельности нашей компании. У нас вы можете найти широкий ряд пластиковой фурнитуры, необходимой для производства мебели, а также различные комплектующие для обустройства детских и спортивных площадок.</p>
							<p class="p-main-epilog__paragraph">Мы делаем всё возможное, чтобы стать надёжным партнёром вашей компании. У нас вы обязательно найдёте продукцию для разных сфер производства, потому что «Заглушка.Ру» – крупнейший в России производитель и поставщик пластиковой фурнитуры.</p>
						</section>
					@endif
				@elseif(App::isLocale('it'))
					<section class="p-main-epilog__block">
						<h2 class="p-main-epilog__title">L'azienda "Zaglushka.Ru" è uno dei più grandi produttori e fornitori di accessori in plastica </h2>
						<p class="p-main-epilog__paragraph">Produciamo e vendiamo tappi, supporti, vite a manopola, cappucci e altri prodotti plastici già per più di 25 anni. "Zaglushka.Ru" sono più di 50'000'000 articoli disponibili a magazzino a San Pietroburgo, più di 20'000 articoli nel catalogo, circa 2' 500 titoli di articoli che produciamo, superficie totale dei magazzini è di 7'500 m ², circa 30'000 clienti felici ogni anno. Oggi possiamo dire con certezza che la nostra azienda è in prima linea nel mercato russo di accessori in plastica. </p>
						<p class="p-main-epilog__paragraph">I tappi di plastica sono la base del nostro assortimento. Hanno una serie di vantaggi: resistenti alla corrosione, ai raggi solari e agli sbalzi di temperatura, sono facili da montare e durono a lungo. I tappi di plastica presentati nel nostro catalogo sono universalmente utilizzabili e applicabili in diversi settori di produzione.</p>
					</section>
					<section class="p-main-epilog__block">
						<h2 class="p-main-epilog__title">Una vasta gamma di tappi in plastica</h2>
						<p class="p-main-epilog__paragraph">La nostra azienda offre <a class="link" href="{{ route('slug', ['slug' => Category::cachedOne(60)->slug()]) }}">i tappi per tubi di diverse dimensioni</a> e forme, e di gamma ampia di colori. Lavoriamo costantemente per ampliare l'assortimento: sviluppiamo nuovi prodotti, compresa la produzione dei prodotti in base alle richieste personalizzate, progettiamo e produciamo nuovi stampi, firmiamo contratti con i fornitori russi e stranieri.</p>
						<p class="p-main-epilog__paragraph">La nostra produzione dei tappi in polipropilene, dei piedini, delle viti a manopola e altri accessori fornisce costantemente la merce al magazzino. Di conseguenza il 90% degli articoli del catalogo sono sempre disponibili. Collaborando con noi ottenete i prodotti ad un prezzo competitivo e nei tempi più brevi possibili. </p>
						<p class="p-main-epilog__paragraph">La produzione di tappi in plastica è l'attività principale, ma non unica della nostra azienda. Da noi potete trovare una vasta gamma di forniture di plastica necessarie per la produzione di mobili, nonché accessori vari per parchi giochi per bambini e per aree sportive. </p>
						<p class="p-main-epilog__paragraph">Facciamo tutto il possibile per essere un partner affidabile della vostra azienda. Da noi sicuramente troverete i prodotti per diversi settori di produzione, perché "Zaglushka.Ru" è uno dei più grandi produttori e fornitori di accessori di plastica in Russia.</p>
					</section>
				@else
					<section class="p-main-epilog__block">
						<h2 class="p-main-epilog__title">The company "Zaglushka.Ru" is the largest manufacturer and supplier of plastic fittings</h2>
						<p class="p-main-epilog__paragraph">We produce and sell plugs, supports, clips, caps and other plastic accessories for over 25 years. "Zaglushka.Ru" has more than 50'000'000 products in stock in Saint-Petersburg and more than 20'000 items in the catalogue, about 2'500 types of products and about 30'000 satisfied clients every year, the total area of warehouse premises is 7'500 m2. Today we can confidently say that our company takes the leading positions on the Russian market of plastic fittings.</p>
						<p class="p-main-epilog__paragraph">Plastic plugs are the basis of our range. They have a number of advantages: resistant to corrosion, sunlight and temperature extremes, simple and easy to install and work for many years. Plastic plugs, presented in our catalogue, are universal in use and applied in different spheres of production.</p>
					</section>
					<section class="p-main-epilog__block">
						<h2 class="p-main-epilog__title">Plastic plugs in a wide range</h2>
						<p class="p-main-epilog__paragraph">Our company offers <a class="link" href="{{ route('slug', ['slug' => Category::cachedOne(60)->slug()]) }}">plugs for tubes of different sizes</a> and shapes in various colors. We are constantly working on expanding the range: develop new products, including produce products according to individual needs, design and manufacture new molds, conclude contracts with Russian and foreign suppliers.</p>
						<p class="p-main-epilog__paragraph">Own production of polypropylene plugs, supports, clamps and fittings assures warehouse production, so 90% of the catalog products are always in stock. Cooperating with us, you get products at a competitive price in the shortest possible time.</p>
						<p class="p-main-epilog__paragraph">Manufacturing of plastic plugs is the main but not the only direction of our company. Here you can find a wide range of plastic fittings needed for the production of furniture and various accessories for arrangement of playgrounds and sports grounds.</p>
						<p class="p-main-epilog__paragraph">We do our best to become a reliable partner of your company. We are sure you will find products for different spheres of production, because "Zaglushka.Ru" is the largest Russian manufacturer and supplier of plastic accessories.</p>
					</section>
				@endif
			</div>
		</div>
	</div>
@endsection
