@php
    declare(strict_types=1);
    use App\Models\City;
    use morphos\Russian\GeographicalNamesInflection;

    Meta::set('title', _t('О компании', 'pages'));
    Meta::set('keywords', 'пластиковые заглушки для труб резьбовые опоры пластиковые декоративные заглушки мебельные латодержатели колпачки колесные опоры', 'pages');
@endphp

@extends('layouts.master')

@section('content')
    {!! Breadcrumbs::render('about') !!}
    <div class="container">
        <div class="page-header">
            <div class="page-header__title-wrap">
                <h1 class="page__title">{!! _t('О компании', 'pages') !!}</h1>
            </div>
        </div>
        <div class="page">
            <div class="page__content page__content_text page__content_background">
                @if(App::isLocale('ru'))
                    @if(Config::get('app.subdomain'))
                        <p>
                            Наша компания уже 25 лет работает на территории г. {{ GeographicalNamesInflection::getCase(City::cachedOne((int) session('current_city'))->title, 'родительный') }}. За эти годы мы установили успешные
                            партнёрские взаимоотношения с крупнейшими компаниями города и области. Мы предлагаем
                            пластиковые заглушки для труб различного сечения, резьбовые регулируемые опоры и подпятники,
                            ручки-фиксаторы, колпачки и различные канатные конструкции и комплектующие для детских
                            комплексов. Сегодня мы сотрудничаем со всеми градообразующими предприятиями, поставляя
                            пластиковую продукцию для разных сфер производства: машиностроения, химической и
                            нефтехимической отраслей, мебельной промышленности и др., а также предлагаем изделия для
                            обустройства детских площадок и спортивных комплексов.
                        </p>
                        <p>
                            Профессиональный подход, постоянное расширение ассортимента за счёт собственного
                            производства, индивидуальная работа по
                            запросам, гарантия качества и безопасности каждого изделия – мы делаем всё, чтобы стать
                            партнёром, которому доверяют и с которым комфортно работать.
                        </p>
                    @else
                        <p>Наша компания успешно работает на российском рынке уже более 25 лет! Постоянно вкладывая в
                            развитие новых направлений и расширяясь, на данный момент является <strong>крупнейшим в
                                России</strong> производителем и поставщиком пластиковой фурнитуры.</p>
                        <p>Большая часть продукции всегда <strong>в наличии</strong> на нашем складе в Санкт-Петербурге
                            в больших количествах. На каждое изделие есть <strong>сертификат качества и паспорт</strong>,
                            которые, по вашему желанию, мы можем предоставить в любой момент. Ассортимент постоянно
                            пополняется и подстраивается под нужды и потребности наших коллег и партнеров.</p>
                        <p>Мы всегда готовы рассмотреть ваши пожелания и предложения по удобным для Вас условиям работы.
                            Просто напишите, какие заглушки и опоры Вы используете, и мы отправим Вам коробку с
                            бесплатными образцами по указанному адресу в любой момент. По любым возникающим у Вас
                            вопросам звоните или пишите нам <a href="{{ route('contacts') }}" class="link">по
                                указанным контактам.</a></p>
                        <img class="p-about__image" src="/images/about/zaglushkaru_company15.jpg" alt="">
                        <img class="p-about__image" src="/images/about/zaglushkaru_company14.jpg" alt="">
                        <img class="p-about__image" src="/images/about/zaglushkaru_company13.jpg" alt="">
                        <img class="p-about__image" src="/images/about/zaglushkaru_company12.jpg" alt="">
                        <img class="p-about__image" src="/images/about/zaglushkaru_company10.jpg" alt="">
                        <img class="p-about__image" src="/images/about/zaglushkaru_company11.jpg" alt="">
                        <img class="p-about__image" src="/images/about/zaglushkaru_company08.jpg" alt="">
                        <img class="p-about__image" src="/images/about/zaglushkaru_company01.jpg" alt="">
                        <img class="p-about__image" src="/images/about/zaglushkaru_company03.jpg" alt="">
                        <img class="p-about__image" src="/images/about/zaglushka_company21.jpg" alt="">
                    @endif
                @elseif (App::isLocale('it'))
                    <p>La nostra azienda lavora con successo nel mercato russo da più di 25 anni! Espandendosi
                        costantemente e investendo in nuove direzioni, al giorno d'oggi la nostra azienda è <strong>il
                            più grande produttore e fornitore di accessori di plastica in Russia</strong>.</p>
                    <p>La maggior parte dei prodotti è sempre <strong>disponibile</strong> nel nostro magazzino di San
                        Pietroburgo in grandi quantità. Ogni prodotto ha <strong>un certificato di qualità e
                            passaporto</strong> che possiamo fornire in qualsiasi momento a Vostra richiesta.
                        L'assortimento si rifornisce costantemente e si adegua alle esigenze e alle richieste dei nostri
                        colleghi e partner.</p>
                    <p>Siamo sempre pronti a prendere in considerazione i Vostri desideri e suggerimenti per le
                        condizioni di lavoro più comode per Voi. Basta scriverci quali tappi e piedini usate e vi
                        invieremo una scatola di campioni gratuiti presso l'indirizzo specificato. Se avete domande non
                        esitate a contattarci: chiamate o scrivete noi a <a href="{{ route('contacts') }}" class="link">questi
                            contatti</a>.</p>
                    <img class="p-about__image" src="/images/about/zaglushkaru_company15.jpg" alt="">
                    <img class="p-about__image" src="/images/about/zaglushkaru_company14.jpg" alt="">
                    <img class="p-about__image" src="/images/about/zaglushkaru_company13.jpg" alt="">
                    <img class="p-about__image" src="/images/about/zaglushkaru_company12.jpg" alt="">
                    <img class="p-about__image" src="/images/about/zaglushkaru_company10.jpg" alt="">
                    <img class="p-about__image" src="/images/about/zaglushkaru_company11.jpg" alt="">
                    <img class="p-about__image" src="/images/about/zaglushkaru_company08.jpg" alt="">
                    <img class="p-about__image" src="/images/about/zaglushkaru_company01.jpg" alt="">
                    <img class="p-about__image" src="/images/about/zaglushkaru_company03.jpg" alt="">
                    <img class="p-about__image" src="/images/about/zaglushka_company21.jpg" alt="">
                @else
                    <p>Our company successfully works on the Russian market for more than 25 years! Constantly investing
                        in the development of new directions and expanding, at the moment it is <strong>the largest
                            manufacturer</strong> and supplier of plastic accessories <strong>in Russia</strong>.</p>
                    <p>Most products are always <strong>in stock</strong> at our warehouse in Saint-Petersburg in large
                        quantities. On each product we have <strong>a quality certificate and passport</strong>, which,
                        at Your request, we can provide at any time. The range is constantly updated and adapts to the
                        needs of our colleagues and partners.</p>
                    <p>We are always ready to consider your wishes and offers about comfortable working conditions for
                        you. Just write us what plugs and supports you use and we'll send you a box of free samples at
                        the specified address at any time. For any questions call or write us <a
                                href="{{ route('contacts') }}" class="link">at the contacts.</a></p>
                    <img class="p-about__image" src="/images/about/zaglushkaru_company15.jpg" alt="">
                    <img class="p-about__image" src="/images/about/zaglushkaru_company14.jpg" alt="">
                    <img class="p-about__image" src="/images/about/zaglushkaru_company13.jpg" alt="">
                    <img class="p-about__image" src="/images/about/zaglushkaru_company12.jpg" alt="">
                    <img class="p-about__image" src="/images/about/zaglushkaru_company10.jpg" alt="">
                    <img class="p-about__image" src="/images/about/zaglushkaru_company11.jpg" alt="">
                    <img class="p-about__image" src="/images/about/zaglushkaru_company08.jpg" alt="">
                    <img class="p-about__image" src="/images/about/zaglushkaru_company01.jpg" alt="">
                    <img class="p-about__image" src="/images/about/zaglushkaru_company03.jpg" alt="">
                    <img class="p-about__image" src="/images/about/zaglushka_company21.jpg" alt="">
                @endif
            </div>
        </div>
    </div>
@endsection