@php
    declare(strict_types=1);

    use App\Models\Category;
@endphp

<div class="p-delivery__body">
    <p class="p-delivery__paragraph">
        Заказы отправляются каждый будний день по всей территории РФ.
        <br/>
        Доставка возможна как до терминала транспортной компании в вашем городе, так и «до дверей» по указанному адресу. Мы оформляем все необходимые документы и за свой счет довозим товар до склада транспортной компании в г. Санкт-Петербурге. Вы оплачиваете только услуги транспортной компании.
    </p>
    <p class="p-delivery__paragraph">
        Мы также предлагаем бесплатную доставку по указанному в заказе адресу. В случае, если на ваш заказ, распространяется бесплатная доставка «до дверей», мы оплачиваем все транспортные расходы.
    </p>
    @if($CurrentCity && $CurrentCity->free_delivery_price)
        <div class="p-delivery__title">Условия бесплатной доставки «до дверей»:</div>
        <ul class="p-delivery__list">
            <li>сумма заказа от {{ number_format($CurrentCity->free_delivery_price, 0, ' ', ' ') }} рублей (для города {{ $CurrentCity->title }}), без учета товаров со скидкой;</li>
            <li>заказ полностью оплачен;</li>
            <li>заказ не содержит крупногабаритные товары: <a class="link" href="{{ route('slug', ['slug' => Category::cachedOne(499)->slug()]) }}">горки</a>,
                <a class="link" href="{{ route('slug', ['slug' => Category::cachedOne(541)->slug()]) }}">стальные цепи</a>,
                <a class="link" href="{{ route('slug', ['slug' => Category::cachedOne(498)->slug()]) }}">сиденья</a> и
                <a class="link" href="{{ route('slug', ['slug' => Category::cachedOne(498)->slug()]) }}">каркасы для стульев</a>.
            </li>
        </ul>
    @endif
    <div class="p-delivery__title p-delivery__title_slider">Сотрудничаем с более чем 40 транспортными компаниями:</div>
    <div class="p-delivery__slider-wrap">
        <div class="p-delivery__slider-container swiper-container">
            <div class="p-delivery__slider swiper-wrapper">
                <div class="p-delivery__slider-item swiper-slide">
                    <img src="{{ asset('images/p-delivery/1.png') }}">
                </div>
{{--
                <div class="p-delivery__slider-item swiper-slide">
                    <img src="{{ asset('images/p-delivery/2.png') }}">
                </div>
--}}
                <div class="p-delivery__slider-item swiper-slide">
                    <img src="{{ asset('images/p-delivery/3.png') }}">
                </div>
                <div class="p-delivery__slider-item swiper-slide">
                    <img src="{{ asset('images/p-delivery/4.png') }}">
                </div>
                <div class="p-delivery__slider-item swiper-slide">
                    <img src="{{ asset('images/p-delivery/5.png') }}">
                </div>
                <div class="p-delivery__slider-item swiper-slide">
                    <img src="{{ asset('images/p-delivery/6.png') }}">
                </div>
                <div class="p-delivery__slider-item swiper-slide">
                    <img src="{{ asset('images/p-delivery/7.png') }}">
                </div>
{{--
                <div class="p-delivery__slider-item swiper-slide">
                    <img src="{{ asset('images/p-delivery/8.png') }}">
                </div>
--}}
            </div>
            <div class="p-delivery__slider-next"></div>
            <div class="p-delivery__slider-prev"></div>
        </div>
    </div>
</div>
