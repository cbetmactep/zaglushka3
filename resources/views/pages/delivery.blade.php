@php
    declare(strict_types=1);

    use App\Models\City;
    use App\Models\Category;
    use App\Helpers\CityFileParser;
    use morphos\Russian\GeographicalNamesInflection;

    Meta::set('title', _t('Доставка', 'common'));

    if ($CurrentCity && $CurrentCity->id != 25) {
        $Tks = CityFileParser::cityFileParser($CurrentCity->title);
    }
@endphp

@extends('layouts.master')

@section('title', _t('Доставка продукции', 'pages'))

@section('content')
    {!! Breadcrumbs::render('delivery') !!}
    <div class="container">
        <div class="page-header">
            <div class="page-header__title-wrap">
                <h1 class="page__title">{!! _t('Доставка', 'pages') !!}</h1>
            </div>
        </div>
        <div class="page">
            <div class="page__content page__content_text page__content_background">
                @if(App::isLocale('ru'))
                    @if (isset($Tks) && count($Tks))
                        <section class="p-delivery">
                            <div class="p-delivery-tab-header">
                                <div class="p-delivery-tab-header__item p-delivery-tab-header__item_active">{!! _t('по ' . GeographicalNamesInflection::getCase(City::cachedOne((int) session('current_city'))->title, 'дательный'), 'pages') !!}</div>
                                <div class="p-delivery-tab-header__item" data-hash="russia">{!! _t('по России', 'pages') !!}</div>
                                <div class="p-delivery-tab-header__item" data-hash="sng">{!! _t('по СНГ', 'pages') !!}</div>
                                <div class="p-delivery-tab-header__item" data-hash="europe">{!! _t('по Европе', 'pages') !!}</div>
                            </div>
                            <div class="p-delivery-tab">
                                <div class="p-delivery-tab__item p-delivery-tab__item_active">
                                    <div class="p-delivery__body">
                                        <p class="p-delivery__paragraph">
                                            Заказы отправляются каждый будний день.
                                            <br/>
                                            Доставка возможна как до терминала выбранной транспортной компании, так и «до дверей», по указанному вами адресу.
                                            <br/>
                                            Мы за свой счет довозим товар до склада транспортной компании в Санкт-Петербурге, отправляем его, оформляем документы. Вы оплачиваете только услуги транспортной компании. В случае, если на ваш заказ распространяется действие бесплатной доставки «  до дверей»  , мы оплачиваем все транспортные расходы.
                                        </p>
                                        @if($CurrentCity && $CurrentCity->free_delivery_price)
                                            <div class="p-delivery__title">Бесплатная доставка «до парадной» осуществляется при одновременном выполнении следующих условий:</div>
                                            <ul class="p-delivery__list">
                                                <li>сумма заказа от {{ number_format($CurrentCity->free_delivery_price, 0, ' ', ' ') }} рублей (для города {{ $CurrentCity->title }}), без учета товаров со скидкой;</li>
                                                <li>заказ полностью оплачен;</li>
                                                <li>заказ не содержит крупногабаритные товары: <a class="link" href="{{ route('slug', ['slug' => Category::cachedOne(499)->slug()]) }}">горки</a>,
                                                    <a class="link" href="{{ route('slug', ['slug' => Category::cachedOne(541)->slug()]) }}">стальные цепи</a>,
                                                    <a class="link" href="{{ route('slug', ['slug' => Category::cachedOne(498)->slug()]) }}">сиденья</a> и
                                                    <a class="link" href="{{ route('slug', ['slug' => Category::cachedOne(498)->slug()]) }}">каркасы для стульев</a>.
                                                </li>
                                            </ul>
                                        @endif
                                    </div>
                                    <div class="mob-p-delivery-tc">
                                        <div class="mob-p-delivery-tc__header">
                                            <div class="mob-p-delivery-tc__title">Транспортные компании</div>
                                            @component('components.icon', ['name' => 'arrow-menu', 'attributes' => ['class' => 'mob-p-delivery-tc__arrow']])])@endcomponent
                                        </div>
                                        <div class="mob-p-delivery-tc__body">
                                            <div class="mob-p-delivery-tc__list">
                                                <div class="p-delivery-map-tc__item">
                                                    <label class="p-delivery-map-tc__checkbox" tabindex="0">
                                                        <input class="js-mob-p-delivery-map-tc-checkbox-all" type="checkbox" checked>
                                                        <span class="js-p-delivery-map-tc-item-name">
                                                            @component('components.icon', ['name' => 'check'])@endcomponent
                                                            Все
                                                        </span>
                                                    </label>
                                                    <div class="mob-p-delivery-tc__question">
                                                        <span>?</span>
                                                        <div class="mob-p-delivery-tc__popover">
                                                            Сотрудничаем более чем с 40 транспортными компаниями, подробную информацию уточняйте у менеджеров
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mob-p-delivery-tc__list js-mob-p-delivery-map-tc-list"></div>
                                        </div>
                                    </div>
                                    <div class="p-delivery__map p-delivery__map_tks">
                                        <div class="page__map-wrap" id="ymaps-delivery"></div>
                                        <div class="page__map-touch">
                                            {!! _t('Чтобы переместить карту, проведите по ней двумя пальцами', 'pages') !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="p-delivery-tab__item">
                                    @component('pages.includes.delivery-russia')@endcomponent
                                </div>
                                <div class="p-delivery-tab__item">
                                    @component('pages.includes.delivery-russia')@endcomponent
                                </div>
                                <div class="p-delivery-tab__item">
                                    @component('pages.includes.delivery-russia')@endcomponent
                                </div>
                            </div>
                        </section>
                        <template class="js-p-delivery-map-tc">
                            <div class="p-delivery-map-tc">
                                <div class="p-delivery-map-tc__title">{!! _t('Транспортные компании', 'pages') !!}</div>
                                <div class="p-delivery-map-tc__list js-p-delivery-map-tc-list"></div>
                                <div class="p-delivery-map-tc__text">
                                    Сотрудничаем с более чем <br/>
                                    <span class="p-delivery-map-tc__link">
                                        40 транспортными компаниями.<span class="p-delivery-map-tc__link-image">
                                            @component('components.icon', ['name' => 'close-thin', 'attributes' => ['class' => 'p-delivery-map-tc__link-close']])])@endcomponent
                                            <img src="{{ asset('images/tk.png') }}">
                                        </span>
                                    </span>
                                    Подробную информацию уточняйте у менеджеров.
                                </div>
                            </div>
                        </template>
                        <template class="js-p-delivery-map-tc-item">
                            <div class="p-delivery-map-tc__item">
                                <label class="p-delivery-map-tc__checkbox" tabindex="0">
                                    <input class="js-p-delivery-map-tc-checkbox" type="checkbox" checked>
                                    <span class="js-p-delivery-map-tc-item-name">
                                        @component('components.icon', ['name' => 'check'])@endcomponent
                                    </span>
                                </label>
                            </div>
                        </template>
                    @else
                        <section class="p-delivery">
                            <div class="p-delivery-tab-header">
                                <div class="p-delivery-tab-header__item p-delivery-tab-header__item_active">{!! _t('по Санкт-Петербургу', 'pages') !!}</div>
                                <div class="p-delivery-tab-header__item" data-hash="russia">{!! _t('по России', 'pages') !!}</div>
                                <!--<div class="p-delivery-tab-header__item" data-hash="sng">{!! _t('по СНГ', 'pages') !!}</div>
                                <div class="p-delivery-tab-header__item" data-hash="europe">{!! _t('по Европе', 'pages') !!}</div> -->
                            </div>
                            <div class="p-delivery-tab">
                                <div class="p-delivery-tab__item p-delivery-tab__item_active">
                                    <div class="p-delivery__body">
                                        <p class="p-delivery__paragraph">
                                            Доставка осуществляется только по будням, на следующий день после оформления и
                                            согласования заказа.
                                            <br/>
                                            Возможен наличный и безналичный расчеты.
                                        </p>
                                        <div class="p-delivery__title">Бесплатная доставка «до парадной» осуществляется при одновременном выполнении следующих условий:</div>
                                        <ul class="p-delivery__list">
                                            <li>сумма заказа от {{ number_format(City::cachedOne(25)->free_delivery_price, 0, ' ', ' ') }} рублей, без учета товаров со скидкой;</li>
                                            <li>заказ полностью оплачен;</li>
                                            <li>заказ не содержит крупногабаритные товары: <a class="link" href="{{ route('slug', ['slug' => Category::cachedOne(499)->slug()]) }}">горки</a>,
                                                <a class="link" href="{{ route('slug', ['slug' => Category::cachedOne(541)->slug()]) }}">стальные цепи</a>,
                                                <a class="link" href="{{ route('slug', ['slug' => Category::cachedOne(498)->slug()]) }}">сиденья</a> и
                                                <a class="link" href="{{ route('slug', ['slug' => Category::cachedOne(498)->slug()]) }}">каркасы для стульев</a>.</li>
                                        </ul>
                                    </div>
                                    <div class="p-delivery-mob-delivery">
                                        <input class="p-delivery-mob-delivery__input" placeholder="Введите адрес">
                                        <div class="p-delivery-mob-delivery__price-wrap">
                                            <div class="p-delivery-mob-delivery__price-text">
                                                {!! _t('Стоимость доставки', 'pages') !!}
                                            </div>
                                            <div class="p-delivery-mob-delivery__price">
                                                <span class="js-delivery-mob-delivery-manager" style="display: none;">
                                                    {!! _t('рассчитывается менеджером', 'order') !!}
                                                </span>
                                                <span class="js-delivery-mob-delivery-chargeable" style="display: none;">
                                                    <span class="js-delivery-mob-delivery-price"></span> {{ _t('р.') }}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="p-delivery__map">
                                        <div class="p-delivery__map-wrap" id="ymaps-delivery"></div>
                                        <div class="p-delivery__map-touch">
                                            {!! _t('Чтобы переместить карту, проведите по ней двумя пальцами', 'pages') !!}
                                        </div>
                                    </div>
                                    <div class="p-delivery-mob-price">
                                        <div class="p-delivery-mob-price__block">
                                            {!! _t('Стоимость доставки', 'pages') !!}
                                        </div>
                                        <div class="p-delivery-mob-price__block">
                                            <div class="p-delivery-mob-price__title">
                                                {!! _t('В пределах КАД:', 'pages') !!}
                                            </div>
                                            <div class="p-delivery-mob-price__list">
                                                <div class="p-delivery-mob-price__item">
                                                    {!! _t('Зона %s — %s рублей', 'pages', [1, 200]) !!}
                                                </div>
                                                <div class="p-delivery-mob-price__item">
                                                    {!! _t('Зона %s — %s рублей', 'pages', [2, 250]) !!}
                                                </div>
                                                <div class="p-delivery-mob-price__item">
                                                    {!! _t('Зона %s — %s рублей', 'pages', [3, 300]) !!}
                                                </div>
                                                <div class="p-delivery-mob-price__item">
                                                    {!! _t('Зона %s — %s рублей', 'pages', [4, 400]) !!}
                                                </div>
                                                <div class="p-delivery-mob-price__item">
                                                    {!! _t('Зона %s — %s рублей', 'pages', [5, 500]) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="p-delivery-mob-price__block">
                                            <div class="p-delivery-mob-price__title">
                                                {!! _t('За пределы КАД:', 'pages') !!}
                                            </div>
                                            {!! _t('250 рублей + 10 рублей за каждый километр от КАД.', 'pages') !!}
                                        </div>
                                    </div>
                                    <template class="js-p-delivery-map">
                                        <div class="p-delivery-map">
                                            <div class="p-delivery-map__title">{!! _t('Стоимость доставки', 'pages') !!}</div>
                                            <div class="p-delivery-map__subtitle">{!! _t('В пределах КАД:', 'pages') !!}</div>
                                            <div class="p-delivery-map__list">
                                                <div class="p-delivery-map__item">
                                                    {!! File::get(base_path() . '/public/images/arrow-contacts.svg') !!}
                                                    <span class="p-delivery-map__item-text">{!! _t('Зона %s — %s рублей', 'pages', [1, 200]) !!}</span>
                                                </div>
                                                <div class="p-delivery-map__item">
                                                    {!! File::get(base_path() . '/public/images/arrow-contacts.svg') !!}
                                                    <span class="p-delivery-map__item-text">{!! _t('Зона %s — %s рублей', 'pages', [2, 250]) !!}</span>
                                                </div>
                                                <div class="p-delivery-map__item">
                                                    {!! File::get(base_path() . '/public/images/arrow-contacts.svg') !!}
                                                    <span class="p-delivery-map__item-text">{!! _t('Зона %s — %s рублей', 'pages', [3, 300]) !!}</span>
                                                </div>
                                                <div class="p-delivery-map__item">
                                                    {!! File::get(base_path() . '/public/images/arrow-contacts.svg') !!}
                                                    <span class="p-delivery-map__item-text">{!! _t('Зона %s — %s рублей', 'pages', [4, 400]) !!}</span>
                                                </div>
                                                <div class="p-delivery-map__item">
                                                    {!! File::get(base_path() . '/public/images/arrow-contacts.svg') !!}
                                                    <span class="p-delivery-map__item-text">{!! _t('Зона %s — %s рублей', 'pages', [5, 500]) !!}</span>
                                                </div>
                                            </div>
                                            <div class="p-delivery-map__subtitle">{!! _t('За пределы КАД:', 'pages') !!}</div>
                                            <div class="p-delivery-map__text">{!! _t('250 рублей + 10 рублей за каждый километр от КАД.', 'pages') !!}</div>
                                        </div>
                                    </template>
                                </div>
                                <div class="p-delivery-tab__item">
                                    @component('pages.includes.delivery-russia')@endcomponent
                                </div>
                                <!--<div class="p-delivery-tab__item">
                                    <div class="p-delivery__body">
                                        <p class="p-delivery__paragraph">
                                            Компания «Заглушка.Ру» осуществляет грузоперевозки через сеть логистических парнтеров в любую из стран СНГ и предлагает сервис высокого качества и комплексный подход. Возможна отправка таких типов грузов как: негабаритные, тяжеловесные.
                                        </p>
                                        <p class="p-delivery__paragraph">
                                            Мы рады предложить вам:
                                        </p>
                                        <ul class="p-delivery__list">
                                            <li>Широкую сеть региональных контрагентов</li>
                                            <li>Многократно отработанные маршруты</li>
                                            <li>Многолетний опыт подбора грузоперевозок обеспечивает четкость работы</li>
                                            <li>Постоянный контроль за соблюдением сроков</li>
                                            <li>Знание особенностей транспортировки разных типов грузов</li>
                                            <li>Грамотное оформление и декларирование товаров "от точки" "до точки"</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="p-delivery-tab__item">
                                    <div class="p-delivery__body">
                                        <p class="p-delivery__paragraph">
                                            Компания "Заглушка.Ру" обеспечит безупречный сервис, полный комплекс транспортных услуг и таможенное оформление, предоставляем целый ряд индивидуальных решений, способных удовлетворить буквально все требования клиента.
                                        </p>
                                        <p class="p-delivery__paragraph">
Огромный опыт позволяет нам в точности соблюдать установленные сроки, обеспечивать целостность и сохранность грузов, а также предлагать множество полезных дополнительных услуг, значительно облегчающих вам процесс транспортировки самых различных товаров. Наши специалисты имеют многолетний опыт работы, они организуют и проконтролируют все стадии процесса грузоперевозки. Профессионально организуем неординарные мультимодальные схемы транспортировки.
                                        </p>
                                        <p class="p-delivery__paragraph">В соответствии с особенностями конкретных грузов, их объемом и весом, предложим целый ряд дополнительных преимуществ:</p>
                                        <ul class="p-delivery__list">
                                            <li>максимально ускорим доставку в Европу;</li>
                                            <li>обеспечим условия сохранности груза;</li>
                                            <li>сведем к минимуму временные затраты на получение разрешительной документации;</li>
                                            <li>разработаем оптимальный маршрут и окажем помощь в выборе транспорта.</li>
                                        </ul>
                                    </div>
                                </div> -->
                            </div>
                        </section>
                    @endif
                @elseif(App::isLocale('it'))
                    <section class="p-delivery">
                        <h2 class="p-delivery__title">Gentili clienti!</h2>
                        <p>
                            Siamo lieti di informarvi che da gennaio 2015 abbiamo lanciato una nuova promozione per le principali città della Russia:
                            <br/>
                            "<strong>Consegna gratuita degli ordini al domicilio del cliente!</strong>"
                        </p>
                        Condizioni del servizio:
                        <ul>
                            <li>il totale dell'ordine deve essere uguale o superiore a quello impostato per la città selezionata</li>
                            <li>il servizio è disponibile solo per gli ordini pagati completamente</li>
                            <li>il servizio non è disponibile per gli ordini con sconti, prezzi speciali o condizioni di consegna speciali</li>
                            <li>il servizio non è disponibile per oggetti di grandi dimensioni come scivoli, duglie di corda, ghiere per le attrezzature sportive, sedili e telai per sedie.</li>
                        </ul>
                    </section>
                    <section class="p-delivery">
                        <h2 class="p-delivery__title">Consegna in Russia:</h2>
                        <p>
                            La nostra azienda invia ordini tutti i giorni lavorativi della settimana su tutto il territorio della Federazione Russa con le compagnie di trasporto Delovye Linii, PEK o DPD.
                            <br>
                            È possibile effettuare la consegna della merce al terminale nella vostra città della compagnia di trasporto selezionata oppure direttamente al domicilio specificato.
                            <br>
                            Portiamo la merce a nostre spese fino al magazzino della compagnia di trasporto a San Pietroburgo, la consegniamo e facciamo tutti i documenti. Voi pagate solo il servizio della compagnia di trasporto. Nel caso in cui per il vostro ordine è attiva la promozione "<strong>Consegna gratuita degli ordini al domicilio del cliente!</strong>" paghiamo noi tutte le spese di trasporto!
                        </p>
                    </section>
                    <section class="p-delivery">
                        <h2 class="p-delivery__title">Consegna a San Pietroburgo:</h2>
                        <p>
                            La consegna si effettua quotidianamente nei giorni lavorativi con i veicoli propri.
                            <br>
                            Il costo della consegna entro l'anello della tangenziale (è possibile pagare tramite bancogiro oppure con i contanti nel momento di consegna della merce):
                        </p>
                        <table class="p-delivery__table">
                            <tbody>
                            <tr>
                                <td>Zona 1 - 200 rubli</td>
                                <td>Zona 3 - 300 rubli</td>
                                <td>Zona 5 - 500 rubli</td>
                            </tr>
                            <tr>
                                <td>Zona 2 - 250 rubli</td>
                                <td colspan="2">Zona 4 - 400 rubli</td>
                            </tr>
                            </tbody>
                        </table>
                        <p>
                            Costo della consegna fuori l'anello della tangenziale:
                            <br>
                            250 rubli + 10 rubli per ogni chilometro fuori l'anello della tangenziale.
                        </p>
                    </section>
                @else
                    <p>
                        Zaglushka.ru is pleased to feature fast and affordable shipping to your location. Our logistics specialists are always ready to help you with the cost of service delivery. We use the services of companies like Fedex, TNT, EMS and others.
                        We are very careful with our goods. Before sending all the boxes are checked. Our goods are put on pallets and sent out using carefully selected transport partners. All relevant information about dimensions of boxes and pallets and quantity of goods on pallets can be found in the catalogue Zaglushka.ru. Smaller orders can be sent as a parcel.
                        We are always looking for the best transport company. We ship the goods to the terminal in your city or service door-to-door. Express delivery is also available if you need your items expedited. For bulk orders and extra large items, we will arrange truck freight delivery to the location of your choice. Shipping charges are kept as low as possible and will be computed at time of order depending on the weight of items ordered and where they are shipped.
                    </p>
                    <p>
                        Thanks for shopping Zaglushka.ru!
                    </p>
                @endif
            </div>
        </div>
    </div>
    @if(App::isLocale('ru'))
        @if(isset($Tks) && count($Tks))
            <script type="text/javascript" async charset="utf-8" src="https://api-maps.yandex.ru/2.1/?lang={{App::isLocale('ru') ? 'ru-RU' : 'en-US'}}&&apikey=0aa2beeb-3754-4cd0-9523-2427dad0448a&mode={{ App::environment() === 'production' ? 'release' : 'debug' }}&onload=mapInit"></script>
            <script>
                function mapInit(){
                    var inputSearch = new ymaps.control.SearchControl({
                            options: {
                                size: App.getType() === 'xs' ? 'medium' : 'large',
                                provider: 'yandex#search',
                                position: {
                                    right: 48,
                                    top: 10
                                }
                            }
                        }),
                        names = {
                            dellin: 'Деловые линии'{{--,
                            pecom: 'ПЭК'--}}
                        },
                        phones = {
                            dellin: '8 800 100 80 00'{{--,
                            pecom: '+7 495 660 11 11'--}}
                        },
                        colors = [
                            'blue', 'red', 'darkOrange', 'night', 'darkBlue', 'pink', 'gray', 'brown'
                        ],
                        collections = JSON.parse('{!! str_replace('\"', '\\\"', json_encode($Tks)) !!}');

                    window.map = new ymaps.Map('ymaps-delivery', {
                        center: [29.943114704018953, 59.93705033492059],
                        zoom: 10,
                        type: 'yandex#map',
                        controls: ['fullscreenControl', inputSearch]
                    }, {
                        suppressMapOpenBlock: true
                    });

                    window.collections = {};

                    var i = 0;

                    $.each(collections, function (index, list) {
                        window.collections[index] = new ymaps.GeoObjectCollection(null, {
                            preset: 'islands#' + colors[i] + 'DotIconWithCaption'
                        });

                        $.each(list, function (indexList, item) {
                            window.collections[index].add(new ymaps.Placemark(item.coords, {
                                balloonContentBody: [
                                    '<address>',
                                        (item.name ? 'Адрес: <strong>' + item.name + '</strong><br/>' : ''),
                                        'Телефон: <a href="tel:' + phones[index] + '">' + phones[index] + '</a>',
                                    '</address>'
                                ].join('')
                            }));
                        });

                        map.geoObjects.add(window.collections[index]);
                        i++;
                    });

                    map.setBounds(map.geoObjects.getBounds());

                    if (map.getZoom() > 12) {
                        map.setZoom(12)
                    }

                    map.controls.add('zoomControl', {
                        size: 'large',
                        position: {
                            left: 'auto',
                            right: 10,
                            top: 207
                        }
                    });

                    var CustomControlClass = function (options) {
                        CustomControlClass.superclass.constructor.call(this, options);
                        this._$content = null;
                        this._geocoderDeferred = null;
                    };

                    ymaps.util.augment(CustomControlClass, ymaps.collection.Item, {
                        onAddToMap: function (map) {
                            CustomControlClass.superclass.onAddToMap.call(this, map);
                            this._lastCenter = null;
                            this.getParent().getChildElement(this).then(this._onGetChildElement, this);
                        },

                        onRemoveFromMap: function (oldMap) {
                            this._lastCenter = null;
                            if (this._$content) {
                                this._$content.remove();
                                this._mapEventGroup.removeAll();
                            }
                            CustomControlClass.superclass.onRemoveFromMap.call(this, oldMap);
                        },

                        _onGetChildElement: function (parentDomContainer) {
                            var control = this;

                            control._$content = $($('.js-p-delivery-map-tc').html()).appendTo(parentDomContainer);
                            control._mapEventGroup = control.getMap().events.group();

                            var i = 0,
                                $list = control._$content.find('.p-delivery-map-tc__list'),
                                $mobList = $('.js-mob-p-delivery-map-tc-list'),
                                $mobAll = $('.js-mob-p-delivery-map-tc-checkbox-all');

                            setTimeout(function () {
                                control.getMap().geoObjects.each(function (collection) {
                                    var $item = $($('.js-p-delivery-map-tc-item').html()),
                                        collectionName = names[Object.keys(collections)[i]],
                                        $itemCheckbox = $item.find('.js-p-delivery-map-tc-checkbox');

                                    $item.find('.js-p-delivery-map-tc-item-name').append(collectionName);

                                    var $mobItem = $item.clone(),
                                        $mobItemCheckbox = $mobItem.find('.js-p-delivery-map-tc-checkbox');

                                    $list.append($item);
                                    $mobList.append($mobItem);

                                    $item.find('.js-p-delivery-map-tc-checkbox').on('change', function () {
                                        var checked = $(this).prop('checked');

                                        collection.options.set('visible', checked);
                                        $mobItemCheckbox.prop('checked', checked);
                                    });

                                    $mobItemCheckbox.on('change', function () {
                                        console.log($itemCheckbox)
                                        $itemCheckbox.prop('checked', $(this).prop('checked')).trigger('change');
                                    });

                                    i++;
                                });

                                var $checkboxes = $list.find('.js-p-delivery-map-tc-checkbox');

                                $checkboxes.on('change', function () {
                                    var checkeds = [];

                                    $checkboxes.each(function () {
                                        checkeds.push($(this).prop('checked'));
                                    });

                                    $mobAll.prop('checked', checkeds.every(isTrue));

                                    function isTrue(value) {
                                        return value
                                    }
                                });

                                $mobAll.on('change', function () {
                                    var checked = $(this).prop('checked');

                                    $checkboxes.each(function () {
                                        $(this).prop('checked', checked).trigger('change');
                                    });
                                });
                            });
                        }
                    });

                    var customControl = new CustomControlClass();
                    map.controls.add(customControl, {
                        float: 'none',
                        position: {
                            top: 40,
                            left: 10
                        }
                    });

                    if (Modernizr.touchevents) {
                        map.behaviors.disable('drag');

                        var $map = $('.p-delivery__map'),
                            $wrap = $('.page__map-wrap'),
                            classWrapDisabled = 'page__map-wrap_disabled';

                        console.log($map)

                        $map.on('touchmove', function (event) {
                            if(event.touches.length < 2) {
                                $wrap.addClass(classWrapDisabled);
                            }
                        })

                        $map.on('touchend', function () {
                            $wrap.removeClass(classWrapDisabled);
                        })
                    }
                }
            </script>
        @else
            <script type="text/javascript">
                function init_map_spb(ymaps) {
                    var inputSearch = new ymaps.control.SearchControl({
                        options: {
                            size: App.getType() === 'xs' ? 'medium' : 'large',
                            provider: 'yandex#search',
                            position: {
                                right: 48,
                                top: 10
                            }
                        }
                    });

                    var map = new ymaps.Map('ymaps-delivery', {
                        center: [29.943114704018953, 59.93705033492059],
                        zoom: 10,
                        type: 'yandex#map',
                        controls: ['fullscreenControl', inputSearch]
                    }, {
                        suppressMapOpenBlock: true
                    });

                    map.controls.add('zoomControl', {
                        size: 'large',
                        position: {
                            left: 'auto',
                            right: 10,
                            top: 207
                        }
                    });

                    var CustomControlClass = function (options) {
                        CustomControlClass.superclass.constructor.call(this, options);
                        this._$content = null;
                        this._geocoderDeferred = null;
                    };

                    ymaps.util.augment(CustomControlClass, ymaps.collection.Item, {
                        onAddToMap: function (map) {
                            CustomControlClass.superclass.onAddToMap.call(this, map);
                            this._lastCenter = null;
                            this.getParent().getChildElement(this).then(this._onGetChildElement, this);
                        },

                        onRemoveFromMap: function (oldMap) {
                            this._lastCenter = null;
                            if (this._$content) {
                                this._$content.remove();
                                this._mapEventGroup.removeAll();
                            }
                            CustomControlClass.superclass.onRemoveFromMap.call(this, oldMap);
                        },

                        _onGetChildElement: function (parentDomContainer) {
                            this._$content = $($('.js-p-delivery-map').html()).appendTo(parentDomContainer);
                            this._mapEventGroup = this.getMap().events.group();
                        }
                    });

                    var customControl = new CustomControlClass();
                    map.controls.add(customControl, {
                        float: 'none',
                        position: {
                            top: 40,
                            left: 10
                        }
                    });

                    var collections = {},
                        colors = ['#40b27c', '#ffdb4d', '#cb98ff', '#fb867e', '#81aafc'],
                        selectors = {
                            list: '.p-delivery-map__list',
                            item: '.p-delivery-map__item'
                        },
                        classes = {
                            listActive: 'p-delivery-map__list_active',
                            itemActive: 'p-delivery-map__item_active'
                        };

                    App.delivery._zones.forEach(function (zone) {
                        if (!collections[zone.zone]) {
                            collections[zone.zone] = new ymaps.GeoObjectCollection();
                        }

                        var collection = collections[zone.zone];


                        collection.add(new ymaps.Polygon([
                            zone.coords.map(function (item) { return [item[1], item[0]] })
                        ], {
                            hintContent: 'Зона ' + zone.zone + ' — ' + zone.price + ' рублей',
                            zone: zone.zone
                        }, {
                            fillColor: colors[zone.zone - 1],
                            interactivityModel: 'default#transparent',
                            strokeColor: colors[zone.zone - 1],
                            strokeWidth: 2,
                            fillOpacity: 0.2,
                            strokeOpacity: 0.5
                        }));
                    });

                    setTimeout(function () {
                        $.each(collections, function (i) {
                            var collection = this;

                            map.geoObjects.add(this);

                            /**
                             * Наведение мыши на участок карты
                             */
                            collection.events.add('mouseenter', function () {
                                $(selectors.list).addClass(classes.listActive);
                                $(selectors.item).eq(i - 1).addClass(classes.itemActive);

                                collection.each(function (geoObject) {
                                    geoObject.options.set('fillOpacity', 0.5);
                                    geoObject.options.set('strokeOpacity', 1);
                                });

                                $.each(collections, function (i2) {
                                    var collection2 = this;

                                    if (i !== i2) {
                                        collection2.each(function (geoObject) {
                                            geoObject.options.set('fillOpacity', 0.2);
                                            geoObject.options.set('strokeOpacity', 0.5);
                                        });
                                    }
                                })
                            });

                            /**
                             * Выход курсора с участка карты
                             */
                            collection.events.add('mouseleave', function () {
                                $(selectors.list).removeClass(classes.listActive);
                                $(selectors.item).eq(i - 1).removeClass(classes.itemActive);

                                $.each(collections, function () {
                                    var collection2 = this;

                                    collection2.each(function (geoObject) {
                                        geoObject.options.set('fillOpacity', 0.2);
                                        geoObject.options.set('strokeOpacity', 0.5);
                                    });
                                })
                            });

                            /**
                             * Наведение мыши на пункт меню
                             */
                            $(selectors.item).eq(i - 1).on('mouseenter', function () {
                                $(selectors.list).addClass(classes.listActive);
                                $(this).addClass(classes.itemActive);

                                collection.each(function (geoObject) {
                                    geoObject.options.set('fillOpacity', 0.5);
                                    geoObject.options.set('strokeOpacity', 1);
                                });

                                $.each(collections, function (i2) {
                                    var collection2 = this;

                                    if (i !== i2) {
                                        collection2.each(function (geoObject) {
                                            geoObject.options.set('fillOpacity', 0.2);
                                            geoObject.options.set('strokeOpacity', 0.5);
                                        });
                                    }
                                })
                            });

                            /**
                             * Выход курсора с пункта меню
                             */
                            $(selectors.item).eq(i - 1).on('mouseleave', function () {
                                $(selectors.list).removeClass(classes.listActive);
                                $(this).removeClass(classes.itemActive);

                                $.each(collections, function () {
                                    var collection2 = this;

                                    collection2.each(function (geoObject) {
                                        geoObject.options.set('fillOpacity', 0.2);
                                        geoObject.options.set('strokeOpacity', 0.5);
                                    });
                                })
                            });
                        });
                    }, 100)

                    if (Modernizr.touchevents) {
                        map.behaviors.disable('drag');

                        var $map = $('.p-delivery__map'),
                            $wrap = $('.p-delivery__map-wrap'),
                            classWrapDisabled = 'p-delivery__map-wrap_disabled'

                        $map.on('touchmove', function (event) {
                            if(event.touches.length < 2) {
                                $wrap.addClass(classWrapDisabled);
                            }
                        })

                        $map.on('touchend', function () {
                            $wrap.removeClass(classWrapDisabled);
                        })
                    }
                }
            </script>
            <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/2.1/?lang={{App::isLocale('ru') ? 'ru-RU' : 'en-US'}}&coordorder=longlat&apikey=0aa2beeb-3754-4cd0-9523-2427dad0448a&load=package.full&wizard=constructor&onload=init_map_spb"></script>
        @endif
    @endif
@endsection
