@php
    declare(strict_types=1);
    use App\Models\City;
    use morphos\Russian\GeographicalNamesInflection;

    Meta::set('title', _t('Способы оплаты', 'pages'));
    Meta::set('keywords', 'пластиковые заглушки для труб резьбовые опоры пластиковые декоративные заглушки мебельные латодержатели колпачки колесные опоры', 'pages');
@endphp

@extends('layouts.master')

@section('content')
    {!! Breadcrumbs::render('pay') !!}
    <div class="container">
        <div class="page-header">
            <div class="page-header__title-wrap">
                <h1 class="page__title">{!! _t('Способы оплаты', 'pages') !!}</h1>
            </div>
        </div>
        <div class="page">
            <div class="page__content page__content_text page__content_background">
                @if(App::isLocale('ru'))
                    @if(Config::get('app.subdomain'))
                        <p>
                            В зависимости от того, являетесь ли вы юридическим или физическим лицом, вы можете ознакомиться с условиями и способами оплаты услуг.
                        </p>
                        <h2>Для юридических лиц</h2>
                        <h3 class="pay pay-wallet">Безналичный расчет</h3>
                        <p>
                            Размер комиссии зависит от банка, через который осуществляется платеж. Оплата фиксируется по рабочим дням, только по факту поступления средств на расчетный счет компании. <a href="/files/rekvizity.pdf" target="_blank">Реквизиты компании</a>.
                        </p>
                    @else
                        <p>
                            В зависимости от того, являетесь ли вы юридическим или физическим лицом, вы можете ознакомиться с условиями и способами оплаты услуг.
                        </p>
                        <h2>Для юридических лиц</h2>
                        <h3 class="pay pay-wallet">Безналичный расчет</h3>
                        <p>
                            Размер комиссии зависит от банка, через который осуществляется платеж. Оплата фиксируется по рабочим дням, только по факту поступления средств на расчетный счет компании. <a href="/files/rekvizity.pdf" target="_blank">Реквизиты компании</a>
                        </p>
                        <h2>Для физических лиц</h2>
                        <h3>Банковские карты</h3>
                        <div class="payments-logo">
                            <ul class="payments">
                                <li class="visa"></li>
                                <li class="maestro"></li>
                                <li class="mastercard"></li>
                                <li class="mir"></li>
                                <li class="jcb"></li>
                            </ul>
                        </div>
                        <p>Автоматическое зачисление средств в течение 10–15 минут. Совершить оплату можно с карт Visa, Visa Electron, MasterCard, Maestro, Eurocard и МИР.</p>
                        <div class="payments-logo">
                            <ul class="payments">
                                <li class="apple"></li>
                                <li class="google"></li>
                            </ul>
                        </div>
                        <h3>Электронные деньги</h3>
                        <div class="payments-logo">
                            <ul class="payments">
                                <li class="yandexmoney"></li>
                                <li class="webmoney"></li>
                                <li class="qiwi"></li>
                            </ul>
                        </div>
                        <p>Автоматическое зачисление средств в течение 5–10 минут.</p>
                        <h3>Интернет-банки</h3>
                        <div class="payments-logo">
                            <ul class="payments">
                                <li class="sberbank"></li>
                                <li class="alfa-click"></li>
                                <li class="tinkoff"></li>
                            </ul>
                        </div>
                        <p>Автоматическое зачисление средств в течение 5–10 минут.</p>
                        <h3>Наложенный платеж для физических и юридических лиц</h3>
                        
                        <div class="payments-logo">
                            <ul class="payments">
                                <li class="nn-icon"></li>
                            </ul>
                        </div>
                                                
                        <p>Наложенный платеж – это оплата товара при его получении. Способ оплаты – наличный или безналичный расчет. Отправка посылок наложенным платежом осуществляется с помощью транспортной компании СДЭК.</p>
                        
                    @endif
                @elseif (App::isLocale('it'))
                    
                @else
                    
                @endif
            </div>
        </div>
    </div>
@endsection