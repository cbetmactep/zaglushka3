@php
    declare(strict_types=1);
@endphp

@extends('layouts.master')

@php
    Meta::set('title', _t('Контакты', 'pages'));

    // количество артикулов в категориях "фурнитура"
    /*$implementCounter = Cache::remember('implement_counter_new', 60 * 24, function () {

        $parentCategoryIds = [
            514,
            34,
            39,
            40,
            60,
            61,
            465,
            210,
            467
        ];

        $categoryIds = \App\Models\Category::whereIn('parent_id', $parentCategoryIds)
            ->where(['enabled' => 1])
            ->pluck('id')
            ->toArray();

        return floor((\App\Models\Item::whereIn('cat_id', $categoryIds)->where(['enabled' => 1])->count() / 100)) * 100;
    });*/
	$implementCounter = 7000;
@endphp

@section('content')
    {!! Breadcrumbs::render('contacts') !!}
    <div class="container">
        <div class="page-header">
            <div class="page-header__title-wrap">
                <h1 class="page__title">{!! _t('Контакты', 'pages') !!}</h1>
            </div>
        </div>
        <div class="page">
            @if(Config::get('app.subdomain'))
                <div class="p-contacts-phone">
                    <div class="p-contacts-phone__row">
                        <div class="p-contacts-phone__col">
                            <div class="p-contacts-phone__tel">
                                <div class="p-contacts-phone__icon">
                                    <img src="{{ asset('/images/contacts/phone.svg') }}">
                                </div>
                                <span>
                                    <span class="p-contacts-phone__text-large">
                                        {{ $CurrentCity->phone }}
                                    </span>
                                    {{ $CurrentCity->title }}
                                </span>
                            </div>
                            <div class="p-contacts-phone__tel">
                                <div class="p-contacts-phone__icon"></div>
                                <span>
                                    <span class="p-contacts-phone__text-large">
                                        8 (800) 555 04 99
                                    </span>
                                    (бесплатно по России)
                                </span>
                            </div>
                        </div>
                        <div class="p-contacts-phone__col">
                            <div class="p-contacts-phone__tel">
                                <div class="p-contacts-phone__icon">
                                    <img src="{{ asset('/images/contacts/calendar.svg') }}">
                                </div>
                                <span>Пн – Пт: с 9:00 до 18:00 (по Москве)</span>
                            </div>
                            <div class="p-contacts-phone__tel">
                                <div class="p-contacts-phone__icon">
                                    <img src="{{ asset('/images/contacts/mail.svg') }}">
                                </div>
                                <span><a href="mailto:sales@zaglushka.ru">sales@zaglushka.ru</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class="contacts-container" style="width:100%; display: flex; margin-bottom: 20px;">
                <div class="p-contacts-map-address">
                    <div class="p-contacts-map-address__block p-contacts-map-address__block_office">
                    <div class="p-contacts-map-address__title">{!! _t('Офис / склад', 'pages') !!}</div>
                        <div class="p-contacts-map-address__item">
                            <img src="{{ asset('images/contacts/pointer.svg') }}">
                            {!! _t('Россия, г. Санкт-Петербург, <br/>п. Парголово ул. Подгорная, дом 6. Въезд с ул. Железнодорожная, дом 11 На территории АНГАР № 12', 'pages') !!}
                        </div>
                        <div class="p-contacts-map-address__item">
                            <img src="{{ asset('images/contacts/phone.svg') }}">
                            {!! _t('8 (800) 555 04 99 (бесплатно по России)', 'pages') !!} <br/>{{ $CurrentCity && $CurrentCity->phone ? $CurrentCity->phone . ' — ' . $CurrentCity->title : '' }}
                        </div>
                        <div class="p-contacts-map-address__item p-contacts-map-address__item_mail">
                            <img src="{{ asset('images/contacts/mail.svg') }}">
                            sales@zaglushka.ru
                        </div>
                        <div class="p-contacts-map-address__item">
                            <img src="{{ asset('images/contacts/calendar.svg') }}">
                            {!! _t('Пн – Пт: с %s до %s', 'pages', ['9:00', '18:00']) !!}
                        </div>
                    </div>
                    <div class="p-contacts-map-address__block p-contacts-map-address__block_production">
                        <div class="p-contacts-map-address__title">{!! _t('Производство', 'pages') !!}</div>
                        <div class="p-contacts-map-address__item">
                            <img src="{{ asset('images/contacts/pointer.svg') }}">
                            {!! _t('Россия, г. Санкт-Петербург, <br/> ул. Минеральная, д. 32 <br/> (Машиностроительный завод «Арсенал»)', 'pages') !!}
                        </div>
                    </div>
                </div>
                <div class="js-p-contacts-map p-contacts-map-address__mobile-wrap">
                </div>
                <div class="p-contacts-map" style="margin-bottom:0px;">
                    <div class="p-contacts-map__wrap" id="ymaps-contacts"></div>
                    <div class="p-contacts-map__touch">
                        {!! _t('Чтобы переместить карту, проведите по ней двумя пальцами', 'pages') !!}
                    </div>
                </div>
                <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&mode=debug" type="text/javascript"></script>
                <script>
                    ymaps.ready(function () {
                        var myMap = new ymaps.Map('ymaps-contacts', {
                            center: [60.08326275002967, 30.257762788450172],
                            zoom: 13,
                            controls: []
                        });

                        var fullscreenControl = new ymaps.control.FullscreenControl();
                        myMap.controls.add(fullscreenControl);

                        var zoomControl = new ymaps.control.ZoomControl({
                            options: {
                                float: 'none',
                                position: {
                                    left: 'auto',
                                    right: 10,
                                    top: 145
                                }
                            }
                        });
                        myMap.controls.add(zoomControl);

                        function addRoutePanelControl() {
                            myMap.controls.add('routePanelControl', {
                                float: 'none',
                                position: {
                                    top: 47,
                                    left: 10
                                }
                            });

                            var routePanel = myMap.controls.get('routePanelControl');

                            routePanel.routePanel.state.set({
                                type: 'masstransit',
                                to: 'посёлок Парголово, Санкт-Петербург, Железнодорожная улица, 11',
                                toEnabled: false
                            });
                        }

                        var CustomControlClass = function (options) {
                            CustomControlClass.superclass.constructor.call(this, options);
                            this._$content = null;
                            this._$content = null;
                            this._geocoderDeferred = null;
                            this.options.setName('contactsPanel');
                        };

                        ymaps.util.augment(CustomControlClass, ymaps.collection.Item, {
                            onAddToMap: function (map) {
                                CustomControlClass.superclass.onAddToMap.call(this, map);
                                this._lastCenter = null;
                                this.getParent().getChildElement(this).then(this._onGetChildElement, this);
                            },

                            onRemoveFromMap: function (oldMap) {
                                this._lastCenter = null;
                                if (this._$content) {
                                    this._$content.remove();
                                    this._mapEventGroup.removeAll();
                                }
                                CustomControlClass.superclass.onRemoveFromMap.call(this, oldMap);
                            },

                            _onGetChildElement: function (parentDomContainer) {
                                this._$content = $($('.js-p-contacts-map').html()).addClass('p-contacts-map-address_in-map').appendTo(parentDomContainer);
                                this._mapEventGroup = this.getMap().events.group();
                            }
                        });


                        var customControl = new CustomControlClass();

                        myMap.controls.add(customControl, {
                            float: 'none',
                            position: {
                                top: 47,
                                left: 10
                            },
                            options: {
                                name: 'contactsPanel'
                            }
                        });

                        var myPlacemark = new ymaps.Placemark([60.082786, 30.258744], {
                            hintContent: 'Склад',
                            balloonContent: 'Склад'
                        });

                        myMap.geoObjects.add(myPlacemark);

                        var myButton = new ymaps.control.Button({
                            data: {
                                image: '/images/route.svg',
                                content: 'Построить маршрут'
                            },
                            options: {
                                maxWidth: 240
                            }
                        });

                        myButton.events.add('click', function () {
                            if (myMap.controls.get('routePanelControl')) {
                                myMap.controls.remove('routePanelControl');
                                $('.p-contacts-map-address').show();
                                myPlacemark.options.set('visible', true);
                            } else {
                                $('.p-contacts-map-address').hide();
                                addRoutePanelControl();
                                myPlacemark.options.set('visible', false);
                            }
                        });

                        myMap.controls.add(myButton, {
                            float: 'none',
                            position: {
                                top: 10,
                                left: 10
                            }
                        });

                        if (Modernizr.touchevents) {
                            myMap.behaviors.disable('drag');

                            var $map = $('.p-contacts-map'),
                                $wrap = $('.p-contacts-map__wrap'),
                                classWrapDisabled = 'p-contacts-map__wrap_disabled'

                            $map.on('touchmove', function (event) {
                                if(event.touches.length < 2) {
                                    $wrap.addClass(classWrapDisabled);
                                }
                            })

                            $map.on('touchend', function () {
                                $wrap.removeClass(classWrapDisabled);
                            })
                        }
                    });
                </script>
                </div>
            @endif
            <div class="p-contacts">
                <h2 class="p-contacts__title">{!! _t('Менеджеры по работе с клиентами', 'pages') !!}</h2>
                <section class="contacts">
                    <div class="contacts__title-wrap">
                        <h3 class="contacts__title">
                            <span>
                                {!! _t('Направление «Фурнитура»', 'pages') !!}
                            </span>
                            <div class="contacts__tooltip"
                                 data-title="{!! _t('Направление включает в себя более %s наименований, среди которых заглушки для труб, опоры, фиксаторы, подпятники, колпачки, переходники и др.', 'pages', [$implementCounter]) !!}"></div>
                        </h3>
                    </div>
					<div class="contacts__subtitle-wrap">
                        <h3 class="contacts__subtitle">{!! _t('Оптовый отдел', 'pages') !!}</h3>
					</div>
                    <div class="contacts-item">
                        <div class="contacts-item__img">
                            <img src="{{ asset('images/managers/kraynova.png') }}" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">{!! _t('Галина Крайнова', 'pages') !!}</div>
                            <div class="contacts-item__email">gk@zaglushka.ru</div>
                            <div class="contacts-item__phone">{!! _t('доб.', 'pages') !!} 127</div>
                        </div>
                    </div>
                    <div class="contacts-item">
                        <div class="contacts-item__img">
                            <img src="{{ asset('images/managers/dernov.png') }}" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">{!! _t('Михаил Дернов', 'pages') !!}</div>
                            <div class="contacts-item__email">m.dernov@zaglushka.ru</div>
                            <div class="contacts-item__phone">{!! _t('доб.', 'pages') !!} 104</div>
                        </div>
                    </div>
                    <div class="contacts-item">
                        <div class="contacts-item__img">
                            <img src="{{ asset('images/managers/timofeev.png') }}" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">{!! _t('Денис Тимофеев', 'pages') !!}</div>
                            <div class="contacts-item__email">d.timofeev@zaglushka.ru</div>
                            <div class="contacts-item__phone">{!! _t('доб.', 'pages') !!} 113</div>
                        </div>
                    </div>
                    <div class="contacts-item">
                        <div class="contacts-item__img">
							<img src="{{ asset('images/managers/malenchev.jpg') }}" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">{!! _t('Максим Аленчев', 'pages') !!}</div>
                            <div class="contacts-item__email">m.alenchev@zaglushka.ru</div>
                            <div class="contacts-item__phone">{!! _t('доб.', 'pages') !!} 135</div>
                        </div>
                    </div>
                    <div class="contacts-item">
                        <div class="contacts-item__img">
		                    <img src="{{ asset('images/managers/npetrakova.png') }}" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">{!! _t('Надежда Петракова', 'pages') !!}</div>
                            <div class="contacts-item__email">n.petrakova@zaglushka.ru</div>
                            <div class="contacts-item__phone">{!! _t('доб.', 'pages') !!} 125</div>
                        </div>
                    </div>
                    <div class="contacts-item">
                        <div class="contacts-item__img">
							<img src="{{ asset('images/managers/atumarkin.png') }}" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">{!! _t('Алексей Тумаркин', 'pages') !!}</div>
                            <div class="contacts-item__email">a.tumarkin@zaglushka.ru</div>
                            <div class="contacts-item__phone">{!! _t('доб.', 'pages') !!} 126</div>
                        </div>
                    </div>
                    <div class="contacts-item">
                        <div class="contacts-item__img">
							<img src="{{ asset('images/managers/rprusskiy.png') }}" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">{!! _t('Роман Прусский', 'pages') !!}</div>
                            <div class="contacts-item__email">r.prusskiy@zaglushka.ru</div>
                            <div class="contacts-item__phone">{!! _t('доб.', 'pages') !!} 124</div>
                        </div>
                    </div>
                    <div class="contacts-item">
                        <div class="contacts-item__img">
                            <img src="{{ asset('images/managers/sshubarcov.png') }}" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">{!! _t('Сергей Шубарцов', 'pages') !!}</div>
                            <div class="contacts-item__email">sshubarcov@zaglushka.ru</div>
                            <div class="contacts-item__phone">{!! _t('доб.', 'pages') !!} 141</div>
                        </div>
                    </div>
                    <div class="contacts-item">
                        <div class="contacts-item__img">
                            <img src="{{ asset('images/managers/dchistyakov.png') }}" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">{!! _t('Дмитрий Чистяков', 'pages') !!}</div>
                            <div class="contacts-item__email">d.chistyakov@zaglushka.ru</div>
                            <div class="contacts-item__phone">{!! _t('доб.', 'pages') !!} 101</div>
                        </div>
                    </div>
                    <div class="contacts__subtitle-wrap">
                        <h3 class="contacts__subtitle">{!! _t('Розничный отдел', 'pages') !!}</h3>
                    </div>
                    <div class="contacts-item">
                        <div class="contacts-item__img">
                            <img src="{{ asset('images/managers/strekalovskaya.png') }}" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">{!! _t('Марина Стрекаловская', 'pages') !!}</div>
                            <div class="contacts-item__email">ms@zaglushka.ru</div>
                            <div class="contacts-item__phone">{!! _t('доб.', 'pages') !!} 121</div>
                        </div>
                    </div>
                    <div class="contacts-item">
                        <div class="contacts-item__img">
							<img src="{{ asset('images/managers/aglotov.jpg') }}" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">{!! _t('Александр Глотов', 'pages') !!}</div>
                            <div class="contacts-item__email">a.glotov@zaglushka.ru</div>
                            <div class="contacts-item__phone">{!! _t('доб.', 'pages') !!} 137</div>
                        </div>
                    </div>
                </section>
                <section class="contacts">
                    <div class="contacts__title-wrap">
                        <h3 class="contacts__title">
                            <span>
                                {!! _t('Направление «Малые архитектурные формы»', 'pages') !!}
                            </span>
                            <div class="contacts__tooltip"
                                 data-title="{!! _t('В данном направлении представлены различные комплектующие и канатные конструкции для детских площадок и спортивных комплексов.', 'pages') !!}"></div>
                        </h3>
                    </div>
                    <div class="contacts-item">
                        <div class="contacts-item__img">
                            <img src="{{ asset('images/managers/dyachkov.png') }}" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">{!! _t('Дмитрий Дьячков', 'pages') !!}</div>
                            <div class="contacts-item__email">d.dyachkov@zaglushka.ru</div>
                            <div class="contacts-item__phone">{!! _t('доб.', 'pages') !!} 123</div>
                        </div>
                    </div>
                    <div class="contacts-item">
                        <div class="contacts-item__img">
                            <img src="{{ asset('images/managers/bagaeva.png') }}" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">{!! _t('Светлана Багаева', 'pages') !!}</div>
                            <div class="contacts-item__email">s.bagaeva@zaglushka.ru</div>
                            <div class="contacts-item__phone">{!! _t('доб.', 'pages') !!} 129</div>
                        </div>
                    </div>
                    <div class="contacts-item">
                        <div class="contacts-item__img">
                            <img src="{{ asset('images/managers/krasnova.png') }}" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">{!! _t('Анастасия Краснова', 'pages') !!}</div>
                            <div class="contacts-item__email">a.krasnova@zaglushka.ru</div>
                            <div class="contacts-item__phone">{!! _t('доб.', 'pages') !!} 139</div>
                        </div>
                    </div>
                    <div class="contacts-item">
                        <div class="contacts-item__img">
                            <img src="{{ asset('images/managers/evdokimov.png') }}" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">{!! _t('Вадим Евдокимов', 'pages') !!}</div>
                            <div class="contacts-item__email">vevdokimov@zaglushka.ru</div>
                            <div class="contacts-item__phone">{!! _t('доб.', 'pages') !!} 146</div>
                        </div>
                    </div>
                    <div class="contacts-item">
                        <div class="contacts-item__img ">
                            <img src="{{ asset('images/managers/aromanova.png') }}" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">{!! _t('Альбина Романова', 'pages') !!}</div>
                            <div class="contacts-item__email">a.romanova@zaglushka.ru</div>
                            <div class="contacts-item__phone">{!! _t('доб.', 'pages') !!} 116</div>
                        </div>
                    </div>
                </section>
                <section class="contacts">
                    <div class="contacts__title-wrap">
                        <h3 class="contacts__title">
                            <span>
                                {!! _t('Направление «Мебель»', 'pages') !!}
                            </span>
                            <div class="contacts__tooltip"
                                 data-title="{!! _t('В данном направлении представлены пластиковые стулья и мебельные комплектующие от ведущих итальянских производителей.', 'pages') !!}"></div>
                        </h3>
                    </div>
                    <div class="contacts-item">
                        <div class="contacts-item__img">
                            <img src="{{ asset('images/managers/samoylova.png') }}" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">{!! _t('Екатерина Самойлова', 'pages') !!}</div>
                            <div class="contacts-item__email">es@zaglushka.ru</div>
                            <div class="contacts-item__phone">{!! _t('доб.', 'pages') !!} 130</div>
                        </div>
                    </div>
                    <div class="contacts-item">
                        <div class="contacts-item__img">
                            <img src="{{ asset('images/managers/asmelkova.png') }}" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">{!! _t('Анна Смелкова', 'pages') !!}</div>
                            <div class="contacts-item__email">asmelkova@zaglushka.ru</div>
                            <div class="contacts-item__phone">{!! _t('доб.', 'pages') !!} 188</div>
                        </div>
                    </div>
                </section>
                <section class="contact-item">
                    <div class="contacts-item"><a href="/files/rekvizity.pdf" target="_blank">Реквизиты компании</a></div>
                </section>
            </div>
            <!--<div class="p-contacts p-contacts_block">
                <h2 class="p-contacts__title">{!! _t('Производство', 'pages') !!}</h2>
                <section class="contacts">
                    <div class="contacts__title-wrap">
                        <h3 class="contacts__title">
                            <span>
                                {!! _t('Главный технолог', 'pages') !!}
                            </span>
                            <div class="contacts__tooltip" data-title="{!! _t('Решение вопросов, связанных с производством продукции под заказ и расчетом пресс-форм: <ul><li>разработка и изготовление нового изделия по индивидуальному запросу;</li><li>внесение изменений в любой артикул, представленный в каталоге (например, доработка технических и визуальных характеристик);</li><li>производство изделия на вашей пресс-форме.</li>') !!}"></div>
                        </h3>
                    </div>
                    <div class="contacts-item contacts-item_block">
                        <div class="contacts-item__img">
                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEXt5eMcSspCAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">{!! _t('Игорь Кузьмин', 'pages') !!}</div>
                            <div class="contacts-item__email">I.kuzmin@zaglushka.ru</div>
                            <div class="contacts-item__phone">{!! _t('доб.', 'pages') !!} 182</div>
                        </div>
                    </div>
                </section>
            </div>
-->
        </div>
    </div>
@endsection