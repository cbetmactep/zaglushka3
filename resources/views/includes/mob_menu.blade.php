<div class="mob-menu">
        <div class="mob-menu-city">
            <div class="mob-menu-city__left">
                @component('components.icon', ['name' => 'pointer', 'attributes' => ['class' => 'mob-menu-city__icon']])@endcomponent
            </div>
            <div class="mob-menu-city__right">
                <div class="mob-menu-city__name js-delivery-city">
                    {!! $CurrentCity ? $CurrentCity->title : 'Выберите город'!!}
                    @component('components.icon', ['name' => 'arrow-thin', 'attributes' => ['class' => 'mob-menu-city__name-icon']])])@endcomponent
                </div>
                @if($CurrentCity && $CurrentCity->phone)
                    <a class="mob-menu-city__phone" href="tel:+{{ Formatter::phone($CurrentCity->phone) }}">{{ $CurrentCity->phone }}</a>
                @endif
            </div>
        </div>
    <div class="mob-menu-list">
        <div class="mob-menu-list__title">
            {!! _t('Меню', 'common') !!}
        </div>
        <!--<a class="mob-menu-list__item" href="{{ route('home') }}">
            <span>
                {!! _t('Главная', 'common') !!}
            </span>
        </a>-->
        <a class="mob-menu-list__item" href="{{ route('home') }}">
            <span>
                {!! _t('Продукция', 'common') !!}
            </span>
        </a>
        <a class="mob-menu-list__item" href="{{ route('aboutnew') }}">
            <span>
                {!! _t('О компании', 'common') !!}
            </span>
        </a>
        <a class="mob-menu-list__item" href="{{ route('delivery') }}">
            <span>
                {!! _t('Доставка', 'common') !!}
            </span>
        </a>
        <a class="mob-menu-list__item" href="{{ route('contacts') }}">
            <span>
                {!! _t('Контакты', 'common') !!}
            </span>
        </a>
    </div>
    <div class="mob-menu-list">
        <div class="mob-menu-list__title">
            {!! _t('Покупки', 'common') !!}
        </div>
        <a class="mob-menu-list__item" href="{{ route('cart') }}">
            @component('components.icon', ['name' => 'basket', 'attributes' => ['class' => 'mob-menu-list__item-icon']])@endcomponent
            <span>
                {!! _t('Корзина', 'common') !!}
            </span>
        </a>
        <a class="mob-menu-list__item" href="{{ route('specials') }}">
            <span>
                {!! _t('Товары со скидками', 'common') !!}
            </span>
        </a>
    </div>
</div>