@php
    declare(strict_types=1);
@endphp

<div class="mob-filter">
    <div class="mob-filter-header">
        <div class="mob-filter-header__title">{!! _t('Фильтры', 'common') !!}</div>
        <div class="mob-filter-header__close js-mob-filter-close">
            @component('components.icon', ['name' => 'cross', 'attributes' => ['class' => 'mob-filter-header__close-icon']])@endcomponent
        </div>
    </div>
    <form class="mob-filter-block js-mob-filter-checkbox-form">
        <div class="mob-filter-block__title">{!! _t('Показывать товар', 'common') !!}</div>
        <label class="mob-filter-block__item">
            <span class="mob-filter-block__checkbox">
                <input class="js-mob-filter-checkbox" type="checkbox" {{ Request::get('special') == 1 ? 'checked' : '' }} name="special" value="1">
                <span>
                    @component('components.icon', ['name' => 'checkcart'])@endcomponent
                </span>
            </span>
            <span>
                {!! _t('Со скидкой', 'search') !!}
            </span>
        </label>
        <label class="mob-filter-block__item">
            <span class="mob-filter-block__checkbox">
                <input class="js-mob-filter-checkbox" type="checkbox" {{ Request::get('exist') == 1 ? 'checked' : '' }} name="exist" value="1">
                <span>
                    @component('components.icon', ['name' => 'checkcart'])@endcomponent
                </span>
            </span>
            <span>
                {!! _t('В наличии', 'search') !!}
            </span>
        </label>
    </form>
    @if(count($Tags))
        <div class="mob-filter-block mob-filter-block_tag @if(count($tagIds)) mob-filter-block_active @endif" data-category-link="{{ route('slug', ['slug' => $Category->slug()]) }}">
            <div class="mob-filter-block__title">{!! _t('Особенности', 'common') !!}</div>
            @foreach($Tags as $Tag)
                <label class="mob-filter-block__item mob-filter-block__item_tag @if(in_array($Tag->id, $tagIds)) mob-filter-block__item_tag_active @endif @if(isset($CurrentTag) && $CurrentTag->id === $Tag->id) mob-filter-block__item_tag_first @endif " data-id="{{ $Tag->id }}" data-href="{{ route('tags', ['categorySlug' => $Category->slug(), 'tagSlug' => $Tag->slug()] + Request::except('page')) }}">
                    <div class="mob-filter-block__checkbox">
                        <input class="js-mob-filter-tag-checkbox" type="checkbox" @if(in_array($Tag->id, $tagIds)) checked @endif>
                        <span>
                            @component('components.icon', ['name' => 'checkcart'])@endcomponent
                        </span>
                    </div>
                    <div>
                        {{ $Tag->title }}
                    </div>
                    @if ($Tag->isBreadcrumbsImageExists($Category))
                        <div class="mob-filter-block__image">
                            <img src="{{ $Tag->getBreadcrumbsImage($Category) }}" height="60">
                        </div>
                    @endif
                </label>
            @endforeach
        </div>
    @endif
    <a class="mob-filter-banner js-mob-filter-banner">
        <div class="mob-filter-banner__text">{!! _t('Найдено %s предложений', 'common', ['<span class="js-mob-filter-banner-quant"></span>']) !!}</div>
    </a>
</div>