@php
    declare(strict_types=1);
@endphp
<footer class="footer" style="margin-bottom:120px;">
    <div class="footer__top">
        <div class="footer__container">
            <div class="footer__row footer__row_top">
                <div class="footer__cols">
                    <div class="footer__col footer__col_issues">
                        {{ _t('Есть вопросы?', 'common') }}
                    </div>
                    <div class="footer__col">
                        <span class="footer__phone-label">{{ _t('Звоните:', 'common') }}</span> <span class="footer__phone">@if(App::isLocale('ru')) 8 (800) 555 04 99 @else +7 (812) 242 80 99 @endif</span>
                        @if(App::isLocale('ru'))
                            <br/>
                            <span class="footer__phone-text">({{ _t('звонок по России бесплатный', 'common') }})</span>
                        @endif
                    </div>
                    <div class="footer__col">
                        @if($CurrentCity)
                            @if($CurrentCity->phone)
                                <span class="footer__phone">{{ $CurrentCity->phone }}</span>
                                <br/>
                            @endif
                            <span class="footer__small link js-footer-city">{!! $CurrentCity->title !!} @component('components.icon', ['name' => 'arrow-thin', 'attributes' => ['class' => 'footer__icon']])])@endcomponent</span>
                        @endif
                    </div>
                    <div class="footer__col">          
                        <a class="header-price" href="{{ asset('files/price.xlsx') }}" target="_blank">
                            @component('components.icon', ['name' => 'download', 'attributes' => ['class' => 'header-price__icon']])])@endcomponent
                            <span class="header-price__text">{!! _t('Прайс-лист', 'common') !!}</span>
                        </a>
                    </div>
                </div>
                <div class="footer__city footer__col_right">
                    <span class="footer__mail-label">{{ _t('Или пишите:', 'common') }}</span> <a class="footer__mail" href="mailto:sales@zaglushka.ru">sales@zaglushka.ru</a>
                </div>
            </div>
        </div>
    </div>
    <div class="footer__bottom">
        <div class="footer__container">
            <div class="footer__row footer__row_bottom">
                <div class="footer__copyrighted">© 1991–{{ date('Y')}} ООО «{!! _t('Заглушка.pу', 'common') !!}» </div>
                <div class="footer-icons">
                    <p style="margin-right: 10px;">
                        <span>Получайте самые выгодные предложения первыми – Подпишитесь на:</span>
                    </p>
                    <a class="social" href="https://www.instagram.com/zaglushkaspb/" target="_blank" style="margin-top:10px; color:white;margin-right:10px;">
                        <img src="/images/icons/inst.png" width="30" height="30">
                    </a>
                    <a class="social" href="https://www.youtube.com/channel/UCOPNLGn8SzBevpTPs4_VWQA" target="_blank" style="color:white;margin-top:10px;margin-right:10px;">
                        <img src="/images/icons/youtube.png" width="30" height="30">
                    </a>
                    <!--
                    <a class="footer-icons__item" href="https://twitter.com/zaglushkaru" target="_blank">
                        @component('components.icon', ['name' => 'twitter'])@endcomponent
                    </a>-->
                    <a class="social" href="https://vk.com/zaglushka_ru" target="_blank" style="color:white;margin-top:10px;margin-right:10px;">
                        <img src="/images/icons/vk.png" width="30" height="30">
                    </a>
                    <a class="social" href="viber://chat?number=79211868689" target="_blank" style="color:white;margin-top:10px;margin-right:10px;">
                        <img src="/images/icons/viber.png" width="30" height="30">
                    </a>
                    <!--
                    <a class="footer-icons__item" href="https://api.whatsapp.com/send?phone=79119532546" target="_blank">
                        @component('components.icon', ['name' => 'whatsapp'])@endcomponent
                    </a>
                    <a class="footer-icons__item" href="https://t.me/Zaglushka" target="_blank">
                        @component('components.icon', ['name' => 'telegram'])@endcomponent
                    </a>
                    -->
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="corona-alert" style="width:100%; height: 120px; position:fixed;bottom: 0px; background: #01997c">
    <div class="container">
        <p style="color: white;"><strong style="color: white;">Уважаемые клиенты!</strong></p>
        <p style="color: white;">В связи с введением мер по противодействию распространения коронавируса для физических лиц, временно закрыт вход на склад в г. Санкт-Петербурге. <a href="#" style="color: white;" class='show-corona-popup'>Подробнее...</a> </p>
    </div>
</div>
<div class="corona-popup" style="display: none; z-index: 100500; flex-direction: column; width:100%; height: 100%; position: fixed; background-color: rgba(0,0,0,0.5); justify-content: center;top:0px;left:0px;">
    <div class="corona-popup-content" style="width: 630px; height: auto; min-height: 100px; background:white;margin: 0 auto;">
        <div class="header-corona" style="padding-top: 30px; padding-left: 50px; padding-right: 50px; padding-bottom: 20px; background-color: #28987c;">
            <p style="color: white; text-transform: uppercase">
                В связи с введением мер по противодействию распространения коронавируса <strong>для физических лиц временно закрыт вход на склад в г. Cанкт-петербурге.</strong>
            </p>
            <div class='corona-elements' style='position:relative; top: -100px; left: -40px;margin-bottom:-60px;'>
                <img src="/images/corona/corona-left.svg" style="width:50px; height: 50px;" />
                <a href="#" class="corona-close" style='position: absolute; right: -60px;'><img src="/images/corona/corona-close.svg" style="width: 20px; height:20px;"/></a>
                <img src="/images/corona/corona-right.svg" style="width:50px; height: 50px;position: absolute;top: 60px;right:-60px;" />
            </div>
        </div>
        <div class="corona-content" style="padding-top: 20px; padding-left: 50px; padding-right: 50px; padding-bottom: 20px;">
            <p style="color: #4e4d4d; text-transform: uppercase">Мы работаем, соблюдая меры предосторожности,<br /> и остаемся на связи! Действует бесконтактная доставка!</p>
            <p style="color: #4e4d4d;">Заказывайте необходимые изделия онлайн или по телефону<br /> и оплачивайте покупку картой. Для получения оплаченных товаров воспользуйтесь нашей услугой бесконтактной доставки или бесконтактным самовывозом. Все подробности уточняйте у менеджеров</p>
        </div>
        <div class="corona-list-content" style="padding-top: 20px; padding-left: 40px; padding-right: 50px; padding-bottom: 20px; background-color: #e6eeea; display: flex; flex-direction: column; justify-content: space-around">
            <div class="" style="display: flex">
                <img src="/images/corona/1.svg" style="width: 50px; height: 50px;">
                <p style="padding-left: 30px;margin-top:7px;">все сотрудники проходят процедуру оценки состояния здоровья и измерение температуры</p>
            </div>
            <div class="" style="display: flex">
                <img src="/images/corona/2.svg" style="width: 50px; height: 50px;">
                <p style="padding-left: 30px;margin-top:7px;">все офисные и производственные помещения регулярно обрабатываются антисептиком</p>
            </div>
            <div class="" style="display: flex">
                <img src="/images/corona/3.svg" style="width: 50px; height: 50px;">
                <p style="padding-left: 30px; margin-bottom:32px;">все сотрудники обеспечены масками и перчатками</p>
            </div>
            <div class="" style="display: flex">
                <img src="/images/corona/4.svg" style="width: 50px; height: 50px;">
                <p style="padding-left: 30px;margin-top:7px;">прямые контакты между сотрудниками минимизированы и заменены на дистанционные</p>
            </div>
        </div>
        <div class="corona-footer" style="padding-top: 20px; padding-left: 50px; padding-right: 50px; padding-bottom: 20px; background-color: #28987c;">
            <p style="color: white; text-transform: uppercase">
                БЕРЕГИТЕ СЕБЯ И СОБЛЮДАЙТЕ РЕЖИМ САМОИЗОЛЯЦИИ – ЗАКАЗЫВАЙТЕ И ОПЛАЧИВАЙТЕ ТОВАР ОНЛАЙН, ИСПОЛЬЗУЙТЕ НАШУ БЕСКОНТАКТНУЮ ДОСТАВКУ!
            </p>
        </div>
    </div>
</div>