@php
    declare(strict_types=1);
@endphp

<div class="page-loader">
    {!! _t('Идет расчет стоимости доставки...', 'order') !!}
</div>