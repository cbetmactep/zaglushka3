@php
    declare(strict_types=1);
@endphp

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    @if(Config::get('app.subdomain') == 'perm')
        <meta name="yandex-verification" content="b6fd9e1466925154" />
    @else
        <meta name="yandex-verification" content="b6fd9e1466925154" />
    @endif
<meta name="yandex-verification" content="4fde9c5d02f29a45" />
    <meta name="google-site-verification" content="e4tASF_7pJTLMjNzdhvYsHjeZZRPvT5bmac5HaohaGg" />

    <link rel="apple-touch-icon" sizes="180x180" href="{{ '/images/favicon/apple-touch-icon.png' }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ '/images/favicon/favicon-32x32.png' }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ '/images/favicon/favicon-16x16.png' }}">
    <link rel="manifest" href="{{ '/images/favicon/manifest.json' }}">
    <link rel="canonical" href="{{\Request::url()}}"/>
    <link rel="mask-icon" href="{{ '/images/favicon/safari-pinned-tab.svg' }}" color="#000000">
    <link rel="shortcut icon" href="{{ '/images/favicon/favicon.ico' }}">
    <meta name="msapplication-config" content="{{ '/images/favicon/browserconfig.xml' }}">
    <meta name="theme-color" content="#ffffff">


    {!! Meta::tag('robots') !!}

    {!! Meta::tag('site_name', _t('Заглушка.Ру', 'common')) !!}
    {!! Meta::tag('locale', App::getLocale()) !!}

    {!! Meta::tag('title') !!}
    {!! Meta::tag('description') !!}
    {!! Meta::tag('keywords') !!}

    {!! Meta::tag('image', asset('images/logo.png')) !!}

    <link rel="stylesheet" href="/css/style.css"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600&amp;subset=cyrillic-ext,latin-ext" rel="stylesheet">
    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-W2ZZDRL');</script>
<!-- End Google Tag Manager -->
<!-- Global site tag (gtag.js) - Google Ads: 660999990 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-660999990"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-660999990');
</script>

</head>