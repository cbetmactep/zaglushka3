@php
    declare(strict_types=1);
@endphp

<div class="top-of-page">
    <img src="/images/back_to_top.png" style="max-width: 120px; position: absolute; z-index:100; bottom: 50px; left: 30px;"/>
    <!-- <span>{!! _t('вверх', 'common') !!}</span> -->
</div>