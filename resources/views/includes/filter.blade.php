@php
    declare(strict_types=1);

    use App\Models\ItemSize;
@endphp

@if($filter === 1)
    <input type="hidden" name="type" value="{{ $filter }}">
    <div class="n-filter__text">{!! _t('Укажите наружный диаметр трубы:', 'search') !!}</div>
    <div class="n-filter__icon n-filter__icon_bottom">
        {!! File::get(base_path() . '/public/images/filter/circle.svg') !!}
    </div>
    <div class="n-filter__inputs">
        <div class="n-filter__input-wrap">
            <input class="js-search-size n-filter__input" name="size_1" value="{{ Request::get('size_1') }}" placeholder="20 {!! _t('мм', 'search') !!}">
        </div>
    </div>
@elseif($filter === 2)
    <input type="hidden" name="type" value="{{ $filter }}">
    <div class="n-filter__text">{!! _t('Укажите размер трубы:', 'search') !!}</div>
    <div class="n-filter__icon n-filter__icon_bottom">
        {!! File::get(base_path() . '/public/images/filter/square.svg') !!}
    </div>
    <div class="n-filter__inputs">
        <div class="n-filter__input-wrap">
            <input class="js-search-size n-filter__input" name="size_1" value="{{ Request::get('size_1') }}" placeholder="20 {!! _t('мм', 'search') !!}">
        </div>
    </div>
@elseif($filter === 3)
    <input type="hidden" name="type" value="{{ $filter }}">
    <div class="n-filter__text">{!! _t('Укажите размеры трубы:', 'search') !!}</div>
    <div class="n-filter__icon n-filter__icon_bottom">
        {!! File::get(base_path() . '/public/images/filter/rectangle.svg') !!}
    </div>
    <div class="n-filter__inputs n-filter__inputs_two">
        <div class="n-filter__input-wrap">
            <input class="js-search-size n-filter__input" name="size_1" value="{{ Request::get('size_1') }}" placeholder="20 {!! _t('мм', 'search') !!}">
        </div>
        <div class="n-filter__input-wrap">
            <input class="js-search-size n-filter__input" name="size_2" value="{{ Request::get('size_2') }}" placeholder="40 {!! _t('мм', 'search') !!}">
        </div>
    </div>
@elseif($filter === 4)
    <input type="hidden" name="type" value="{{ $filter }}">
    <div class="n-filter__text">{!! _t('Укажите размеры трубы:', 'search') !!}</div>
    <div class="n-filter__icon n-filter__icon_bottom">
        {!! File::get(base_path() . '/public/images/filter/oval.svg') !!}
    </div>
    <div class="n-filter__inputs n-filter__inputs_two">
        <div class="n-filter__input-wrap">
            <input class="js-search-size n-filter__input" name="size_1" value="{{ Request::get('size_1') }}" placeholder="20 {!! _t('мм', 'search') !!}">
        </div>
        <div class="n-filter__input-wrap">
            <input class="js-search-size n-filter__input" name="size_2" value="{{ Request::get('size_2') }}" placeholder="40 {!! _t('мм', 'search') !!}">
        </div>
    </div>
@elseif($filter === 5)
    <input type="hidden" name="type" value="{{ $filter }}">
    <div class="n-filter__text">{!! _t('Укажите размеры трубы:', 'search') !!}</div>
    <div class="n-filter__icon n-filter__icon_bottom">
        {!! File::get(base_path() . '/public/images/filter/semioval.svg') !!}
    </div>
    <div class="n-filter__inputs n-filter__inputs_two">
        <div class="n-filter__input-wrap">
            <input class="js-search-size n-filter__input" name="size_1" value="{{ Request::get('size_1') }}" placeholder="20 {!! _t('мм', 'search') !!}">
        </div>
        <div class="n-filter__input-wrap">
            <input class="js-search-size n-filter__input" name="size_2" value="{{ Request::get('size_2') }}" placeholder="40 {!! _t('мм', 'search') !!}">
        </div>
    </div>
@elseif($filter === 6)
    <input type="hidden" name="type" value="{{ $filter }}">
    <div class="n-filter__text">{!! _t('Выберите параметры:', 'search') !!}</div>
    <div class="n-filter__icon">
        {!! File::get(base_path() . '/public/images/filter/thread.svg') !!}
    </div>
    <div class="n-filter__inputs n-filter__inputs_two">
        @component('components.filter-select', ['size' => '1', 'request' => Request::get('size_1', ''), 'placeholder' => _t('Марка резьбы', 'search'), 'sizes' => ItemSize::getSizes($filter, 1)])])@endcomponent
        @component('components.filter-select', ['size' => '2', 'request' => Request::get('size_2', ''), 'placeholder' => _t('Длина резьбы', 'search'), 'sizes' => ItemSize::getSizes($filter, 2)])])@endcomponent
        @component('components.filter-select', ['size' => '3', 'request' => Request::get('size_3', ''), 'placeholder' => _t('Размер основания', 'search'), 'sizes' => ItemSize::getSizes($filter, 3)])])@endcomponent
    </div>
@elseif($filter === 8)
    <input type="hidden" name="type" value="{{ $filter }}">
    <div class="n-filter__text">{!! _t('Выберите стандарт резьбы:', 'search') !!}</div>
    <div class="n-filter__icon">
        {!! File::get(base_path() . '/public/images/filter/nut.svg') !!}
    </div>
    <div class="n-filter__inputs">
        @component('components.filter-select', ['size' => '1', 'request' => Request::get('size_1', ''), 'sizes' => ItemSize::getSizes($filter, 1)])])@endcomponent
    </div>
@elseif($filter === 23)
    @php
        $filter = 8;
    @endphp
    <input type="hidden" name="type" value="{{ $filter }}">
    <div class="n-filter__text">{!! _t('Выберите стандарт резьбы:', 'search') !!}</div>
    <div class="n-filter__icon">
        {!! File::get(base_path() . '/public/images/filter/23.svg') !!}
    </div>
    <div class="n-filter__inputs">
        @component('components.filter-select', ['size' => '1', 'request' => Request::get('size_1', ''), 'sizes' => ItemSize::getSizes($filter, 1)])])@endcomponent
    </div>
@elseif($filter === 24)
    @php
        $filter = 8;
    @endphp
    <input type="hidden" name="type" value="{{ $filter }}">
    <div class="n-filter__text">{!! _t('Выберите стандарт резьбы:', 'search') !!}</div>
    <div class="n-filter__icon">
        {!! File::get(base_path() . '/public/images/filter/24.svg') !!}
    </div>
    <div class="n-filter__inputs">
        @component('components.filter-select', ['size' => '1', 'request' => Request::get('size_1', ''), 'sizes' => ItemSize::getSizes($filter, 1)])])@endcomponent
    </div>
@elseif($filter === 25)
    @php
        $filter = 8;
    @endphp
    <input type="hidden" name="type" value="{{ $filter }}">
    <div class="n-filter__text">{!! _t('Выберите стандарт резьбы:', 'search') !!}</div>
    <div class="n-filter__icon">
        {!! File::get(base_path() . '/public/images/filter/25.svg') !!}
    </div>
    <div class="n-filter__inputs">
        @component('components.filter-select', ['size' => '1', 'request' => Request::get('size_1', ''), 'sizes' => ItemSize::getSizes($filter, 1)])])@endcomponent
    </div>
@elseif($filter === 26)
    @php
        $filter = 8;
    @endphp
    <input type="hidden" name="type" value="{{ $filter }}">
    <div class="n-filter__text">{!! _t('Выберите стандарт резьбы:', 'search') !!}</div>
    <div class="n-filter__icon">
        {!! File::get(base_path() . '/public/images/filter/26.svg') !!}
    </div>
    <div class="n-filter__inputs">
        @component('components.filter-select', ['size' => '1', 'request' => Request::get('size_1', ''), 'sizes' => ItemSize::getSizes($filter, 1)])])@endcomponent
    </div>
@elseif($filter === 9)
    <input type="hidden" name="type" value="{{ $filter }}">
    <div class="n-filter__text">{!! _t('Укажите размеры трубы:', 'search') !!}</div>
    <div class="n-filter__icon n-filter__icon_bottom">
        {!! File::get(base_path() . '/public/images/filter/ellipse.svg') !!}
    </div>
    <div class="n-filter__inputs n-filter__inputs_two">
        <div class="n-filter__input-wrap">
            <input class="js-search-size n-filter__input" name="size_1" value="{{ Request::get('size_1') }}" placeholder="20 {!! _t('мм', 'search') !!}">
        </div>
        <div class="n-filter__input-wrap">
            <input class="js-search-size n-filter__input" name="size_2" value="{{ Request::get('size_2') }}" placeholder="40 {!! _t('мм', 'search') !!}">
        </div>
    </div>
@elseif($filter === 10)
    <input type="hidden" name="type" value="{{ $filter }}">
    <div class="n-filter__text">{!! _t('Выберите размер ДУ:', 'search') !!}</div>
    <div class="n-filter__icon n-filter__icon_bottom">
        {!! File::get(base_path() . '/public/images/filter/circle-two.svg') !!}
    </div>
    <div class="n-filter__inputs">
        @component('components.filter-select', ['size' => '1', 'request' => Request::get('size_1', ''), 'sizes' => ItemSize::getSizes($filter, 1)])])@endcomponent
    </div>
@elseif($filter === 11)
    <input type="hidden" name="type" value="{{ $filter }}">
    <div class="n-filter__text">{!! _t('Укажите диаметр отверстия:', 'search') !!}</div>
    <div class="n-filter__icon">
        {!! File::get(base_path() . '/public/images/filter/washers.svg') !!}
    </div>
    <div class="n-filter__inputs">
        <div class="n-filter__input-wrap">
            <input class="js-search-size n-filter__input" name="size_1" value="{{ Request::get('size_1') }}" placeholder="20 {!! _t('мм', 'search') !!}">
        </div>
    </div>
@elseif($filter === 15)
    <input type="hidden" name="type" value="{{ $filter }}">
    <div class="n-filter__text">{!! _t('Выберите параметры:', 'search') !!}</div>
    <div class="n-filter__icon">
        {!! File::get(base_path() . '/public/images/filter/pennut.svg') !!}
    </div>
    <div class="n-filter__inputs n-filter__inputs_two">
        @component('components.filter-select', ['size' => '1', 'request' => Request::get('size_1', ''), 'placeholder' => _t('Марка резьбы', 'search'), 'sizes' => ItemSize::getSizes($filter, 1)])])@endcomponent
        @component('components.filter-select', ['size' => '2', 'request' => Request::get('size_2', ''), 'placeholder' => _t('Размер рукоятки', 'search'), 'sizes' => ItemSize::getSizes($filter, 2)])])@endcomponent
    </div>
@elseif($filter === 16)
    @php
        $filter = 15;
    @endphp
    <input type="hidden" name="type" value="{{ $filter }}">
    <div class="n-filter__text">{!! _t('Выберите параметры:', 'search') !!}</div>
    <div class="n-filter__icon">
        {!! File::get(base_path() . '/public/images/filter/pennut.svg') !!}
    </div>
    <div class="n-filter__inputs n-filter__inputs_two">
        @component('components.filter-select', ['size' => '1', 'request' => Request::get('size_1', ''), 'placeholder' => _t('Марка резьбы', 'search'), 'sizes' => ItemSize::getSizes($filter, 1)])])@endcomponent
        @component('components.filter-select', ['size' => '2', 'request' => Request::get('size_2', ''), 'placeholder' => _t('Размер рукоятки', 'search'), 'sizes' => ItemSize::getSizes($filter, 2)])])@endcomponent
    </div>
@elseif($filter === 14)
    <input type="hidden" name="type" value="{{ $filter }}">
    <div class="n-filter__text">{!! _t('Выберите параметры:', 'search') !!}</div>
    <div class="n-filter__icon">
        {!! File::get(base_path() . '/public/images/filter/penbolt.svg') !!}
    </div>
    <div class="n-filter__inputs n-filter__inputs_two">
        @component('components.filter-select', ['size' => '1', 'request' => Request::get('size_1', ''), 'placeholder' => _t('Марка резьбы', 'search'), 'sizes' => ItemSize::getSizes($filter, 1)])])@endcomponent
        @component('components.filter-select', ['size' => '2', 'request' => Request::get('size_2', ''), 'placeholder' => _t('Длина резьбы', 'search'), 'sizes' => ItemSize::getSizes($filter, 2)])])@endcomponent
        @component('components.filter-select', ['size' => '3', 'request' => Request::get('size_3', ''), 'placeholder' => _t('Размер рукоятки', 'search'), 'sizes' => ItemSize::getSizes($filter, 3)])])@endcomponent
    </div>
@elseif($filter === 21)
    @php
        $filter = 14;
    @endphp
    <input type="hidden" name="type" value="{{ $filter }}">
    <div class="n-filter__text">{!! _t('Выберите параметры:', 'search') !!}</div>
    <div class="n-filter__icon">
        {!! File::get(base_path() . '/public/images/filter/21.svg') !!}
    </div>
    <div class="n-filter__inputs n-filter__inputs_two">
        @component('components.filter-select', ['size' => '1', 'request' => Request::get('size_1', ''), 'placeholder' => _t('Марка резьбы', 'search'), 'sizes' => ItemSize::getSizes($filter, 1)])])@endcomponent
        @component('components.filter-select', ['size' => '2', 'request' => Request::get('size_2', ''), 'placeholder' => _t('Длина резьбы', 'search'), 'sizes' => ItemSize::getSizes($filter, 2)])])@endcomponent
        @component('components.filter-select', ['size' => '3', 'request' => Request::get('size_3', ''), 'placeholder' => _t('Размер рукоятки', 'search'), 'sizes' => ItemSize::getSizes($filter, 3)])])@endcomponent
    </div>
@elseif($filter === 22)
    @php
        $filter = 15;
    @endphp
    <input type="hidden" name="type" value="{{ $filter }}">
    <div class="n-filter__text">{!! _t('Выберите параметры:', 'search') !!}</div>
    <div class="n-filter__icon">
        {!! File::get(base_path() . '/public/images/filter/22.svg') !!}
    </div>
    <div class="n-filter__inputs n-filter__inputs_two">
        @component('components.filter-select', ['size' => '1', 'request' => Request::get('size_1', ''), 'placeholder' => _t('Марка резьбы', 'search'), 'sizes' => ItemSize::getSizes($filter, 1)])])@endcomponent
        @component('components.filter-select', ['size' => '2', 'request' => Request::get('size_2', ''), 'placeholder' => _t('Размер рукоятки', 'search'), 'sizes' => ItemSize::getSizes($filter, 2)])])@endcomponent
    </div>
@elseif($filter === 18)
    <input type="hidden" name="type" value="{{ $filter }}">
    <div class="n-filter__text">{!! _t('Выберите параметры:', 'search') !!}</div>
    <div class="n-filter__icon">
        {!! File::get(base_path() . '/public/images/filter/flush_holder.svg') !!}
    </div>
    <div class="n-filter__inputs n-filter__inputs_two">
        @component('components.filter-select', ['size' => '1', 'request' => Request::get('size_1', ''), 'placeholder' => _t('Ширина латы', 'search'), 'sizes' => ItemSize::getSizes($filter, 1)])])@endcomponent
        @component('components.filter-select', ['size' => '2', 'request' => Request::get('size_2', ''), 'placeholder' => _t('Толщина латы', 'search'), 'sizes' => ItemSize::getSizes($filter, 2)])])@endcomponent
    </div>
@elseif($filter === 19)
    <input type="hidden" name="type" value="{{ $filter }}">
    <div class="n-filter__text">{!! _t('Выберите параметры изделия:', 'search') !!}</div>
    <div class="n-filter__icon">
        {!! File::get(base_path() . '/public/images/filter/flush_holder_under-two.svg') !!}
    </div>
    <div class="n-filter__inputs n-filter__inputs_two">
        @component('components.filter-select', ['size' => '1', 'request' => Request::get('size_1', ''), 'placeholder' => _t('Диаметр трубы', 'search'), 'sizes' => ItemSize::getSizes($filter, 1)])])@endcomponent
        @component('components.filter-select', ['size' => '2', 'request' => Request::get('size_2', ''), 'placeholder' => _t('Ширина латы', 'search'), 'sizes' => ItemSize::getSizes($filter, 2)])])@endcomponent
        @component('components.filter-select', ['size' => '3', 'request' => Request::get('size_3', ''), 'placeholder' => _t('Толщина латы', 'search'), 'sizes' => ItemSize::getSizes($filter, 3)])])@endcomponent
    </div>
@elseif($filter === 17)
    <input type="hidden" name="type" value="{{ $filter }}">
    <div class="n-filter__text">{!! _t('Укажите размеры изделия:', 'search') !!}</div>
    <div class="n-filter__icon">
        {!! File::get(base_path() . '/public/images/filter/shim.svg') !!}
    </div>
    <div class="n-filter__inputs n-filter__inputs_two">
        <div class="n-filter__input-wrap">
            <input class="js-search-size n-filter__input" name="size_1" value="{{ Request::get('size_1') }}" placeholder="8 {!! _t('мм', 'search') !!}">
        </div>
        <div class="n-filter__input-wrap">
            <input class="js-search-size n-filter__input" name="size_2" value="{{ Request::get('size_2') }}" placeholder="20 {!! _t('мм', 'search') !!}">
        </div>
    </div>
@elseif($filter === 12)
    <input type="hidden" name="type" value="{{ $filter }}">
    <div class="n-filter__text">{!! _t('Выберите параметры:', 'search') !!}</div>
    <div class="n-filter__icon">
        {!! File::get(base_path() . '/public/images/filter/flush_holder_under.svg') !!}
    </div>
    <div class="n-filter__inputs n-filter__inputs_two">
        @php
            $size = (int) Request::get('size');
            $f2 = $size ?: 0;
            $f3 = $size ? ($size + 1) : 0;
        @endphp
        @component('components.filter-select', ['request' => Request::get('size', ''), 'placeholder' => _t('Стандарт резьбы', 'search'), 'sizes' => ItemSize::$filterTypes, 'class' => 'js-filter-sizes'])])@endcomponent
        @component('components.filter-select', ['size' => $f2, 'request' => Request::get('size_' . $f2, ''), 'placeholder' => _t('Резьба', 'search'), 'sizes' => ItemSize::getSizes($filter, $f2)])])@endcomponent
        @component('components.filter-select', ['size' => $f3, 'request' => Request::get('size_' . $f3, ''), 'placeholder' => _t('Шаг резьбы', 'search'), 'sizes' => ItemSize::getSizes($filter, $f3)])])@endcomponent
    </div>
@elseif($filter === 20)
    @php
        $filter = 12;
    @endphp
    <input type="hidden" name="type" value="{{ $filter }}">
    <div class="n-filter__text">{!! _t('Выберите параметры:', 'search') !!}</div>
    <div class="n-filter__icon">
        {!! File::get(base_path() . '/public/images/filter/flush_holder_under_2.svg') !!}
    </div>
    <div class="n-filter__inputs n-filter__inputs_two">
        @php
            $size = (int) Request::get('size');
            $f2 = $size ?: 0;
            $f3 = $size ? ($size + 1) : 0;
        @endphp
        @component('components.filter-select', ['request' => Request::get('size', ''), 'placeholder' => _t('Стандарт резьбы', 'search'), 'sizes' => ItemSize::$filterTypes, 'class' => 'js-filter-sizes'])])@endcomponent
        @component('components.filter-select', ['size' => $f2, 'request' => Request::get('size_' . $f2, ''), 'placeholder' => _t('Резьба', 'search'), 'sizes' => ItemSize::getSizes($filter, $f2)])])@endcomponent
        @component('components.filter-select', ['size' => $f3, 'request' => Request::get('size_' . $f3, ''), 'placeholder' => _t('Шаг резьбы', 'search'), 'sizes' => ItemSize::getSizes($filter, $f3)])])@endcomponent
    </div>
@elseif($filter === 13)
    <input type="hidden" name="type" value="{{ $filter }}">
    <div class="n-filter__text">{!! _t('Выберите параметры:', 'search') !!}</div>
    <div class="n-filter__icon">
        {!! File::get(base_path() . '/public/images/filter/outer_thread.svg') !!}
    </div>
    <div class="n-filter__inputs n-filter__inputs_two">
        @php
            $size = (int) Request::get('size');
            $f2 = $size ?: 0;
            $f3 = $size ? ($size + 1) : 0;
        @endphp
        @component('components.filter-select', ['request' => Request::get('size', ''), 'placeholder' => _t('Стандарт резьбы', 'search'), 'sizes' => ItemSize::$filterTypes, 'class' => 'js-filter-sizes'])])@endcomponent
        @component('components.filter-select', ['size' => $f2, 'request' => Request::get('size_' . $f2, ''), 'placeholder' => _t('Резьба', 'search'), 'sizes' => ItemSize::getSizes($filter, $f2)])])@endcomponent
        @component('components.filter-select', ['size' => $f3, 'request' => Request::get('size_' . $f3, ''), 'placeholder' => _t('Шаг резьбы', 'search'), 'sizes' => ItemSize::getSizes($filter, $f3)])])@endcomponent
    </div>
@endif