@php
    declare(strict_types=1);

    use App\Models\City;

    $Cities = City::cachedAll();

    $changeLanguageUrl = Request::getRequestUri();
    if (in_array(Request::segment(1), Config::get('app.locales')) && Request::segment(1) != 'ru') {
        $changeLanguageUrl = mb_substr($changeLanguageUrl, 3);
    }

    $SupposedCity = session('supposed_city') ? City::cachedOne(session('supposed_city')) : null;

@endphp
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W2ZZDRL"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<header class="header">
    <div class="header__top">
        <div class="header__container">
            <div class="header__row">
                <a class="header__logo" href="{{ route('home') }}">
		    <!-- logo.svg?v=2-->
                    <img src="{{ asset('images/logo.svg?v=2') }}" alt="{!! _t('Заглушка.Ру', 'common') !!}" width="300" height="61">
                </a>
                @if(App::isLocale('ru'))
                    <div class="header-contacts header-contacts_city">
                        @component('components.icon', ['name' => 'marker', 'attributes' => ['class' => 'header-contacts__icon']])])@endcomponent
                        <div class="header-contacts__body">
                            @if($CurrentCity && $CurrentCity->phone)
                                <a class="header-contacts__phone" href="tel:+{{ Formatter::phone($CurrentCity->phone) }}">{{ $CurrentCity->phone }}</a>
                            @endif
                            <div class="header-contacts__city" tabindex="0">
                                <span class="header-contacts__city-text js-header-city" {{ $CurrentCity ? 'data-id=' . $CurrentCity->id : '' }}>{!! $CurrentCity ? $CurrentCity->title : 'Выберите город'!!}</span>
                                @component('components.icon', ['name' => 'arrow-menu', 'attributes' => ['class' => 'header-contacts__city-icon']])])@endcomponent
                            </div>
                        </div>
                        @if($SupposedCity)
                            <div class="header-popup-your-city header-popup-your-city_active">
                                <div class="header-popup-your-city__title">{!! _t('Ваш город', 'common') !!} {{ $SupposedCity->title }}? </div>
                                <div class="header-popup-your-city__buttons">
                                    <button class="header-popup-your-city__btn-yes js-your-city-button" data-city="{{ $SupposedCity->id }}" data-value="yes">{!! _t('Да', 'common') !!}</button>
                                    <button class="header-popup-your-city__btn-no js-your-city-button" data-value="no">{!! _t('Выбрать другой город', 'common') !!}</button>
                                </div>
                            </div>
                            @php
                                session(['supposed_city' => 0]);
                            @endphp
                        @endif
                    </div>
                    <div class="header-contacts">
                        @component('components.icon', ['name' => 'phone', 'attributes' => ['class' => 'header-contacts__icon']])])@endcomponent
                        <div class="header-contacts__body">
                            <a class="header-contacts__phone" href="tel:+78005550499">8 (800) 555 04 99</a>
                            <div class="header-contacts__text">{!! _t('Бесплатно по России', 'common') !!}</div>
                        </div>
                    </div>
                @else
                    <div class="header-contacts">
                        @component('components.icon', ['name' => 'phone', 'attributes' => ['class' => 'header-contacts__icon']])])@endcomponent
                        <div class="header-contacts__body">
                            <a class="header-contacts__phone" href="tel:+78005550499">+7 (812) 242 80 99</a>
                        </div>
                    </div>
                @endif
                    <div class="header-contacts header-contacts__social" style="display: flex; justify-content: space-around; flex-flow: wrap;">
                        <div style="margin-top:18px;">
                            <a class="social" href="https://msng.link/o/?Zaglushka_ru=tg"  target="_blank" style="color: white;">
                                <img src="/images/icons/telegram.png" width="30" height="30">
                            </a>
                            <a class="social" href="https://vk.com/im?sel=-187690751" target="_blank" style="margin-left: 18px;color: white;">
                                <img src="/images/icons/vk.png" width="30" height="30">
                            </a>
                            <a class="social" href="https://wa.me/79119532546" target="_blank" style="margin-left: 18px;color: white;">
                                <img src="/images/icons/whatsapp.png" width="30" height="30">
                            </a>
                            <a class="social" href="mailto:sales@zaglushka.ru" target="_blank" style="margin-left: 18px;color: white;">
                                <img src="/images/icons/e-mail.png" width="30" height="30">
                            </a>
                            <p style="flex-basis:100%; text-align:center; margin-top:0px; margin-bottom: 0px;">
                                <span>Задать вопрос в соц сети</span>
                            </p>
                        </div>
                        <div>
                        </div>
                    </div>
                @component('includes.header_flags', ['changeLanguageUrl' => $changeLanguageUrl])@endcomponent
            </div>
        </div>
    </div>
    @include('includes.nav')
    <div class="header__container">
        @yield('header')
    </div>
</header>
@component('includes.change_city', ['CurrentCity' => $CurrentCity, 'Cities' => $Cities])@endcomponent