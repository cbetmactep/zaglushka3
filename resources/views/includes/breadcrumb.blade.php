@php
    declare(strict_types=1);

    use App\Models\Redis\Cart as RedisCart;

    $hints = [
        'пластиковые горки',
        'резьбовые опоры',
        'хромированные заглушки',
        'фиксаторы М8',
        'качели',
        'латодержатели',
        'переходники для труб',
        'квадратные заглушки 20х20',
        'заглушки для отверстий',
    ];

    // todo дублируется запрос
    $inCart = RedisCart::count();

@endphp

<div class="breadcrumb__wrap">
    <div class="breadcrumb">
        <div class="breadcrumb__container">
            <div class="breadcrumb__row">
                <div class="breadcrumb__column-list">
                    <div class="breadcrumb-item__start">
                        @component('components.icon', ['name' => 'arrow-breadcrumbs'])])@endcomponent
                    </div>
                    @foreach($items as $item)
                        @if(isset($item['href']))
                            <a class="breadcrumb-item" href="{{ $item['href'] }}">
                                <div class="breadcrumb-item__title">{{ $item['title'] }}</div>
                                @component('components.icon', ['name' => 'arrow-braedcrumbs2', 'attributes' => ['class' => 'breadcrumb-item__icon']])])@endcomponent
                            </a>
                        @else
                            <div class="breadcrumb-item">
                                <div class="breadcrumb-item__title">{{ $item['title'] }}</div>
                                @component('components.icon', ['name' => 'arrow-braedcrumbs2', 'attributes' => ['class' => 'breadcrumb-item__icon']])])@endcomponent
                            </div>
                        @endif
                    @endforeach
                </div>
                @isset($pageActions)
                    <div class="breadcrumb__column-actions">
                        @component('includes.page_actions')@endcomponent
                    </div>
                @endisset
            </div>
        </div>
    </div>
</div>
<div class="breadcrumb-fixed js-breadcrumb-fixed">
    <div class="breadcrumb-fixed__container">
        <div class="breadcrumb-fixed__row">
            <div class="breadcrumb-fixed__column-list">
                <div class="breadcrumb-fixed-item__start">
                    @component('components.icon', ['name' => 'arrow-breadcrumbs'])])@endcomponent
                </div>
                @foreach($items as $item)
                    @if(isset($item['href']))
                        <a class="breadcrumb-fixed-item" href="{{ $item['href'] }}">
                            <div class="breadcrumb-fixed-item__title">{{ $item['title'] }}</div>
                            @component('components.icon', ['name' => 'arrow-braedcrumbs2', 'attributes' => ['class' => 'breadcrumb-fixed-item__icon']])])@endcomponent
                        </a>
                    @else
                        <div class="breadcrumb-fixed-item">
                            <div class="breadcrumb-fixed-item__title">{{ $item['title'] }}</div>
                            @component('components.icon', ['name' => 'arrow-braedcrumbs2', 'attributes' => ['class' => 'breadcrumb-fixed-item__icon']])])@endcomponent
                        </div>
                    @endif
                @endforeach
            </div>
            <div class="breadcrumb-fixed-links js-nav-block">
                <div class="breadcrumb-fixed-links__item">
                <a class="breadcrumb-fixed-links__link js-nav-link-search" href="{{ route('search') }}">
                    @component('components.icon', ['name' => 'search', 'attributes' => ['class' => 'breadcrumb-fixed-links__link-icon']])@endcomponent
                </a>
                </div>
                <div class="breadcrumb-fixed-links__item">
                    <a class="breadcrumb-fixed-links__link" href="{{ route('cart') }}">
                        @component('components.icon', ['name' => 'basket', 'attributes' => ['class' => 'breadcrumb-fixed-links__link-icon']])@endcomponent
                        <span class="js-nav-cart-count-block breadcrumb-fixed-links__tooltip" @if(!$inCart) style="display: none;" @endif>
                            <span class="js-nav-cart-count">{{ Formatter::quantity($inCart, false) }}</span> <span>{!! _t('шт.', 'common') !!}</span>
                        </span>
                    </a>
                </div>
            </div>
            <div class="breadcrumb-fixed-links breadcrumb-fixed-links_search js-nav-block-search" style="display: none;">
                <form action="{{ route('search') }}" class="breadcrumb-fixed-links-search__input-wrap">
                    <input name="article" class="breadcrumb-fixed-links-search__input js-nav-search-input js-search-article" autocomplete="off" placeholder="{!! _t('Введите артикул или название, например:', 'search') . ' ' . _t($hints[mt_rand(0, count($hints) - 1)]) !!}">
                    <button class="breadcrumb-fixed-links-search__input-icon">
                        @component('components.icon', ['name' => 'search'])@endcomponent
                    </button>
                </form>
                <a href="{{ route('search') }}" class="breadcrumb-fixed-links-search__link">{!! _t('Расширенный поиск', 'common') !!}</a>
                @component('components.icon', ['name' => 'close-thin', 'attributes' => ['class' => 'breadcrumb-fixed-links-search__icon js-nav-search-close']])@endcomponent
            </div>
        </div>
    </div>
</div>