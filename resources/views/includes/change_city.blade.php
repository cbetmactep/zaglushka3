@php
    declare(strict_types=1);

    use App\Models\City;

    $cities = [
        City::COUNTRY_RUSSIA => [
            'Москва',
            'Санкт-Петербург',
            'Волгоград',
            'Воронеж',
            'Екатеринбург',
            'Казань',
            'Красноярск',
            'Нижний Новгород',
            'Новосибирск',
            'Омск',
            'Пермь',
            'Ростов-на-Дону',
            'Самара',
            'Уфа',
            'Челябинск',
        ],
        City::COUNTRY_KAZAKHSTAN => [
            'Алматы',
            'Астана',
            'Шымкент',
            'Караганды',
            'Актобе',
            'Тараз',
            'Павлодар',
            'Усть-Каменогорск',
            'Семей',
            'Костанай',
            'Уральск',
            'Атырау',
            'Кызылорда',
            'Петропавловск',
            'Актау',
        ],
        City::COUNTRY_BELARUS => [
            'Минск',
            'Брест',
            'Гродно',

            'Гомель',
            'Витебск',
            'Могилев',

            'Бобруйск',
            'Барановичи',
            'Новополоцк',

            'Пинск',
            'Борисов',
            'Мозырь',

            'Полоцк',
            'Слоним',
            'Лида',
        ],
        City::COUNTRY_UKRAINE => [
            'Киев',
            'Харьков',
            'Одесса',
            'Днепр',
            'Донецк',
            'Запорожье',
            'Львов',
            'Кривой Рог',
            'Николаев',
            'Мариуполь',
            'Луганск',
            'Винница',
            'Макеевка',
            'Херсон',
            'Полтава',
        ],
    ]

@endphp
@component('components.modal', ['name' => 'changeCity', 'class' => 'change-city'])
    <div class="change-city__header">
        <div class="change-city__country">
            <div class="change-city__country-item @if(!isset($CurrentCity) || (isset($CurrentCity) && $CurrentCity->country === 1)) change-city__country-item_active @endif"
                 data-type="1">
                <img src="{{ asset('/images/flags/russia.svg') }}">
                <span class="change-city__country-name">Россия</span>
            </div>
            <div class="change-city__country-item @if(isset($CurrentCity) && $CurrentCity->country === 2) change-city__country-item_active @endif "
                 data-type="2">
                <img src="{{ asset('/images/flags/kazakhstan.svg') }}">
                <span class="change-city__country-name">Казахстан</span>
            </div>
            <div class="change-city__country-item @if(isset($CurrentCity) && $CurrentCity->country === 3) change-city__country-item_active @endif "
                 data-type="3">
                <img src="{{ asset('/images/flags/belarus.svg') }}">
                <span class="change-city__country-name">Беларусь</span>
            </div>
            <div class="change-city__country-item @if(isset($CurrentCity) && $CurrentCity->country === 4) change-city__country-item_active @endif "
                 data-type="4">
                <img src="{{ asset('/images/flags/ukraine.svg') }}">
                <span class="change-city__country-name">Украина</span>
            </div>
        </div>
    </div>
    <div class="change-city__body @if(!isset($CurrentCity) || $CurrentCity->country === 1) change-city__body_active @endif "
         data-type="1">
        <div class="change-city__title">Осуществляем доставку по всей России</div>
        <form class="change-city__input">
            <input name="county" type="hidden" value="1">
            <input name="query" class="change-city__input-field" placeholder="Введите название населенного пункта"
                   data-placeholder="Введите название населенного пункта"
                   data-mob-placeholder="Введите название города">
            @component('components.icon', ['name' => 'search', 'attributes' => ['class' => 'change-city__input-icon']])@endcomponent
        </form>
        <div class="change-city__cities">
            @php
                $columns = 3;
                $rows = ceil(count($cities[1]) / $columns);
            @endphp
            @for ($row = 0; $row < $rows; $row++)
                @foreach ($cities[1] as $k => $cityTitle)
                    @if ($k % $rows == $row)
                        @php
                            $City = City::cachedByTitle($cityTitle);
                        @endphp
                        @if($City)
                            <div class="change-city__link @if(isset($CurrentCity) && $CurrentCity->id === $City->id) change-city__link_active @endif "
                                 data-id="{{ $City->id }}">
                                {{ $City->title }}
                            </div>
                        @endif
                    @endif
                @endforeach
            @endfor
        </div>
    </div>
    <div class="change-city__body @if(isset($CurrentCity) && $CurrentCity->country === 2) change-city__body_active @endif "
         data-type="2">
        <div class="change-city__title">Осуществляем доставку по всему Казахстану</div>
        <form class="change-city__input">
            <input name="county" type="hidden" value="2">
            <input name="query" class="change-city__input-field" placeholder="Введите название населенного пункта"
                   data-placeholder="Введите название населенного пункта"
                   data-mob-placeholder="Введите название города">
            @component('components.icon', ['name' => 'search', 'attributes' => ['class' => 'change-city__input-icon']])@endcomponent
        </form>
        <div class="change-city__cities">
            @php
                $columns = 3;
                $rows = ceil(count($cities[2]) / $columns);
            @endphp
            @for ($row = 0; $row < $rows; $row++)
                @foreach ($cities[2] as $k => $cityTitle)
                    @if ($k % $rows == $row)
                        @php
                            $City = City::cachedByTitle($cityTitle);
                        @endphp
                        @if($City)
                            <div class="change-city__link @if(isset($CurrentCity) && $CurrentCity->id === $City->id) change-city__link_active @endif "
                                 data-id="{{ $City->id }}">
                                {{ $City->title }}
                            </div>
                        @endif
                    @endif
                @endforeach
            @endfor
        </div>
    </div>
    <div class="change-city__body @if(isset($CurrentCity) && $CurrentCity->country === 3) change-city__body_active @endif "
         data-type="3">
        <div class="change-city__title">Осуществляем доставку по всей Беларуси</div>
        <form class="change-city__input">
            <input name="county" type="hidden" value="3">
            <input name="query" class="change-city__input-field" placeholder="Введите название населенного пункта"
                   data-placeholder="Введите название населенного пункта"
                   data-mob-placeholder="Введите название города">
            @component('components.icon', ['name' => 'search', 'attributes' => ['class' => 'change-city__input-icon']])@endcomponent
        </form>
        <div class="change-city__cities">
            @php
                $columns = 3;
                $rows = ceil(count($cities[3]) / $columns);
            @endphp
            @for ($row = 0; $row < $rows; $row++)
                @foreach ($cities[3] as $k => $cityTitle)
                    @if ($k % $rows == $row)
                        @php
                            $City = City::cachedByTitle($cityTitle);
                        @endphp
                        @if($City)
                            <div class="change-city__link @if(isset($CurrentCity) && $CurrentCity->id === $City->id) change-city__link_active @endif "
                                 data-id="{{ $City->id }}">
                                {{ $City->title }}
                            </div>
                        @endif
                    @endif
                @endforeach
            @endfor
        </div>
    </div>
    <div class="change-city__body @if(isset($CurrentCity) && $CurrentCity->country === 4) change-city__body_active @endif "
         data-type="4">
        <div class="change-city__title">Осуществляем доставку по всей Украине</div>
        <form class="change-city__input">
            <input name="county" type="hidden" value="4">
            <input name="query" class="change-city__input-field" placeholder="Введите название населенного пункта"
                   data-placeholder="Введите название населенного пункта"
                   data-mob-placeholder="Введите название города">
            @component('components.icon', ['name' => 'search', 'attributes' => ['class' => 'change-city__input-icon']])@endcomponent
        </form>
        <div class="change-city__cities">
            @php
                $columns = 3;
                $rows = ceil(count($cities[4]) / $columns);
            @endphp
            @for ($row = 0; $row < $rows; $row++)
                @foreach ($cities[4] as $k => $cityTitle)
                    @if ($k % $rows == $row)
                        @php
                            $City = City::cachedByTitle($cityTitle);
                        @endphp
                        @if($City)
                            <div class="change-city__link @if(isset($CurrentCity) && $CurrentCity->id === $City->id) change-city__link_active @endif "
                                 data-id="{{ $City->id }}">
                                {{ $City->title }}
                            </div>
                        @endif
                    @endif
                @endforeach
            @endfor
        </div>
    </div>
@endcomponent