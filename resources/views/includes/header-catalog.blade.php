@php
    declare(strict_types=1);

    use App\Models\Category;
    use App\Models\ItemHot;

    $Categories = Category::getTopCategories();
    $icons = ['discount', 'high_quality', 'in_stock', 'manufacture', 'protection', 'new']
@endphp

<div class="header-catalog__wrap">
    <div class="header-catalog">
        <div class="header-catalog-menu">
            @foreach($Categories as $Category)
                @if($Category->id!=40 and $Category->id!=39 and $Category->id!=210)
                <a class="header-catalog-menu__item @if($loop->first) header-catalog-menu__item_active @endif " href="{{ route('slug', ['slug' => $Category->slug()]) }}">
                    <div class="header-catalog-menu__left">
                        {{ $Category->getTitle() }}
                    </div>
                    <div class="header-catalog-menu__right">
                        <img class="header-catalog-menu__arrow" src="{{ asset('/images/arrow_orange.svg') }}">
                    </div>
                </a>
                @endif
            @endforeach
            <a class="header-catalog-menu__link" href="{{route('specials')}}">
                <div class="header-catalog-menu__left">
                    <img class="header-catalog-menu__icon" src="{{ asset('/images/sale.svg') }}">
                    <span>{!! _t('Товары со скидками', 'common') !!}</span>
                </div>
                <div class="header-catalog-menu__right">
                    <img class="header-catalog-menu__arrow" src="{{ asset('/images/arrow_orange.svg') }}">
                </div>
            </a>
        </div>
        <div class="header-catalog-items">
            @foreach($Categories as $Category)
                @php
                    $ItemsHotArray = ItemHot::getBanners($Category->id);
                @endphp
                <div class="header-catalog-items__list @if($loop->first) header-catalog-items__list_active @endif @if(count($ItemsHotArray)) header-catalog-items__list_banner @endif">
                    @foreach($Category->subcategories as $subcategory)
                        <a class="header-catalog-items__item" href="{{ route('slug', ['slug' => $subcategory->slug()]) }}">
                            <div class="header-catalog-items__image">
                                @if($subcategory->isBreadcrumbsImageExists())
                                    <img src="{{ $subcategory->getBreadcrumbsImage() }}" height="100" alt="{{ $subcategory->getTitle() }}">
                                @elseif ($subcategory->isImageExists(Category::IMAGE_PATH_LIST))
                                    <img src="{{ $subcategory->getImageLink(Category::IMAGE_PATH_LIST) }}" height="100" alt="{{ $subcategory->getTitle() }}">
                                @elseif (!$subcategory->parent_id && $subcategory->subcategories[0]->isImageExists(Category::IMAGE_PATH_LIST))
                                    <img src="{{ $subcategory->subcategories[0]->getImageLink(Category::IMAGE_PATH_LIST) }}" height="100" alt="{{ $subcategory->getTitle() }}">
                                @endif
                            </div>
                            <span class="header-catalog-items__text">
                                {{ $subcategory->getTitle() }}
                                <div class="header-catalog-items__tooltip">
                                    <span class="nowrap">
                                        @component('components.icon', ['name' => 'checkcart', 'attributes' => ['class' => 'header-catalog-items__tooltip-icon']])@endcomponent
                                        {!! _t('в наличии', 'common') !!}
                                    </span>
                                    <br/>
                                    <span class="nowrap">
                                        {{ Formatter::quantity($subcategory->itemsCount()) }}
                                    </span>
                                </div>
                            </span>
                        </a>
                    @endforeach
                    @if(count($ItemsHotArray))
                        <div class="header-catalog-banner__list-wrapper swiper-container">
                            <div class="header-catalog-banner__list swiper-wrapper">
                                @foreach($ItemsHotArray as $ItemsHot)
                                    @if($ItemsHot[0]->size)
                                        @foreach($ItemsHot as $ItemHot)
                                            <div class="header-catalog-banner swiper-slide">
                                                <div class="header-catalog-banner__block">
                                                    <div class="header-catalog-banner__title">{{ $ItemHot->title }}</div>
                                                    <div class="header-catalog-banner__text">
                                                        @if(isset($ItemHot->text) && $ItemHot->text)
                                                            {!! nl2br($ItemHot->text) !!}
                                                        @endif
                                                    </div>
                                                    <a class="header-catalog-banner__link" href="{{ $ItemHot->getObjectUrl() }}">Подробнее</a>
                                                </div>
                                                <div class="header-catalog-banner__image">
                                                    <img src="{{ ($File = $ItemHot->file()) ? asset('/files/' . $File->src) : '' }}" alt="{{ $ItemHot->title }}">
                                                </div>
                                                <div class="header-catalog-banner__theses">
                                                    <div class="header-catalog-banner__thesis">
                                                        @isset($ItemHot->icon_id_1)
                                                            <div class="header-catalog-banner__thesis-image">
                                                                <img src="{{ asset('/images/hot-icons/' . $icons[$ItemHot->icon_id_1] . '.svg') }}">
                                                            </div>
                                                        @endisset
                                                        <div class="header-catalog-banner__thesis-text">
                                                            {{ $ItemHot->add_text_1 }}
                                                        </div>
                                                    </div>
                                                    <div class="header-catalog-banner__thesis">
                                                        @isset($ItemHot->icon_id_2)
                                                            <div class="header-catalog-banner__thesis-image">
                                                                <img src="{{ asset('/images/hot-icons/' . $icons[$ItemHot->icon_id_2] . '.svg') }}">
                                                            </div>
                                                        @endisset
                                                        <div class="header-catalog-banner__thesis-text">
                                                            {{ $ItemHot->add_text_2 }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @else
                                        @foreach($ItemsHot as $ItemHot)
                                            <div class="header-catalog-banner header-catalog-banner_small swiper-slide">
                                                <div class="header-catalog-banner__image">
                                                    <a href="{{ $ItemHot->getObjectUrl() }}">
                                                        <img src="{{ ($File = $ItemHot->file()) ? asset('/files/' . $File->src) : '' }}" alt="{{ $ItemHot->title }}">
                                                    </a>
                                                </div>
                                                <div class="header-catalog-banner__block">
                                                    <div class="header-catalog-banner__title">{{ $ItemHot->title }}</div>
                                                    <div class="header-catalog-banner__text">
                                                        @if(isset($ItemHot->text) && $ItemHot->text)
                                                            {!! nl2br($ItemHot->text) !!}
                                                        @endif
                                                    </div>
                                                    <a class="header-catalog-banner__link" href="{{ $ItemHot->getObjectUrl() }}">Подробнее</a>
                                                </div>
                                                @isset($ItemHot->icon_id_1)
                                                    <div class="header-catalog-banner__icon">
                                                        <img src="{{ asset('/images/hot-icons/' . $icons[$ItemHot->icon_id_1] . '.svg') }}">
                                                    </div>
                                                @endisset
                                            </div>
                                        @endforeach
                                    @endif
                                @endforeach
                            </div>
                            <div class="header-catalog-banner__next"></div>
                            <div class="header-catalog-banner__prev"></div>
                        </div>
                    @endif
                </div>
            @endforeach
        </div>
    </div>
</div>