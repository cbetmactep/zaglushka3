@php
    declare(strict_types=1);
@endphp

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    {!! Meta::tag('robots') !!}

    {!! Meta::tag('site_name', _t('Заглушка.Ру', 'common')) !!}
    {!! Meta::tag('locale', App::getLocale()) !!}

    {!! Meta::tag('image', asset('images/logo.png')) !!}

    {!! Meta::tag('title') !!}
    {!! Meta::tag('description') !!}

    <link rel="stylesheet" href="{{ mix('css/print.css') }}"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&amp;subset=cyrillic-ext,latin-ext" rel="stylesheet">
</head>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        window.print();
    });
</script>