@php
//    declare(strict_types=1); // todo
@endphp

<div class="page-actions">
    <a class="page-actions__item js-page-actions__item_print" target="_blank" href="{{ Request::fullUrlWithQuery(["print" => "1"]) }}">
        @component('components.icon', ['name' => 'print', 'attributes' => ['class' => 'page-actions__icon']])])@endcomponent
        <span class="page-actions__title">{{ _t('Распечатать', 'common') }}</span>
    </a>
    <a class="page-actions__item js-page-actions__item_excel" download href="{{ Request::fullUrlWithQuery(["excel" => "1"]) }}">
        @component('components.icon', ['name' => 'excel', 'attributes' => ['class' => 'page-actions__icon']])])@endcomponent
        <span class="page-actions__title">{{ _t('Скачать (excel)', 'common') }}</span>
    </a>
</div>
