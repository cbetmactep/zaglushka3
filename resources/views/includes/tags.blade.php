@php
    declare(strict_types=1);
@endphp

@if(isset($Tags) && count($Tags))
    <div class="tags__wrap">
        <div class="tags @isset($CurrentTag) tags_active @endisset" data-category-link="{{ route('slug', ['slug' => $Category->slug()]) }}">
            <div class="tags__row swiper-container">
                <div class="tags__list swiper-wrapper">
                    @foreach($Tags as $Tag)
                        @php
                            $href = route('tags', ['categorySlug' => $Category->slug(), 'tagSlug' => $Tag->slug()] + Request::except('page'))
                        @endphp
                        <div class="tags__item-wrap swiper-slide">
                            <a class="tags__item @if(in_array($Tag->id, $tagIds)) tags__item_active @endif @if(isset($CurrentTag) && $CurrentTag->id === $Tag->id) tags__item_first @endif " data-id="{{ $Tag->id }}" data-href="{{ $href }}" href="{{ $href }}">
                                <div class="tags__image">
                                    @if ($Tag->isBreadcrumbsImageExists($Category))
                                        <img src="{{ $Tag->getBreadcrumbsImage($Category) }}" height="70">
                                    @endif
                                </div>
                                <div class="tags__title">{{ $Tag->title }}</div>
                                <div class="tags__check">
                                    @component('components.icon', ['name' => 'checkcart'])@endcomponent
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
                <div class="tags__next"></div>
                <div class="tags__prev"></div>
            </div>
        </div>
    </div>
@endif