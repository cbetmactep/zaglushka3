@php
    declare(strict_types=1);
@endphp

@if(Config::get('app.subdomain') === 'ekb')
    @php $yaCounter = 49611556; @endphp
@elseif(Config::get('app.subdomain') === 'rostov-na-donu')
    @php $yaCounter = 49611607; @endphp
@elseif(Config::get('app.subdomain') === 'kazan')
    @php $yaCounter = 49611577; @endphp
@elseif(Config::get('app.subdomain') === 'omsk')
    @php $yaCounter = 49611604; @endphp
@elseif(Config::get('app.subdomain') === 'chelyabinsk')
    @php $yaCounter = 49611628; @endphp
@elseif(Config::get('app.subdomain') === 'irkutsk')
    @php $yaCounter = 49611913; @endphp
@elseif(Config::get('app.subdomain') === 'nnovgorod')
    @php $yaCounter = 49611598; @endphp
@elseif(Config::get('app.subdomain') === 'ulanude')
    @php $yaCounter = 49612411; @endphp
@elseif(Config::get('app.subdomain') === 'samara')
    @php $yaCounter = 49611619; @endphp
@elseif(Config::get('app.subdomain') === 'chita')
    @php $yaCounter = 49612342; @endphp
@elseif(Config::get('app.subdomain') === 'novosib')
    @php $yaCounter = 49611568; @endphp
@elseif(Config::get('app.subdomain') === 'krasnodar')
    @php $yaCounter = 49612030; @endphp
@elseif(Config::get('app.subdomain') === 'blago')
    @php $yaCounter = 49611733; @endphp
@elseif(Config::get('app.subdomain') === 'voronezh')
    @php $yaCounter = 49611565; @endphp
@elseif(Config::get('app.subdomain') === 'habar')
    @php $yaCounter = 49612369; @endphp
@elseif(Config::get('app.subdomain') === 'perm')
    @php $yaCounter = 49611529; @endphp
@elseif(Config::get('app.subdomain') === 'vologda')
    @php $yaCounter = 49611802; @endphp
@elseif(Config::get('app.subdomain') === 'krasnoyarsk')
    @php $yaCounter = 49611583; @endphp
@elseif(Config::get('app.subdomain') === 'vladiv')
    @php $yaCounter = 49611772; @endphp
@elseif(Config::get('app.subdomain') === 'tver')
    @php $yaCounter = 49612465; @endphp
@elseif(Config::get('app.subdomain') === 'murm')
    @php $yaCounter = 49612111; @endphp
@elseif(Config::get('app.subdomain') === 'tula')
    @php $yaCounter = 49612444; @endphp
@elseif(Config::get('app.subdomain') === 'barnaul')
    @php $yaCounter = 49611706; @endphp
@elseif(Config::get('app.subdomain') === 'saratov')
    @php $yaCounter = 49612306; @endphp
@elseif(Config::get('app.subdomain') === 'ulyan')
    @php $yaCounter = 49612396; @endphp
@elseif(Config::get('app.subdomain') === 'kemerovo')
    @php $yaCounter = 49611964; @endphp
@elseif(Config::get('app.subdomain') === 'tomsk')
    @php $yaCounter = 49612450; @endphp
@elseif(Config::get('app.subdomain') === 'novokuz')
    @php $yaCounter = 49612171; @endphp
@elseif(Config::get('app.subdomain') === 'volgograd')
    @php $yaCounter = 49611574; @endphp
@elseif(Config::get('app.subdomain') === 'arch')
    @php $yaCounter = 49611685; @endphp
@elseif(Config::get('app.subdomain') === 'astr')
    @php $yaCounter = 49611688; @endphp
@elseif(Config::get('app.subdomain') === 'kirov')
    @php $yaCounter = 49611973; @endphp
@elseif(Config::get('app.subdomain') === 'oren')
    @php $yaCounter = 49612219; @endphp
@elseif(Config::get('app.subdomain') === 'syktyv')
    @php $yaCounter = 49612474; @endphp
@elseif(Config::get('app.subdomain') === 'ufa')
    @php $yaCounter = 49611625; @endphp
@elseif(Config::get('app.subdomain') === 'izhevsk')
    @php $yaCounter = 49611910; @endphp
@elseif(Config::get('app.subdomain') === 'tumen')
    @php $yaCounter = 49612417; @endphp
@elseif(Config::get('app.subdomain') === 'smol')
    @php $yaCounter = 49612522; @endphp
@elseif(Config::get('app.subdomain') === 'nabchelny')
    @php $yaCounter = 49612120; @endphp
@elseif(Config::get('app.subdomain') === 'yaroslavl')
    @php $yaCounter = 49612306; @endphp
@elseif(Config::get('app.subdomain') === 'lipeck')
    @php $yaCounter = 49612045; @endphp
@elseif(Config::get('app.subdomain') === 'penza')
    @php $yaCounter = 49612237; @endphp
@elseif(Config::get('app.subdomain') === 'ryaz')
    @php $yaCounter = 49612279; @endphp
@elseif(Config::get('app.subdomain') === 'petrozav')
    @php $yaCounter = 49612243; @endphp
@elseif(Config::get('app.subdomain') === 'orel')
    @php $yaCounter = 49612216; @endphp
@elseif(Config::get('app.subdomain') === 'saransk')
    @php $yaCounter = 49612297; @endphp
@elseif(Config::get('app.subdomain') === 'belgorod')
    @php $yaCounter = 49611709; @endphp
@elseif(Config::get('app.subdomain') === 'kursk')
    @php $yaCounter = 49612039; @endphp
@elseif(Config::get('app.subdomain') === 'nizhkam')
    @php $yaCounter = 49612165; @endphp
@elseif(Config::get('app.subdomain') === 'velnov')
    @php $yaCounter = 49611760; @endphp
@elseif(Config::get('app.subdomain') === 'kalinin')
    @php $yaCounter = 49611940; @endphp
@elseif(Config::get('app.subdomain') === 'cherep')
    @php $yaCounter = 49612354; @endphp
@elseif(Config::get('app.subdomain') === 'bryansk')
    @php $yaCounter = 49611751; @endphp
@elseif(Config::get('app.subdomain') === 'kostroma')
    @php $yaCounter = 49612021; @endphp
@elseif(Config::get('app.subdomain') === 'sochi')
    @php $yaCounter = 49612510; @endphp
@elseif(Config::get('app.subdomain') === 'magnit')
    @php $yaCounter = 49612060; @endphp
@elseif(Config::get('app.subdomain') === 'novoros')
    @php $yaCounter = 49612180; @endphp
@elseif(Config::get('app.subdomain') === 'stavr')
    @php $yaCounter = 49612504; @endphp
@elseif(Config::get('app.subdomain') === 'chebor')
    @php $yaCounter = 49612360; @endphp
@elseif(Config::get('app.subdomain') === 'neftekamsk')
    @php $yaCounter = 49612150; @endphp
@elseif(Config::get('app.subdomain') === 'tolyatti')
    @php $yaCounter = 49612453; @endphp
@elseif(Config::get('app.subdomain') === 'arm')
    @php $yaCounter = 49611682; @endphp
@elseif(Config::get('app.subdomain') === 'nizhtag')
    @php $yaCounter = 49612168; @endphp
@elseif(Config::get('app.subdomain') === 'kolomna')
    @php $yaCounter = 49611982; @endphp
@elseif(Config::get('app.subdomain') === 'podolsk')
    @php $yaCounter = 49612252; @endphp
@elseif(Config::get('app.subdomain') === 'obninsk')
    @php $yaCounter = 49612204; @endphp
@elseif(Config::get('app.subdomain') === 'serp')
    @php $yaCounter = 49612525; @endphp
@elseif(Config::get('app.subdomain') === 'zheldor')
    @php $yaCounter = 49611886; @endphp
@elseif(Config::get('app.subdomain') === 'balash')
    @php $yaCounter = 49611700; @endphp
@elseif(Config::get('app.subdomain') === 'bugul')
    @php $yaCounter = 49611754; @endphp
@elseif(Config::get('app.subdomain') === 'velluki')
    @php $yaCounter = 49611757; @endphp
@elseif(Config::get('app.subdomain') === 'vladik')
    @php $yaCounter = 49611778; @endphp
@elseif(Config::get('app.subdomain') === 'vladimir')
    @php $yaCounter = 49611784; @endphp
@elseif(Config::get('app.subdomain') === 'dmitrov')
    @php $yaCounter = 49611868; @endphp
@elseif(Config::get('app.subdomain') === 'ivanovo')
    @php $yaCounter = 49611901; @endphp
@elseif(Config::get('app.subdomain') === 'kaluga')
    @php $yaCounter = 49611943; @endphp
@elseif(Config::get('app.subdomain') === 'krasnogorsk')
    @php $yaCounter = 49612027; @endphp
@elseif(Config::get('app.subdomain') === 'pskov')
    @php $yaCounter = 49612258; @endphp
@elseif(Config::get('app.subdomain') === 'pyatigorsk')
    @php $yaCounter = 49612264; @endphp
@elseif(Config::get('app.subdomain') === 'tagan')
    @php $yaCounter = 49612471; @endphp
@elseif(Config::get('app.subdomain') === 'tambov')
    @php $yaCounter = 49612468; @endphp
@elseif(Config::get('app.subdomain') === 'uhta')
    @php $yaCounter = 49612381; @endphp
@elseif(Config::get('app.subdomain') === 'abakan')
    @php $yaCounter = 49611631; @endphp
@elseif(Config::get('app.subdomain') === 'alm')
    @php $yaCounter = 49611634; @endphp
@elseif(Config::get('app.subdomain') === 'anadyr')
    @php $yaCounter = 49611637; @endphp
@elseif(Config::get('app.subdomain') === 'anapa')
    @php $yaCounter = 49611643; @endphp
@elseif(Config::get('app.subdomain') === 'angarsk')
    @php $yaCounter = 49611649; @endphp
@elseif(Config::get('app.subdomain') === 'apat')
    @php $yaCounter = 49611676; @endphp
@elseif(Config::get('app.subdomain') === 'apsh')
    @php $yaCounter = 49611679; @endphp
@elseif(Config::get('app.subdomain') === 'ach')
    @php $yaCounter = 49611691; @endphp
@elseif(Config::get('app.subdomain') === 'balakovo')
    @php $yaCounter = 49611697; @endphp
@elseif(Config::get('app.subdomain') === 'belogorsk')
    @php $yaCounter = 49611712; @endphp
@elseif(Config::get('app.subdomain') === 'berez')
    @php $yaCounter = 49611715; @endphp
@elseif(Config::get('app.subdomain') === 'biysk')
    @php $yaCounter = 49611718; @endphp
@elseif(Config::get('app.subdomain') === 'birob')
    @php $yaCounter = 49611730; @endphp
@elseif(Config::get('app.subdomain') === 'borov')
    @php $yaCounter = 49611736; @endphp
@elseif(Config::get('app.subdomain') === 'bratsk')
    @php $yaCounter = 49611742; @endphp
@elseif(Config::get('app.subdomain') === 'vichuga')
    @php $yaCounter = 49611763; @endphp
@elseif(Config::get('app.subdomain') === 'volgodonsk')
    @php $yaCounter = 49611793; @endphp
@elseif(Config::get('app.subdomain') === 'bolgh')
    @php $yaCounter = 49611799; @endphp
@elseif(Config::get('app.subdomain') === 'volhov')
    @php $yaCounter = 49611808; @endphp
@elseif(Config::get('app.subdomain') === 'vorkuta')
    @php $yaCounter = 49611814; @endphp
@elseif(Config::get('app.subdomain') === 'voskr')
    @php $yaCounter = 49611817; @endphp
@elseif(Config::get('app.subdomain') === 'vsevol')
    @php $yaCounter = 49611826; @endphp
@elseif(Config::get('app.subdomain') === 'vyborg')
    @php $yaCounter = 49611835; @endphp
@elseif(Config::get('app.subdomain') === 'vyksa')
    @php $yaCounter = 49611838; @endphp
@elseif(Config::get('app.subdomain') === 'gatch')
    @php $yaCounter = 49611841; @endphp
@elseif(Config::get('app.subdomain') === 'glazov')
    @php $yaCounter = 49611844; @endphp
@elseif(Config::get('app.subdomain') === 'goralt')
    @php $yaCounter = 49611850; @endphp
@elseif(Config::get('app.subdomain') === 'grozn')
    @php $yaCounter = 49611853; @endphp
@elseif(Config::get('app.subdomain') === 'dzergh')
    @php $yaCounter = 49611856; @endphp
@elseif(Config::get('app.subdomain') === 'dimgrad')
    @php $yaCounter = 49611865; @endphp
@elseif(Config::get('app.subdomain') === 'dolgopr')
    @php $yaCounter = 49611871; @endphp
@elseif(Config::get('app.subdomain') === 'dubna')
    @php $yaCounter = 49611874; @endphp
@elseif(Config::get('app.subdomain') === 'eysk')
    @php $yaCounter = 49611877; @endphp
@elseif(Config::get('app.subdomain') === 'essent')
    @php $yaCounter = 49611880; @endphp
@elseif(Config::get('app.subdomain') === 'zhelgor')
    @php $yaCounter = 49611883; @endphp
@elseif(Config::get('app.subdomain') === 'zhig')
    @php $yaCounter = 49611889; @endphp
@elseif(Config::get('app.subdomain') === 'zhuk')
    @php $yaCounter = 49611895; @endphp
@elseif(Config::get('app.subdomain') === 'ivanteevka')
    @php $yaCounter = 49611904; @endphp
@elseif(Config::get('app.subdomain') === 'ishim')
    @php $yaCounter = 49611916; @endphp
@elseif(Config::get('app.subdomain') === 'yoshola')
    @php $yaCounter = 49611919; @endphp
@elseif(Config::get('app.subdomain') === 'kamural')
    @php $yaCounter = 49611949; @endphp
@elseif(Config::get('app.subdomain') === 'kamshaht')
    @php $yaCounter = 49611952; @endphp
@elseif(Config::get('app.subdomain') === 'kamyshin')
    @php $yaCounter = 49611961; @endphp
@elseif(Config::get('app.subdomain') === 'kingi')
    @php $yaCounter = 49611967; @endphp
@elseif(Config::get('app.subdomain') === 'kirishi')
    @php $yaCounter = 49611970; @endphp
@elseif(Config::get('app.subdomain') === 'kisl')
    @php $yaCounter = 49611976; @endphp
@elseif(Config::get('app.subdomain') === 'kovrov')
    @php $yaCounter = 49611979; @endphp
@elseif(Config::get('app.subdomain') === 'komsamur')
    @php $yaCounter = 49612009; @endphp
@elseif(Config::get('app.subdomain') === 'kondopoga')
    @php $yaCounter = 49612015; @endphp
@elseif(Config::get('app.subdomain') === 'korolev')
    @php $yaCounter = 49612018; @endphp
@elseif(Config::get('app.subdomain') === 'kotlas')
    @php $yaCounter = 49612024; @endphp
@elseif(Config::get('app.subdomain') === 'kstovo')
    @php $yaCounter = 49612033; @endphp
@elseif(Config::get('app.subdomain') === 'kurgan')
    @php $yaCounter = 49612036; @endphp
@elseif(Config::get('app.subdomain') === 'kyzyl')
    @php $yaCounter = 49612042; @endphp
@elseif(Config::get('app.subdomain') === 'magad')
    @php $yaCounter = 49612051; @endphp
@elseif(Config::get('app.subdomain') === 'maykop')
    @php $yaCounter = 49612069; @endphp
@elseif(Config::get('app.subdomain') === 'mahach')
    @php $yaCounter = 49612081; @endphp
@elseif(Config::get('app.subdomain') === 'mezhdu')
    @php $yaCounter = 49612084; @endphp
@elseif(Config::get('app.subdomain') === 'miass')
    @php $yaCounter = 49612093; @endphp
@elseif(Config::get('app.subdomain') === 'minvody')
    @php $yaCounter = 49612096; @endphp
@elseif(Config::get('app.subdomain') === 'minus')
    @php $yaCounter = 49612099; @endphp
@elseif(Config::get('app.subdomain') === 'mozh')
    @php $yaCounter = 49612108; @endphp
@elseif(Config::get('app.subdomain') === 'murom')
    @php $yaCounter = 49612117; @endphp
@elseif(Config::get('app.subdomain') === 'naz')
    @php $yaCounter = 49612135; @endphp
@elseif(Config::get('app.subdomain') === 'nal')
    @php $yaCounter = 49612141; @endphp
@elseif(Config::get('app.subdomain') === 'nah')
    @php $yaCounter = 49612144; @endphp
@elseif(Config::get('app.subdomain') === 'nevin')
    @php $yaCounter = 49612147; @endphp
@elseif(Config::get('app.subdomain') === 'nefteug')
    @php $yaCounter = 49612153; @endphp
@elseif(Config::get('app.subdomain') === 'nizhvar')
    @php $yaCounter = 49612159; @endphp
@elseif(Config::get('app.subdomain') === 'novomosk')
    @php $yaCounter = 49612174; @endphp
@elseif(Config::get('app.subdomain') === 'novocheb')
    @php $yaCounter = 49612186; @endphp
@elseif(Config::get('app.subdomain') === 'novocher')
    @php $yaCounter = 49612189; @endphp
@elseif(Config::get('app.subdomain') === 'novuren')
    @php $yaCounter = 49612192; @endphp
@elseif(Config::get('app.subdomain') === 'norilsk')
    @php $yaCounter = 49612195; @endphp
@elseif(Config::get('app.subdomain') === 'noyab')
    @php $yaCounter = 49612201; @endphp
@elseif(Config::get('app.subdomain') === 'ozersk')
    @php $yaCounter = 49612207; @endphp
@elseif(Config::get('app.subdomain') === 'oktyabr')
    @php $yaCounter = 49612213; @endphp
@elseif(Config::get('app.subdomain') === 'orzu')
    @php $yaCounter = 49612225; @endphp
@elseif(Config::get('app.subdomain') === 'orsk')
    @php $yaCounter = 49612228; @endphp
@elseif(Config::get('app.subdomain') === 'petrkam')
    @php $yaCounter = 49612249; @endphp
@elseif(Config::get('app.subdomain') === 'puschino')
    @php $yaCounter = 49612261; @endphp
@elseif(Config::get('app.subdomain') === 'ramen')
    @php $yaCounter = 49612270; @endphp
@elseif(Config::get('app.subdomain') === 'ryb')
    @php $yaCounter = 49612273; @endphp
@elseif(Config::get('app.subdomain') === 'salavat')
    @php $yaCounter = 49612288; @endphp
@elseif(Config::get('app.subdomain') === 'salehard')
    @php $yaCounter = 49612291; @endphp
@elseif(Config::get('app.subdomain') === 'sarapul')
    @php $yaCounter = 49612303; @endphp
@elseif(Config::get('app.subdomain') === 'sarov')
    @php $yaCounter = 49612549; @endphp
@elseif(Config::get('app.subdomain') === 'satka')
    @php $yaCounter = 49612546; @endphp
@elseif(Config::get('app.subdomain') === 'sevdvinsk')
    @php $yaCounter = 49612543; @endphp
@elseif(Config::get('app.subdomain') === 'seversk')
    @php $yaCounter = 49612537; @endphp
@elseif(Config::get('app.subdomain') === 'sergposad')
    @php $yaCounter = 49612534; @endphp
@elseif(Config::get('app.subdomain') === 'sortavala')
    @php $yaCounter = 49612522; @endphp
@elseif(Config::get('app.subdomain') === 'sosnbor')
    @php $yaCounter = 49612516; @endphp
@elseif(Config::get('app.subdomain') === 'stoskol')
    @php $yaCounter = 49612501; @endphp
@elseif(Config::get('app.subdomain') === 'sterl')
    @php $yaCounter = 49612495; @endphp
@elseif(Config::get('app.subdomain') === 'surgut')
    @php $yaCounter = 49612489; @endphp
@elseif(Config::get('app.subdomain') === 'syzran')
    @php $yaCounter = 49612477; @endphp
@elseif(Config::get('app.subdomain') === 'tihvin')
    @php $yaCounter = 49612462; @endphp
@elseif(Config::get('app.subdomain') === 'tobol')
    @php $yaCounter = 49612456; @endphp
@elseif(Config::get('app.subdomain') === 'tuapse')
    @php $yaCounter = 49612447; @endphp
@elseif(Config::get('app.subdomain') === 'tutaev')
    @php $yaCounter = 49612429; @endphp
@elseif(Config::get('app.subdomain') === 'tynda')
    @php $yaCounter = 49612429; @endphp
@elseif(Config::get('app.subdomain') === 'uray')
    @php $yaCounter = 49612393; @endphp
@elseif(Config::get('app.subdomain') === 'usinsk')
    @php $yaCounter = 49612390; @endphp
@elseif(Config::get('app.subdomain') === 'ussur')
    @php $yaCounter = 49612387; @endphp
@elseif(Config::get('app.subdomain') === 'ustilimsk')
    @php $yaCounter = 49612384; @endphp
@elseif(Config::get('app.subdomain') === 'fryaz')
    @php $yaCounter = 49612372; @endphp
@elseif(Config::get('app.subdomain') === 'hanty')
    @php $yaCounter = 49612366; @endphp
@elseif(Config::get('app.subdomain') === 'cherkessk')
    @php $yaCounter = 49612348; @endphp
@elseif(Config::get('app.subdomain') === 'chernogol')
    @php $yaCounter = 49612345; @endphp
@elseif(Config::get('app.subdomain') === 'shahty')
    @php $yaCounter = 49612336; @endphp
@elseif(Config::get('app.subdomain') === 'schelkobo')
    @php $yaCounter = 49612327; @endphp
@elseif(Config::get('app.subdomain') === 'elista')
    @php $yaCounter = 49612324; @endphp
@elseif(Config::get('app.subdomain') === 'engels')
    @php $yaCounter = 49612318; @endphp
@elseif(Config::get('app.subdomain') === 'uzhsah')
    @php $yaCounter = 49612315; @endphp
@elseif(Config::get('app.subdomain') === 'yakutsk')
    @php $yaCounter = 49612312; @endphp
@elseif(Config::get('app.subdomain') === 'mogilev')
    @php $yaCounter = 49612105; @endphp
@else
    @php $yaCounter = 10727185; @endphp
@endif

<script>
    if (!window.App.CONST) {
        window.App.CONST = {}
    }
    window.App.CONST.YA_COUNTER = {{ $yaCounter }}
</script>


@if(App::isLocale('ru'))
    <!-- RedHelper -->
    <script id="rhlpscrtg" type="text/javascript" charset="utf-8" async="async" src="https://web.redhelper.ru/service/main.js?c=zaglushkaru"></script>
    <!--/Redhelper -->
@endif
