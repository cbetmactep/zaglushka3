@php
    declare(strict_types=1);

    use App\Models\Redis\Cart as RedisCart;

    // todo дублируется запрос
    $inCart = RedisCart::count();

    $hints = [
        'пластиковые горки',
        'резьбовые опоры',
        'хромированные заглушки',
        'фиксаторы М8',
        'качели',
        'латодержатели',
        'переходники для труб',
        'квадратные заглушки 20х20',
        'заглушки для отверстий',
    ];
@endphp

<nav class="nav">
    <div class="nav__container">
        <div class="nav__row">
            <div class="nav__left">
                <div class="nav__item js-nav-link-catalog">
                    <a class="nav__link" href="{{ route('home') }}">
                        <span>
                            {!! _t('Продукция', 'common') !!}
                        </span>
                        @if(!Route::is('catalog'))
                            @component('components.icon', ['name' => 'arrow-menu', 'attributes' => ['class' => 'nav__link-arrow']])@endcomponent
                        @endif
                    </a>
                    @if(!Route::is('catalog'))
                        @component('includes.header-catalog')@endcomponent
                    @endif
                </div>
                <div class="nav__item">
                    <a class="nav__link" href="{{ route('aboutnew') }}">{!! _t('О компании', 'common') !!}</a>
                </div>
                <div class="nav__item">
                    <a class="nav__link" href="{{ route('delivery') }}">{!! _t('Доставка', 'common') !!}</a>
                </div>
                <div class="nav__item">
                    <a class="nav__link" href="{{ route('contacts') }}">{!! _t('Контакты', 'common') !!}</a>
                </div>
                <div class="nav__item">
                    <a class="nav__link" href="{{ route('pay') }}">{!! _t('Оплата', 'common') !!}</a>
                </div>
            </div>
            <div class="nav__mob-menu">
                @component('components.icon', ['name' => 'hamburger-menu'])@endcomponent
            </div>
            <a class="nav__mob-phone" href="tel:+78005550499">8 (800) 555 04 99</a>
            <div class="nav__right">
                <div class="nav__item">
                    <a class="nav__link" href="{{ route('search') }}" href="{{ route('search') }}">
                        @component('components.icon', ['name' => 'search', 'attributes' => ['class' => 'nav__link-icon nav__link-icon_search']])@endcomponent
                        <span class="nav__link-text">
                            {!! _t('Поиск', 'common') !!}
                        </span>
                    </a>
                </div>
                <div class="nav__item">
                    <a class="nav__link" href="{{ route('cart') }}">
                        @component('components.icon', ['name' => 'basket', 'attributes' => ['class' => 'nav__link-icon nav__link-icon_basket']])@endcomponent
                        <span class="nav__link-text js-nav-cart-title" @if($inCart) style="display: none;" @endif>
                            {!! _t('Корзина', 'common') !!}
                        </span>
                        <span class="nav__tooltip js-nav-cart-count-block" @if(!$inCart) style="display: none;" @endif>
                            <span class="js-nav-cart-count">{{ Formatter::quantity($inCart, false) }}</span> <span>{!! _t('шт.', 'common') !!}</span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</nav>
