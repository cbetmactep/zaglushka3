@php
    declare(strict_types=1);
@endphp

@if($CurrentCity && $CurrentCity->free_delivery_price)
    <div class="delivery-info">
        <div class="delivery-info__container">
            <div>
                <span class="delivery-info__text">{!! _t('Бесплатная доставка "до дверей" в городе %s при заказе от %s рублей.', 'common', [$CurrentCity->title, '<span class="nowrap">' . number_format($CurrentCity->free_delivery_price, 0, ' ', ' ') . '</span>']) !!}</span>
                <a class="delivery-info__link" href="{{ route('delivery') }}">{!! _t('Подробнее', 'common') !!}</a>
                @component('components.icon', ['name' => 'close-thin', 'attributes' => ['class' => 'js-delivery-info-close delivery-info__close']])@endcomponent
            </div>
        </div>
    </div>

    <script>
        // Отображение баннера о доставке при смени города

        var $el = document.querySelector('.js-header-city'),
          cityId = $el.dataset.id,
          $banner = document.querySelector('.delivery-info'),
          $btnClose = document.querySelector('.js-delivery-info-close');

        if(localStorage.city !== cityId && cityId !== '0') {
            localStorage.city = cityId;
            $banner.classList.add('delivery-info_active');
        }

        $btnClose.addEventListener('click', function () {
            $banner.remove();
        })
    </script>
@endif