@php
    declare(strict_types=1);
@endphp

@if(!Config::get('app.subdomain'))
    <div class="header-flags{{ isset($class) ? ' ' . $class : '' }}">
        <div class="header-flags__current">
            @component('components.icon', ['name' => 'flag_' . App::getLocale(), 'attributes' => ['class' => 'header-flags__icon header-flags__icon_current']])@endcomponent
        </div>
        @component('components.icon', ['name' => 'arrow-menu', 'attributes' => ['class' => 'header-flags__arrow']])@endcomponent
        <ul class="header-flags__list">
            @foreach(Config::get('app.locales') as $locale)
                @if(!App::isLocale($locale))
                    <li class="header-flags__item">
                        <a class="header-flags__link" href="{{ URL::to((('ru' == $locale) ? '' : $locale) . $changeLanguageUrl) }}">
                            @component('components.icon', ['name' => 'flag_' . $locale, 'attributes' => ['class' => 'header-flags__icon']])@endcomponent
                            <span class="header-flags__text">{{ Config::get('app.locale_names')[$locale] }}</span>
                        </a>
                    </li>
                @endif
            @endforeach
        </ul>
    </div>
@endif
