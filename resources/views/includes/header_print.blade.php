@php
    declare(strict_types=1);

    use App\Models\Manager;

    $Managers = Manager::where(['enabled' => 1])->get();
@endphp

<table class="header">
    <tr class="header__row">
        <td class="header__logo"><img src="{{ Request::get('excel') ? (public_path() . '/images/logo_print.png') : asset('images/logo_print.png') }}"></td>
        <td class="header__address">
            <strong>{!! _t('ОТДЕЛ ПРОДАЖ', 'common') !!}</strong>
            <br>
            <strong>{!! _t('тел.', 'common') !!} 7 (812) 242-80-99</strong>
            <br>
            <strong>{!! _t('тел.', 'common') !!} 7 (800) 555-04-99</strong>
            <br>
            <br>
            <p>
                <strong>{!! _t('АДРЕС СКЛАДА:', 'common') !!}</strong>
                <br>
                {!! _t('г. Санкт-Петербург, п. Парголово ул. Подгорная, дом 6. Въезд с ул. Железнодорожная, дом 11 На территории АНГАР № 12 График работы: Пн-Пт, 9:00 - 18:00', 'common') !!}
            </p>
        </td>
        <td class="header__contacts">
{{--
            <p>
                <strong>{!! _t('МЕНЕДЖЕРЫ ПО РАБОТЕ С КЛИЕНТАМИ:', 'common') !!}</strong>
            </p>
            <table>
                <tbody>
                    @foreach($Managers as $Manager)
                        <tr>
                            <td>{{ $Manager->title }}</td>
                            <td>{!! _t('доб.', 'common') !!} {{ $Manager->phone }}</td>
                        </tr>
                        <tr>
                            <td colspan="2">{{ $Manager->email }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
--}}
        </td>
    </tr>
</table>

