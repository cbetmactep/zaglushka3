@php
	declare(strict_types=1);

	Meta::set('title', _t('Специальные предложения', 'pages'));
@endphp

@extends('layouts.master')

@section('content')
	{!! Breadcrumbs::render('specials') !!}
	<div class="container">
		<div class="page-header">
			<div class="page-header__title-wrap">
				<h1	class="page__title">{{ _t('Специальные предложения', 'pages') }}</h1>
			</div>
			<div class="page-header__actions">
				@component('includes.page_actions')@endcomponent
			</div>
		</div>
		<div class="page">
			<div class="page__content">
				@component('item.list', ['Items' => $Items, 'firstItem' => $Items->firstItem()])@endcomponent
			</div>
		</div>

		{{ $Items->appends(Request::except('page'))->links('components.pagination') }}
	</div>
@endsection