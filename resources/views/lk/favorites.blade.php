@php
    declare(strict_types=1);

    Meta::set('title', _t('Избранное', 'lk'));
@endphp

@extends('layouts.master')

@section('content')
    @component('lk.layout')
        <div class="lk__header">
            <div class="lk__title">{{ _t('Избранное', 'lk') }}</div>
            <div class="lk__header-right">
                <select class="lk-favorites__select">
                    <option>Все категории товаров</option>
                    <option>Не все категории товаров</option>
                </select>
                <div class="lk__header-input">
                    <div class="lk-input">
                        <input class="lk-input__input" placeholder="Поиск по артикулу">
                        <button class="lk-input__button">Найти</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="lk-favorites__block">
            <div class="lk-favorites__block-title">Заглушки для труб (15)</div>
            <div class="lk-favorites__list">
                @foreach(range(1, 6) as $i)
                    <div class="lk-favorites-item">
                        <div class="lk-favorites-item__images">
                            <img class="lk-favorites-item__image" src="/images/item/57/3457/5.jpg" width="80" height="80">
                            <img class="lk-favorites-item__image" src="/images/item/57/3457/6.jpg" width="80" height="80">
                        </div>
                        <div class="lk-favorites-item__article">Артикул: <a class="lk-favorites-item__link" href="#">15-30ОВЧК</a></div>
                        <div class="lk-favorites-item__color">
                            Цвет
                            <select class="lk-favorites-item__select">
                                <option>Черный</option>
                                <option>Не черный</option>
                            </select>
                        </div>
                        <div>В наличии 650 000 шт.</div>
                    </div>
                @endforeach
            </div>
            <div class="lk-favorites__footer">
                <div class="lk-favorites__footer-item">Показать еще 6 товаров @component('components.icon', ['name' => 'arrow-thin', 'attributes' => ['class' => 'lk-favorites__footer-icon']])])@endcomponent</div>
                <div class="lk-favorites__footer-item lk-favorites__footer-item_right">Показать все @component('components.icon', ['name' => 'arrow-thin', 'attributes' => ['class' => 'lk-favorites__footer-icon']])])@endcomponent</div>
            </div>
        </div>
    @endcomponent
@endsection
