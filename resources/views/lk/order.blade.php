@php
    declare(strict_types=1);

    Meta::set('title', _t('Заказ №%d', 'lk', [12345678]));
@endphp

@extends('layouts.master')

@section('content')
    @component('lk.layout')
        <div class="lk-order__header">
            <div class="lk__title">{{ _t('Заказ №%d', 'lk', [12345678]) }}</div>
            <div class="lk-actions">
                <a class="lk-actions__item" target="_blank" href="#">
                    @component('components.icon', ['name' => 'print', 'attributes' => ['class' => 'lk-actions__icon']])])@endcomponent
                    <span class="lk-actions__title">{{ _t('Распечатать', 'common') }}</span>
                </a>
                <a class="lk-actions__item" download href="#">
                    @component('components.icon', ['name' => 'excel', 'attributes' => ['class' => 'lk-actions__icon']])])@endcomponent
                    <span class="lk-actions__title">{{ _t('Скачать (excel)', 'common') }}</span>
                </a>
            </div>
        </div>
        <div class="lk-order-status">
            <div class="lk-order-status-tooltips">
                <div class="lk-order-status-tooltips__item">
                    <div class="lk-order-status-tooltips__title">В обработке</div>
                    <div class="lk-order-status-tooltips__date">15.02.2018</div>
                </div>
                <div class="lk-order-status-tooltips__item">
                    <div class="lk-order-status-tooltips__title">Собирается</div>
                    <div class="lk-order-status-tooltips__date">15.02.2018</div>
                </div>
                <div class="lk-order-status-tooltips__item">
                    <div class="lk-order-status-tooltips__title">Собран</div>
                    <div class="lk-order-status-tooltips__date">15.02.2018</div>
                </div>
                <div class="lk-order-status-tooltips__item">
                    <div class="lk-order-status-tooltips__title">В пути</div>
                    <div class="lk-order-status-tooltips__date">15.02.2018</div>
                </div>
                <div class="lk-order-status-tooltips__item">
                    <div class="lk-order-status-tooltips__title">Готов к выдаче</div>
                    <div class="lk-order-status-tooltips__date">15.02.2018</div>
                </div>
                <div class="lk-order-status-tooltips__item lk-order-status-tooltips__item_disabled">
                    <div class="lk-order-status-tooltips__title">Выполнен</div>
                </div>
            </div>
            <div class="lk-order-status-points">
                <div class="lk-order-status-points__item lk-order-status-points__item_in-processing">
                    @component('components.icon', ['name' => 'check', 'attributes' => ['class' => 'lk-order-status-points__icon']])@endcomponent
                </div>
                <div class="lk-order-status-points__item lk-order-status-points__item_going">
                    @component('components.icon', ['name' => 'check', 'attributes' => ['class' => 'lk-order-status-points__icon']])@endcomponent
                </div>
                <div class="lk-order-status-points__item lk-order-status-points__item_assembled">
                    @component('components.icon', ['name' => 'check', 'attributes' => ['class' => 'lk-order-status-points__icon']])@endcomponent
                    <div class="lk-order-status-points__date">Срок доставки ~ 4–6 дней</div>
                </div>
                <div class="lk-order-status-points__item lk-order-status-points__item_in-transit">
                    @component('components.icon', ['name' => 'check', 'attributes' => ['class' => 'lk-order-status-points__icon']])@endcomponent
                </div>
                <div class="lk-order-status-points__item lk-order-status-points__item_ready">
                    @component('components.icon', ['name' => 'check', 'attributes' => ['class' => 'lk-order-status-points__icon']])@endcomponent
                </div>
                <div class="lk-order-status-points__item lk-order-status-points__item_completed lk-order-status-points__item_disabled">
                    @component('components.icon', ['name' => 'check', 'attributes' => ['class' => 'lk-order-status-points__icon']])@endcomponent
                </div>
            </div>
        </div>
        <div class="lk-order-text">
            <p class="lk-order-text__p">
                Дата оформления: 15.02.2018
                <br/>
                Стоимость заказа: 45 000 р. (оплачен)
            </p>
            <p class="lk-order-text__p">
                <span class="lk-order-text__title">Доставка:</span>
                <br/>
                Адрес: г. Санкт-Петербург, п. Парголово, ул. Подгорная, д. 6
                <br/>
                Стоимость: бесплатно
            </p>
        </div>
        <div class="lk-order-table">
            <table class="lk-order-table__table">
                <thead>
                <tr class="lk-order-table__thead">
                    <th class="lk-order-table__th">№</th>
                    <th class="lk-order-table__th">Фото</th>
                    <th class="lk-order-table__th">Артикул</th>
                    <th class="lk-order-table__th">Описание</th>
                    <th class="lk-order-table__th">Цвет</th>
                    <th class="lk-order-table__th">Кол-во,<br/>штук</th>
                    <th class="lk-order-table__th">Цена<br/>за штуку</th>
                    <th class="lk-order-table__th">Сумма</th>
                </tr>
                </thead>
                <tbody>
                <tr class="lk-order-table__tr">
                    <td class="lk-order-table__td">1</td>
                    <td class="lk-order-table__td">
                        <img src="/images/item/55/255/1.jpg" height="55" width="55">
                    </td>
                    <td class="lk-order-table__td">
                        <a href="#" class="lk-order-table__link">15-30ОВЧК</a>
                    </td>
                    <td class="lk-order-table__td lk-order-table__description">Заглушка пластиковая внутренняя для труб овального сечения с внешними габаритами сечения 15x30 мм</td>
                    <td class="lk-order-table__td">Белый</td>
                    <td class="lk-order-table__td">2 500</td>
                    <td class="lk-order-table__td">3,5 р.</td>
                    <td class="lk-order-table__td">8 750 р.</td>
                </tr>
                <tr class="lk-order-table__tr">
                    <td class="lk-order-table__td">1</td>
                    <td class="lk-order-table__td">
                        <img src="/images/item/55/255/1.jpg" height="55" width="55">
                    </td>
                    <td class="lk-order-table__td">
                        <a href="#" class="lk-order-table__link">15-30ОВЧК</a>
                    </td>
                    <td class="lk-order-table__td lk-order-table__description">Заглушка пластиковая внутренняя для труб овального сечения с внешними габаритами сечения 15x30 мм</td>
                    <td class="lk-order-table__td">Белый</td>
                    <td class="lk-order-table__td">2 500</td>
                    <td class="lk-order-table__td">3,5 р.</td>
                    <td class="lk-order-table__td">8 750 р.</td>
                </tr>
                </tbody>
            </table>
            <div class="lk-order-table__total">Итого: 45 000 р.</div>
            <div class="lk-order-table__footer">
                <button class="lk-order-table__button">Повторить заказ</button>
            </div>
        </div>
    @endcomponent
@endsection
