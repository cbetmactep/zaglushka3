@php
    declare(strict_types=1);
@endphp

<div class="container">
    <div class="page-header">
        <div class="page-header__title-wrap">
            <h1 class="page__title">{!! _t('Личный кабинет', 'lk') !!}</h1>
        </div>
    </div>
    <div class="page">
        <div class="page__content page__content_background page__content_no-padding">
            <div class="lk">
                <div class="lk-menu">
                    <div class="lk-menu__list">
                        <a class="lk-menu__item {{ Route::is('lk') || Route::is('lk_order') ? 'lk-menu__item_active' : '' }}" href="{{ route('lk') }}">
                            <div class="lk-menu__item-icon-wrap">
                                @component('components.icon', ['name' => 'lk-orders', 'attributes' => ['class' => 'lk-menu__item-icon lk-menu__item-icon_orders']])@endcomponent
                            </div>
                            <div class="lk-menu__item-name">{!! _t('Мои заказы', 'lk') !!}</div>
                        </a>
                        <a class="lk-menu__item {{ Route::is('lk_search') ? 'lk-menu__item_active' : '' }}" href="{{ route('lk_search') }}">
                            <div class="lk-menu__item-icon-wrap">
                                @component('components.icon', ['name' => 'lk-search', 'attributes' => ['class' => 'lk-menu__item-icon lk-menu__item-icon_search']])@endcomponent
                            </div>
                            <div class="lk-menu__item-name">{!! _t('Поиск по артикулу', 'lk_search') !!}</div>
                        </a>
                        <a class="lk-menu__item {{ Route::is('lk_favorites') ? 'lk-menu__item_active' : '' }}" href="{{ route('lk_favorites') }}">
                            <div class="lk-menu__item-icon-wrap">
                                @component('components.icon', ['name' => 'lk-favorites', 'attributes' => ['class' => 'lk-menu__item-icon lk-menu__item-icon_favorites']])@endcomponent
                            </div>
                            <div class="lk-menu__item-name">{!! _t('Избранное', 'lk_favorites') !!}</div>
                        </a>
                        <a class="lk-menu__item {{ Route::is('lk_popular') ? 'lk-menu__item_active' : '' }}" href="{{ route('lk_popular') }}">
                            <div class="lk-menu__item-icon-wrap">
                                @component('components.icon', ['name' => 'lk-popular', 'attributes' => ['class' => 'lk-menu__item-icon lk-menu__item-icon_popular']])@endcomponent
                            </div>
                            <div class="lk-menu__item-name">{!! _t('Популярные товары', 'lk_popular') !!}</div>
                        </a>
                        <a class="lk-menu__item {{ Route::is('lk_requisites') ? 'lk-menu__item_active' : '' }}" href="{{ route('lk_requisites') }}">
                            <div class="lk-menu__item-icon-wrap">
                                @component('components.icon', ['name' => 'lk-requisites', 'attributes' => ['class' => 'lk-menu__item-icon lk-menu__item-icon_requisites']])@endcomponent
                            </div>
                            <div class="lk-menu__item-name">{!! _t('Реквизиты', 'lk_requisites') !!}</div>
                        </a>
                        <a class="lk-menu__item {{ Route::is('lk_feedback') ? 'lk-menu__item_active' : '' }}" href="{{ route('lk_feedback') }}">
                            <div class="lk-menu__item-icon-wrap"></div>
                            <div class="lk-menu__item-name">{!! _t('Обратная связь', 'lk_feedback') !!}</div>
                        </a>
                        <a class="lk-menu__item {{ Route::is('lk_settings') ? 'lk-menu__item_active' : '' }}" href="{{ route('lk_settings') }}">
                            <div class="lk-menu__item-icon-wrap"></div>
                            <div class="lk-menu__item-name">{!! _t('Настройки', 'lk_settings') !!}</div>
                        </a>
                    </div>
                    <div class="lk-menu-chisel lk-menu-chisel_balance">
                        <div class="lk-menu-chisel__icon"></div>
                        <div class="lk-menu-chisel__title">{!! _t('На счету', 'lk') !!}</div>
                        <div class="lk-menu-chisel__count">120 234</div>
                    </div>
                    <div class="lk-menu-chisel lk-menu-chisel_bonus">
                        <div class="lk-menu-chisel__icon-wrap">
                            @component('components.icon', ['name' => 'lk-offers', 'attributes' => ['class' => 'lk-menu-chisel__icon lk-menu-chisel__icon_bonus']])@endcomponent
                        </div>
                        <div class="lk-menu-chisel__title">{!! _t('Бонусы', 'lk') !!}</div>
                        <div class="lk-menu-chisel__count">500</div>
                    </div>
                </div>
                <div class="lk__content">
                    {{ $slot }}
                </div>
            </div>
        </div>
    </div>
</div>
