@php
    declare(strict_types=1);

    Meta::set('title', _t('Настройки', 'lk'));
@endphp

@extends('layouts.master')

@section('content')
    @component('lk.layout')
        <div class="lk__title lk__title_border">{{ _t('Настройки', 'lk') }}</div>
        <div class="lk-settings__block">
            <div class="lk-settings__title">Смена пароля</div>
            <div class="lk-settings-password">
                <div class="lk-settings-password__input-wrap">
                    <input class="lk-settings-password__input" type="password" placeholder="Введите новый пароль">
                </div>
                <button class="lk-settings-password__button">Сменить</button>
            </div>
        </div>
        <div class="lk-settings__block">
            <div class="lk-settings__title">Оповещения о статусе заказа</div>
            <div class="lk-settings__sub-title">Информировать о смене статуса заказа</div>
            <ul class="lk-settings-checkbox-list">
                <li class="lk-settings-checkbox-list__item">
                    <label class="lk-settings__checkbox">
                        <input type="checkbox">
                        <span>
                            @component('components.icon', ['name' => 'check'])@endcomponent
                            по SMS
                        </span>
                    </label>
                </li>
                <li class="lk-settings-checkbox-list__item">
                    <ul>
                        <li class="lk-settings-checkbox-list__item">
                            <label class="lk-settings__checkbox">
                                <input type="checkbox">
                                <span>
                                    @component('components.icon', ['name' => 'check'])@endcomponent
                                    в обработке
                                </span>
                            </label>
                        </li>
                        <li class="lk-settings-checkbox-list__item">
                            <label class="lk-settings__checkbox">
                                <input type="checkbox">
                                <span>
                                    @component('components.icon', ['name' => 'check'])@endcomponent
                                    собирается
                                </span>
                            </label>
                        </li>
                        <li class="lk-settings-checkbox-list__item">
                            <label class="lk-settings__checkbox">
                                <input type="checkbox">
                                <span>
                                    @component('components.icon', ['name' => 'check'])@endcomponent
                                    собран
                                </span>
                            </label>
                        </li>
                        <li class="lk-settings-checkbox-list__item">
                            <label class="lk-settings__checkbox">
                                <input type="checkbox">
                                <span>
                                    @component('components.icon', ['name' => 'check'])@endcomponent
                                    в пути
                                </span>
                            </label>
                        </li>
                        <li class="lk-settings-checkbox-list__item">
                            <label class="lk-settings__checkbox">
                                <input type="checkbox">
                                <span>
                                    @component('components.icon', ['name' => 'check'])@endcomponent
                                    готов к выдаче
                                </span>
                            </label>
                        </li>
                    </ul>
                </li>
                <li class="lk-settings-checkbox-list__item">
                    <label class="lk-settings__checkbox">
                        <input type="checkbox">
                        <span>
                            @component('components.icon', ['name' => 'check'])@endcomponent
                            по электронной почте
                        </span>
                    </label>
                </li>
            </ul>
            <button class="lk-settings__button-checkbox">Сохранить</button>
        </div>
        <div class="lk-settings__block">
            <div class="lk-settings__title">Отслежываемые товары</div>
            <div class="lk-settings__sub-title">Отслеживайте интересующий вас товар</div>
        </div>
    @endcomponent
@endsection
