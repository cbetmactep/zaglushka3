@php
    declare(strict_types=1);

    Meta::set('title', _t('Популярные товары', 'lk'));
@endphp

@extends('layouts.master')

@section('content')
    @component('lk.layout')
        <div class="lk__header">
            <div class="lk__title">{{ _t('Популярные товары', 'lk') }}</div>
            <div class="lk__header-right">
                <div class="lk-date-btn">
                    <div class="lk-date-btn__icon"></div>
                    <div class="lk-date-btn__title">Даты заказов</div>
                </div>
                <div class="lk__header-input">
                    <div class="lk-input">
                        <input class="lk-input__input" placeholder="Поиск по артикулу">
                        <button class="lk-input__button">Найти</button>
                    </div>
                </div>
            </div>
        </div>
        <table class="lk-popular">
            <thead>
                <tr>
                    <th class="lk-popular__th">№</th>
                    <th class="lk-popular__th">Фото</th>
                    <th class="lk-popular__th">Артикул</th>
                    <th class="lk-popular__th">Описание</th>
                    <th class="lk-popular__th">Цвет</th>
                    <th class="lk-popular__th">Заказано,<br/>штук</th>
                    <th class="lk-popular__th"></th>
                </tr>
            </thead>
            <tbody>
                @foreach(range(1, 7) as $i)
                    <tr>
                        <td class="lk-popular__td">{{ $i }}</td>
                        <td class="lk-popular__td"><img src="/images/item/92/8992/1.jpg" width="75" height="75"></td>
                        <td class="lk-popular__td"><a href="#" class="lk-popular__link">15-30ОВЧК</a></td>
                        <td class="lk-popular__td lk-popular__description">Заглушка пластиковая внутренняя для труб овального сечения с внешними габаритами сечения 15x30 мм</td>
                        <td class="lk-popular__td">Белый</td>
                        <td class="lk-popular__td">12 500</td>
                        <td class="lk-popular__td"><button class="lk-popular__button">Заказать</button></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endcomponent
@endsection
