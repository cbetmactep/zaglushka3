@php
    declare(strict_types=1);

    Meta::set('title', _t('Поиск по артикулу', 'lk'));
@endphp

@extends('layouts.master')

@section('content')
    @component('lk.layout')
        <div class="lk__title lk__title_border">{{ _t('Поиск по артикулу', 'lk') }}</div>
        <div class="lk-search-header">
            <input class="lk-search-header__input" placeholder="Введите артикул">
            <button class="lk-search-header__button">Найти</button>
        </div>
        @foreach(range(1, 3) as $i)
            <div class="lk-search-item">
            <div class="lk-search-item__left">
                <img src="/images/item/92/1192/5.jpg" width="90" height="90">
            </div>
            <div class="lk-search-item__right">
                <div class="lk-search-item__header">
                    <div class="lk-search-item__article">Артикул: <a href="#" class="lk-search-item__link">20-20ПЧК</a></div>
                    <select class="lk-search-item__select">
                        <option>Черный</option>
                        <option>Другой</option>
                    </select>
                    <div class="lk-search-item__stock">В наличии 650 000 шт.</div>
                    <button class="lk-search-item__button">Заказать</button>
                </div>
                <table class="lk-search-item-table">
                    <thead class="lk-search-item-table__thead">
                    <tr>
                        <th class="lk-search-item-table__th">Цвет</th>
                        <th class="lk-search-item-table__th lk-search-item-table__price">от <span class="nowrap">10 000</span></th>
                        <th class="lk-search-item-table__th lk-search-item-table__price">от <span class="nowrap">5 000</span></th>
                        <th class="lk-search-item-table__th lk-search-item-table__price">от <span class="nowrap">1 000</span></th>
                        <th class="lk-search-item-table__th lk-search-item-table__price">от 500</th>
                        <th class="lk-search-item-table__th lk-search-item-table__price">от 100</th>
                        <th class="lk-search-item-table__th lk-search-item-table__price">Розница</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="lk-search-item-table__tr">
                        <td class="lk-search-item-table__td">
                            чёрный
                        </td>
                        <td class="lk-search-item-table__td lk-search-item-table__price">
                            2,6 р.
                        </td>
                        <td class="lk-search-item-table__td lk-search-item-table__price">
                            3,2 р.
                        </td>
                        <td class="lk-search-item-table__td lk-search-item-table__price">
                            3,8 р.
                        </td>
                        <td class="lk-search-item-table__td lk-search-item-table__price">
                            5,6 р.
                        </td>
                        <td class="lk-search-item-table__td lk-search-item-table__price">
                            <div class="lk-search-item-table__price-old">8,0 р.</div>
                            <span class="lk-search-item-table__discount lk-search-item-table__discount_active ">−50 %</span>
                            <div>4,0 р.</div>
                        </td>
                        <td class="lk-search-item-table__td lk-search-item-table__price">
                            <div class="lk-search-item-table__price-old">15,0 р.</div>
                            <span class="lk-search-item-table__discount ">−30 %</span>
                            <div>10,5 р.</div>
                        </td>
                    </tr>
                    <tr class="lk-search-item-table__tr">
                        <td class="lk-search-item-table__td">другие</td>
                        <td class="lk-search-item-table__td lk-search-item-table__price">
                            <div class="lk-search-item-table__price-text">
                                3,4 р.
                            </div>
                        </td>
                        <td class="lk-search-item-table__td lk-search-item-table__price">
                            <div class="lk-search-item-table__price-text">
                                4,2 р.
                            </div>
                        </td>
                        <td class="lk-search-item-table__td lk-search-item-table__price">
                            <div class="lk-search-item-table__price-text">
                                4,9 р.
                            </div>
                        </td>
                        <td class="lk-search-item-table__td lk-search-item-table__price">
                            <div class="lk-search-item-table__price-text">
                                7,3 р.
                            </div>
                        </td>
                        <td class="lk-search-item-table__td lk-search-item-table__price">
                            <div class="lk-search-item-table__price-text">
                                10,4 р.
                            </div>
                        </td>
                        <td class="lk-search-item-table__td lk-search-item-table__price">
                            <div class="lk-search-item-table__price-text">
                                19,5 р.
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        @endforeach
    @endcomponent
@endsection
