@php
    declare(strict_types=1);

    Meta::set('title', _t('Обратная связь', 'lk'));
@endphp

@extends('layouts.master')

@section('content')
    @component('lk.layout')
        <div class="lk__title">{{ _t('Обратная связь', 'lk') }}</div>
        Обратная связь
    @endcomponent
@endsection
