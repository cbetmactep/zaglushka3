@php
    declare(strict_types=1);

    Meta::set('title', _t('Мои заказы', 'lk'));
@endphp

@extends('layouts.master')

@section('content')
    @component('lk.layout')
        <div class="lk__title">{{ _t('Мои заказы', 'lk') }}</div>
        <div class="lk-orders__header">
            <div>
                <div class="lk-input">
                    <input class="lk-input__input" placeholder="Поиск по артикулу или номеру заказа">
                    <button class="lk-input__button">Найти</button>
                </div>
            </div>
            <div class="lk-orders__header-right">
                <div class="lk-date-btn">
                    <div class="lk-date-btn__icon"></div>
                    <div class="lk-date-btn__title">Даты заказов</div>
                </div>
                <div class="lk-status">
                    <div class="lk-status__title">Статус</div>
                    <div class="lk-status__item">В обработке</div>
                    <div class="lk-status__item">Собирается</div>
                    <div class="lk-status__item">Собран</div>
                    <div class="lk-status__item">В пути</div>
                    <div class="lk-status__item">Готов к выдаче</div>
                    <div class="lk-status__item">Выполнен</div>
                    <div class="lk-status__item">Отменен</div>
                </div>
            </div>
        </div>
        <table class="lk-table-order">
            <thead>
                <tr class="lk-table-order__thead">
                    <th class="lk-table-order__th">Дата<br/>заказа</th>
                    <th class="lk-table-order__th">Номер</th>
                    <th class="lk-table-order__th">Стоимость</th>
                    <th class="lk-table-order__th">Статус</th>
                    <th class="lk-table-order__th">Дата измен.<br/>статуса</th>
                    <th class="lk-table-order__th">Оплата</th>
                </tr>
            </thead>
            <tbody>
                <tr class="lk-table-order__tr">
                    <td class="lk-table-order__td lk-table-order__date">15.02.2018</td>
                    <td class="lk-table-order__td">
                        <a href="{{ route('lk_order') }}" class="lk-table-order__link">1345678</a>
                    </td>
                    <td class="lk-table-order__td">
                        <div class="lk-table-order__price">
                            <div class="lk-table-order__price-row">
                                <div>Заказ:</div>
                                <div>15 000 р.</div>
                            </div>
                            <div class="lk-table-order__price-row">
                                <div>Доставка:</div>
                                <div>3 000 р.</div>
                            </div>
                        </div>
                    </td>
                    <td class="lk-table-order__td">
                        <div class="lk-table-order__status lk-table-order__status_in-processing">В обработке</div>
                        <div class="lk-table-order__status-date">Доставка через ~ 4–7 дней</div>
                    </td>
                    <td class="lk-table-order__td">15.08.2018</td>
                    <td class="lk-table-order__td">
                        <div class="lk-table-order__payment lk-table-order__payment_success">
                            @component('components.icon', ['name' => 'check', 'attributes' => ['class' => 'lk-table-order__payment-icon']])@endcomponent
                            <span>Оплачен</span>
                        </div>
                    </td>
                </tr>
                <tr class="lk-table-order__tr">
                    <td class="lk-table-order__td lk-table-order__date">15.02.2018</td>
                    <td class="lk-table-order__td">
                        <a href="{{ route('lk_order') }}" class="lk-table-order__link">1345678</a>
                    </td>
                    <td class="lk-table-order__td">
                        <div class="lk-table-order__price">
                            <div class="lk-table-order__price-row">
                                <div>Заказ:</div>
                                <div>15 000 р.</div>
                            </div>
                            <div class="lk-table-order__price-row">
                                <div>Доставка:</div>
                                <div>3 000 р.</div>
                            </div>
                        </div>
                    </td>
                    <td class="lk-table-order__td">
                        <div class="lk-table-order__status lk-table-order__status_assembled">Собран</div>
                    </td>
                    <td class="lk-table-order__td">15.08.2018</td>
                    <td class="lk-table-order__td">Не оплачен</td>
                </tr>
                <tr class="lk-table-order__tr">
                    <td class="lk-table-order__td lk-table-order__date">15.02.2018</td>
                    <td class="lk-table-order__td">
                        <a href="{{ route('lk_order') }}" class="lk-table-order__link">1345678</a>
                    </td>
                    <td class="lk-table-order__td">
                        <div class="lk-table-order__price">
                            <div class="lk-table-order__price-row">
                                <div>Заказ:</div>
                                <div>15 000 р.</div>
                            </div>
                            <div class="lk-table-order__price-row">
                                <div>Доставка:</div>
                                <div>3 000 р.</div>
                            </div>
                        </div>
                    </td>
                    <td class="lk-table-order__td">
                        <div class="lk-table-order__status lk-table-order__status_going">Собирается</div>
                    </td>
                    <td class="lk-table-order__td">15.08.2018</td>
                    <td class="lk-table-order__td">Не оплачен</td>
                </tr>
                <tr class="lk-table-order__tr">
                    <td class="lk-table-order__td lk-table-order__date">15.02.2018</td>
                    <td class="lk-table-order__td">
                        <a href="{{ route('lk_order') }}" class="lk-table-order__link">1345678</a>
                    </td>
                    <td class="lk-table-order__td">
                        <div class="lk-table-order__price">
                            <div class="lk-table-order__price-row">
                                <div>Заказ:</div>
                                <div>15 000 р.</div>
                            </div>
                            <div class="lk-table-order__price-row">
                                <div>Доставка:</div>
                                <div>3 000 р.</div>
                            </div>
                        </div>
                    </td>
                    <td class="lk-table-order__td">
                        <div class="lk-table-order__status lk-table-order__status_in-transit">В пути</div>
                        <div class="lk-table-order__status-date">Доставка через ~ 4–7 дней</div>
                    </td>
                    <td class="lk-table-order__td">15.08.2018</td>
                    <td class="lk-table-order__td">
                        <div class="lk-table-order__payment lk-table-order__payment_success-half">
                            @component('components.icon', ['name' => 'check', 'attributes' => ['class' => 'lk-table-order__payment-icon']])@endcomponent
                            <span>Оплачен<br/>на 50%</span>
                        </div>
                    </td>
                </tr>
                <tr class="lk-table-order__tr">
                    <td class="lk-table-order__td lk-table-order__date">15.02.2018</td>
                    <td class="lk-table-order__td">
                        <a href="{{ route('lk_order') }}" class="lk-table-order__link">1345678</a>
                    </td>
                    <td class="lk-table-order__td">
                        <div class="lk-table-order__price">
                            <div class="lk-table-order__price-row">
                                <div>Заказ:</div>
                                <div>15 000 р.</div>
                            </div>
                            <div class="lk-table-order__price-row">
                                <div>Доставка:</div>
                                <div>3 000 р.</div>
                            </div>
                        </div>
                    </td>
                    <td class="lk-table-order__td">
                        <div class="lk-table-order__status lk-table-order__status_ready">Готов к выдаче</div>
                        <div class="lk-table-order__status-date">Доставка через ~ 4–7 дней</div>
                    </td>
                    <td class="lk-table-order__td">15.08.2018</td>
                    <td class="lk-table-order__td">
                        <div class="lk-table-order__payment lk-table-order__payment_success">
                            @component('components.icon', ['name' => 'check', 'attributes' => ['class' => 'lk-table-order__payment-icon']])@endcomponent
                            <span>Оплачен</span>
                        </div>
                    </td>
                </tr>
                <tr class="lk-table-order__tr">
                    <td class="lk-table-order__td lk-table-order__date">15.02.2018</td>
                    <td class="lk-table-order__td">
                        <a href="{{ route('lk_order') }}" class="lk-table-order__link">1345678</a>
                    </td>
                    <td class="lk-table-order__td">
                        <div class="lk-table-order__price">
                            <div class="lk-table-order__price-row">
                                <div>Заказ:</div>
                                <div>15 000 р.</div>
                            </div>
                            <div class="lk-table-order__price-row">
                                <div>Доставка:</div>
                                <div>3 000 р.</div>
                            </div>
                        </div>
                    </td>
                    <td class="lk-table-order__td">
                        <div class="lk-table-order__status lk-table-order__status_canceled">Отменен</div>
                        <div class="lk-table-order__status-date">Доставка через ~ 4–7 дней</div>
                    </td>
                    <td class="lk-table-order__td">15.08.2018</td>
                    <td class="lk-table-order__td">
                        <div class="lk-table-order__payment lk-table-order__payment_success">
                            @component('components.icon', ['name' => 'check', 'attributes' => ['class' => 'lk-table-order__payment-icon']])@endcomponent
                            <span>Оплачен</span>
                        </div>
                    </td>
                </tr>
                <tr class="lk-table-order__tr">
                    <td class="lk-table-order__td lk-table-order__date">15.02.2018</td>
                    <td class="lk-table-order__td">
                        <a href="{{ route('lk_order') }}" class="lk-table-order__link">1345678</a>
                    </td>
                    <td class="lk-table-order__td">
                        <div class="lk-table-order__price">
                            <div class="lk-table-order__price-row">
                                <div>Заказ:</div>
                                <div>15 000 р.</div>
                            </div>
                            <div class="lk-table-order__price-row">
                                <div>Доставка:</div>
                                <div>3 000 р.</div>
                            </div>
                        </div>
                    </td>
                    <td class="lk-table-order__td">
                        <div class="lk-table-order__status lk-table-order__status_completed">Выполнен</div>
                        <div class="lk-table-order__status-date">Доставка через ~ 4–7 дней</div>
                    </td>
                    <td class="lk-table-order__td">15.08.2018</td>
                    <td class="lk-table-order__td">
                        <div class="lk-table-order__payment lk-table-order__payment_success">
                            @component('components.icon', ['name' => 'check', 'attributes' => ['class' => 'lk-table-order__payment-icon']])@endcomponent
                            <span>Оплачен</span>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    @endcomponent
@endsection
