@php
    declare(strict_types=1);

    Meta::set('title', _t('Данные организации', 'lk'));
@endphp

@extends('layouts.master')

@section('content')
    @component('lk.layout')
        <div class="lk__title lk__title_border">{{ _t('Данные организации', 'lk') }}</div>
        <div class="lk-requisites">
            <div class="lk-requisites__row">
                <label class="lk-requisites__label" for="name">Название организации</label>
                <input class="lk-requisites__input" type="text" id="name" name="name">
            </div>
            <div class="lk-requisites__row">
                <label class="lk-requisites__label" for="legal-address">Юридический адрес</label>
                <input class="lk-requisites__input" type="text" id="legal-address" name="legal-address">
            </div>
            <div class="lk-requisites__row">
                <label class="lk-requisites__label" for="actual-address">Фактический адрес</label>
                <input class="lk-requisites__input" type="text" id="actual-address" name="actual-address">
            </div>
            <div class="lk-requisites__row">
                <label class="lk-requisites__label" for="phone">Телефон</label>
                <input class="lk-requisites__input lk-requisites__input_phone" type="text" id="phone" name="phone">
            </div>
            <div class="lk-requisites__row">
                <label class="lk-requisites__label" for="email">Электронная почта</label>
                <input class="lk-requisites__input lk-requisites__input_email" type="text" id="email" name="email">
            </div>
            <div class="lk-requisites__row">
                <label class="lk-requisites__label" for="inn">ИНН</label>
                <input class="lk-requisites__input lk-requisites__input_inn" type="text" id="inn" name="inn">
            </div>
            <div class="lk-requisites__row">
                <label class="lk-requisites__label" for="kpp">КПП</label>
                <input class="lk-requisites__input lk-requisites__input_inn" type="text" id="kpp" name="kpp">
            </div>
            <div class="lk-requisites__row">
                <label class="lk-requisites__label" for="bank-name">Название банка</label>
                <input class="lk-requisites__input" type="text" id="bank-name" name="bank-name">
            </div>
            <div class="lk-requisites__row">
                <label class="lk-requisites__label" for="corporate-invoice">Кор. счет</label>
                <input class="lk-requisites__input lk-requisites__input_invoice" type="text" id="corporate-invoice" name="corporate-invoice">
            </div>
            <div class="lk-requisites__row">
                <label class="lk-requisites__label" for="invoice">Расчетный счет</label>
                <input class="lk-requisites__input lk-requisites__input_invoice" type="text" id="invoice" name="invoice">
            </div>
            <button class="lk-requisites__button">Сохранить изменения</button>
        </div>
    @endcomponent
@endsection
