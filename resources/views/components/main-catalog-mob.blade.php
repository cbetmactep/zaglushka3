@php
    declare(strict_types=1);
@endphp

<div class="p-main-catalog__row p-main-catalog__row-mob">
    @isset($link)
        <a class="p-main-catalog__link-mob" href="{{ $link }}">
            @component('components.icon', ['name' => 'arrow-braedcrumbs2', 'attributes' => ['class' => 'p-main-catalog__link-mob-icon']])])@endcomponent
        </a>
    @endisset
    <h2 class="p-main-catalog__title">
        {{ $title }}
    </h2>
    @isset($filter)
        @php
            if(Request::get('special') == 1) {
                $filter++;
            }

            if(Request::get('exist') == 1) {
                $filter++;
            }

            if(Request::get('transit') == 1) {
                $filter++;
            }
        @endphp
        <div class="p-main-catalog__filter-mob js-mob-filter-btn">
            {!! _t('Фильтры', 'common') !!}
            <div class="p-main-catalog__filter-mob-num js-mob-filter-num">@if($filter){{ $filter }}@endif</div>
        </div>
    @endisset
</div>