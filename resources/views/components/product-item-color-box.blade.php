@php
    declare(strict_types=1);

    $hexIsGradient = strpos($hex, ':');
    if($hexIsGradient) {
        $hexs = explode(':', $hex);
    } else {
        $hexs = explode(',', $hex);
    }
@endphp

@if($hex)
    @if(count($hexs) === 1)
        <div class="product-item-color-box">
            <div class="product-item-color-box__item @if($hexs[0] === 'FFFFFF') product-item-color-box__item_border @endif" style="background-color: #{{ $hexs[0] }};"></div>
        </div>
    @elseif(count($hexs) === 2 && !$hexIsGradient)
        <div class="product-item-color-box product-item-color-box_is-double">
            <div class="product-item-color-box__item" style="background-color: #{{ $hexs[0] }};"></div>
            <div class="product-item-color-box__item" style="background-color: #{{ $hexs[1] }};"></div>
        </div>
    @elseif(count($hexs) === 2 && $hexIsGradient)
        <div class="product-item-color-box">
            <div class="product-item-color-box__item" style="background: linear-gradient(to right, #{{ $hexs[0] }} 0%, #{{ $hexs[1] }} 100%);"></div>
        </div>
    @elseif(count($hexs) === 3)
        <div class="product-item-color-box product-item-color-box_is-triple">
            <div class="product-item-color-box__item" style="background-color: #{{ $hexs[0] }};"></div>
            <div class="product-item-color-box__item" style="background-color: #{{ $hexs[1] }};"></div>
            <div class="product-item-color-box__item" style="background-color: #{{ $hexs[2] }};"></div>
        </div>
    @elseif(count($hexs) === 4)
        <div class="product-item-color-box product-item-color-box_is-quadruple">
            <div class="product-item-color-box__item" style="background-color: #{{ $hexs[0] }};"></div>
            <div class="product-item-color-box__item" style="background-color: #{{ $hexs[1] }};"></div>
            <div class="product-item-color-box__item" style="background-color: #{{ $hexs[2] }};"></div>
            <div class="product-item-color-box__item" style="background-color: #{{ $hexs[3] }};"></div>
        </div>
    @endif
@endif