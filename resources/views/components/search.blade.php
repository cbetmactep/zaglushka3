@php
    declare(strict_types=1);

    use App\Models\Category;
@endphp

@if(count($Items) > 0)
    @foreach($Items as $parentCategoryId => $GroupItems)
        @php
            $catItems = new \Illuminate\Database\Eloquent\Collection();
        @endphp
        @foreach($GroupItems as $categoryId => $CategoryItems)
            @php
                $catItems = $catItems->merge($CategoryItems);
            @endphp
        @endforeach
        <div class="page-header">
            <div class="page-header__title-wrap">
                <h2 class="page__title">{{ Category::cachedOne($parentCategoryId)->title }}</h2>
            </div>
        </div>
        @component('item.list', ['Items' => $catItems, 'firstItem' => 1])@endcomponent
    @endforeach
@else
    <div class="p-search">
        <div class="page__content page__content_background p-search_forms">
            {{ _t('По вашему запросу ничего не найдено.', 'common') }}
        </div>
    </div>
@endif