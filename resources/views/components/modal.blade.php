@php
    declare(strict_types=1);
@endphp

<div id="{{ $name }}Modal" class="modal_hide @isset($class){{ $class }}@endisset">
    <div class="modal__row">
        <a href="#close-modal" rel="modal:close" class="close-modal">
            {{ _t('Закрыть', 'common') }}
            @component('components.icon', ['name' => 'close-thin', 'attributes' => ['class' => 'close-modal__icon']])])@endcomponent
        </a>
        {{ $slot }}
    </div>
</div>