@php
    declare(strict_types=1);
@endphp

@if($paginator->hasPages())
    <div class="pagination">
        <ul class="pagination__list">
            @if($paginator->currentPage() > 3)
                <li class="pagination__item"><a class="pagination__link" href="{{ $paginator->url(1) }}">1</a></li>
            @endif

            @if($paginator->currentPage() > 4)
                <li class="pagination__item pagination__item_disabled">...</li>
            @endif

            @foreach(range(1, $paginator->lastPage()) as $i)
                @if($i >= $paginator->currentPage() - 2 && $i <= $paginator->currentPage() + 2)
                    @if($i == $paginator->currentPage())
                        <li class="pagination__item pagination__item_active">{{ $i }}</li>
                    @else
                        <li class="pagination__item">
                            <a class="pagination__link" href="{{ $paginator->url($i) }}">{{ $i }}</a>
                        </li>
                    @endif
                @endif
            @endforeach

            @if($paginator->currentPage() < $paginator->lastPage() - 3)
                <li class="pagination__item pagination__item_disabled">...</li>
            @endif

            @if($paginator->currentPage() < $paginator->lastPage() - 2)
                <li class="pagination__item">
                    <a class="pagination__link" href="{{ $paginator->url($paginator->lastPage()) }}">
                        {{ $paginator->lastPage() }}
                    </a>
                </li>
            @endif
        </ul>
    </div>
@endif
