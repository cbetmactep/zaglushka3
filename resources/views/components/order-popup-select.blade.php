@php
    declare(strict_types=1);
@endphp

<div class="order-popup-table__select">
    {{ $slot }}
    @component('components.icon', ['name' => 'arrows'])])@endcomponent
</div>