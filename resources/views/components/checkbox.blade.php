@php
    declare(strict_types=1);

    if (!isset($attributes)) {
        $attributes = [];
    }

    $defaultClass = "checkbox__input";

    $attributes['class'] = isset($attributes['class']) ? $attributes['class'] . " " . $defaultClass : $defaultClass;

@endphp

<label class="checkbox">
    <input type="checkbox" name="{{ $name }}"
    @foreach($attributes as $key => $value)
        {{ $key }} @if($value !== true) ="{{ $value }}" @endif
    @endforeach
    >
    <span class="checkbox__title">{{ $title }}</span>
</label>