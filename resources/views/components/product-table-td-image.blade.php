@php
    declare(strict_types=1);
@endphp

@foreach($images as $image)
    <td class="product-table__td product-table__td_image"@isset($image['colspan']) colspan="{{ $image['colspan'] }}"@endisset>
        <a href="{{ $link }}">
            @if ($image['link'])
                <img class="product-table__image" src="{{ $image['link'] }}" alt="{{ $image['alt']}}" width="{{  $image['sizes'][0] ?? 95 }}" height="{{ $image['sizes'][1] ?? 95 }}">
            @elseif(!isset($image['noDummy']) || !$image['noDummy'])
                <img class="product-table__image" src="{{ asset('images/dummy_95.png') }}" alt="" height="95" width="95">
            @endif
        </a>
    </td>
@endforeach
