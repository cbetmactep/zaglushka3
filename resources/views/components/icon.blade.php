@php
    declare(strict_types=1);

    if (!isset($attributes)) {
        $attributes = [];
    }

    $defaultClass = "icon icon_" . $name;

    $attributes['class'] = isset($attributes['class']) ? $attributes['class'] . " " . $defaultClass : $defaultClass;

@endphp

<svg
@foreach($attributes as $key => $value)
    {{ $key }} @if($value !== true) ="{{ $value }}" @endif
@endforeach
>
<use xlink:href="{{ mix('images/sprite.svg') }}#{{ $name }}"></use>
</svg>