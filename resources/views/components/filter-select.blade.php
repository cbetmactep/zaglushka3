@php
    declare(strict_types=1);
    $request = $request ?: ''; // todo wtf
@endphp

<div class="n-filter__select @if($request) n-filter__select_active @endif @if(count($sizes) === 0) n-filter__select_disabled @endif">
    @isset($placeholder)
        <div class="n-filter__select-placeholder">
            {{ $placeholder }}
        </div>
    @endisset
    <select name="{{ isset($size) ? 'size_' . $size : 'size' }}" class="js-product-table-filter-select @isset($class) {{ $class }} @endisset" @if(count($sizes) === 0) disabled @endif>
        <option value="">&nbsp;</option>
        @foreach($sizes as $key => $value)
            <option value="{{ $key }}"{{ htmlentities($request) === (string) $key ? ' selected' : '' }}>{{ $value }}</option>
        @endforeach
    </select>
    @component('components.icon', ['name' => 'close-thin', 'attributes' => ['class' => 'n-filter__select-close']])])@endcomponent
    @component('components.icon', ['name' => 'arrows', 'attributes' => ['class' => 'n-filter__select-icon']])])@endcomponent
</div>