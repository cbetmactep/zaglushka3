@php
    declare(strict_types=1);

    if (!isset($attributes)) {
        $attributes = [];
    }

    $defaultClass = "radio__input";

    $attributes['class'] = isset($attributes['class']) ? $attributes['class'] . " " . $defaultClass : $defaultClass;

@endphp

<label class="radio">
    <input type="radio" name="{{ $name }}"
    @foreach($attributes as $key => $value)
        {{ $key }} @if($value !== true) ="{{ $value }}" @endif
    @endforeach
    >
    <span class="radio__title">{{ $title }}</span>
</label>