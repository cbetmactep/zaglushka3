@php
    declare(strict_types=1);
@endphp

<div class="p-search-item__select @if(count($sizes) === 0) p-search-item__select_disabled @endif">
    @isset($placeholder)
        <div class="p-search-item__select-placeholder">
            {{ $placeholder }}
        </div>
    @endisset
    <select @isset($class) class="{{ $class }}" @endisset name="{{ isset($size) ? 'size_' . $size : 'size' }}" @if(count($sizes) === 0) disabled @endif>
        <option value="">&nbsp;</option>
        @foreach($sizes as $key => $value)
            <option value="{{ $key }}">{{ $value }}</option>
        @endforeach
    </select>
    @component('components.icon', ['name' => 'arrows', 'attributes' => ['class' => 'p-search-item__select-icon']])])@endcomponent
    @component('components.icon', ['name' => 'close-thin', 'attributes' => ['class' => 'p-search-item__select-close']])])@endcomponent
</div>