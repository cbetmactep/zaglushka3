@php
    declare(strict_types=1);
@endphp

<div class="product-table-filter__wrap">
    <form class="product-table-filter n-filter @isset($class) {{ $class }} @endisset">
        <div class="n-filter__left">
            @component('includes.filter', ['filter' => $filter])@endcomponent
            @if(in_array($filter, [1, 2, 3, 4, 5, 9, 11, 17]))
                <button class="n-filter__button">{!! _t('Подобрать', 'search') !!}</button>
            @endif
        </div>
        <div class="n-filter__right">
            <label class="n-filter__checkbox">
                <input class="js-product-table-filter-checkbox" type="checkbox" {{ Request::get('special') == 1 ? 'checked' : '' }} name="special" value="1">
                <span>
                    @component('components.icon', ['name' => 'check'])@endcomponent
                    {!! _t('Со скидкой', 'search') !!}
                </span>
            </label>
            <div class="n-filter__checkbox-wrap">
                <label class="n-filter__checkbox">
                    <input class="js-product-table-filter-checkbox" type="checkbox" {{ Request::get('exist') == 1 ? 'checked' : '' }} name="exist" value="1">
                    <span>
                        @component('components.icon', ['name' => 'check'])@endcomponent
                        {!! _t('В наличии', 'search') !!}
                    </span>
                </label>
                <label class="n-filter__checkbox">
                    <input class="js-product-table-filter-checkbox" type="checkbox" {{ Request::get('transit') == 1 ? 'checked' : '' }} name="transit" value="1">
                    <span>
                        @component('components.icon', ['name' => 'check'])@endcomponent
                        {!! _t('В пути', 'search') !!}
                    </span>
                </label>
            </div>
        </div>
    </form>
</div>
