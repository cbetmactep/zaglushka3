@php
    declare(strict_types=1);

    /** @var \App\Models\Category $Category */
@endphp

@if(count($Items) > 0)
    @component('item.list', ['Items' => $Items, 'firstItem' => $Items->firstItem()]) @endcomponent
@else
    <div class="page">
        <div class="page__content page__content_background">
            {{ _t('Ничего не найдено', 'common') }}
        </div>
    </div>
@endif

{{ $Items->appends(Request::except('page'))->links('components.pagination') }}

@if((!Config::get('app.subdomain')) && count($Items) > 0 && $Items->currentPage() == 1)
    <div class="product-description">
        <div class="product-description__row">
            <h1 class="product-description__title">{{ $CurrentTag->title_h1 }}</h1>
            @if($CurrentTag->descr)
                <section class="product-description-block">
                    {!! $CurrentTag->descr !!}
                </section>
            @endif
        </div>
    </div>
@endif