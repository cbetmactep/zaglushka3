@php
    declare(strict_types=1);

    use App\Models\Color;

    $groupedItems = [];
    foreach ($OrderItems as $OrderItem) {
        $key = $OrderItem->item_id . '_' . $OrderItem->color;

        if (!isset($groupedItems[$key])) {
            $groupedItems[$key] = $OrderItem;

            continue;
        }

        $groupedItems[$key]->quant += $OrderItem->quant;
    }

    $OrderItems = $groupedItems;

    Meta::set('title', _t('Корзина', 'cart'));
@endphp

@extends('layouts.master')

@section('content')
    <div class="container">
        {!! Breadcrumbs::render('cart') !!}
        <div class="page-header">
            <div class="page-header__title-wrap">
                <h1 class="page__title">{!! _t('Корзина', 'cart') !!}</h1>
            </div>
            <div class="page-header__actions">
                @component('includes.page_actions')@endcomponent
            </div>
        </div>
        <div class="page">
            <div class="page__content">
                <div class="order__row">
                    <div class="order__step order__step_cart js-order-step-cart {{ (count($OrderItems) > 0) ? 'order__step_active' : ''}}">
                        <div class="template js-cart-price-text" data-id="0">{!! _t('розница', 'cart') !!}</div>
                        <div class="template js-cart-price-text" data-id="1">{!! _t('от', 'cart') !!} 100</div>
                        <div class="template js-cart-price-text" data-id="2">{!! _t('от', 'cart') !!} 500</div>
                        <div class="template js-cart-price-text" data-id="3">{!! _t('от', 'cart') !!} <span class="nowrap">1 000</span></div>
                        <div class="template js-cart-price-text" data-id="4">{!! _t('от', 'cart') !!} <span class="nowrap">5 000</span></div>
                        <div class="template js-cart-price-text" data-id="5">{!! _t('от', 'cart') !!} <span class="nowrap">10 000</span></div>
                        <div class="template js-cart-price-text" data-id="6">{!! _t('от', 'cart') !!} <span class="nowrap">30 000</span></div>
                        <div class="order-cart__wrap">
                            <table class="product-table order-cart js-order-table">
                                <thead class="product-table__thead">
                                <tr>
                                    <th class="product-table__th product-table__th_number">№</th>
                                    <th class="product-table__th">{!! _t('Фото', 'cart') !!}</th>
                                    <th class="product-table__th product-table__th_article">{!! _t('Артикул', 'cart') !!}</th>
                                    <th class="product-table__th">{!! _t('Описание', 'cart') !!}</th>
                                    <th class="product-table__th">{!! _t('Цвет', 'cart') !!}</th>
                                    <th class="product-table__th">{!! _t('Количество, штук', 'cart') !!}</th>
                                    <th class="product-table__th product-table__td_price">{!! _t('Цена за штуку', 'cart') !!}</th>
                                    <th class="product-table__th product-table__td_price">{!! _t('Сумма', 'cart') !!}</th>
                                    <th class="product-table__th"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($OrderItems as $OrderItem)
                                    <tr class="product-table__tr product-table__tr_double"
                                        data-id="{{ $OrderItem->item_id }}"
                                        data-color="{{ $OrderItem->color }}"
                                        data-width="{{ $OrderItem->item->width }}"
                                        data-length="{{ $OrderItem->item->length }}"
                                        data-height="{{ $OrderItem->item->height }}"
                                        data-prop-weight="{{ $OrderItem->item->prop_weight }}"
                                        data-pack-size="{{ $OrderItem->item->pack_size }}"
                                        data-prop-pack-volume="{{ $OrderItem->item->prop_pack_volume }}"
                                        data-prop-pack-weight-gross="{{ $OrderItem->item->prop_pack_weight_gross }}"
                                        data-remains="{{ $OrderItem->getColor()->remains }}"
                                        @for($i = $OrderItem->getColor()->getPrice($OrderItem->item->price6) ? 6 : 5; $i >= 0; $i--)
                                        data-price-{{ $i }}="{{ round($OrderItem->getColor()->getPrice($OrderItem->item['price' . $i]), 1) }}" data-id="{{ $i }}"
                                        @if($OrderItem->item->getSpecialDiscount($i) && $OrderItem->getColor()->main)
                                        data-price-discount-{{ $i }}="{{ round($OrderItem->item->getDiscountPrice($i), 1) }}"
                                        data-discount-{{ $i }}="{{ $OrderItem->item->getSpecialDiscount($i) }}"
                                            @endif
                                            @endfor
                                    >
                                        <td class="product-table__td product-table__td_number">
                                            <a class="product-table__number" href="{{ route('slug', ['slug' => $OrderItem->item->slug()]) }}" target="_blank">
                                                {{ $loop->index + 1 }}
                                            </a>
                                        </td>
                                        <td class="product-table__td product-table__td_image">
                                            <a href="{{ route('slug', ['slug' => $OrderItem->item->slug()]) }}" target="_blank">
                                                <img class="product-table__image" src="{{ $OrderItem->image ?: asset('images/dummy_95.png') }}" alt="{{ $OrderItem->item->title }}" height="95" width="95">
                                            </a>
                                        </td>
                                        <td class="product-table__td product-table__td_article" >
                                            <a class="product-table__article" href="{{ route('slug', ['slug' => $OrderItem->item->slug()]) }}" target="_blank">
                                                {{ $OrderItem->title }}
                                            </a>
                                        </td>
                                        <td class="product-table__td product-table__td_description">
                                            <span>
                                                {{ $OrderItem->item->short_descr }}
                                                @if(!$OrderItem->getColor()->remains)
                                                    <div style="position: relative">
                                                        <div class="product-table__not-available" style="display: none;">
                                                            <div class="product-table__not-available-icon">
                                                                @component('components.icon', ['name' => 'attention'])@endcomponent
                                                            </div>
                                                            <div class="product-table__not-available-text">
                                                                {!! _t('Обращаем ваше внимание, что этого товара нет в наличии. Срок поставки - до 3-х месяцев.', 'cart') !!}
                                                                <br/>
                                                                {!! _t('У нас есть <a class="link" href="%s">похожие товары</a> в наличии.', 'cart', [route('slug', ['slug' => $OrderItem->item->category->slug()]) . '?exist=1']) !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            </span>
                                        </td>
                                        @if($OrderItem->promo)
                                            <td class="product-table__td product-table__td_free-text" colspan="2">
                                                <span>
                                                    {!! _t('Наш менеджер свяжется с вами, чтобы уточнить количество и цвет бесплатных образцов.', 'cart') !!}
                                                </span>
                                            </td>
                                            <td class="product-table__td product-table__td_price" colspan="2">
                                                <span>
                                                    {!! _t('Бесплатные образцы', 'cart') !!}
                                                </span>
                                            </td>
                                        @else
                                            <td class="product-table__td">
                                                <span>
                                                    {{ _t(Color::cachedOne($OrderItem->color)->name, 'colors') }}
                                                </span>
                                            </td>
                                            <td class="product-table__td">
                                                <input class="product-table__input js-order-quantity" value="{{ $OrderItem->quant }}" min="0" />
                                            </td>
                                            <td class="product-table__td product-table__td_price js-order-price-td order-cart__td">
                                                <span style="position: relative;">
                                                    <b class="nowrap js-order-price-block">
                                                        <span class="js-order-price"></span>
                                                        {{ _t('р.', 'common') }}
                                                    </b>
                                                    <div class="js-order-show-is-middle-price order-cart__middle-block" style="display: none;">
                                                        <div class="order-cart__middle-price js-order-show-is-middle-price">
                                                            {!! _t('Средняя цена с учетом скидки', 'cart') !!}
                                                        </div>
                                                        <div class="order-cart__popover js-order-show-is-middle-price">
                                                            <div class="order-cart__popover-text">
                                                                {!! _t('Скидка распространяется только на изделия в наличии.', 'cart') !!}
                                                            </div>
                                                            <b><span class="js-order-middle-quantity-discount"></span> {!! _t('шт.', 'common') !!} {!! _t('по цене', 'cart') !!} <span class="js-order-middle-price-discount"></span> {!! _t('р.', 'cart') !!}</b>
                                                            <br>
                                                            <b><span class="js-order-middle-quantity"></span> {!! _t('шт.', 'common') !!} {!! _t('по цене', 'cart') !!} <span class="js-order-middle-price"></span> {!! _t('р.', 'cart') !!}</b>
                                                        </div>
                                                    </div>
                                                </span>
                                            </td>
                                            <td class="product-table__td product-table__td_price">
                                                <b class="nowrap js-order-price-total-block">
                                                    <span class="js-order-price-total"></span>
                                                    {{ _t('р.', 'cart') }}
                                                </b>
                                            </td>
                                        @endif
                                        <td class="product-table__td">
                                            <span>
                                                @component('components.icon', ['name' => 'cross', 'attributes' => ['class' => 'js-order-remove order-table__close', 'data-id' => $OrderItem->item->id, 'data-name' => $OrderItem->item->title, 'data-quantity' => $OrderItem->quant]])@endcomponent
                                            </span>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="mob-cart">
                            @foreach($OrderItems as $OrderItem)
                                <div class="mob-cart__item" data-id="{{ $OrderItem->item_id }}" data-color="{{ $OrderItem->color }}">
                                    <div class="mob-cart__header">
                                        <div class="mob-cart__article">
                                            <span class="mob-cart__article-title">{!! _t('Артикул', 'cart') !!}:</span>
                                            <span>{{ $OrderItem->title }}</span>
                                        </div>
                                        @component('components.icon', ['name' => 'close-thin', 'attributes' => ['class' => 'mob-cart__close js-mob-order-remove']])])@endcomponent
                                    </div>
                                    <div class="mob-cart__body">
                                        <div class="mob-cart__left mob-cart__image-wrap">
                                            <a href="{{ route('slug', ['slug' => $OrderItem->item->slug()]) }}" target="_blank">
                                                <img class="mob-cart__image" src="{{ $OrderItem->image ?: asset('images/dummy_95.png') }}" alt="{{ $OrderItem->item->title }}" height="83" width="83">
                                            </a>
                                        </div>
                                        <div class="mob-cart__right mob-cart__r-props-wrap">
                                            @if($OrderItem->promo)
                                                {!! _t('Бесплатные образцы', 'cart') !!}
                                            @else
                                                <div class="mob-cart__r-props">
                                                    <div class="mob-cart__r-props-title">
                                                        {!! _t('Цвет', 'cart') !!}
                                                    </div>
                                                    <div class="mob-cart__r-props-value">
                                                        <b>{{ _t(Color::cachedOne($OrderItem->color)->name, 'colors') }}</b>
                                                    </div>
                                                </div>
                                                <div class="mob-cart__r-props">
                                                    <div class="mob-cart__r-props-title">
                                                        {!! _t('Кол-во', 'cart') !!}
                                                    </div>
                                                    <div class="mob-cart__r-props-value">
                                                        <input class="product-table__input js-mob-order-quantity" value="{{ $OrderItem->quant }}" min="0" type="tel" />
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    @if($OrderItem->promo)
                                        <div class="mob-cart__free-text">
                                            {!! _t('Наш менеджер свяжется с вами, чтобы уточнить количество и цвет бесплатных образцов.', 'cart') !!}
                                        </div>
                                    @else
                                        <div class="mob-cart__props">
                                            <div class="mob-cart__left">
                                                {!! _t('Цена за штуку', 'cart') !!}
                                            </div>
                                            <div class="mob-cart__right">
                                                <b class="nowrap js-mob-order-price-block">
                                                    <span class="js-mob-order-price"></span>
                                                    {{ _t('р.', 'common') }}
                                                </b>
                                            </div>
                                            <div class="js-mob-order-show-is-middle-price mob-cart__middle-block" style="display: none;">
                                                <div class="mob-cart__middle-price">
                                                    {!! _t('Средняя цена с учетом скидки', 'cart') !!}
                                                </div>
                                                <div class="mob-cart__popover">
                                                    <div class="mob-cart__popover-text">
                                                        {!! _t('Скидка распространяется только на изделия в наличии.', 'cart') !!}
                                                    </div>
                                                    <b><span class="js-mob-order-middle-quantity-discount"></span> {!! _t('шт.', 'common') !!} {!! _t('по цене', 'cart') !!} <span class="js-mob-order-middle-price-discount"></span> {!! _t('р.', 'cart') !!}</b>
                                                    <br>
                                                    <b><span class="js-mob-order-middle-quantity"></span> {!! _t('шт.', 'common') !!} {!! _t('по цене', 'cart') !!} <span class="js-mob-order-middle-price"></span> {!! _t('р.', 'cart') !!}</b>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mob-cart__props">
                                            <div class="mob-cart__left">
                                                {!! _t('Сумма', 'cart') !!}
                                            </div>
                                            <div class="mob-cart__right">
                                                <b class="nowrap js-mob-order-price-total-block">
                                                    <span class="js-mob-order-price-total"></span>
                                                    {{ _t('р.', 'cart') }}
                                                </b>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!$OrderItem->getColor()->remains)
                                        <div class="mob-cart__not-available">
                                            <div class="mob-cart__not-available-icon">
                                                @component('components.icon', ['name' => 'attention'])@endcomponent
                                            </div>
                                            <div class="mob-cart__not-available-text">
                                                {!! _t('Обращаем ваше внимание, что этого товара нет в наличии. Срок поставки - до 3-х месяцев.', 'cart') !!}
                                                <br/>
                                                {!! _t('У нас есть <a class="link" href="%s">похожие товары</a> в наличии.', 'cart', [route('slug', ['slug' => $OrderItem->item->category->slug()]) . '?exist=1']) !!}
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            @endforeach
                        </div>
                        <div class="mob-cart__footer">
                            <div class="order-total">
                                <span class="order-total__amount">
                                    <span class="order-total__amount-title">{!! _t('Итого:', 'cart') !!}</span>
                                    <span class="order-total__amount-value">
                                        <span class="js-cart-price"></span>
                                        {!! _t('р.', 'cart') !!}
                                    </span>
                                </span>
                            </div>
                            <div class="order-table-footer">
                                <div>
                                    @if($CurrentCity && $CurrentCity->free_delivery_price)
                                        <div class="order__shipping-info js-order-shipping-info @if($freeDeliveryIsImpossible) order__shipping-info_primary @endif" data-value="{{ $CurrentCity->free_delivery_price }}">
                                            @component('components.icon', ['name' => 'car', 'attributes' => ['class' => 'order__shipping-icon']])])@endcomponent
                                            <div class="order__shipping-text">
                                                @if($freeDeliveryIsImpossible)
                                                    <span class="order__shipping-info-disabled">
                                                        {!! _t('Бесплатная доставка не распространяется на горки, канаты "Викинг", стальные цепи, сиденья и каркасы для стульев.', 'cart') !!}
                                                    </span>
                                                @else
                                                    <span class="order__shipping-info-active">
                                                        {!! _t('Ваш заказ на сумму более %s рублей. Доставка "до дверей" в городе %s бесплатно!', 'cart', [number_format($CurrentCity->free_delivery_price, 0, ' ', ' '), $CurrentCity ? $CurrentCity->title : '']) !!}
                                                    </span>
                                                        <span class="order__shipping-info-disabled">
                                                        {!! _t('До бесплатной доставки "до дверей" в городе %s в заказе не хватает товаров на сумму %s рублей.', 'cart', [$CurrentCity ? $CurrentCity->title : '', '<span class="js-order-shipping-price">' . number_format($CurrentCity->free_delivery_price, 0, ' ', ' ') . '</span>']) !!}
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <div class="order__buttons">
                                    <a class="btn btn_outline_primary order__btn" href="{{ route('order') }}">{!! _t('Оформить заказ', 'cart') !!}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="order__step js-order-step-cart-empty {{ (count($OrderItems) === 0) ? 'order__step_active' : ''}}">
                        <div class="order-empty">
                            <h2 class="order-empty__title">{!! _t('Ваша корзина пуста', 'cart') !!}</h2>
                            <p class="order-empty__text">{!! _t('Для добавления товаров в корзину <a href="%s" class="link">перейдите в каталог</a>.', 'cart', [route('catalog')]) !!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection