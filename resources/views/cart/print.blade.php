@php
    declare(strict_types=1);

    use App\Models\OrderItem;
    use App\Models\Color;

    $totalPrice = 0;

    Meta::set('title', _t('Корзина', 'cart'));
@endphp

@extends('layouts.print')

@section('content')
    @if(count($OrderItems))
        <table class="list">
            <thead>
            <tr>
                <th>№</th>
                <th>{!! _t('Фото', 'cart')  !!}</th>
                <th>{!! _t('Артикул', 'cart')  !!}</th>
                <th>{!! _t('Описание', 'cart') !!}</th>
                <th>{!! _t('Цвет', 'cart') !!}</th>
                <th class="nowrap">{!! _t('Кол-во штук', 'cart') !!}</th>
                <th>{!! _t('Оптовая группа', 'cart') !!}</th>
                <th class="nowrap">{!! _t('Цена за 1 штуку', 'cart') !!}</th>
                <th>{!! _t('Сумма', 'cart') !!}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($OrderItems as $OrderItem)
                <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>
                        @if($OrderItem->image)
                            <img src="{{ $OrderItem->image }}" alt="{{ $OrderItem->title }}" width="90">
                        @endif
                    </td>
                    <td>{{ $OrderItem->title }}</td>
                    <td>{{ $OrderItem->item->short_descr }}</td>
                    <td>{{ _t(Color::cachedOne($OrderItem->color)->name, 'colors') }}</td>
                    @if($OrderItem->promo)
                        <td colspan="4">{!! _t('Бесплатные образцы', 'cart') !!}</td>
                    @else
                        <td>{{ $OrderItem->quant }}</td>
                        <td>{!! OrderItem::getWholeSaleGroupText($OrderItem->getWholeSaleGroup()) !!}</td>
                        <td class="nowrap">{{ Formatter::price($OrderItem->price, true) }}</td>
                        <td class="nowrap">{{ Formatter::price(round($OrderItem->price, 1) * $OrderItem->quant, true) }}</td>
                    @endif
                    @php
                        $totalPrice += round($OrderItem->price, 1) * $OrderItem->quant;
                    @endphp
                </tr>
            @endforeach
            </tbody>
        </table>
        <p class="total">
            {{ _t('Стоимость продукции:', 'cart') }} {{ _t('%s руб.', 'cart', [Formatter::price($totalPrice)]) }}
        </p>
        @if(!$freeDeliveryIsImpossible && $CurrentCity && $CurrentCity->free_delivery_price)
            @if($totalPrice >= $CurrentCity->free_delivery_price)
                <p>
                    {!! _t('Ваш заказ на сумму более %s рублей. Доставка "до дверей" в городе %s бесплатно!', 'cart', [number_format($CurrentCity->free_delivery_price, 0, ',', ' '), $CurrentCity->title]) !!}
                </p>
            @else
                <p>
                    {!! _t('До бесплатной доставки "до дверей" в городе %s в заказе не хватает товаров на сумму %s рублей.', 'cart', [$CurrentCity->title, number_format($CurrentCity->free_delivery_price - $totalPrice, 0, ',', ' ')]) !!}
                </p>
            @endif
        @endif
        <p>{!! _t('Бесплатная доставка не распространяется на горки, канаты "Викинг", стальные цепи, сиденья и каркасы для стульев.', 'cart') !!}</p>
    @endif
@endsection