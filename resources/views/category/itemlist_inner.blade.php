@php
    declare(strict_types=1);

    use App\Models\Category;

    /** @var Category $Category */
@endphp

@if(count($Items) > 0)
    @component('item.list', ['Items' => $Items, 'firstItem' => $Items->firstItem()])@endcomponent
@else
    <div class="page">
        <div class="page__content page__content_background">
            {{ _t('Раздел наполняется', 'catalog') }}
        </div>
    </div>
@endif

@if(count($SimilarItems) > 0)
    <div class="page-header">
        <div class="page-header__title-wrap">
            <h2 class="page__title">{!! _t('Под выбранный размер также подходят:', 'catalog') !!}</h2>
        </div>
    </div>
    @foreach($SimilarItems as $parentCategoryId => $GroupItems)
        <div class="page-header">
            <div class="page-header__title-wrap">
                <h2 class="page__title">{{ Category::cachedOne($parentCategoryId)->getTitle() }}</h2>
            </div>
        </div>
        @php
            $catItems = new \Illuminate\Database\Eloquent\Collection();
        @endphp
        @foreach($GroupItems as $categoryId => $CategoryItems)
            @php
                $catItems = $catItems->merge($CategoryItems);
            @endphp
        @endforeach
        @component('item.list', ['Items' => $catItems, 'firstItem' => 1])
        @endcomponent
    @endforeach
@else
    {{ $Items->appends(Request::except('page'))->links('components.pagination') }}
@endif

@if((!Config::get('app.subdomain') || $Category->description_acceptable_for_subdomains) && count($Items) > 0 && $Items->currentPage() == 1)
    <div class="product-description">
        <div class="product-description__row">
            <h1 class="product-description__title">{{ $Category->getTitle(true) }}</h1>
            @if($Category->getDescription() && count($SimilarItems) == 0)
                <section class="product-description-block">
                    {!! $Category->getDescription() !!}
                </section>
            @endif
        </div>
    </div>
@endif
{{--


@if((!Config::get('app.subdomain')) && count($Items) > 0 && $Items->currentPage() == 1)
    <div class="product-description">
        <div class="product-description__row">
            <h1 class="product-description__title">{{ $CurrentTag->title_h1 }}</h1>
            @if($CurrentTag->descr)
                <section class="product-description-block">
                    {!! $CurrentTag->descr !!}
                </section>
            @endif
        </div>
    </div>
@endif
--}}
