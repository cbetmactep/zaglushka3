@php
    declare(strict_types=1);

    use App\Models\Category;

    /** @var Category $Category */
    /** @var Category $ParentCategory */

    Meta::set('title', $Category->getMetaTitle($Category));
    Meta::set('description', $Category->getMetaDescription($Category));
    Meta::set('keywords', $Category->getMetaKeywords($Category));

    if ($Category->isImageExists(Category::IMAGE_PATH_LIST)) {
        Meta::set('image', asset($Category->getImageLink(Category::IMAGE_PATH_LIST)));
    }

    $tagIds = array_filter(array_map(function ($elem) {return (int) $elem;}, explode(',', Request::get('tags', ''))), function($elem) {return $elem !== 0;});
    if (isset($CurrentTag)) {
        array_push($tagIds, $CurrentTag->id);
    }
@endphp

@extends('layouts.master')

@section('content')
    {!! Breadcrumbs::render('category', $Category) !!}
    @component('includes.breadcrumb', [
        'items' => [
            [
                'title' => $ParentCategory->getTitle(),
                'href' => route('slug', ['slug' => $ParentCategory->slug()])
            ],
            [
                'title' => $Category->getTitle()
            ]
        ],
        'pageActions' => true
    ])@endcomponent
    <div class="container">
                
                @php
                if($Category->getTitle()=="На болт/гайку") {
                    echo "<img src=\"/images/Banner_tpd_1.jpg\" alt=\"\" style=\"margin-top: 20px; margin-bottom:20px; display: block; margin-left:-15px; max-width: 1140px;\"/>";
                }
                @endphp
		
		<!-- УРЛ: {{\Request::getPathInfo() }}-->
		@if(\Request::getPathInfo()=="/kruglye-zaglushki/hromirovannye")
		    <img src="/images/Banner_2.jpg" alt="" style="margin-top: 20px; margin-bottom:20px; display: block; margin-left:-15px; max-width: 1140px;">
		@endif
                
		@if(\Request::getPathInfo()=="/kvadratnye-zaglushki/c-rezboi")
		    <img src="/images/banner_5.jpg" alt="" style="margin-top: 20px; margin-bottom:20px; display: block; margin-left:-15px; max-width: 1140px;">
		@endif
                

                @php
                if(\Request::url()=="/kvadratnye-zaglushki/c-rezboi") {
                    echo "<img src=\"/images/banner_5.jpg\" alt=\"\" style=\"margin-top: 20px; margin-bottom:20px; display: block; margin-left:-15px; max-width: 1140px;\"/>";
                }
                @endphp

                @php
                if(\Request::url()=="/kruglye-zaglushki/hromirovannye") {
                    echo "<img src=\"/images/Banner_2.jpg\" alt=\"\" style=\"margin-top: 20px; margin-bottom:20px; display: block; margin-left:-15px; max-width: 1140px;\"/>";
                }
                @endphp


		<!--<img src="/images/banner.jpg" alt="" style="margin: 20px auto; display: block">-->
        @component('includes.tags', ['Tags' => $TopTags, 'Category' => $Category, 'tagIds' => $tagIds])@endcomponent
        @component('components.main-catalog-mob', ['link' => route('catalog'), 'title' => $Category->getTitle(), 'filter' => count($tagIds)])@endcomponent
        @component('includes.mob_filter', ['Tags' => $TopTags, 'Category' => $Category, 'tagIds' => $tagIds])@endcomponent
        @if($Category->getFilterId())
            @component('components.product_table_filter', ['filter' => $Category->getFilterId(), 'class' => 'js-item-list-form'])@endcomponent
        @endif

        <div class="js-item-list">
            @component('category.itemlist_inner', ['Category' => $Category, 'Items' => $Items, 'SimilarItems' => $SimilarItems])@endcomponent
        </div>
    </div>
@endsection
