@php
    declare(strict_types=1);

    use App\Models\Category;

    Meta::set('title', _t('Список товаров', 'catalog'));
@endphp

@extends('layouts.print')

@section('content')
    @if(count($Items) > 0)
        @component('item.list_print', ['Items' => $Items]) @endcomponent
    @else
        <div class="page__content">
            <div class="page__content page__content_background">
                {{ _t('По вашему запросу ничего не найдено.', 'catalog') }}
            </div>
        </div>
    @endif

    @if(isset($SimilarItems) && count($SimilarItems) > 0)
        <h2 class="page__title">{!! _t('Под выбранный размер также подходят:', 'catalog') !!}</h2>
        @foreach($SimilarItems as $parentCategoryId => $GroupItems)
            <h2 class="page__title">{{ Category::cachedOne($parentCategoryId)->getTitle() }}</h2>
            @php
                $catItems = new \Illuminate\Database\Eloquent\Collection();
            @endphp
            @foreach($GroupItems as $categoryId => $CategoryItems)
                @php
                    $catItems = $catItems->merge($CategoryItems);
                @endphp
            @endforeach
            @component('item.list_print', ['Items' => $catItems, 'firstItem' => 1])
            @endcomponent
        @endforeach
    @endif

    <p>{!! _t('Все цены указаны в рублях и включают в себя НДС.', 'catalog') !!}</p>
    <p>{!! _t('Все цены указаны при условии 100%% предоплаты и действительны в течение 5 дней.', 'catalog') !!}</p>
    <p>{!! _t('Все цены указаны при условии самовывоза продукции со склада в г. Санкт-Петербург', 'catalog') !!}</p>
    <p>{!! _t('(для иногородних клиентов доставка до склада ТК в г. Санкт-Петербург бесплатна).', 'catalog') !!}</p>
    <p>{!! _t('Все цены указаны за стандартный цвет для каждого вида изделия', 'catalog') !!}</p>
    <p>{!! _t('(изменение цвета на любой из предлагаемой палитры +30%% к стоимости, изменение цвета на любой по системе RAL +50%% к стоимости).', 'catalog') !!}</p>
    <p>{!! _t('О возможных сроках поставки заказного товара, необходимо уточнять заранее.', 'catalog') !!}</p>
@endsection
