@php
    declare(strict_types=1);

    use App\Models\Category;

    /** @var Category $Category */
    /** @var Category $ParentCategory */

    $tagIds = array_filter(array_map(function ($elem) {return (int) $elem;}, explode(',', Request::get('tags', ''))), function($elem) {return $elem !== 0;});
    if (isset($CurrentTag)) {
        array_push($tagIds, $CurrentTag->id);
    }
@endphp

@extends('layouts.master')

@section('content')
    {!! Breadcrumbs::render('tags', $Category, $CurrentTag) !!}
    @component('includes.breadcrumb', [
        'items' => [
            [
                'title' => $ParentCategory->getTitle(),
                'href' => route('slug', ['slug' => $ParentCategory->slug()])
            ],
            [
                'title' => $Category->getTitle()
            ]
        ],
        'pageActions' => true
    ])@endcomponent
    <div class="container">


                <!-- УРЛ: {{\Request::getPathInfo() }}-->
                @if(\Request::getPathInfo()=="/kruglye-zaglushki/hromirovannye")
                    <img src="/images/Banner_2.jpg" alt="" style="margin-top: 20px; margin-bottom:20px; display: block; margin-left:-15px; max-width: 1140px;">
                @endif

                @php
                if(\Request::url()=="/kruglye-zaglushki/hromirovannye") {
                    echo "<img src=\"/images/Banner_2.jpg\" alt=\"\" style=\"margin-top: 20px; margin-bottom:20px; display: block; margin-left:-15px; max-width: 1140px;\"/>";
                }
                @endphp

                
                
                @if(\Request::getPathInfo()=="/rezbovye-reguliruemye/osnovanie-shestigrannik")
                    <img src="/images/banner_3.jpg" alt="" style="margin-top: 20px; margin-bottom:20px; display: block; margin-left:-15px; max-width: 1140px;">
                @endif

                @php
                if(\Request::url()=="/rezbovye-reguliruemye/osnovanie-shestigrannik") {
                    echo "<img src=\"/images/banner_3.jpg\" alt=\"\" style=\"margin-top: 20px; margin-bottom:20px; display: block; margin-left:-15px; max-width: 1140px;\"/>";
                }
                @endphp                
                
                @if(\Request::getPathInfo()=="/rezbovye-reguliruemye/krugloe-osnovanie")
                    <img src="/images/banner_3.jpg" alt="" style="margin-top: 20px; margin-bottom:20px; display: block; margin-left:-15px; max-width: 1140px;">
                @endif

                @php
                if(\Request::url()=="/rezbovye-reguliruemye/krugloe-osnovanie") {
                    echo "<img src=\"/images/banner_3.jpg\" alt=\"\" style=\"margin-top: 20px; margin-bottom:20px; display: block; margin-left:-15px; max-width: 1140px;\"/>";
                }
                @endphp

                
                
                @if(\Request::getPathInfo()=="/kruglye-zaglushki/c-rezboi")
                    <img src="/images/banner_4.jpg" alt="" style="margin-top: 20px; margin-bottom:20px; display: block; margin-left:-15px; max-width: 1140px;">
                @endif

                @php
                if(\Request::url()=="/kruglye-zaglushki/c-rezboi") {
                    echo "<img src=\"/images/banner_4.jpg\" alt=\"\" style=\"margin-top: 20px; margin-bottom:20px; display: block; margin-left:-15px; max-width: 1140px;\"/>";
                }
                @endphp
                
                @if(\Request::getPathInfo()=="/kvadratnye-zaglushki/c-rezboi")
                    <img src="/images/banner_5.jpg" alt="" style="margin-top: 20px; margin-bottom:20px; display: block; margin-left:-15px; max-width: 1140px;">
                @endif

                @php
                if(\Request::url()=="/kvadratnye-zaglushki/c-rezboi") {
                    echo "<img src=\"/images/banner_5.jpg\" alt=\"\" style=\"margin-top: 20px; margin-bottom:20px; display: block; margin-left:-15px; max-width: 1140px;\"/>";
                }
                @endphp

        @component('includes.tags', ['Tags' => $TopTags, 'CurrentTag' => $CurrentTag, 'Category' => $Category, 'tagIds' => $tagIds])@endcomponent
        @component('components.main-catalog-mob', ['link' => route('catalog'), 'title' => $CurrentTag->getTitle(), 'filter' => count($tagIds)])@endcomponent
        @component('includes.mob_filter', ['Tags' => $TopTags, 'CurrentTag' => $CurrentTag, 'Category' => $Category, 'tagIds' => $tagIds])@endcomponent
        @if($Category->getFilterId())
            @component('components.product_table_filter', ['filter' => $Category->getFilterId(), 'class' => 'js-item-list-form'])@endcomponent
        @endif

        <div class="js-item-list">
            @component('components.tags_subcategory', ['Items' => $Items, 'CurrentTag' => $CurrentTag])@endcomponent
        </div>

    </div>
@endsection
