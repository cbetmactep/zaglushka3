@php
    declare(strict_types=1);
@endphp

@extends('layouts.master')

@section('content')
    {!! Breadcrumbs::render('tags', $Category, $CurrentTag) !!}
    @component('includes.breadcrumb', [
        'items' => [
            [
                'title' => $Category->getTitle(),
                'href' => route('slug', ['slug' => $Category->slug()])
            ],
            [
                'title' => $CurrentTag->getTitle()
            ]
        ],
        'pageActions' => true
    ])@endcomponent
    <div class="container">
        @if(count($Items) > 0)
            @component('item.list', ['Items' => $Items, 'firstItem' => $Items->firstItem()])@endcomponent
        @else
            <div class="page">
                <div class="page__content page__content_background">
                    {{ _t('Ничего не найдено', 'catalog') }}
                </div>
            </div>
        @endif

            

                <!-- УРЛ: {{\Request::getPathInfo() }}-->
                @if(\Request::getPathInfo()=="/kruglye-zaglushki/hromirovannye")
                    <img src="/images/Banner_2.jpg" alt="" style="margin-top: 20px; margin-bottom:20px; display: block; margin-left:-15px; max-width: 1140px;">
                @endif;

                @php
                if(\Request::url()=="/kruglye-zaglushki/hromirovannye") {
                    echo "<img src=\"/images/Banner_2.jpg\" alt=\"\" style=\"margin-top: 20px; margin-bottom:20px; display: block; margin-left:-15px; max-width: 1140px;\"/>";
                }
                @endphp
    @php
                if(Request::url()=="kruglye-zaglushki/hromirovannye") {
                    echo "<img src=\"/images/Banner_2.jpg\" alt=\"\" style=\"margin-top: 20px; margin-bottom:20px; display: block; margin-left:-15px; max-width: 1140px;\"/>";
                }
                @endphp
        {{ $Items->appends(Request::except('page'))->links('components.pagination') }}

        @if((!Config::get('app.subdomain') || $Category->description_acceptable_for_subdomains) && count($Items) > 0 && $Items->currentPage() == 1)
            <div class="product-description">
                <div class="product-description__row">
                    <h1 class="product-description__title">{{ $CurrentTag->title_h1 }}</h1>
                    @if($CurrentTag->descr)
                        <section class="product-description-block">
                            {!! $CurrentTag->descr !!}
                        </section>
                    @endif
                </div>
            </div>
        @endif

        @if(App::isLocale('ru') && count($TopTags))
            <div class="p-catalog-tags">
                @foreach($TopTags as $Tag)
                    <a class="link" href="{{ route('tags', ['categorySlug' => $Category->slug(), 'tagSlug' => $Tag->slug()]) }}">{{ $Tag->title }}</a>
                @endforeach
            </div>
        @endif
    </div>
@endsection
