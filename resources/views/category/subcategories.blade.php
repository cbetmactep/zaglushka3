@php
    declare(strict_types=1);

    use App\Models\Category;

    /** @var Category $Category */
    /** @var Category $Subcategory */

    Meta::set('title', $Category->getMetaTitle($Category));
    Meta::set('description', $Category->getMetaDescription($Category));
    Meta::set('keywords', $Category->getMetaKeywords($Category));

    if ($Category->isImageExists(Category::IMAGE_PATH_LIST)) {
        Meta::set('image', asset($Category->getImageLink(Category::IMAGE_PATH_LIST)));
    }
@endphp

@extends('layouts.master')

@section('content')
    {!! Breadcrumbs::render('category', $Category) !!}
    @component('includes.breadcrumb', [
        'items' => [
            [
                'title' => $Category->getTitle()
            ]
        ]
    ])@endcomponent
    <div class="container">
        <div class="page">
			<!--<img src="/images/banner.jpg" alt="" style="margin: 10px auto 20px">-->

<!-- УРЛ: {{\Request::getPathInfo() }}-->
                @if(\Request::getPathInfo()=="/kvadratnye-zaglushki/c-rezboi")
                    <img src="/images/banner_5.jpg" alt="" style="margin-top: 20px; margin-bottom:20px; display: block; margin-left:-15px; max-width: 1140px;">
                @endif

                @php
                if(\Request::url()=="/kvadratnye-zaglushki/c-rezboi") {
                    echo "<img src=\"/images/banner_5.jpg\" alt=\"\" style=\"margin-top: 20px; margin-bottom:20px; display: block; margin-left:-15px; max-width: 1140px;\"/>";
                }
                @endphp
                
                <!-- УРЛ: {{\Request::getPathInfo() }}-->
                @if(\Request::getPathInfo()=="/kruglye-zaglushki/hromirovannye")
                    <img src="/images/Banner_2.jpg" alt="" style="margin-top: 20px; margin-bottom:20px; display: block; margin-left:-15px; max-width: 1140px;">
                @endif

                @php
                if(\Request::url()=="/kruglye-zaglushki/hromirovannye") {
                    echo "<img src=\"/images/Banner_2.jpg\" alt=\"\" style=\"margin-top: 20px; margin-bottom:20px; display: block; margin-left:-15px; max-width: 1140px;\"/>";
                }
                @endphp

            <div class="page__content">
                <div class="catalog">
                    @component('components.main-catalog-mob', ['link' => route('catalog'), 'title' => $Category->getTitle()])@endcomponent
                    <div class="catalog-item__mob-wraps">
                        @foreach($Category->subcategories as $Subcategory)
                            <div class="catalog-item__wrap">
                                <a class="catalog-item" href="{{ route('slug', ['slug' => $Subcategory->slug()]) }}">
                                    <div class="catalog-item__image">
                                        @if($Subcategory->isImageExists(Category::IMAGE_PATH_LIST))
                                            <img src="{{$Subcategory->getImageLink(Category::IMAGE_PATH_LIST)}}" width="160" height="160" alt="{{$Subcategory->title}}">
                                        @endif
                                    </div>
                                    <div class="catalog-item__name">
                                        <span>
                                            {{$Subcategory->getTitle()}}
                                        </span>
                                    </div>
                                    <div class="catalog-item__counter">
                                        @component('components.icon', ['name' => 'checkcart', 'attributes' => ['class' => 'catalog-item__counter-icon']])@endcomponent
                                        {!! _t('в наличии', 'common') !!} {{ Formatter::quantity($Subcategory->itemsCount()) }}
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        @if(App::isLocale('ru') && count($TopTags))
            <div class="p-catalog-tags">
                @foreach($TopTags as $Tag)
                    <a class="link" href="{{ route('tags', ['categorySlug' => $Category->slug(), 'tagSlug' => $Tag->slug()]) }}">{{ $Tag->title }}</a>
                @endforeach
            </div>
        @endif

        @if(!Config::get('app.subdomain') || $Category->description_acceptable_for_subdomains)
            <div class="p-catalog-description">
                <div class="p-catalog-description__row">
                    <h1 class="p-catalog-description__title">{{ $Category->getTitle() }}</h1>
                        {!! $Category->getDescription() !!}
                </div>
            </div>
        @endif
    </div>
@endsection