@php
    declare(strict_types=1);

    use App\Models\Category;

    /** @var Category $Category */
    /** @var Category $SubCategory */

     Meta::set('title', _t('Продукция', 'catalog'));
@endphp

@extends('layouts.master')

@section('content')
    {!! Breadcrumbs::render('catalog') !!}
    <div class="container">
        <div class="page">
			<!--<img src="/images/banner.jpg" alt="" style="margin: 20px auto">-->
            <div class="page__content page__content_background page__content_catalog">
                <div class="cat">
                    <div class="cat-menu__wrap">
                        <div class="cat-menu">
                            @foreach($TopCategories as $Category)
                                <div class="cat-menu__item">
                                    {{ $Category->getTitle() }}
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="cat__body">
                        @foreach($TopCategories as $Category)
                            <div class="cat-item">
                                <div class="cat-item__header">{{ $Category->getTitle() }}</div>
                                <div class="cat-item__list">
                                    @foreach($Category->subcategories as $SubCategory)
                                        <a class="cat-item__item" href="{{ route('slug', ['slug' => $SubCategory->slug()]) }}">
                                            <div class="cat-item__item-image">
                                                @if($SubCategory->isImageExists(Category::IMAGE_PATH_LIST))
                                                    <img src="{{$SubCategory->getImageLink(Category::IMAGE_PATH_LIST)}}" width="100" height="100" alt="{{ $SubCategory->getTitle() }}">
                                                @endif
                                            </div>
                                            <div class="cat-item__item-name">
                                                {{$SubCategory->getTitle()}}
                                                <div class="cat-item__tooltip">
                                                    <span class="nowrap">
                                                        @component('components.icon', ['name' => 'checkcart', 'attributes' => ['class' => 'cat-item__tooltip-icon']])@endcomponent
                                                        {!! _t('в наличии', 'common') !!}
                                                    </span>
                                                    <br/>
                                                    <span class="nowrap">
                                                        {{ Formatter::quantity($SubCategory->itemsCount()) }}
                                                    </span>
                                                </div>
                                            </div>
                                        </a>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="page__content page__content_catalog_mob">
                <div class="p-main-catalog__wrap_mob">
                    <section class="p-main-catalog">
                        <div class="p-main-catalog__row">
                            <h2 class="p-main-catalog__title">
                                <a class="p-main-catalog__link" href="{{ route('catalog') }}">{!! _t('Продукция', 'common') !!}</a>
                            </h2>
                        </div>
                        <div class="catalog">
                            @foreach($TopCategories as $category)
                                <div class="catalog-item__wrap">
                                    <a class="catalog-item" href="{{ route('slug', ['slug' => $category->slug()]) }}">
                                        <div class="catalog-item__image">
                                            @if($category->isBreadcrumbsImageExists())
                                                <img src="{{ $category->getBreadcrumbsImage() }}" height="160" alt="{{ $category->getTitle() }}">
                                            @elseif ($category->isImageExists(Category::IMAGE_PATH_LIST))
                                                <img src="{{ $category->getImageLink(Category::IMAGE_PATH_LIST) }}" height="160" alt="{{ $category->getTitle() }}">
                                            @endif
                                        </div>
                                        <div class="catalog-item__name">{{ $category->getTitle() }}</div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
@endsection
