@php
    declare(strict_types=1);
@endphp

<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    @include('includes.head_print')
    @include('includes.header_print')
    <body>
        <main>
            @yield('content')
        </main>
    </body>
</html>