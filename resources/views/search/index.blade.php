@php
	declare(strict_types=1);

    use App\Models\ItemSize;

    $hints = [
        'пластиковые горки',
        'резьбовые опоры',
        'хромированные заглушки',
        'фиксаторы М8',
        'качели',
        'латодержатели',
        'переходники для труб',
        'квадратные заглушки 20х20',
        'заглушки для отверстий',
    ];

	Meta::set('title', _t('Поиск', 'search'));
@endphp

@extends('layouts.master')

@section('content')
	{!! Breadcrumbs::render('search') !!}
	<div class="container">
		<div class="p-search-article p-search-article_no-title">
			<div class="p-search-article__wrap">
				<div class="p-search-article__title">{!! _t('Поиск по артикулу / названию', 'search') !!}</div>
				<form class="p-search-article__form">
					<input class="p-search-article__input js-search-article" autocomplete="off"
						   placeholder="{!! _t('Введите артикул или название, например:', 'search') . ' ' . _t($hints[mt_rand(0, count($hints) - 1)]) !!}" name="article">
					<button class="p-search-article__btn" disabled>{!! _t('Найти', 'search') !!}</button>
				</form>
			</div>
		</div>
		<div class="p-search">
			<div class="p-search-tab-header">
				<div class="p-search-tab-header__item p-search-tab-header__item_active">{!! _t('Поиск изделий по трубе / отверстию', 'search') !!}</div>
				<div class="p-search-tab-header__item" data-hash="product-parameters">{!! _t('Поиск по параметрам изделия', 'search') !!}</div>
			</div>
			<div class="p-search-tab">
				<div class="p-search-tab__item p-search-tab__item_active">
					<div class="p-search-item">
						<div class="p-search-item__title">{!! _t('Круглая', 'search') !!}</div>
						<div class="p-search-item__container">
							<form class="p-search-item__wrap p-search-item__wrap_narrow p-search-item__wrap_circle">
								<input type="hidden" name="type" value="1">
								<div class="p-search-item__image">
									{!! File::get(base_path() . '/public/images/search/circle.svg') !!}
								</div>
								<div class="p-search-item__inputs">
									<input class="js-search-size p-search-item__input" name="size_1" placeholder="20 {!! _t('мм', 'search') !!}">
									<div class="p-search-item__btn-wrap">
										<button class="p-search-item__btn" disabled>{!! _t('Найти', 'search') !!}</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="p-search-item">
						<div class="p-search-item__title">{!! _t('Квадратная', 'search') !!}</div>
						<div class="p-search-item__container">
							<form class="p-search-item__wrap p-search-item__wrap_narrow p-search-item__wrap_square">
								<input type="hidden" name="type" value="2">
								<div class="p-search-item__image">
									{!! File::get(base_path() . '/public/images/search/square.svg') !!}
								</div>
								<div class="p-search-item__inputs">
									<input class="js-search-size p-search-item__input" name="size_1" placeholder="20 {!! _t('мм', 'search') !!}">
									<div class="p-search-item__btn-wrap">
										<button class="p-search-item__btn" disabled>{!! _t('Найти', 'search') !!}</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="p-search-item">
						<div class="p-search-item__title">{!! _t('Прямоугольная', 'search') !!}</div>
						<div class="p-search-item__container">
							<form class="p-search-item__wrap p-search-item__wrap_narrow p-search-item__wrap_rectangle">
								<input type="hidden" name="type" value="3">
								<div class="p-search-item__image p-search-item__image_rectangle">
									{!! File::get(base_path() . '/public/images/search/rectangle.svg') !!}
								</div>
								<div class="p-search-item__inputs">
									<input class="js-search-size p-search-item__input" name="size_1" placeholder="20 {!! _t('мм', 'search') !!}">
									<input class="js-search-size p-search-item__input" name="size_2" placeholder="40 {!! _t('мм', 'search') !!}">
									<div class="p-search-item__btn-wrap">
										<button class="p-search-item__btn" disabled>{!! _t('Найти', 'search') !!}</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="p-search-item">
						<div class="p-search-item__title">{!! _t('Овальная', 'search') !!}</div>
						<div class="p-search-item__container">
							<form class="p-search-item__wrap p-search-item__wrap_narrow p-search-item__wrap_oval">
								<input type="hidden" name="type" value="4">
								<div class="p-search-item__image">
									{!! File::get(base_path() . '/public/images/search/oval.svg') !!}
								</div>
								<div class="p-search-item__inputs">
									<input class="js-search-size p-search-item__input" name="size_1" placeholder="20 {!! _t('мм', 'search') !!}">
									<input class="js-search-size p-search-item__input" name="size_2" placeholder="40 {!! _t('мм', 'search') !!}">
									<div class="p-search-item__btn-wrap">
										<button class="p-search-item__btn" disabled>{!! _t('Найти', 'search') !!}</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="p-search-item">
						<div class="p-search-item__title">{!! _t('Труба ДУ (DN)', 'search') !!}</div>
						<div class="p-search-item__container">
							<form class="p-search-item__wrap p-search-item__wrap_narrow p-search-item__wrap_circle-two">
								<input type="hidden" name="type" value="10">
								<div class="p-search-item__image">
									{!! File::get(base_path() . '/public/images/search/circle-two.svg') !!}
								</div>
								<div class="p-search-item__inputs">
									@component('components.search-select', ['size' => '1', 'sizes' => ItemSize::getSizes(10, 1)])])@endcomponent
									<div class="p-search-item__btn-wrap">
										<button class="p-search-item__btn" disabled>{!! _t('Найти', 'search') !!}</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="p-search-item">
						<div class="p-search-item__title">{!! _t('Эллипсоидная', 'search') !!}</div>
						<div class="p-search-item__container">
							<form class="p-search-item__wrap p-search-item__wrap_narrow p-search-item__wrap_ellipse">
								<input type="hidden" name="type" value="9">
								<div class="p-search-item__image">
									{!! File::get(base_path() . '/public/images/search/ellipse.svg') !!}
								</div>
								<div class="p-search-item__inputs">
									<input class="js-search-size p-search-item__input" name="size_1" placeholder="20 {!! _t('мм', 'search') !!}">
									<input class="js-search-size p-search-item__input" name="size_2" placeholder="40 {!! _t('мм', 'search') !!}">
									<div class="p-search-item__btn-wrap">
										<button class="p-search-item__btn" disabled>{!! _t('Найти', 'search') !!}</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="p-search-item">
						<div class="p-search-item__title">{!! _t('Полуовальная', 'search') !!}</div>
						<div class="p-search-item__container">
							<form class="p-search-item__wrap p-search-item__wrap_narrow p-search-item__wrap_semioval">
								<input type="hidden" name="type" value="5">
								<div class="p-search-item__image">
									{!! File::get(base_path() . '/public/images/search/semioval.svg') !!}
								</div>
								<div class="p-search-item__inputs">
									<input class="js-search-size p-search-item__input" name="size_1" placeholder="20 {!! _t('мм', 'search') !!}">
									<input class="js-search-size p-search-item__input" name="size_2" placeholder="40 {!! _t('мм', 'search') !!}">
									<div class="p-search-item__btn-wrap">
										<button class="p-search-item__btn" disabled>{!! _t('Найти', 'search') !!}</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="p-search-item">
						<div class="p-search-item__title">{!! _t('Отверстие', 'search') !!}</div>
						<div class="p-search-item__container">
							<form class="p-search-item__wrap p-search-item__wrap_narrow p-search-item__wrap_washers">
								<input type="hidden" name="type" value="11">
								<div class="p-search-item__image">
									{!! File::get(base_path() . '/public/images/search/washers.svg') !!}
								</div>
								<div class="p-search-item__inputs">
									<input class="js-search-size p-search-item__input" name="size_1" placeholder="20 {!! _t('мм', 'search') !!}">
									<div class="p-search-item__btn-wrap">
										<button class="p-search-item__btn" disabled>{!! _t('Найти', 'search') !!}</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="p-search-tab__item">
					<div class="p-search-item">
						<div class="p-search-item__title">{!! _t('Резьбовые опоры', 'search') !!}</div>
						<div class="p-search-item__container">
							<form class="p-search-item__wrap p-search-item__wrap_thread">
								<input type="hidden" name="type" value="6">
								<div class="p-search-item__image">
									{!! File::get(base_path() . '/public/images/search/thread.svg') !!}
								</div>
								<div class="p-search-item__inputs">
									@component('components.search-select', ['size' => '1', 'placeholder' => _t('Марка резьбы', 'search'), 'sizes' => ItemSize::getSizes(6, 1)])])@endcomponent
									@component('components.search-select', ['size' => '2', 'placeholder' => _t('Длина резьбы', 'search'), 'sizes' => ItemSize::getSizes(6, 2)])])@endcomponent
									@component('components.search-select', ['size' => '3', 'placeholder' => _t('Размер основания', 'search'), 'sizes' => ItemSize::getSizes(6, 3)])])@endcomponent
									<div class="p-search-item__btn-wrap">
										<button class="p-search-item__btn" disabled>{!! _t('Найти', 'search') !!}</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="p-search-item">
						<div class="p-search-item__title">{!! _t('Колпачки на болт / гайку', 'search') !!}</div>
						<div class="p-search-item__container">
							<form class="p-search-item__wrap p-search-item__wrap_nut">
								<input type="hidden" name="type" value="8">
								<div class="p-search-item__image">
									{!! File::get(base_path() . '/public/images/search/nut.svg') !!}
								</div>
								<div class="p-search-item__inputs">
									@component('components.search-select', ['size' => '1', 'sizes' => ItemSize::getSizes(8, 1)])])@endcomponent
									<div class="p-search-item__btn-wrap">
										<button class="p-search-item__btn" disabled>{!! _t('Найти', 'search') !!}</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="p-search-item">
						<div class="p-search-item__title">{!! _t('Фиксаторы с болтом', 'search') !!}</div>
						<div class="p-search-item__container">
							<form class="p-search-item__wrap p-search-item__wrap_penbolt">
								<input type="hidden" name="type" value="14">
								<div class="p-search-item__image">
									{!! File::get(base_path() . '/public/images/search/penbolt.svg') !!}
								</div>
								<div class="p-search-item__inputs">
									@component('components.search-select', ['size' => '1', 'placeholder' => _t('Марка резьбы', 'search'), 'sizes' => ItemSize::getSizes(14, 1)])])@endcomponent
									@component('components.search-select', ['size' => '2', 'placeholder' => _t('Длина резьбы', 'search'), 'sizes' => ItemSize::getSizes(14, 2)])])@endcomponent
									@component('components.search-select', ['size' => '3', 'placeholder' => _t('Размер основания', 'search'), 'sizes' => ItemSize::getSizes(14, 3)])])@endcomponent
									<div class="p-search-item__btn-wrap">
										<button class="p-search-item__btn" disabled>{!! _t('Найти', 'search') !!}</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="p-search-item">
						<div class="p-search-item__title">{!! _t('Фиксаторы с гайкой', 'search') !!}</div>
						<div class="p-search-item__container">
							<form class="p-search-item__wrap p-search-item__wrap_pennut">
								<input type="hidden" name="type" value="15">
								<div class="p-search-item__image">
									{!! File::get(base_path() . '/public/images/search/pennut.svg') !!}
								</div>
								<div class="p-search-item__inputs">
									@component('components.search-select', ['size' => '1', 'placeholder' => _t('Марка резьбы', 'search'), 'sizes' => ItemSize::getSizes(15, 1)])])@endcomponent
									@component('components.search-select', ['size' => '2', 'placeholder' => _t('Размер рукоятки', 'search'), 'sizes' => ItemSize::getSizes(15, 2)])])@endcomponent
									<div class="p-search-item__btn-wrap">
										<button class="p-search-item__btn" disabled>{!! _t('Найти', 'search') !!}</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="p-search-item">
						<div class="p-search-item__title">{!! _t('Защита наружной резьбы', 'search') !!}</div>
						<div class="p-search-item__container">
							<form class="p-search-item__wrap p-search-item__wrap_flush_holder_under">
								<input type="hidden" name="type" value="12">
								<div class="p-search-item__image">
									{!! File::get(base_path() . '/public/images/search/flush_holder_under.svg') !!}
								</div>
								<div class="p-search-item__inputs">
									@component('components.search-select', ['placeholder' => _t('Стандарт резьбы', 'search'), 'class' => 'js-filter-sizes', 'sizes' => [
										'1' => 'M',
										'3' => 'GAS/BSP',
										'5' => 'UNF/JIC',
									]])])@endcomponent
									@component('components.search-select', ['placeholder' => _t('Резьба', 'search'), 'sizes' => []])])@endcomponent
									@component('components.search-select', ['placeholder' => _t('Шаг резьбы', 'search'), 'sizes' => []])])@endcomponent
									<div class="p-search-item__btn-wrap">
										<button class="p-search-item__btn" disabled>{!! _t('Найти', 'search') !!}</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="p-search-item">
						<div class="p-search-item__title">{!! _t('Защита внутренней резьбы', 'search') !!}</div>
						<div class="p-search-item__container">
							<form class="p-search-item__wrap p-search-item__wrap_outer_thread">
								<input type="hidden" name="type" value="13">
								<div class="p-search-item__image">
									{!! File::get(base_path() . '/public/images/search/outer_thread.svg') !!}
								</div>
								<div class="p-search-item__inputs">
									@component('components.search-select', ['placeholder' => _t('Стандарт резьбы', 'search'), 'class' => 'js-filter-sizes', 'sizes' => [
										'1' => 'M',
										'3' => 'GAS/BSP',
										'5' => 'UNF/JIC',
									]])])@endcomponent
									@component('components.search-select', ['placeholder' => _t('Резьба', 'search'), 'sizes' => []])])@endcomponent
									@component('components.search-select', ['placeholder' => _t('Шаг резьбы', 'search'), 'sizes' => []])])@endcomponent
									<div class="p-search-item__btn-wrap">
										<button class="p-search-item__btn" disabled>{!! _t('Найти', 'search') !!}</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="p-search-item">
						<div class="p-search-item__title">{!! _t('Латодержатели', 'search') !!}</div>
						<div class="p-search-item__container">
							<form class="p-search-item__wrap p-search-item__wrap_flush_holder">
								<input type="hidden" name="type" value="18">
								<div class="p-search-item__image">
									{!! File::get(base_path() . '/public/images/search/flush_holder.svg') !!}
								</div>
								<div class="p-search-item__inputs">
									@component('components.search-select', ['size' => '1', 'placeholder' => _t('Ширина латы', 'search'), 'sizes' => ItemSize::getSizes(18, 1)])])@endcomponent
									@component('components.search-select', ['size' => '2', 'placeholder' => _t('Толщина латы', 'search'), 'sizes' => ItemSize::getSizes(18, 2)])])@endcomponent
									<div class="p-search-item__btn-wrap">
										<button class="p-search-item__btn" disabled>{!! _t('Найти', 'search') !!}</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="p-search-item">
						<div class="p-search-item__title">{!! _t('Шайбы', 'search') !!}</div>
						<div class="p-search-item__container">
							<form class="p-search-item__wrap p-search-item__wrap_shim">
								<input type="hidden" name="type" value="17">
								<div class="p-search-item__image">
									{!! File::get(base_path() . '/public/images/search/shim.svg') !!}
								</div>
								<div class="p-search-item__inputs">
									<input class="js-search-size p-search-item__input" name="size_1" placeholder="8 {!! _t('мм', 'search') !!}">
									<input class="js-search-size p-search-item__input" name="size_2" placeholder="20 {!! _t('мм', 'search') !!}">
									<div class="p-search-item__btn-wrap">
                                        <button class="p-search-item__btn" disabled>{!! _t('Найти', 'search') !!}</button>
                                    </div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection