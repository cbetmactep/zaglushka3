@php
    declare(strict_types=1);

    use App\Models\Category;

    Meta::set('title', _t('Результаты поиска', 'search'));
@endphp

@extends('layouts.print')

@section('content')

    @if(count($Items) > 0)
        @foreach($Items as $parentCategoryId => $GroupItems)
            @php
                $catItems = new \Illuminate\Database\Eloquent\Collection();
            @endphp
            @foreach($GroupItems as $categoryId => $CategoryItems)
                @php
                    $catItems = $catItems->merge($CategoryItems);
                @endphp
            @endforeach
            <h1>{{ Category::cachedOne($parentCategoryId)->title }}</h1>
            @component('item.list_print', ['Items' => $catItems, 'firstItem' => 1])
            @endcomponent
        @endforeach
    @else
        {{ _t('По вашему запросу ничего не найдено.', 'search') }}
    @endif

@endsection