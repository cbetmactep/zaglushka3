@php
    declare(strict_types=1);

    Meta::set('title', _t('Результаты поиска', 'search'));
@endphp

@extends('layouts.master')

@section('content')
    {!! Breadcrumbs::render('search') !!}
    <div class="container">
        <div class="page-header">
            <div class="page-header__title-wrap">
                <h1 class="page__title">{!! _t('Результаты поиска', 'search') !!}</h1>
            </div>
            <div class="page-header__actions">
                @component('includes.page_actions')@endcomponent
            </div>
        </div>
        @if(Request::get('type') > 0)
            @component('components.product_table_filter', ['filter' => (int) Request::get('type'), 'class' => 'js-item-list-form'])@endcomponent
        @else
            <div class="p-search-article">
                <div class="p-search-article__wrap">
                    <div class="p-search-article__title">{!! _t('Поиск по артикулу / названию', 'search') !!}</div>
                    <form class="p-search-article__form">
                        <input class="p-search-article__input js-search-article" autocomplete="off" placeholder="{!! _t('Введите артикул или название, например: заглушки для мебели', 'search') !!}" name="article" value="{{ Request::get('article') }}">
                        <button class="p-search-article__btn">{!! _t('Найти', 'search') !!}</button>
                    </form>
                    <div class="p-search-article__text">
                        {!! _t('Не нашли то, что искали? Воспользуйтесь <a href="%s" class="link">расширенным поиском</a>.', 'search', [route('search')]) !!}
                    </div>
                </div>
            </div>
        @endif

        <div class="js-item-list">
            @component('components.search', ['Items' => $Items])@endcomponent
        </div>
    </div>
@endsection