@php
    declare(strict_types=1);

    use App\Models\Item;
@endphp

<table class="list">
    <thead>
    <tr>
        <th rowspan="2">№</th>
        <th rowspan="2">{!! _t('Артикул', 'item')  !!}</th>
        <th rowspan="2">{!! _t('Схема', 'item') !!}</th>
        <th rowspan="2">{!! _t('Фото', 'item') !!}</th>
        <th colspan="6">{!! _t('Цена, руб. за 1 шт.', 'item') !!}</th>
        <th rowspan="2">{!! _t('Упаковка', 'item') !!}</th>
    </tr>
    <tr>
        <th>{!! _t('более 10 000', 'item') !!}</th>
        <th>{!! _t('от 5 000 до 10 000', 'item') !!}</th>
        <th>{!! _t('от 1 000 до 5 000', 'item') !!}</th>
        <th>{!! _t('от 500 до 1 000', 'item') !!}</th>
        <th>{!! _t('от 100 до 500', 'item') !!}</th>
        <th>{!! _t('розница до 100', 'item') !!}</th>
    </tr>
    </thead>
    <tbody>

    @foreach($Items as $Item)
        <tr>
            <td>
                {{ $loop->index + 1 }}
            </td>
            @php
                $remains = $Item->remains();
            @endphp
            <td>
                {{ $Item->title }}
                @if($remains)
                    <br />
                    <span class="text_color_green text_size_smile">{!! _t('в наличии', 'item') !!}</span>
                @endif
            </td>
            <td>
                @if($Item->isImageExists(Item::IMAGE_TYPE_18))
                    <img src="{{ (Request::get('excel') ? public_path() : '') . $Item->getImageLink(Item::IMAGE_TYPE_18) }}" alt="Схема {{ $Item->title }}">
                @endif
            </td>
            <td>
                @if($Item->isImageExists(Item::IMAGE_TYPE_5))
                    <img src="{{ (Request::get('excel') ? public_path() : '') . $Item->getImageLink(Item::IMAGE_TYPE_5) }}" alt="{{ $Item->title }}">
                @endif
            </td>
            <td @if($Item->special && $Item->getSpecialDiscount(5))class="price-specials" @endif>
                @if ($Item->special && $Item->getSpecialDiscount(5))
                    <div><s>{{ Formatter::price($Item->price5, true) }}</s></div>
                    <div class="price-discount @if($Item->getSpecialDiscount(5) === 50) price-discount_hot @endif">-{{ $Item->getSpecialDiscount(5) }}%</div>
                    {{ Formatter::price($Item->getDiscountPrice($Item->getSpecialDiscount(5), $Item->price5), true) }}
                @else
                    {{ Formatter::price($Item->price5, true) }}
                @endif
            </td>
            <td @if($Item->special && $Item->getSpecialDiscount(4))class="price-specials" @endif>
                @if ($Item->special && $Item->getSpecialDiscount(4))
                    <div><s>{{ Formatter::price($Item->price4, true) }}</s></div>
                    <div class="price-discount @if($Item->getSpecialDiscount(4) === 50) price-discount_hot @endif">-{{ $Item->getSpecialDiscount(4) }}%</div>
                    {{ Formatter::price($Item->getDiscountPrice($Item->getSpecialDiscount(4), $Item->price4), true) }}
                @else
                    {{ Formatter::price($Item->price4, true) }}
                @endif
            </td>
            <td @if($Item->special && $Item->getSpecialDiscount(3))class="price-specials" @endif>
                @if ($Item->special && $Item->getSpecialDiscount(3))
                    <div><s>{{ Formatter::price($Item->price3, true) }}</s></div>
                    <div class="price-discount @if($Item->getSpecialDiscount(3) === 50) price-discount_hot @endif">-{{ $Item->getSpecialDiscount(3) }}%</div>
                    {{ Formatter::price($Item->getDiscountPrice($Item->getSpecialDiscount(3), $Item->price3), true) }}
                @else
                    {{ Formatter::price($Item->price3, true) }}
                @endif
            </td>
            <td @if($Item->special && $Item->getSpecialDiscount(2))class="price-specials" @endif>
                @if ($Item->special && $Item->getSpecialDiscount(2))
                    <div><s>{{ Formatter::price($Item->price2, true) }}</s></div>
                    <div class="price-discount @if($Item->getSpecialDiscount(2) === 50) price-discount_hot @endif">-{{ $Item->getSpecialDiscount(2) }}%</div>
                    {{ Formatter::price($Item->getDiscountPrice($Item->getSpecialDiscount(2), $Item->price2), true) }}
                @else
                    {{ Formatter::price($Item->price2, true) }}
                @endif
            </td>
            <td @if($Item->special && $Item->getSpecialDiscount(1))class="price-specials" @endif>
                @if ($Item->special && $Item->getSpecialDiscount(1))
                    <div><s>{{ Formatter::price($Item->price1, true) }}</s></div>
                    <div class="price-discount @if($Item->getSpecialDiscount(1) === 50) price-discount_hot @endif">-{{ $Item->getSpecialDiscount(1) }}%</div>
                    {{ Formatter::price($Item->getDiscountPrice($Item->getSpecialDiscount(1), $Item->price1), true) }}
                @else
                    {{ Formatter::price($Item->price1, true) }}
                @endif
            </td>
            <td @if($Item->special && $Item->getSpecialDiscount(0))class="price-specials" @endif>
                @if ($Item->special && $Item->getSpecialDiscount(0))
                    <div><s>{{ Formatter::price($Item->price0, true) }}</s></div>
                    <div class="price-discount @if($Item->getSpecialDiscount(0) === 50) price-discount_hot @endif">-{{ $Item->getSpecialDiscount(0) }}%</div>
                    {{ Formatter::price($Item->getDiscountPrice($Item->getSpecialDiscount(0), $Item->price0), true) }}
                @else
                    {{ Formatter::price($Item->price0, true) }}
                @endif
            </td>
            <td>{{ $Item->pack_size ? $Item->pack_size . ' ' . _t('шт.', 'common') : '' }}</td>
        </tr>
    @endforeach
    </tbody>
</table>