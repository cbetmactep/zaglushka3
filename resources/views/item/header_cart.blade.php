@php
    declare(strict_types=1);
   
@endphp
<div class="header-cart" id="headerCart">
    <div class="header-cart__body">
        <div class="header-cart__header">
            <div class="header-cart__title">
                <div class="header-cart__header-icon">
                    @component('components.icon', ['name' => 'checkcart'])])@endcomponent
                </div>
                <span>{!! _t('Товар добавлен в корзину', 'item') !!}</span>
            </div>
            @component('components.icon', ['name' => 'close-thin', 'attributes' => ['class' => 'header-cart__close']])])@endcomponent
        </div>
        <div>
            <table class="header-cart__table">
                <thead>
                    <tr>
                        <th colspan="2"></th>
                        <th>{!! _t('Цвет', 'item') !!}</th>
                        <th>{!! _t('Кол-во', 'item') !!}</th>
                        <th>{!! _t('Сумма', 'item') !!}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <img class="header-cart__table-img" src="">
                        </td>
                        <td><span class="header-cart__table-descr"></span></td>
                        <td class="js-header-cart__color"></td>
                        <td class="header-cart__nowrap"><span class="js-header-cart__count"></span> {{ _t('шт.', 'item') }}</td>
                        <td class="header-cart__nowrap"><span class="js-header-cart__price-block"><span class="js-header-cart__price"></span> {!! _t('р.', 'item') !!}</span></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="header-cart__footer" style="flex-flow: wrap">
            <div class="header-cart__delivery">
                <div class="header-cart__delivery-text header-cart__delivery-text_active">
                    @component('components.icon', ['name' => 'car', 'attributes' => ['class' => 'header-cart__delivery-icon']])])@endcomponent
                    <span class="header-cart__delivery-text-disabled">{!! _t('Доставка «до дверей» в %s бесплатно!', 'item', []) !!}</span>
                    <span class="header-cart__delivery-text-active">{!! _t('Для бесплатной доставки добавьте в корзину товаров на %s ', 'item', ['<span class="header-cart__delivery-text-value"></span>']) !!}{!! _t('р.', 'item') !!}</span>
                </div>
            </div>
            <div style="display: flex; justify-content: space-between; padding-top:10px; padding-bottom: 10px; width:100%;">
            <a href="#" class="btn btn_outline_primary header-cart__btn header-cart__close header-cart__close-btn" style="flex-basis: 48%;">{!! _t('Продолжить покупки', 'item') !!}</a>
            <a href="{{ route('cart') }}" class="btn btn_outline_primary header-cart__btn js-popup_checkout" style="flex-basis: 48%;">{!! _t('Оформить заказ', 'item') !!}</a>
            </div>
        </div>
    </div>
</div>
<div class="header-cart header-cart_free" id="headerCartFree">
    <div class="header-cart__body">
        <div class="header-cart__header">
            <div class="header-cart__title">
                <div class="header-cart__header-icon">
                    @component('components.icon', ['name' => 'checkcart'])])@endcomponent
                </div>
                <span>{!! _t('Бесплатные образцы добавлены в корзину', 'item') !!}</span>
            </div>
        </div>
        <div class="header-cart__footer-free">
            <a href="{{ route('cart') }}" class="btn btn_outline_primary header-cart__btn">{!! _t('Оформить заказ', 'item') !!}</a>
        </div>
    </div>
</div>