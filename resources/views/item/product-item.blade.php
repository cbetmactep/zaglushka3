@php
    declare(strict_types=1);

    use App\Models\Color;

    $hexIsGradient = strpos(Color::cachedOne($itemColor->color)->hex, ':');
    if($hexIsGradient) {
        $hexs = explode(':', Color::cachedOne($itemColor->color)->hex);
    } else {
        $hexs = explode(',', Color::cachedOne($itemColor->color)->hex);
    }
@endphp
<tr data-color="{{ $itemColor->color }}">
    <td>
        @if(Color::cachedOne($itemColor->color)->hex)
            @component('components.product-item-color-box', ['hex' => Color::cachedOne($itemColor->color)->hex])@endcomponent
        @endif
    </td>
    <td class="product-item-availability__key">
        {{ _t(Color::cachedOne($itemColor->color)->name, 'colors') }}
        @if (Auth::check() && (Auth::user()->isManager() || Auth::user()->isContent()))
            ({{ _t('арт.', 'item') }} {{ $itemColor->code }})
        @endif
    </td>
    @if($itemColor->remains || count($TransitItems) > 0)
        <td class="product-item-availability__value">
            {{ Formatter::quantity($itemColor->remains) }}
        </td>
    @endif
    @if(count($TransitItems) > 0)
        @php
            $dates = [];
            $noDateQ = 0;
        @endphp
        @foreach($TransitItems as $TransitItem)
            @if(strtotime($TransitItem->date) <= time())
                @php
                    $noDateQ += $TransitItem->quant;
                @endphp
            @else
                @php
                    if (!isset($dates[$TransitItem->date])) {
                        $dates[$TransitItem->date] = 0;
                    }

                    $dates[$TransitItem->date] += $TransitItem->quant
                @endphp
            @endif
        @endforeach
        @foreach($dates as $key => $value)
            @if($loop->iteration === 1)
                <td class="product-item-availability__transit">
            @else
                <tr>
                    <td colspan="3"></td>
                    <td class="product-item-availability__transit">
            @endif
                + {{ _t('едет', 'item') }}
                {{ number_format($value, 0, ',', ' ') }} {{ _t('шт.', 'common') }}
                ({{ strtotime($key) ? date('d.m', strtotime($key)) : date('d.m')}})
            @if($loop->iteration === 1)
                </td>
            @else
                    </td>
                </tr>
            @endif
        @endforeach
        @if($noDateQ > 0)
            @if(count($dates) === 0)
                <td class="product-item-availability__transit">
            @else
                <tr>
                    <td colspan="3"></td>
                    <td class="product-item-availability__transit">
            @endif
                + {{ _t('едет', 'item') }}
                {{ Formatter::quantity($noDateQ) }}

            @if(count($dates) === 0)
                </td>
            @else
                    </td>
                </tr>
            @endif
        @endif
    @endif
</tr>