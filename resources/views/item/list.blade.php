@php
    declare(strict_types=1);

    use App\Models\Item;
    use App\Models\Color;
    use App\Models\ItemSame;
    use App\Models\ItemColor;

    /** @var Item[] $Items */
    /** @var Item $Item */
@endphp
        @foreach($Items as $key => $Item)
            @php
                $countRow = 0;
            @endphp
            @foreach($Item->itemCachedColors() as $itemColor)
                @php
                    $TransitItems = $itemColor->getTransitItems();
                @endphp
                @if($itemColor->remains || count($TransitItems) > 0)
                    @php
                        $countRow++;
                    @endphp
                @endif
                @if(count($TransitItems) > 0)
                    @php
                        $countRow++;
                    @endphp
                @endif
                @if ($countRow > 4)
                    @break;
                @endif
            @endforeach
            @if($countRow==0)
                @php
                    unset($Items[$key]);
                    $Items[$key]=$Item;
                @endphp
            @endif
        @endforeach
<div class="product-table__wrap">
    <table class="product-table js-product-table-fix-head">
        <thead class="product-table__thead">
            <tr>
                <th class="product-table__th product-table__th_number" rowspan="2">№</th>
                <th class="product-table__th product-table__th_article" rowspan="2">{!! _t('Артикул', 'item') !!}</th>
                <th class="product-table__th" rowspan="2" colspan="2"></th>
                <th class="product-table__th product-table__th_price product-table__th_price_center" colspan="6">{!! _t('Цена за 1 шт.', 'item') !!}</th>
                <th class="product-table__th product-table__th_colors" rowspan="2">{!! _t('Наличие', 'item') !!}</th>
            </tr>
            <tr>
                <th class="product-table__th product-table__th_price">{!! _t('от', 'item') !!} <span class="nowrap">10 000</span></th>
                <th class="product-table__th product-table__th_price">{!! _t('от', 'item') !!} <span class="nowrap">5 000</span></th>
                <th class="product-table__th product-table__th_price">{!! _t('от', 'item') !!} <span class="nowrap">1 000</span></th>
                <th class="product-table__th product-table__th_price">{!! _t('от', 'item') !!} 500</th>
                <th class="product-table__th product-table__th_price">{!! _t('от', 'item') !!} 100</th>
                <th class="product-table__th product-table__th_price">{!! _t('розница', 'item') !!}</th>
            </tr>
        </thead>
        <tbody>
        @foreach($Items as $key => $Item)
            @php
                $countRow = 0;
            @endphp
            @foreach($Item->itemCachedColors() as $itemColor)
                @php
                    $TransitItems = $itemColor->getTransitItems();
                @endphp
                @if($itemColor->remains || count($TransitItems) > 0)
                    @php
                        $countRow++;
                    @endphp
                @endif
                @if(count($TransitItems) > 0)
                    @php
                        $countRow++;
                    @endphp
                @endif
                @if ($countRow > 4)
                    @break;
                @endif
            @endforeach

            <tr class="product-table__tr"
                data-id="{{ $Item->id }}"
                data-color="{{ $Item->color ? $Item->color->color : ($Item->mainColor() ? $Item->mainColor()->color : 1) }}"
            >
                <td class="product-table__td product-table__td_number">
                    <a class="product-table__number" href="{{ route('slug', ['slug' => $Item->slug()]) }}">
                        <span>{{ $loop->index + $firstItem }}</span>
                    </a>
                </td>
                <td class="product-table__td product-table__td_article">
                    @if($Item->name)
                        <div class="product-table__name">{{ $Item->name }}</div>
                    @endif
                    <a class="product-table__article" href="{{ route('slug', ['slug' => $Item->slug()]) }}">{{ $Item->title }}</a>
                    @if(App::isLocale('ru') && $Item->under_title)
                        <div>
                            <div class="product-table__promo @if(isset($Item->under_title_color) && $Item->under_title) product-table__promo_{{ $Item->under_title_color }} @endif">
                                {!! nl2br($Item->under_title) !!}
                            </div>
                        </div>
                    @endif
                    @if($Item->recommend && App::isLocale('ru') && false)
                        <img src="{{ asset('/images/recommend.png') }}">
                    @endif
                </td>
                @component('components.product-table-td-image', [
                    'link' => route('slug', ['slug' => $Item->slug()]),
                    'images' => [
                        [
                            'link' => $Item->isImageExists(Item::IMAGE_TYPE_18) ? $Item->getImageLink(Item::IMAGE_TYPE_18) : null,
                            'alt' => _t('Схема', 'item') . ' ' . $Item->title,
                            'sizes' => [110, 95],
                        ],
                        [
                            'link' => $Item->isImageExists(Item::IMAGE_TYPE_5) ? $Item->getImageLink(Item::IMAGE_TYPE_5) : null,
                            'alt' => $Item->title,
                        ],
                    ]
                ])])@endcomponent
                @if($Item->only_two_prices)
                    @component('components.product-table-td-image', [
                        'link' => route('slug', ['slug' => $Item->slug()]),
                        'images' => [
                            [
                                'link' => $Item->isImageExists(Item::IMAGE_TYPE_31) ? $Item->getImageLink(Item::IMAGE_TYPE_31) : null,
                                'alt' => _t('Схема', 'item') . ' ' . $Item->title,
                                'colspan' => 2,
                                'noDummy' => true,
                            ],
                            [
                                'link' => $Item->isImageExists(Item::IMAGE_TYPE_32) ? $Item->getImageLink(Item::IMAGE_TYPE_32) : null,
                                'alt' => $Item->title,
                                'colspan' => 2,
                                'noDummy' => true,
                            ],
                        ]
                    ])])@endcomponent
                @endif
                @for($i = 5; $i >= 0; $i--)
                    @if($i < 2 || !$Item->only_two_prices)
                        <td class="product-table__td product-table__td_price">
                            <div class="product-table__price-text @if(max($Item->price0, $Item->price1, $Item->price2, $Item->price3, $Item->price4, $Item->price5) > 999999) product-table__price-text_small @endif">
                                @if ($Item->getSpecialDiscount($i))
                                    <div class="product-table__price-old">{{ Formatter::price($Item['price' . $i], true) }}</div>
                                    <span class="product-table__discount @if($Item->getSpecialDiscount($i) === 50)product-table__discount_active @endif">−{{ $Item->getSpecialDiscount($i) }} %</span>
                                    <div>{{ Formatter::price($Item->getDiscountPrice($i), true) }}</div>
                                @else
                                    {{ Formatter::price($Item['price' . $i], true) }}
                                @endif
                                @if($i === 5 && $Item->price6 > 0)
                                    <div class="product-table__seven">
                                        {!! _t('цена %s при заказе от %s штук', 'item', ['<b>' . Formatter::price($Item->price6, true) . '</b>', '<span class="nowrap">30 000</span>']) !!}
                                    </div>
                                @endif
                            </div>
                        </td>
                    @endif
                @endfor
                <td class="product-table__td product-table__td_colors" style="min-width: 240px; max-width:240px; width: 240px;">
                    <div class="product-table-colors @if($countRow > 4) product-table-colors_narrowed @endif ">
                        @if($countRow)
                            <div class="product-table-colors__wrap">
                                <div class="product-table-colors__list">
                                    @foreach($Item->itemCachedColors() as $itemColor)
                                        @php
                                            $TransitItems = $itemColor->getTransitItems();
                                        @endphp
                                        @if($itemColor->remains || count($TransitItems) > 0)
                                            <div class="product-table-colors__item">
                                                <div class="product-table-colors__key">
                                                    @component('components.product-item-color-box', ['hex' => Color::cachedOne($itemColor->color)->hex])@endcomponent
                                                    {{ _t(Color::cachedOne($itemColor->color)->name, 'colors') }}
                                                </div>
                                                <div class="product-table-colors__value">
                                                    {{ Formatter::quantity($itemColor->remains) }}
                                                </div>
                                            </div>
                                        @endif
                                        @if(count($TransitItems) > 0)
                                            @php
                                                $dates = [];
                                                $quant = 0;
                                            @endphp
                                            @foreach($TransitItems as $TransitItem)
                                                @php
                                                    $quant += $TransitItem->quant;
                                                @endphp
                                            @endforeach
                                            @if($quant > 0)
                                                <div class="product-table-colors__item product-table-colors__item_dummy">
                                                    <div class="product-table-colors__key product-table-colors__key_dummy">
                                                        + {{ _t('едет', 'item') }}
                                                    </div>
                                                    <div class="product-table-colors__value">
                                                        {{ Formatter::quantity($quant) }}
                                                    </div>
                                                </div>
                                            @endif
                                        @endif
                                    @endforeach
                                </div>
                                @if($countRow > 4)
                                    <div class="product-table-colors__more">
                                        <span class="product-table-colors-active">{{ _t('свернуть', 'item') }}</span>
                                        <span class="product-table-colors-disabled">{{ _t('другие цвета', 'item') }}</span>
                                        @component('components.icon', ['name' => 'arrow-thin', 'attributes' => ['class' => 'product-table-colors__icon']])])@endcomponent
                                    </div>
                                @endif
                            </div>
                        @else
                            <div class="product-table-colors__not-available">
                                <div class="product-table-colors__not-available-active">
                                    {{ _t('В производстве', 'item') }}
                                </div>
                                <div class="product-table-colors__not-available-disabled">
                                    {{ _t('Срок поставки', 'item') }}
                                    <br/>
                                    @if($Item->preorder_availability_days_min || $Item->preorder_availability_days_max)
                                        {{ Formatter::preOrderAvailability($Item->preorder_availability_days_min, $Item->preorder_availability_days_max) }}
                                    @else
                                        {!! _t('спрашивайте у менеджера', 'item') !!}
                                    @endif
                                </div>
                            </div>
                        @endif
                    </div>
                </td>
                <td class="product-table__td product-table__td_add_to_cart" style="min-width:60px; max-width: 60px; width: 60px;">
                    <a class="js-order-modal product-item-table__btn product-item-table__cart" style="border: 0px; background: #fff;padding: 0px;" data-goodId="{{$Item->id}}" style="color: #c54004 !important;">
                        <img src="{{ asset('/images/cart.svg') }}" style="width:20px; height: 20px;">
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@section('header')
    @include('item.header_cart', ['Item' => NULL])
@endsection

@php
    $priceTitle = [
        _t('розница', 'item'),
        _t('от', 'item') . ' 100',
        _t('от', 'item') . ' 500',
        _t('от', 'item') . ' 1 000',
        _t('от', 'item') . ' 5 000',
        _t('от', 'item') . ' 10 000',
        _t('от', 'item') . ' 30 000',
    ];
@endphp
<div class="mob-product__list">
    @foreach($Items as $Item)
        <div class="mob-product">
            <div class="mob-product__header">
                <div class="mob-product__index">
                    <span>
                        {{ $loop->index + $firstItem }}
                    </span>
                </div>
                <div class="mob-product__article-text">
                    {{ _t('Артикул:', 'item') }}
                </div>
                <div class="mob-product__article">
                    {{ $Item->title }}
                </div>
            </div>
            <div class="mob-product__body">
                <a class="mob-product__images" href="{{ route('slug', ['slug' => $Item->slug()]) }}">
                    @if($Item->isImageExists(Item::IMAGE_TYPE_5))
                        <img class="mob-product__image" src="{{ $Item->getImageLink(Item::IMAGE_TYPE_5) }}" alt="{{ $Item->title }}" loading="lazy">
                    @endif
                    @if($Item->isImageExists(Item::IMAGE_TYPE_18))
                        <img class="mob-product__image" src="{{ $Item->getImageLink(Item::IMAGE_TYPE_18) }}" alt="{{ _t('Схема', 'item') }} {{ $Item->title }}" loading="lazy">
                    @endif
                </a>
                <div class="mob-product__prices-wrap">
                    <table class="mob-product__prices">
                        <thead>
                            <tr>
                                <td>{!! _t('Кол-во, шт.', 'item') !!}</td>
                                <td>{!! _t('Цена за шт.', 'item') !!}</td>
                            </tr>
                        </thead>
                        <tbody>
                            @for($i = 6; $i >= 0; $i--)
                                @if($Item['price' . $i])
                                    <tr>
                                        <td>
                                            {{ $priceTitle[$i] }}
                                        </td>
                                        <td>
                                            @if ($Item->getSpecialDiscount($i))
                                                <div class="mob-product__price mob-product__price_discount">
                                                    <span class="mob-product__price-discount">%</span>
                                                    {{ Formatter::price($Item->getDiscountPrice($i), true) }}
                                                </div>
                                            @else
                                                <div class="mob-product__price">
                                                    {{ Formatter::price($Item['price' . $i], true) }}
                                                </div>
                                            @endif
                                        </td>
                                    </tr>
                                @endif
                            @endfor
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="mob-product__footer">
                @php
                    $allRemains = 0;
                @endphp
                @foreach($Item->itemCachedColors() as $itemColor)
                    @php
                        $allRemains += $itemColor->remains;
                    @endphp
                @endforeach
                <div class="mob-product__footer" style="flex-wrap: wrap;">
                    @if ($allRemains)
                        <div class="mob-product__remains">
                            {!! _t('на складе %s', 'item', [Formatter::quantity($allRemains)]) !!}
                        </div>
                    @else
                        <div class="mob-product__remains mob-product__remains_gray">
                            {!! _t('нет на складе', 'item') !!}
                        </div>
                    @endif
                    <a class="mob-product__link" href="{{ route('slug', ['slug' => $Item->slug()]) }}">
                        {!! _t('Подробнее', 'common') !!}
                        @component('components.icon', ['name' => 'arrow-menu', 'attributes' => ['class' => 'mob-product__link-icon']])])@endcomponent
                    </a>
                </div>
            </div>
            <div>
                <a class="js-order-modal product-item-table__btn product-item-table__cart product-item-table__cart-mobile add-cart" style="border: 0px;padding: 5px; background-color:#00997b;color:white;width:100%;text-align:center;" data-goodId="{{$Item->id}}" style="color: #c54004 !important; ">
                    В КОРЗИНУ >
                </a>
            </div>
        </div>

    @endforeach
</div>
@section('header')
    @include('item.header_cart', ['Item' => NULL])
@endsection