@php
//    declare(strict_types=1); // todo

    use App\Models\Color;
    use App\Models\Item;
    use App\Models\ItemSame;
    use App\Models\ItemColor;

    /** @var Item $Item */
    $showProps = $Item->prop_pipes || $Item->prop_matherial || $Item->prop_weight;


    $showPropPackage = $Item->prop_package || $Item->prop_pack_width || $Item->prop_pack_length || $Item->prop_pack_height ||
			$Item->pack_size || $Item->prop_pack_weight_net || $Item->prop_pack_weight_gross || $Item->prop_pack_volume;


    $showPropPallet = $Item->prop_pellet_width || $Item->prop_pellet_length || $Item->prop_pellet_height ||
			$Item->prop_pellet_quant_pack;

    $showPackage = $showPropPackage || $showPropPallet;

    $mainColor = $Item->mainColor();

    if (!$mainColor) { // todo временный фикс
        $mainColor = new ItemColor();
        $mainColor->add_price = 0;
        $mainColor->color = 1;
    }

    $hasPrice = $Item->price0 > 0 || $Item->price1 > 0 || $Item->price2 > 0 || $Item->price3 > 0 || $Item->price4 > 0 || $Item->price5 > 0 || $Item->price6 > 0;
@endphp

@extends('layouts.master')

@section('content')
    {!! Breadcrumbs::render('item', $Category, $Item) !!}
    @component('includes.breadcrumb', [
        'items' => [
            [
                'title' => $ParentCategory->getTitle(),
                'href' => route('slug', ['slug' => $ParentCategory->slug()])
            ],
            [
                'title' => $Category->getTitle(),
                'href' => route('slug', ['slug' => $Category->slug()])
            ],
            [
                'title' => $Item->getTitle()
            ]
        ],
        'pageActions' => true
    ])@endcomponent
    <script>
        window.dataLayer.push({
            "ecommerce": {
                "detail": {
                    "products": [
                        {
                            "id": "{{$Item->id}}",
                            "name": "{{$Item->title}}",
                            "price": {{$Item->price0}},
                            "brand": "ZAGLUSHKA.RU",
                            "category": "{{$ParentCategory->getTitle()}} {{$Category->getTitle()}}",
                        }
                    ]
                }
            }
        });
        gtag('event', 'view_item', {
            "item": [
                {
                    "id": "{{$Item->id}}",
                    "name": "{{$Item->title}}",
                    "brand": "ZAGLUSHKA.RU",
                    "price": {{$Item->price0}},
                    "category": "{{$ParentCategory->getTitle()}} {{$Category->getTitle()}}"
                }
            ]
        });
    </script>
    <div class="container">
		<!--<img src="/images/banner.jpg" alt="" style="margin: 20px auto; display: block">-->
        @component('components.main-catalog-mob', ['link' =>  route('slug', ['slug' => $Category->slug()]), 'title' => $Item->getTitle()])@endcomponent
        <div class="p-product" itemscope itemtype="http://schema.org/Product">
            @component('item.schema', ['ItemColors' => $ItemColors, 'Item' => $Item])@endcomponent
            <div itemscope itemtype="http://schema.org/Offer" itemprop="offers" class="hidden">
                <meta itemprop="priceCurrency" content="RUB"/>
                <meta itemprop="price" content="{{ $Item->price5 }}"/>
            </div>
            <div class="p-product__content">
                <div class="p-product__row">
                    <div class="product-item-photo">
                        @if(count($images))
                            <div class="product-item-photo__list-wrapper swiper-container">
                                @if($Item->recommend && App::isLocale('ru'))
                                    <div class="product-item-photo__promo"><img src="{{ asset('/images/catalog/recommend.png') }}" title="{{ _t('Заглушка.Ру рекомендует', 'item')}}" alt="{{ _t('Заглушка.Ру рекомендует', 'item') }}"></div>
                                @endif
                                <div class="product-item-photo__list swiper-wrapper">
                                @foreach($images as $image)
                                    <div class="product-item-photo__item swiper-slide" data-image="{{ $image['image'] }}">
                                        <img src="{{ asset($image['image']) }}" alt="{{ $Item->title }}" width="280" height="280" itemprop="image">
                                    </div>
                                @endforeach
                                </div>
                                <div class="product-item-photo__next"></div>
                                <div class="product-item-photo__prev"></div>
                            </div>
                            <div class="p-product__photos">
                                <ul class="p-product-photos">
                                    @foreach($images as $image)
                                        <li data-slide-index="{{ $loop->index }}" class="p-product-photos__item {{ $loop->iteration == 1 ? 'p-product-photos__item_active' : '' }}">
                                            <img class="p-product-photos__image {{ $Item->getImageLink(Item::IMAGE_TYPE_5) == $image['thumb'] ? 'js-order-img' : '' }}" src="{{ asset($image['thumb']) }}" width="95" height="95" alt="{{ $Item->title }}">
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @else
                            <img src="{{ asset('images/dummy_280.png') }}">
                        @endif
                    </div>
                    <div class="product-item-description">
                        <div class="product-item__title">{{ _t('Артикул:', 'item') }}
                            <div class="product-item__title-articul" itemprop="name">{{ $Item->title }}</div>
                        </div>
                        <div class="product-item__tab product-item__tab_first">
                            <div class="tabs">
                                <div class="tabs-nav__list">
                                    <div class="tabs-nav__item tabs-nav__item_active">
                                        {{ _t('Наличие', 'item') }}
                                    </div>
                                    @if($showProps)
                                        <div class="tabs-nav__item">{{ _t('Характеристики', 'item') }}</div>
                                    @endif
                                    @if($showPackage)
                                        <div class="tabs-nav__item">{{ _t('Упаковка', 'item') }}</div>
                                    @endif
                                    @if(count($Files) > 0)
                                        <div class="tabs-nav__item">{{ _t('Скачать', 'item') }}</div>
                                    @endif
                                    {{--<div class="tabs-nav__item">{{ _t('Доставка') }}</div>--}}
                                    <div class="tabs-nav__item">{{ _t('Оплата', 'item') }}</div>
                                </div>
                                <div class="tabs-content__list">
                                    <div class="tabs-content__item tabs-content__item_active">
                                        <div class="tabs-content__item-row">
                                            @php
                                                $countColors = count($ItemColors);
                                            @endphp
                                            <div class="product-item-availability">
                                                <div class="product-item-availability__list-wrap">
                                                    <table class="product-item-availability__list" data-id="{{ $Item->id }}">
                                                        <tbody>
                                                        @php
                                                            $nonExistsColors = [];
                                                            $existsColors = [];
                                                        @endphp
                                                            @foreach($ItemColors as $itemColor)
                                                                @php
                                                                    $TransitItems = $itemColor->getTransitItems();
                                                                @endphp
                                                                @if ($itemColor->remains || count($TransitItems) > 0)
                                                                
                                                                      @php
                                                                        if($itemColor->prop_features==45) { 
                                                                            $existsColors[0][] = $itemColor;
                                                                        }
                                                                        if($itemColor->prop_features==46) { 
                                                                            $existsColors[1][] = $itemColor;
                                                                        }
                                                                        if($itemColor->prop_features==NULL) { 
                                                                            $existsColors[10000][] = $itemColor;
                                                                        }
                                                                    @endphp 
                                                                @else
                                                                    @php
                                                                        $nonExistsColors[] = $itemColor;
                                                                    @endphp
                                                                
                                                                      @php
                                                                        if($itemColor->prop_features==45) { 
                                                                            $nonExistsColors1[0][] = $itemColor;
                                                                        }
                                                                        if($itemColor->prop_features==46) { 
                                                                            $nonExistsColors1[1][] = $itemColor;
                                                                        }
                                                                        if($itemColor->prop_features==NULL) { 
                                                                            $nonExistsColors1[10000][] = $itemColor;
                                                                        }
                                                                    @endphp 
                                                                @endif
                                                            @endforeach
                                                            
                                                            @if(count($existsColors) > 0)
                                                                @foreach($existsColors as $group_num => $itemColorGroup)
                                                                    @if($group_num==0) 
                                                                            <tr>
                                                                                <td colspan="4">
                                                                                    <span>Под резину</span>
                                                                                </td>
                                                                            </tr>
                                                                        @foreach($itemColorGroup as $itemColor)
                                                                            @component('item.product-item', [
                                                                                'itemColor'    => $itemColor,
                                                                                'Item'         => $Item,
                                                                                'TransitItems' => $itemColor->getTransitItems(),
                                                                            ])@endcomponent
                                                                        @endforeach
                                                                    @endif
                                                                    @if($group_num==1) 
                                                                            <tr>
                                                                                <td colspan="4">
                                                                                    <span>Под засыпку</span>
                                                                                </td>
                                                                            </tr>
                                                                        @foreach($itemColorGroup as $itemColor)
                                                                            @component('item.product-item', [
                                                                                'itemColor'    => $itemColor,
                                                                                'Item'         => $Item,
                                                                                'TransitItems' => $itemColor->getTransitItems(),
                                                                            ])@endcomponent
                                                                        @endforeach
                                                                    @endif
                                                                    @if($group_num==10000)
                                                                        @foreach($itemColorGroup as $itemColor)
                                                                            @component('item.product-item', [
                                                                                'itemColor'    => $itemColor,
                                                                                'Item'         => $Item,
                                                                                'TransitItems' => $itemColor->getTransitItems(),
                                                                            ])@endcomponent
                                                                        @endforeach
                                                                    @endif
 
                                                                
                                                                @endforeach
                                                                
                                                                
                                                                
                                                            
                                                            
                                                            @endif

                                                            @if(count($nonExistsColors) > 0)
                                                                <tr>
                                                                    <td colspan="3">
                                                                        <div class="product-item-availability__title">
                                                                            {{ _t('В производстве', 'item') }}
                                                                            @if($Item->preorder_availability_days_min || $Item->preorder_availability_days_max)
                                                                                {{ Formatter::preOrderAvailability($Item->preorder_availability_days_min, $Item->preorder_availability_days_max) }}:
                                                                            @else
                                                                                {{ _t('', 'item')}}:
                                                                            @endif
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                @foreach($nonExistsColors1 as $group_num => $itemColorGroup)
                                                                    @if($group_num==0) 
                                                                            <tr>
                                                                                <td colspan="4">
                                                                                    <span>Под резину</span>
                                                                                </td>
                                                                            </tr>
                                                                        @foreach($itemColorGroup as $itemColor)
                                                                            @component('item.product-item', [
                                                                                'itemColor'    => $itemColor,
                                                                                'Item'         => $Item,
                                                                                'TransitItems' => $itemColor->getTransitItems(),
                                                                            ])@endcomponent
                                                                        @endforeach
                                                                    @endif
                                                                    @if($group_num==1) 
                                                                            <tr>
                                                                                <td colspan="4">
                                                                                    <span>Под засыпку</span>
                                                                                </td>
                                                                            </tr>
                                                                        @foreach($itemColorGroup as $itemColor)
                                                                            @component('item.product-item', [
                                                                                'itemColor'    => $itemColor,
                                                                                'Item'         => $Item,
                                                                                'TransitItems' => $itemColor->getTransitItems(),
                                                                            ])@endcomponent
                                                                        @endforeach
                                                                    @endif
                                                                    @if($group_num==10000)
                                                                        @foreach($itemColorGroup as $itemColor)
                                                                            @component('item.product-item', [
                                                                                'itemColor'    => $itemColor,
                                                                                'Item'         => $Item,
                                                                                'TransitItems' => $itemColor->getTransitItems(),
                                                                            ])@endcomponent
                                                                        @endforeach
                                                                    @endif
 
                                                                
                                                                @endforeach
                                                            @endif
                                                        </tbody>
                                                    </table>
                                                    <div class="product-item-availability__more">
                                                        <span class="product-item-availability-active">{{ _t('Свернуть', 'item') }}</span>
                                                        <span class="product-item-availability-disabled">{{ _t('Показать еще', 'item') }} <span class="js-product-item-availability-count"></span> <span class="js-product-item-availability-color"></span></span>
                                                        @component('components.icon', ['name' => 'arrow-thin', 'attributes' => ['class' => 'product-item-availability__icon']])])@endcomponent
                                                        <div class="hidden js-product-item-availability-text">{{ _t('цвет', 'item') }}</div>
                                                        <div class="hidden js-product-item-availability-text">{{ _t('цвета', 'item') }}</div>
                                                        <div class="hidden js-product-item-availability-text">{{ _t('цветов', 'item') }}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if($showProps)
                                        <div class="tabs-content__item">
                                            <table class="product-item-description-table">
                                                <tbody>
                                                    @if($Item->prop_pipes)
                                                        <tr>
                                                            <td class="product-item-description-table__td">{{ _t($Item->fit()->title, 'properties') }}
                                                                :
                                                            </td>
                                                            <td class="product-item-description-table__td">{{ $Item->prop_pipes }}</td>
                                                        </tr>
                                                    @endif
                                                    @if($Item->prop_matherial)
                                                        <tr>
                                                            <td class="product-item-description-table__td">{{ _t('Материал:', 'item') }}</td>
                                                            <td class="product-item-description-table__td"
                                                                itemprop="material">{{ _t($Item->material()->title, 'properties') }}</td>
                                                        </tr>
                                                    @endif
                                                    <tr>
                                                        <td class="product-item-description-table__td">{{ _t('Цвета:', 'item') }}</td>
                                                        <td class="product-item-description-table__td">{!! join(', ', array_map(function($color) { return '<span itemprop="color">' . _t(Color::cachedOne($color)->name, 'colors') . '</span>'; }, $Item->getAvailableColors())) !!}</td>
                                                    </tr>
                                                    @if($Item->prop_weight)
                                                        <tr>
                                                            <td class="product-item-description-table__td">{{ _t('Вес, гр.:', 'item') }}</td>
                                                            <td class="product-item-description-table__td" itemprop="weight">{{ $Item->prop_weight }}</td>
                                                        </tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    @endif
                                    @if($showPackage)
                                        <div class="tabs-content__item">
                                            <table class="product-item-description-table">
                                                <tbody>
                                                @if($Item->prop_package)
                                                    <tr>
                                                        <td class="product-item-description-table__td">{{ _t('Тип упаковки:', 'item') }}</td>
                                                        <td class="product-item-description-table__td">{{ _t($Item->package()->title, 'properties') }}</td>
                                                    </tr>
                                                @endif
                                                @if($Item->pack_size)
                                                    <tr>
                                                        <td class="product-item-description-table__td">{{ _t('Количество в упаковке, шт.:', 'item') }}</td>
                                                        <td class="product-item-description-table__td">{{ $Item->pack_size }}</td>
                                                    </tr>
                                                @endif
                                                @if($Item->prop_pack_width && $Item->prop_pack_length && $Item->prop_pack_height)
                                                    <tr>
                                                        <td class="product-item-description-table__td">{{ _t('Габариты упаковки (ШхГхВ), мм:', 'item') }}</td>
                                                        <td class="product-item-description-table__td">
                                                            {{ $Item->prop_pack_width }}x{{ $Item->prop_pack_length }}x{{ $Item->prop_pack_height }}</td>
                                                    </tr>
                                                @endif
                                                @if($Item->prop_pack_weight_net && $Item->prop_pack_weight_gross)
                                                    <tr>
                                                        <td class="product-item-description-table__td">{{ _t('Вес нетто / брутто упаковки, кг:', 'item') }}</td>
                                                        <td class="product-item-description-table__td">{{ $Item->prop_pack_weight_net }}
                                                            / {{ $Item->prop_pack_weight_gross }}</td>
                                                    </tr>
                                                @elseif($Item->prop_pack_weight_net)
                                                    <tr>
                                                        <td class="product-item-description-table__td">{{ _t('Вес нетто упаковки, кг:', 'item') }}</td>
                                                        <td class="product-item-description-table__td">{{ $Item->prop_pack_weight_net }}</td>
                                                    </tr>
                                                @endif
                                                @if($Item->prop_pack_volume)
                                                    <tr>
                                                        <td class="product-item-description-table__td">{{_t('Объем упаковки, м', 'item')}}³:
                                                        </td>
                                                        <td class="product-item-description-table__td">{{ $Item->prop_pack_volume }}</td>
                                                    </tr>
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    @endif
                                    @if(count($Files) > 0)
                                        <div class="tabs-content__item">
                                            @foreach($Files as $File)
                                                <div>
                                                    <a target="_blank" href="{{$File->getImageLink()}}">{{$File->title}}</a>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endif
                                    <div class="tabs-content__item">
                                        <ul class="product-item-info-payment__list">
                                            <li class="product-item-info-payment__item">{{ _t('По выставленному счёту (для юр. лиц)', 'item') }}</li>
                                            <li class="product-item-info-payment__item">{{ _t('Банковской картой', 'item') }}
                                                <img class="product-item-info-payment__icon-card" src="{{ asset('/images/card/visa.svg') }}" alt="VISA" title="VISA">
                                                <img class="product-item-info-payment__icon-card" src="{{ asset('/images/card/mastercard.svg') }}" alt="MasterCard" title="MasterCard">
                                                <img class="product-item-info-payment__icon-card" src="{{ asset('/images/card/mir.svg') }}" alt="{{ _t('Мир', 'item') }}" title="{{ _t('Мир', 'item') }}">
                                            </li>
                                            <li class="product-item-info-payment__item">{{ _t('По номеру кошелька WebMoney, Яндекс.Деньги или QIWI', 'item') }}</li>
                                            <li class="product-item-info-payment__item">{{ _t('Наличными при получении (для физ. лиц)', 'item') }}</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="product-item__table-wrap @if($Item->only_two_prices) product-item__table-wrap_short @endif">
                                <div class="hidden">
                                    <span class="js-order-price-text" data-id="6">{{ _t('от', 'item') }} 30 000 {{ _t('шт.', 'item') }}</span>
                                    <span class="js-order-price-text" data-id="5">{{ _t('от', 'item') }} 10 000 {{ _t('шт.', 'item') }}</span>
                                    <span class="js-order-price-text" data-id="4">{{ _t('от', 'item') }} 5 000 {{ _t('шт.', 'item') }}</span>
                                    <span class="js-order-price-text" data-id="3">{{ _t('от', 'item') }} 1 000 {{ _t('шт.', 'item') }}</span>
                                    <span class="js-order-price-text" data-id="2">{{ _t('от', 'item') }} 500 {{ _t('шт.', 'item') }}</span>
                                    <span class="js-order-price-text" data-id="1">{{ _t('от', 'item') }} 100 {{ _t('шт.', 'item') }}</span>
                                    <span class="js-order-price-text" data-id="0">{{ _t('розница', 'item') }}</span>
                                </div>
                                <table class="product-item-table">
                                    <thead class="product-item-table__thead">
                                        <tr>
                                            @if(!$Item->only_two_prices)
                                                @if($ItemColors->unique('add_price')->count() > 1 && $hasPrice)
                                                    <th class="product-item-table__th">{!! _t('Цвет', 'item') !!}</th>
                                                @endif
                                                @if($Item->price6 > 0)
                                                    <th class="product-item-table__th product-item-table__price">{{ _t('от', 'item') }} <span class="nowrap">30 000</span></th>
                                                @endif
                                                <th class="product-item-table__th product-item-table__price">{{ _t('от', 'item') }} <span class="nowrap">10 000</span></th>
                                                <th class="product-item-table__th product-item-table__price">{{ _t('от', 'item') }} <span class="nowrap">5 000</span></th>
                                                <th class="product-item-table__th product-item-table__price">{{ _t('от', 'item') }} <span class="nowrap">1 000</span></th>
                                                <th class="product-item-table__th product-item-table__price">{{ _t('от', 'item') }} 500</th>
                                            @endif
                                            <th class="product-item-table__th product-item-table__price">{{ _t('от', 'item') }} 100</th>
                                            <th class="product-item-table__th product-item-table__price">{{ _t('Розница', 'item') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="product-item-table__tr">
                                            @if($ItemColors->unique('add_price')->count() > 1 && $hasPrice)
                                                <td class="product-item-table__td">
                                                    {{ _t(Color::cachedOne($mainColor->color)->name, 'colors') }}
                                                </td>
                                            @endif
                                            @if($hasPrice)
                                                @for($i = ($Item->price6 > 0) ? 6 : 5; $i >= 0; $i--)
                                                
                                                    @if($i < 2 || !$Item->only_two_prices)
                                                        <td class="product-item-table__td product-item-table__price">
                                                            @if ($Item->getSpecialDiscount($i))
                                                                <div class="product-item-table__price-old">{{ Formatter::price($mainColor->getPrice($Item['price' . $i]), true) }}</div>
                                                                <span class="product-item-table__discount @if($Item->getSpecialDiscount($i) === 50)product-item-table__discount_active @endif">−{{ $Item->getSpecialDiscount($i) }} %</span>
                                                                <div>{{ Formatter::price($Item->getDiscountPrice($i), true) }}</div>
                                                            @else
                                                                {{ Formatter::price($mainColor->getPrice($Item['price' . $i]), true) }}
                                                            @endif
                                                        </td>
                                                    @endif
                                                @endfor

                                            @else
                                                <td class="product-item-table__td product-item-table__no-price" colspan="{{ ($Item->price6 > 0) ? 7 : 6 }}">
                                                    {{ _t('Цена по запросу', 'item') }}
                                                </td>
                                            @endif
                                        </tr>
                                        @if($ItemColors->unique('add_price')->count() > 1 && $hasPrice)
                                            @foreach($ItemColors as $ItemColor)
                                                @if(!$ItemColor->main)
                                                    <tr class="product-item-table__tr">
                                                    <td class="product-item-table__td">{{ _t('другие', 'item') }}</td>
                                                    @for($i = ($Item->price6 > 0) ? 6 : 5; $i >= 0; $i--)
                                                        @if($i < 2 || !$Item->only_two_prices)
                                                            <td class="product-item-table__td product-item-table__price">
                                                                <div class="product-item-table__price-text">
                                                                    {{ Formatter::price($ItemColor->getPrice($Item['price' . $i]), true) }}
                                                                </div>
                                                            </td>
                                                        @endif
                                                    @endfor
                                                    </tr>
                                                    @break
                                                @endif
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                                <!-- Микроразметка -->
                                <div class="product-item-table" style="display: none;">
                                    
                                        <div itemscope itemtype="http://schema.org/Offer" itemprop="offers" class="hidden">
                                            <link itemprop="availability" href="http://schema.org/InStock" />

                                            
                                            @if($hasPrice)
                                                @for($i = ($Item->price6 > 0) ? 6 : 5; $i >= 0; $i--)
                                                <div itemprop="priceSpecification" itemscope itemtype="http://schema.org/UnitPriceSpecification">
                                                    <meta itemprop="price" content="{{ $mainColor->getPrice($Item['price'.$i]) }}" />
                                                    @if($i==6)
                                                    <div itemprop="eligibleQuantity" itemscope itemtype="http://schema.org/QuantitativeValue">
                                                        <meta itemprop="unitCode" content="{{ $mainColor->code }}" />
                                                        <meta itemprop="minValue" content="10001">
                                                        <meta itemprop="maxValue" content="100000000000">
                                                    </div>
                                                    @endif
                                                    @if($i==5)
                                                    <div itemprop="eligibleQuantity" itemscope itemtype="http://schema.org/QuantitativeValue">
                                                        <meta itemprop="unitCode" content="{{ $mainColor->code }}" />
                                                        <meta itemprop="minValue" content="5001">
                                                        <meta itemprop="maxValue" content="10000">
                                                    </div>
                                                    @endif
                                                    @if($i==4)
                                                    <div itemprop="eligibleQuantity" itemscope itemtype="http://schema.org/QuantitativeValue">
                                                        <meta itemprop="unitCode" content="{{ $mainColor->code }}" />
                                                        <meta itemprop="minValue" content="1001">
                                                        <meta itemprop="maxValue" content="5000">
                                                    </div>
                                                    @endif
                                                    @if($i==3)
                                                    <div itemprop="eligibleQuantity" itemscope itemtype="http://schema.org/QuantitativeValue">
                                                        <meta itemprop="unitCode" content="{{ $mainColor->code }}" />
                                                        <meta itemprop="minValue" content="501">
                                                        <meta itemprop="maxValue" content="1000">
                                                    </div>
                                                    @endif
                                                    @if($i==2)
                                                    <div itemprop="eligibleQuantity" itemscope itemtype="http://schema.org/QuantitativeValue">
                                                        <meta itemprop="unitCode" content="{{ $mainColor->code }}" />
                                                        <meta itemprop="minValue" content="101">
                                                        <meta itemprop="maxValue" content="500">
                                                    </div>
                                                    @endif
                                                    @if($i==1)
                                                    <div itemprop="eligibleQuantity" itemscope itemtype="http://schema.org/QuantitativeValue">
                                                        <meta itemprop="unitCode" content="{{ $mainColor->code }}" />
                                                        <meta itemprop="minValue" content="1">
                                                        <meta itemprop="maxValue" content="100">
                                                    </div>
                                                    @endif
                                                </div>
                                                @endfor

                                            @else
                                                <td class="product-item-table__td product-item-table__no-price" colspan="{{ ($Item->price6 > 0) ? 7 : 6 }}">
                                                    {{ _t('Цена по запросу', 'item') }}
                                                </td>
                                            @endif
                                            </div>
                                        </div>
                                    
                                </div>
                                <!-- Микроразметка конец -->
                                <div class="product-item-table__under">
                                    <div class="product-item-info-free">
                                        @if($Item->promo)
                                            <div class="product-item-info-free__text">{{ _t('Чтобы убедиться в качестве нашей продукции закажите бесплатные образцы.', 'item') }}</div>
                                            <div class="product-item-info-free__btn js-free-order-modal" data-id="{{ $Item->id }}" data-color="{{ $mainColor->color }}">{{ _t('Получить  бесплатные образцы', 'item') }}</div>
                                        @endif
                                    </div>
                                    <button class="js-order-modal product-item-table__btn add-cart" data-goodId="{{$Item->id}}">{{ _t('Заказать', 'item') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="product-item-info-description">
                        <div class="product-item-info-description__text">
                            {!! $Item->descr !!}
                        </div>
                    </div>
                    <div class="mob-product-item">
                        @php
                            $priceTitle = [
                                _t('розница', 'item'),
                                _t('от', 'item') . ' 100',
                                _t('от', 'item') . ' 500',
                                _t('от', 'item') . ' 1 000',
                                _t('от', 'item') . ' 5 000',
                                _t('от', 'item') . ' 10 000',
                                _t('от', 'item') . ' 30 000',
                            ];
                        @endphp
                        @if($hasPrice)
                            <table class="mob-product-item-prices">
                                <thead>
                                    <tr>
                                        <td>{!! _t('Кол-во, шт.', 'item') !!}</td>
                                        @if($ItemColors->unique('add_price')->count() > 1 && $hasPrice)
                                            <td>
                                                {{ _t(Color::cachedOne($mainColor->color)->name, 'colors') }}
                                            </td>
                                        @else
                                            <td>
                                                {!! _t('Цена за шт.', 'item') !!}
                                            </td>
                                        @endif
                                        @if($ItemColors->unique('add_price')->count() > 1 && $hasPrice)
                                            @foreach($ItemColors as $ItemColor)
                                                @if(!$ItemColor->main)
                                                        <td>{{ _t('Другие цвета', 'item') }}</td>
                                                    @break
                                                @endif
                                            @endforeach
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @for($i = ($Item->price6 > 0) ? 6 : 5; $i >= 0; $i--)
                                        @if($i < 2 || !$Item->only_two_prices)
                                            <tr>
                                                <td>{{ $priceTitle[$i] }}</td>
                                                <td>
                                                    @if ($Item->getSpecialDiscount($i))
                                                        <div class="mob-product-item-prices__price mob-product-item-prices__price_discount">
                                                            <span class="mob-product-item-prices__price-discount">%</span>
                                                            {{ Formatter::price($Item->getDiscountPrice($i), true) }}
                                                        </div>
                                                    @else
                                                        <div class="mob-product-item-prices__price">
                                                            {{ Formatter::price($Item['price' . $i], true) }}
                                                        </div>
                                                    @endif
                                                </td>
                                                @if($ItemColors->unique('add_price')->count() > 1 && $hasPrice)
                                                    <td>
                                                        @foreach($ItemColors as $ItemColor)
                                                            @if(!$ItemColor->main)
                                                                <div class="mob-product-item-prices__price">
                                                                    {{ Formatter::price($ItemColor->getPrice($Item['price' . $i]), true) }}
                                                                </div>
                                                                @break
                                                            @endif
                                                        @endforeach
                                                    </td>
                                                @endif
                                            </tr>
                                        @endif
                                    @endfor
                                </tbody>
                            </table>
                        @endif
                    </div>
                    <button class="js-order-modal mob-product-item__btn add-cart"  data-goodId="{{$Item->id}}">{{ _t('Заказать', 'item') }}</button>
                    @if($Item->promo)
                        <div class="mob-product-item-free">
                            <div class="mob-product-item-free__text">{{ _t('Чтобы убедиться в качестве нашей продукции закажите бесплатные образцы.', 'item') }}</div>
                            <div class="mob-product-item-free__btn js-free-order-modal add-cart" data-id="{{ $Item->id }}" data-color="{{ $mainColor->color }}">{{ _t('Получить  бесплатные образцы', 'item') }}</div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="mob-product-item-list">
            <div class="mob-product-item-list__item">
                <div class="mob-product-item-list__title">
                    @component('components.icon', ['name' => 'arrow-menu', 'attributes' => ['class' => 'mob-product-item-list__arrow']])])@endcomponent
                    @component('components.icon', ['name' => 'in-stock', 'attributes' => ['class' => 'mob-product-item-list__icon']])@endcomponent
                    <span>{{ _t('Наличие', 'item') }}</span>
                </div>
                <div class="mob-product-item-list__body">
                    <div class="mob-product-item-color">
                        @if(count($ItemColors) && count($ItemColors) !== count($nonExistsColors))
                            <div class="mob-product-item-color__block">
                                <div class="mob-product-item-color__title mob-product-item-color__header">
                                    <div>{{ _t('Цвет', 'item') }}</div>
                                    <div>{{ _t('Количество', 'item') }}</div>
                                </div>
                                @foreach($ItemColors as $itemColor)
                                    @php
                                        $TransitItemsQuant = 0;
                                        foreach ($itemColor->getTransitItems() as $TransitItem) {
                                            $TransitItemsQuant += $TransitItem->quant;
                                        }
                                    @endphp
                                    @if ($itemColor->remains || $TransitItemsQuant)
                                        <div class="mob-product-item-color__item">
                                            <div class="mob-product-item-color__line">
                                                <div class="mob-product-item-color__name">
                                                    @if(Color::cachedOne($itemColor->color)->hex)
                                                        @component('components.product-item-color-box', ['hex' => Color::cachedOne($itemColor->color)->hex])@endcomponent
                                                    @endif
                                                    <span>{{ _t(Color::cachedOne($itemColor->color)->name, 'colors') }}</span>
                                                </div>
                                                <div class="mob-product-item-color__quant">
                                                    {{ Formatter::quantity($itemColor->remains) }}
                                                </div>
                                            </div>
                                            @if($TransitItemsQuant)
                                                <div class="mob-product-item-color__line-quant">
                                                    <span>
                                                        + {{ _t('едет', 'item') }}
                                                        {{ Formatter::quantity($TransitItemsQuant) }}
                                                    </span>
                                                </div>
                                            @endif
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        @endif
                        @if(count($nonExistsColors))
                            <div class="mob-product-item-color__block">
                                <div class="mob-product-item-color__title">
                                    {{ _t('В производстве:', 'item') }}
                                </div>
                                @foreach($nonExistsColors as $itemColor)
                                    <div class="mob-product-item-color__item">
                                        <div class="mob-product-item-color__name">
                                            @if(Color::cachedOne($itemColor->color)->hex)
                                                @component('components.product-item-color-box', ['hex' => Color::cachedOne($itemColor->color)->hex])@endcomponent
                                            @endif
                                            <span>{{ _t(Color::cachedOne($itemColor->color)->name, 'colors') }}</span>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="mob-product-item-list__item">
                <div class="mob-product-item-list__title">
                    @component('components.icon', ['name' => 'arrow-menu', 'attributes' => ['class' => 'mob-product-item-list__arrow']])])@endcomponent
                    @component('components.icon', ['name' => 'info', 'attributes' => ['class' => 'mob-product-item-list__icon']])@endcomponent
                    <span>{{ _t('Описание', 'item') }}</span>
                </div>
                <div class="mob-product-item-list__body">
                    <div class="mob-product-item-list__description">
                        {!! $Item->descr !!}
                    </div>
                </div>
            </div>
            @if($showProps)
                <div class="mob-product-item-list__item">
                    <div class="mob-product-item-list__title">
                        @component('components.icon', ['name' => 'arrow-menu', 'attributes' => ['class' => 'mob-product-item-list__arrow']])])@endcomponent
                        @component('components.icon', ['name' => 'characteristics', 'attributes' => ['class' => 'mob-product-item-list__icon']])@endcomponent
                        <span>{{ _t('Характеристики', 'item') }}</span>
                    </div>
                    <div class="mob-product-item-list__body">
                        <div class="mob-product-item-property">
                            @if($Item->prop_pipes)
                                <div class="mob-product-item-property__item">
                                    <div class="mob-product-item-property__title">{{ _t($Item->fit()->title, 'properties') }}:</div>
                                    <div class="mob-product-item-property__value">{{ $Item->prop_pipes }}</div>
                                </div>
                            @endif
                            @if($Item->prop_matherial)
                                <div class="mob-product-item-property__item">
                                    <div class="mob-product-item-property__title">{{ _t('Материал:', 'item') }}</div>
                                    <div class="mob-product-item-property__value"
                                        itemprop="material">{{ _t($Item->material()->title, 'properties') }}</div>
                                </div>
                            @endif
                            <div class="mob-product-item-property__item">
                                <div class="mob-product-item-property__title">{{ _t('Цвета:', 'item') }}</div>
                                <div class="mob-product-item-property__value">{!! join(', ', array_map(function($color) { return '<span itemprop="color">' . _t(Color::cachedOne($color)->name, 'colors') . '</span>'; }, $Item->getAvailableColors())) !!}</div>
                            </div>
                            @if($Item->prop_weight)
                                <div class="mob-product-item-property__item">
                                    <div class="mob-product-item-property__title">{{ _t('Вес, гр.:', 'item') }}</div>
                                    <div class="mob-product-item-property__value" itemprop="weight">{{ $Item->prop_weight }}</div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            @endif
            @if($showPackage)
            <div class="mob-product-item-list__item">
                <div class="mob-product-item-list__title">
                    @component('components.icon', ['name' => 'arrow-menu', 'attributes' => ['class' => 'mob-product-item-list__arrow']])])@endcomponent
                    @component('components.icon', ['name' => 'package', 'attributes' => ['class' => 'mob-product-item-list__icon']])@endcomponent
                    <span>{{ _t('Упаковка', 'item') }}</span>
                </div>
                <div class="mob-product-item-list__body">
                    <div class="mob-product-item-property">
                        @if($Item->prop_package)
                            <div class="mob-product-item-property__item">
                                <div class="mob-product-item-property__title">{{ _t('Тип упаковки:', 'item') }}</div>
                                <div class="mob-product-item-property__value">{{ _t($Item->package()->title, 'properties') }}</div>
                            </div>
                        @endif
                        @if($Item->pack_size)
                            <div class="mob-product-item-property__item">
                                <div class="mob-product-item-property__title">{{ _t('Количество в упаковке, шт.:', 'item') }}</div>
                                <div class="mob-product-item-property__value">{{ $Item->pack_size }}</div>
                            </div>
                        @endif
                        @if($Item->prop_pack_width && $Item->prop_pack_length && $Item->prop_pack_height)
                            <div class="mob-product-item-property__item">
                                <div class="mob-product-item-property__title">{{ _t('Габариты упаковки (ШхГхВ), мм:', 'item') }}</div>
                                <div class="mob-product-item-property__value">
                                    {{ $Item->prop_pack_width }}x{{ $Item->prop_pack_length }}x{{ $Item->prop_pack_height }}</div>
                            </div>
                        @endif
                        @if($Item->prop_pack_weight_net && $Item->prop_pack_weight_gross)
                            <div class="mob-product-item-property__item">
                                <div class="mob-product-item-property__title">{{ _t('Вес нетто / брутто упаковки, кг:', 'item') }}</div>
                                <div class="mob-product-item-property__value">{{ $Item->prop_pack_weight_net }}
                                    / {{ $Item->prop_pack_weight_gross }}</div>
                            </div>
                        @elseif($Item->prop_pack_weight_net)
                            <div class="mob-product-item-property__item">
                                <div class="mob-product-item-property__title">{{ _t('Вес нетто упаковки, кг:', 'item') }}</div>
                                <div class="mob-product-item-property__value">{{ $Item->prop_pack_weight_net }}</div>
                            </div>
                        @endif
                        @if($Item->prop_pack_volume)
                            <div class="mob-product-item-property__item">
                                <div class="mob-product-item-property__title">{{_t('Объем упаковки, м', 'item')}}³:</div>
                                <div class="mob-product-item-property__value">{{ $Item->prop_pack_volume }}</div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            @endif
            <div class="mob-product-item-list__item">
                <div class="mob-product-item-list__title">
                    @component('components.icon', ['name' => 'arrow-menu', 'attributes' => ['class' => 'mob-product-item-list__arrow']])])@endcomponent
                    @component('components.icon', ['name' => 'payment', 'attributes' => ['class' => 'mob-product-item-list__icon']])@endcomponent
                    <span>{{ _t('Оплата', 'item') }}</span>
                </div>
                <div class="mob-product-item-list__body">
                    <ul class="product-item-info-payment__list">
                        <li class="product-item-info-payment__item">{{ _t('По выставленному счёту (для юр. лиц)', 'item') }}</li>
                        <li class="product-item-info-payment__item">{{ _t('Банковской картой', 'item') }}
                            <span class="product-item-info-payment__icon-wrap">
                                <img class="product-item-info-payment__icon-card" src="{{ asset('/images/card/visa.svg') }}" alt="VISA" title="VISA">
                                <img class="product-item-info-payment__icon-card" src="{{ asset('/images/card/mastercard.svg') }}" alt="MasterCard" title="MasterCard">
                                <img class="product-item-info-payment__icon-card" src="{{ asset('/images/card/mir.svg') }}" alt="{{ _t('Мир', 'item') }}" title="{{ _t('Мир', 'item') }}">
                            </span>
                        </li>
                        <li class="product-item-info-payment__item">{{ _t('По номеру кошелька WebMoney, Яндекс.Деньги или QIWI', 'item') }}</li>
                        <li class="product-item-info-payment__item">{{ _t('Наличными при получении (для физ. лиц)', 'item') }}</li>
                    </ul>
                </div>
            </div>
        </div>
        @php
            $sameItems = ItemSame::getItems($Item->id);
        @endphp

        @if($sameItems->count() > 0)
        <div class="container">
            <section class="p-recommended">
                <div class="page-header">
                    <div class="page-header__title-wrap">
                        <h2 class="page__title">{!! _t('Рекомендуем посмотреть:', 'item') !!}</h2>
                    </div>
                </div>
                
                @component('item.list', ['Items' => $sameItems, 'firstItem' => 1])@endcomponent

            </section>
                            </div>
        @endif
    </div>

    
@endsection
@section('header')
    @include('item.header_cart', ['Item' => NULL])
@endsection
