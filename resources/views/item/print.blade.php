@php
    declare(strict_types=1);

    use App\Models\Item;
    use App\Models\Color;
@endphp

@extends('layouts.print')

@section('content')
    @if(count($images))
        <table class="images">
            <tr>
                <td rowspan="2" class="images__large">
                    @if($Item->isImageExists(Item::IMAGE_TYPE_1))
                        <img src="{{ (Request::get('excel') ? public_path() : '') . $Item->getImageLink(Item::IMAGE_TYPE_1) }}" alt="">
                    @endif
                </td>
                <td class="images__small">
                    @if($Item->isImageExists(Item::IMAGE_TYPE_6 ))
                        <img src="{{ (Request::get('excel') ? public_path() : '') . $Item->getImageLink(Item::IMAGE_TYPE_6) }}" alt="">
                    @endif
                </td>
                <td rowspan="2" class="images__large">
                    @if($Item->isImageExists(Item::IMAGE_TYPE_4))
                        <img src="{{ (Request::get('excel') ? public_path() : '') . $Item->getImageLink(Item::IMAGE_TYPE_4) }}" alt="">
                    @elseif($Item->isImageExists(Item::IMAGE_TYPE_8))
                        <img src="{{ (Request::get('excel') ? public_path() : '') . $Item->getImageLink(Item::IMAGE_TYPE_8) }}" alt="">
                    @endif
                </td>
                <td rowspan="2" class="images__large">
                    @if($Item->isImageExists(Item::IMAGE_TYPE_9))
                        <img src="{{ (Request::get('excel') ? public_path() : '') . $Item->getImageLink(Item::IMAGE_TYPE_9) }}" alt="">
                    @elseif($Item->isImageExists(Item::IMAGE_TYPE_10))
                        <img src="{{ (Request::get('excel') ? public_path() : '') . $Item->getImageLink(Item::IMAGE_TYPE_10) }}" alt="">
                    @endif
                </td>
            </tr>
            <tr>
                <td class="images__small">
                    @if($Item->isImageExists(Item::IMAGE_TYPE_7 ))
                        <img src="{{ (Request::get('excel') ? public_path() : '') . $Item->getImageLink(Item::IMAGE_TYPE_7) }}" alt="">
                    @endif
                </td>
            </tr>
        </table>
    @endif
    <p>{{ $Item->short_descr }}</p>
    @if(count($ItemColors))
        <table class="list">
            <thead>
                <tr>
                    <th rowspan="2">{!! _t('Артикул', 'item')  !!}</th>
                    <th rowspan="2">{!! _t('Цвет', 'item') !!}</th>
                    <th colspan="6">{!! _t('Цена, руб. за 1 шт.', 'item') !!}</th>
                    <th rowspan="2">{!! _t('Упаковка', 'item') !!}</th>
                    <th rowspan="2">{!! _t('Наличие на складе на %s', 'item', [Formatter::date()]) !!}</th>
                </tr>
                <tr>
                    <th>{!! _t('более 10 000', 'item') !!}</th>
                    <th>{!! _t('от 5 000 до 10 000', 'item') !!}</th>
                    <th>{!! _t('от 1 000 до 5 000', 'item') !!}</th>
                    <th>{!! _t('от 500 до 1 000', 'item') !!}</th>
                    <th>{!! _t('от 100 до 500', 'item') !!}</th>
                    <th>{!! _t('розница до 100', 'item') !!}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($ItemColors as $Color)
                    <tr>
                        <td>{{ $Color->code }}</td>
                        <td>{{ _t(Color::cachedOne($Color->color)->name, 'colors') }}</td>
                        <td @if($Item->special && $Color->main && $Item->getSpecialDiscount(5))class="price-specials" @endif>
                            @if ($Item->special && $Color->main && $Item->getSpecialDiscount(5))
                                <div><s>{{ Formatter::price($Color->getPrice($Item->price5), true) }}</s></div>
                                <div class="price-discount @if($Item->getSpecialDiscount(5) === 50) price-discount_hot @endif">-{{ $Item->getSpecialDiscount(5) }}%</div>
                                {{ Formatter::price($Item->getDiscountPrice($Item->getSpecialDiscount(5), $Color->getPrice($Item->price5)), true) }}
                            @else
                                {{ Formatter::price($Color->getPrice($Item->price5), true) }}
                            @endif
                        </td>
                        <td @if($Item->special && $Color->main && $Item->getSpecialDiscount(4))class="price-specials" @endif>
                            @if ($Item->special && $Color->main && $Item->getSpecialDiscount(4))
                                <div><s>{{ Formatter::price($Color->getPrice($Item->price4), true) }}</s></div>
                                <div class="price-discount @if($Item->getSpecialDiscount(4) === 50) price-discount_hot @endif">-{{ $Item->getSpecialDiscount(4) }}%</div>
                                {{ Formatter::price($Item->getDiscountPrice($Item->getSpecialDiscount(4), $Color->getPrice($Item->price4)), true) }}
                            @else
                                {{ Formatter::price($Color->getPrice($Item->price4), true) }}
                            @endif
                        </td>
                        <td @if($Item->special && $Color->main && $Item->getSpecialDiscount(3))class="price-specials" @endif>
                            @if ($Item->special && $Color->main && $Item->getSpecialDiscount(3))
                                <div><s>{{ Formatter::price($Color->getPrice($Item->price3), true) }}</s></div>
                                <div class="price-discount @if($Item->getSpecialDiscount(3) === 50) price-discount_hot @endif">-{{ $Item->getSpecialDiscount(3) }}%</div>
                                {{ Formatter::price($Item->getDiscountPrice($Item->getSpecialDiscount(3), $Color->getPrice($Item->price3)), true) }}
                            @else
                                {{ Formatter::price($Color->getPrice($Item->price3), true) }}
                            @endif
                        </td>
                        <td @if($Item->special && $Color->main && $Item->getSpecialDiscount(2))class="price-specials" @endif>
                            @if ($Item->special && $Color->main && $Item->getSpecialDiscount(2))
                                <div><s>{{ Formatter::price($Color->getPrice($Item->price2), true) }}</s></div>
                                <div class="price-discount @if($Item->getSpecialDiscount(2) === 50) price-discount_hot @endif">-{{ $Item->getSpecialDiscount(2) }}%</div>
                                {{ Formatter::price($Item->getDiscountPrice($Item->getSpecialDiscount(2), $Color->getPrice($Item->price2)), true) }}
                            @else
                                {{ Formatter::price($Color->getPrice($Item->price2), true) }}
                            @endif
                        </td>
                        <td @if($Item->special && $Color->main && $Item->getSpecialDiscount(1))class="price-specials" @endif>
                            @if ($Item->special && $Color->main && $Item->getSpecialDiscount(1))
                                <div><s>{{ Formatter::price($Color->getPrice($Item->price1), true) }}</s></div>
                                <div class="price-discount @if($Item->getSpecialDiscount(1) === 50) price-discount_hot @endif">-{{ $Item->getSpecialDiscount(1) }}%</div>
                                {{ Formatter::price($Item->getDiscountPrice($Item->getSpecialDiscount(1), $Color->getPrice($Item->price1)), true) }}
                            @else
                                {{ Formatter::price($Color->getPrice($Item->price1), true) }}
                            @endif
                        </td>
                        <td @if($Item->special && $Color->main && $Item->getSpecialDiscount(0))class="price-specials" @endif>
                            @if ($Item->special && $Color->main && $Item->getSpecialDiscount(0))
                                <div><s>{{ Formatter::price($Color->getPrice($Item->price0), true) }}</s></div>
                                <div class="price-discount @if($Item->getSpecialDiscount(0) === 50) price-discount_hot @endif">-{{ $Item->getSpecialDiscount(0) }}%</div>
                                {{ Formatter::price($Item->getDiscountPrice($Item->getSpecialDiscount(0), $Color->getPrice($Item->price0)), true) }}
                            @else
                                {{ Formatter::price($Color->getPrice($Item->price0), true) }}
                            @endif
                        </td>
                        <td>{{ _t('%d шт.', 'common', [$Item->pack_size]) }}</td>
                        <td><strong>{{ _t('%d шт.', 'common', [$Color->remains]) }}</strong></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif
    <p>{!! _t('Все цены указаны в рублях и включают в себя НДС.', 'item') !!}</p>
    <p>{!! _t('Все цены указаны при условии 100%% предоплаты и действительны в течение 5 дней.', 'item') !!}</p>
    <p>{!! _t('Все цены указаны при условии самовывоза продукции со склада в г. Санкт-Петербург', 'item') !!}</p>
    <p>{!! _t('(для иногородних клиентов доставка до склада ТК в г. Санкт-Петербург бесплатна).', 'item') !!}</p>
    <p>{!! _t('Все цены указаны за стандартный цвет для каждого вида изделия', 'item') !!}</p>
    <p>{!! _t('(изменение цвета на любой из предлагаемой палитры +30%% к стоимости, изменение цвета на любой по системе RAL +50%% к стоимости).', 'item') !!}</p>
    <p>{!! _t('О возможных сроках поставки заказного товара, необходимо уточнять заранее.', 'item') !!}</p>
@endsection
