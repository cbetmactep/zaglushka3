@php
    declare(strict_types=1);
    
    use App\Models\City;
    Meta::set('title', _t('Страница не найдена', 'common'));
    $CurrentCity = App::isLocale('ru') && session('current_city') ? City::cachedOne(session('current_city')) : null;
@endphp

@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="page-header">
            <div class="page-header__title-wrap">
                <h1 class="page__title">{!! _t('Страница не найдена', 'common') !!}</h1>
            </div>
        </div>
        <div class="page">
            <div class="page__content page__content_background">
                {{ _t('К сожалению, страница, которую вы запрашиваете, отсутствует на нашем сервере.', 'common') }}
            </div>
        </div>
    </div>
@endsection