@php
    declare(strict_types=1);

    Meta::set('title', _t('Сравнение', 'favorites'));
@endphp

@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="page-header">
            <div class="page-header__title-wrap">
                <h1 class="page__title page__title_compare">{!! _t('Сравнение', 'favorites') !!}</h1>
            </div>
            <div class="page-header__actions">
                @component('includes.page_actions')@endcomponent
            </div>
        </div>
        <div class="page">
            @if(count($Items) > 0)
                <div class="page__content">
                    @component('user.compare.list_color', ['Colors' => $Items, 'favorites' => true])@endcomponent
                    <div>
                        <div class="row compare-button">
                            <a class="btn btn_outline_primary btn_size_middle compare-flush" href="{{ route('compare') }}">{{ _t('Очистить всё', 'favorites') }}</a>
                        </div>
                    </div>
                </div>
            @else
                <div class="page__content page__content_background">
                    {{ _t('Ничего не выбрано для сравнения', 'favorites') }}
                </div>
            @endif
        </div>
    </div>
    <template id="compare-empty">
        <div class="row">
            <div class="page__content page__content_background">
                {{ _t('Ничего не выбрано для сравнения', 'favorites') }}
            </div>
        </div>
    </template>
@endsection
