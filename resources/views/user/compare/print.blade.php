@php
    declare(strict_types=1);

    use App\Models\Item;
    use App\Models\Color;

    $Items = [];
    $ItemColors = [];
    foreach ($Colors as $Color) {
        if (!isset($Items[$Color->item_id])) {
            $Items[$Color->item_id] = $Color->item;
            $ItemColors[$Color->item_id] = [];
        }

        $ItemColors[$Color->item_id][] = $Color;
    }

    Meta::set('title', _t('Сравнение', 'favorites'));
@endphp

@extends('layouts.print')

@section('content')
    <div class="container">
        @foreach($Items as $Item)
            <table class="compare-images">
                <tbody>
                <tr>
                    @if($Item->isImageExists(Item::IMAGE_TYPE_1))
                        <td>
                            <img src="{{ (Request::get('excel') ? public_path() : '') . $Item->getImageLink(Item::IMAGE_TYPE_1) }}" alt="{{ $Item->title }}">
                        </td>
                    @endif
                    @if($Item->isImageExists(Item::IMAGE_TYPE_2))
                        <td>
                            <img src="{{ (Request::get('excel') ? public_path() : '') . $Item->getImageLink(Item::IMAGE_TYPE_2) }}" alt="{{ $Item->title }}">
                        </td>
                    @endif
                    @if($Item->isImageExists(Item::IMAGE_TYPE_3))
                        <td>
                            <img src="{{ (Request::get('excel') ? public_path() : '') . $Item->getImageLink(Item::IMAGE_TYPE_3) }}" alt="{{ $Item->title }}">
                        </td>
                    @endif
                    @if($Item->isImageExists(Item::IMAGE_TYPE_4))
                        <td>
                            <img src="{{ (Request::get('excel') ? public_path() : '') . $Item->getImageLink(Item::IMAGE_TYPE_4) }}" alt="{{ $Item->title }}">
                        </td>
                    @endif
                    @if($Item->isImageExists(Item::IMAGE_TYPE_9))
                        <td>
                            <img src="{{ (Request::get('excel') ? public_path() : '') . $Item->getImageLink(Item::IMAGE_TYPE_9) }}" alt="{{ $Item->title }}">
                        </td>
                    @endif
                </tr>
                <tr>
                    <td colspan="5">
                        {{ $Item->short_descr }}
                    </td>
                </tr>
                </tbody>
            </table>
            <table class="list">
                <thead>
                <tr>
                    <th rowspan="2">№</th>
                    <th rowspan="2">{!! _t('Артикул', 'favorites')  !!}</th>
                    <th rowspan="2">{!! _t('Цвет', 'favorites') !!}</th>
                    <th colspan="6">{!! _t('Цена, руб. за 1 шт.', 'favorites') !!}</th>
                    <th rowspan="2">{!! _t('Упаковка', 'favorites') !!}</th>
                    <th rowspan="2">{!! _t('Наличие на складе на %s', 'favorites', [Formatter::date()]) !!}</th>
                </tr>
                <tr>
                    <th>{!! _t('более 10 000', 'favorites') !!}</th>
                    <th>{!! _t('от 5 000 до 10 000', 'favorites') !!}</th>
                    <th>{!! _t('от 1 000 до 5 000', 'favorites') !!}</th>
                    <th>{!! _t('от 500 до 1 000', 'favorites') !!}</th>
                    <th>{!! _t('от 100 до 500', 'favorites') !!}</th>
                    <th>{!! _t('розница до 100', 'favorites') !!}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($ItemColors[$Item->id] as $Color)
                    <tr>
                        <td>{{ $loop->index + 1 }}</td>
                        <td>{{ $Color->code }}</td>
                        <td>{{ _t(Color::cachedOne($Color->color)->name, 'colors') }}</td>
                        @for($i = 5; $i >= 0; $i--)
                            <td @if($Item->special && $Item->getSpecialDiscount($i) && $Color->main)class="price-specials" @endif>
                                @if($Item->special && $Item->getSpecialDiscount($i) && $Color->main)
                                    <div><s>{{ Formatter::price($Color->getPrice($Item['price' . $i]), true) }}</s></div>
                                    <div class="price-discount @if($Item->getSpecialDiscount($i) === 50) price-discount_hot @endif"><span>-{{ $Color->item->getSpecialDiscount($i) }}%</span></div>
                                    {{ Formatter::price($Color->item->getDiscountPrice($i), true) }}
                                @else
                                    {{ Formatter::price($Color->getPrice($Item['price' . $i]), true) }}
                                @endif
                            </td>
                        @endfor
                        <td>{{ $Item->pack_size ? $Item->pack_size . ' ' . _t('шт.', 'common') : '' }}</td>
                        <td>{{ number_format($Color->remains, 0, ',', ' ') }} {{ _t('шт.', 'common') }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endforeach
    </div>
    <p>{!! _t('Все цены указаны в рублях и включают в себя НДС.', 'favorites') !!}</p>
    <p>{!! _t('Все цены указаны при условии 100%% предоплаты и действительны в течение 5 дней.', 'favorites') !!}</p>
    <p>{!! _t('Все цены указаны при условии самовывоза продукции со склада в г. Санкт-Петербург', 'favorites') !!}</p>
    <p>{!! _t('(для иногородних клиентов доставка до склада ТК в г. Санкт-Петербург бесплатна).', 'favorites') !!}</p>
    <p>{!! _t('О возможных сроках поставки заказного товара, необходимо уточнять заранее.', 'favorites') !!}</p>
@endsection
