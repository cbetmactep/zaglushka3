@php
    declare(strict_types=1);

    use App\Models\Item;
    use App\Models\Color;
@endphp

<div class="product-table__wrap">
    <table class="product-table js-product-table-fix-head">
        <thead class="product-table__thead">
        <tr>
            <th class="product-table__th product-table__th_number" rowspan="2">№</th>
            <th class="product-table__th product-table__th_article" rowspan="2">{!! _t('Артикул', 'favorites') !!}</th>
            <th class="product-table__th" rowspan="2" colspan="2"></th>
            <th class="product-table__th product-table__th_price product-table__th_price_center" colspan="6">{!! _t('Цена за 1 шт.', 'favorites') !!}</th>
            <th class="product-table__th product-table__th_colors" rowspan="2">{!! _t('Наличие', 'favorites') !!}</th>
        </tr>
        <tr>
            <th class="product-table__th product-table__th_price">{!! _t('от', 'favorites') !!} <span class="nowrap">10 000</span></th>
            <th class="product-table__th product-table__th_price">{!! _t('от', 'favorites') !!} <span class="nowrap">5 000</span></th>
            <th class="product-table__th product-table__th_price">{!! _t('от', 'favorites') !!} <span class="nowrap">1 000</span></th>
            <th class="product-table__th product-table__th_price">{!! _t('от', 'favorites') !!} 500</th>
            <th class="product-table__th product-table__th_price">{!! _t('от', 'favorites') !!} 100</th>
            <th class="product-table__th product-table__th_price">{!! _t('розница', 'favorites') !!}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($Colors as $Color)
            <tr class="product-table__tr"
                data-id="{{ $Color->item_id }}"
                data-color="{{ $Color->color }}"
            >
                <td class="product-table__td product-table__td_number">
                    <a class="product-table__number" href="{{ route('slug', ['slug' => $Color->item->slug()]) }}">
                        <span>{{ $loop->iteration }}</span>
                    </a>
                </td>
                <td class="product-table__td product-table__td_article">
                    @if($Color->item->name)
                        <div class="product-table__name">{{ $Color->item->name }}</div>
                    @endif
                    <a class="product-table__article" href="{{ route('slug', ['slug' => $Color->item->slug()]) }}">{{ $Color->code }}</a>
                    @if($Color->item->under_title && App::isLocale('ru') && false)
                        <div>
                            <div class="product-table__promo @if(isset($Color->item->under_title_color) && $Color->item->under_title) product-table__promo_{{ $Color->item->under_title_color }} @endif">
                                {!! nl2br($Color->item->under_title) !!}
                            </div>
                        </div>
                    @endif
                    @if($Color->recommend && App::isLocale('ru'))
                        <img src="{{ asset('/images/recommend.png') }}">
                    @endif
                </td>

                @component('components.product-table-td-image', [
                    'link' => route('slug', ['slug' => $Color->item->slug()]),
                    'images' => [
                        [
                            'link' => $Color->item->isImageExists(Item::IMAGE_TYPE_18) ? $Color->item->getImageLink(Item::IMAGE_TYPE_18) : null,
                            'alt' => _t('Схема', 'favorites') . ' ' . $Color->title,
                            'sizes' => [110, 95],
                        ],
                        [
                            'link' => $Color->item->isImageExists(Item::IMAGE_TYPE_5) ? $Color->item->getImageLink(Item::IMAGE_TYPE_5) : null,
                            'alt' => $Color->title,
                        ],
                    ]
                ])])@endcomponent
                @if($Color->item->only_two_prices)
                    @component('components.product-table-td-image', [
                        'link' => route('slug', ['slug' => $Color->item->slug()]),
                        'images' => [
                            [
                                'link' => $Color->item->isImageExists(Item::IMAGE_TYPE_31) ? $Color->item->getImageLink(Item::IMAGE_TYPE_31) : null,
                                'alt' => $Color->title,
                                'colspan' => 2,
                                'noDummy' => true,
                            ],
                            [
                                'link' => $Color->item->isImageExists(Item::IMAGE_TYPE_32) ? $Color->item->getImageLink(Item::IMAGE_TYPE_32) : null,
                                'alt' => $Color->title,
                                'colspan' => 2,
                                'noDummy' => true,
                            ],
                        ]
                    ])])@endcomponent
                @endif
                @for($i = 5; $i >= 0; $i--)
                    @if($i < 2 || !$Color->item->only_two_prices)
                        <td class="product-table__td product-table__td_price">
                            <div class="product-table__price-text @if(max($Color->item->price0, $Color->item->price1, $Color->item->price2, $Color->item->price3, $Color->item->price4, $Color->item->price5) > 999999) product-table__price-text_small @endif">
                                @if ($Color->item->getSpecialDiscount($i) && $Color->main)
                                    <div class="product-table__price-old">{{ Formatter::price($Color->item['price' . $i], true) }}</div>
                                    <span class="product-table__discount @if($Color->item->getSpecialDiscount($i) === 50)product-table__discount_active @endif">−{{ $Color->item->getSpecialDiscount($i) }} %</span>
                                    <div>{{ Formatter::price($Color->item->getDiscountPrice($i), true) }}</div>
                                @else
                                    {{ Formatter::price($Color->getPrice($Color->item['price' . $i]), true) }}
                                @endif
                                @if($i === 5 && $Color->item->price6 > 0 && $Color->main)
                                    <div class="product-table__seven">
                                        {!! _t('цена %s при заказе от %s штук', 'catalog', ['<b>' . Formatter::price($Color->item->price6, true) . '</b>', '<span class="nowrap">30 000</span>']) !!}
                                    </div>
                                @endif
                            </div>
                        </td>
                    @endif
                @endfor
                <td class="product-table__td product-table__td_colors">
                    <div class="product-table-colors">
                        @php
                            $TransitItems = $Color->getTransitItems();
                        @endphp
                        <div class="product-table-colors__wrap">
                            <div class="product-table-colors__list">
                                <div class="product-table-colors__item">
                                    <div class="product-table-colors__key">
                                        @component('components.product-item-color-box', ['hex' => Color::cachedOne($Color->color)->hex])@endcomponent
                                        {{ _t(Color::cachedOne($Color->color)->name, 'colors') }}
                                    </div>
                                    <div class="product-table-colors__value">
                                        {{ Formatter::quantity($Color->remains) }}
                                    </div>
                                </div>
                                @if(count($TransitItems) > 0)
                                    @php
                                        $dates = [];
                                        $quant = 0;
                                    @endphp
                                    @foreach($TransitItems as $TransitItem)
                                        @php
                                            $quant += $TransitItem->quant;
                                        @endphp
                                    @endforeach
                                    @if($quant > 0)
                                        <div class="product-table-colors__item product-table-colors__item_dummy">
                                            <div class="product-table-colors__key product-table-colors__key_dummy">
                                                + {{ _t('едет', 'favorites') }}
                                            </div>
                                            <div class="product-table-colors__value">
                                                {{ Formatter::quantity($quant) }}
                                            </div>
                                        </div>
                                    @endif
                                @endif
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

@php
    $priceTitle = [
        _t('розница', 'item'),
        _t('от', 'item') . ' 100',
        _t('от', 'item') . ' 500',
        _t('от', 'item') . ' 1 000',
        _t('от', 'item') . ' 5 000',
        _t('от', 'item') . ' 10 000',
        _t('от', 'item') . ' 30 000',
    ];
@endphp
<div class="mob-product__list">
    @foreach($Colors as $Color)
        <div class="mob-product">
            <div class="mob-product__header">
                <div class="mob-product__index">
                    <span>
                        {{ $loop->iteration }}
                    </span>
                </div>
                <div class="mob-product__article-text">
                    {{ _t('Артикул:', 'item') }}
                </div>
                <div class="mob-product__article">
                    {{ $Color->code }}
                </div>
            </div>
            <div class="mob-product__body">
                <a class="mob-product__images" href="{{ route('slug', ['slug' => $Color->item->slug()]) }}">
                    @if($Color->item->isImageExists(Item::IMAGE_TYPE_5))
                        <img class="mob-product__image" src="{{ $Color->item->getImageLink(Item::IMAGE_TYPE_5) }}" alt="{{ $Color->title }}">
                    @endif
                    @if($Color->item->isImageExists(Item::IMAGE_TYPE_18))
                        <img class="mob-product__image" src="{{ $Color->item->getImageLink(Item::IMAGE_TYPE_18) }}" alt="{{ _t('Схема', 'item') }} {{ $Color->title }}">
                    @endif
                </a>
                <div class="mob-product__prices-wrap">
                    <table class="mob-product__prices">
                        <thead>
                        <tr>
                            <td>{!! _t('Кол-во, шт.', 'item') !!}</td>
                            <td>{!! _t('Цена за шт.', 'item') !!}</td>
                        </tr>
                        </thead>
                        <tbody>
                        @for($i = 6; $i >= 0; $i--)
                            @if($Color->item['price' . $i])
                                <tr>
                                    <td>
                                        {{ $priceTitle[$i] }}
                                    </td>
                                    <td>
                                        @if ($Color->item->getSpecialDiscount($i) && $Color->main)
                                            <div class="mob-product__price mob-product__price_discount">
                                                <span class="mob-product__price-discount">%</span>
                                                {{ Formatter::price($Color->item->getDiscountPrice($i), true) }}
                                            </div>
                                        @else
                                            <div class="mob-product__price">
                                                {{ Formatter::price($Color->getPrice($Color->item['price' . $i]), true) }}
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            @endif
                        @endfor
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="mob-product__footer">
                <div class="mob-product__footer">
                    @if ($Color->remains)
                        <div class="mob-product__remains">
                            {!! _t('на складе %s', 'item', [Formatter::quantity($Color->remains)]) !!}
                        </div>
                    @else
                        <div class="mob-product__remains mob-product__remains_gray">
                            {!! _t('нет на складе', 'item') !!}
                        </div>
                    @endif
                    <a class="mob-product__link" href="{{ route('slug', ['slug' => $Color->item->slug()]) }}">
                        {!! _t('Подробнее', 'common') !!}
                        @component('components.icon', ['name' => 'arrow-menu', 'attributes' => ['class' => 'mob-product__link-icon']])])@endcomponent
                    </a>
                </div>
            </div>
        </div>
    @endforeach
</div>
